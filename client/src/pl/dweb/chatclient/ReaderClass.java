package pl.dweb.chatclient;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by RENT on 2017-02-25.
 */
public class ReaderClass implements Runnable {

    private BufferedReader reader;

    public ReaderClass(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if(reader.ready()) {
                    System.out.println(reader.readLine());
                } else {
                    Thread.sleep(100);
                }
            } catch (IOException e) {
                System.out.println("Nothing in return");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
