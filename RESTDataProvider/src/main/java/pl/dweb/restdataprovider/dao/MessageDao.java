package pl.dweb.restdataprovider.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.dweb.restdataprovider.HibernateSession;
import pl.dweb.restdataprovider.model.Message;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-03-09.
 */
public class MessageDao {

    public MessageDao() {}

    public List<Message> getMaessages() {
        List<Message> returnList = new LinkedList<>();

        Session session = HibernateSession.getSessionFactory().openSession();
        try {
            Transaction transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Message.class);
            returnList.addAll(criteria.list());
            transaction.commit();
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        } finally {
            if(session != null) {
                session.close();
            }
        }

        return returnList;
    }
}
