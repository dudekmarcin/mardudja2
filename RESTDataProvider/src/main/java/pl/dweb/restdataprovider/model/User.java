package pl.dweb.restdataprovider.model;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "login")
    private String login;

    @Column(name = "password_hash")
    private String pass;

//    @OneToMany(mappedBy="sender")
//    List<Message> sendedMessages;
//
//    @OneToMany(mappedBy = "recipient")
//    List<Message> incomingMessages;

    public User() {}

    public User(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

//    public List<Message> getSendedMessages() {
//        return sendedMessages;
//    }
//
//    public void setSendedMessages(List<Message> sendedMessages) {
//        this.sendedMessages = sendedMessages;
//    }
//
//    public List<Message> getIncomingMessages() {
//        return incomingMessages;
//    }
//
//    public void setIncomingMessages(List<Message> incomingMessages) {
//        this.incomingMessages = incomingMessages;
//    }
}
