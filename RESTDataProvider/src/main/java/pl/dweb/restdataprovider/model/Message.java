package pl.dweb.restdataprovider.model;

import javax.persistence.*;

@Entity
@Table(name = "message", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class Message {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "content")
    private String content;

    @Column(name = "timestamp")
    private String timestamp;

    @Column(name = "status")
    private Integer status;

////    @ManyToOne
////    @JoinColumn(name = "sender_id")
//    private User sender;

//    @ManyToOne
//    @JoinColumn(name = "recipient_id")
//    private User recipient;

    public Message() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
