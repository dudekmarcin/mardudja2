package pl.dweb.restdataprovider.servlet;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import pl.dweb.restdataprovider.dao.MessageDao;
import pl.dweb.restdataprovider.model.Message;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api")
public class JSONMesagesProvider {

    @GET
    @Produces("application/json")
    public Response defaultReverser() throws JSONException {
//        StringBuilder sb = new StringBuilder();
//        sb.append("ANKARA");
//        JSONObject jsonObject = new JSONObject() ;
//        jsonObject.put("original", sb.toString());
//        jsonObject.put("reversed", sb.reverse().toString());
//        String result = "" + jsonObject;


        MessageDao dao = new MessageDao();
        List<Message> messages = dao.getMaessages();
        JSONArray jsonArray = new JSONArray();
        Gson gson = new Gson();
        for (Message msg : messages) {
            jsonArray.put(gson.toJson(msg));
        }

        JSONObject o = new JSONObject();
        o.put("values", jsonArray);
        return Response.status(200).entity(o.toString()).build();
    }

    @Path("{word}")
    @GET
    @Produces("application/json")
    public Response reverser(@PathParam("word") String word) throws JSONException {
        StringBuilder sb = new StringBuilder();
        sb.append(word);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("original", sb.toString());
        jsonObject.put("reversed", sb.reverse().toString());
        String result = "" + jsonObject;
        return Response.status(200).entity(result).build();
    }

}
