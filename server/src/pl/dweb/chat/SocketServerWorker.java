package pl.dweb.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Observable;

/**
 * Created by RENT on 2017-02-25.
 */
public class SocketServerWorker extends Observable implements Runnable {

    private BufferedReader reader;
    private PrintWriter writer;
    private Socket socket;
    private ServerClient client;

    public SocketServerWorker(Socket socket) throws IOException {
        this.socket = socket;
        socket.setSoTimeout(5000);
        writer = new PrintWriter(socket.getOutputStream());
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public void run() {
        while(SocketServer.SERVER_WORKING) {
            try {
                if (!reader.ready()) {
                    Thread.sleep(1000);
                    continue;
                }
                String line = reader.readLine();
                if (line != null) {
                    setChanged();
                    notifyObservers(line);
                    System.out.println(line);
                    writer.println(line + " - OK");
                    writer.flush();
                }

            } catch(SocketTimeoutException ste) {
                System.out.println("Nothing heppening");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public ServerClient getClient() {
        return client;
    }

    public void setClient(ServerClient client) {
        this.client = client;
    }

    public void sendMessage(String msg) {
        writer.println(msg);
        writer.flush();
    }
}
