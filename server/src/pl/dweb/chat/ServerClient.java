package pl.dweb.chat;


/**
 * Created by RENT on 2017-02-25.
 */
public class ServerClient {

    private boolean isLoggedIn = false;
    private String login;
    private String password;
    private String ip;

    private SocketServerWorker worker;

    public ServerClient(SocketServerWorker worker) {
        this.worker = worker;
        worker.setClient(this);

        ip = worker.getSocket().getInetAddress().getHostAddress();
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getIp() {
        return ip;
    }

    public SocketServerWorker getWorker() {
        return worker;
    }
}
