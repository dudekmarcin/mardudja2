package pl.dweb.chat;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by RENT on 2017-02-25.
 */
public class SocketServer implements Observer {

    public static final Integer PORT = 10000;
    public static Boolean SERVER_WORKING = true;
    private ServerSocket server;
    private HashMap<String, ServerClient> workers;


    public SocketServer() {
        workers = new HashMap<>();
    }

    public void listen() {

        try {
            server = new ServerSocket(PORT);
            server.setSoTimeout(10000);
        } catch (IOException e) {
            System.out.println("Socket server didn't open");
        }

        while(SERVER_WORKING) {
            try {
                Socket opened = server.accept();
                if(!opened.isClosed()) {
                    ServerClient newClient = new ServerClient(new SocketServerWorker(opened));
                    addNewClient(newClient);
                    System.out.println("opened port - accepted from: " + newClient.getIp());
                    Thread th = new Thread(newClient.getWorker());
                    th.start();
                }
            } catch (SocketTimeoutException ste) {
                System.out.println("No new connection");
            } catch (IOException e) {
                System.out.println("Error from reading");
            }
        }
    }

    public void addNewClient(ServerClient serverClient) {
        workers.put(serverClient.getIp(), serverClient);
        serverClient.getWorker().addObserver(this);
    }

    public synchronized void broadcastMessage(String msg, SocketServerWorker sender) {
        Iterator<ServerClient> it = workers.values().iterator();
        while (it.hasNext()) {
            ServerClient client = it.next();
            SocketServerWorker worker = client.getWorker();
            if(worker.getSocket().isClosed()) {
                workers.remove(client.getIp());
                System.out.println("Usuwam: " + client.getLogin());
                continue;
            }

            if(!worker.equals(sender)) {
                worker.sendMessage(msg);
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        String msg = null;
        if(arg instanceof String) {
            msg = (String) arg;
        }

        SocketServerWorker sender = null;
        if(o instanceof SocketServerWorker) {
            sender = (SocketServerWorker) o;
        }

        System.out.println(sender.getClient().getLogin() + ": " + msg);
        broadcastMessage(msg, sender);
    }
}
