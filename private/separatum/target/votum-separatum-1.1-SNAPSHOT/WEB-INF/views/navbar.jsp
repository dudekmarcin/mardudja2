<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<nav class="gray" role="navigation">
    <div class="nav-wrapper ">
     	<span id="logo-container" class="brand-logo page_title">${label}</span>
    </div>
</nav>
