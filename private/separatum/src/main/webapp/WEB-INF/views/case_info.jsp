<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ include file="header.jsp" %>
<body>
<%@ include file="drawer.jsp" %>
<%@ include file="navbar.jsp" %>


<div class="row margin">
  <div class="col s12 m12">
      <div class="container main">
<!-- start content-->

<div class="section">
	<div class="row">
		<div class="col s12">
			<h5>${aCase.signature} </h5>  <br />
			<p>ID w bazie: ${aCase.caseID}</p>
		<div class="row">
            <div class="col s6">
                <table>
                <thead>
                    <tr><th>Data </th></tr></thead>
                    <tbody>
                        <tr><td>${aCase.getLocalDate()} </td></tr>
                    </tbody>
                    </table>
                    </div>
            <div class="col s6">

<table>
                <thead>
                    <tr><th>Instancja </th></tr></thead>
                    <tbody>
                        <tr><td>${aCase.instance.name}</td></tr>
                    </tbody>
                    </table>
                    </div>
                    </div>

		<div class="row">
            <div class="col s6">
                <table>
                <thead>
                    <tr><th>Liczba sędziów </th></tr></thead>
                    <tbody>
                        <tr><td>${aCase.judgesNumber} </td></tr>
                    </tbody>
                    </table>
                    </div>
            <div class="col s6">

<table>
                <thead>
                    <tr><th>Liczba zdań odrębnych</th></tr></thead>
                    <tbody>
                        <tr><td>${aCase.separatumNumber}</td></tr>
                    </tbody>
                    </table>
                    </div>
                    </div>



            <div class="row">
            <div class="col s6">
            <table><thead><tr><th>Chroniona wartość konstytucyjna <a href="<spring:url value="/add_protected_value?id=${aCase.caseID}" />" class="right green-text"><i class="material-icons">add</i></a></th></tr></thead>
            <tbody>
            <c:forEach items="${aCase.protectedValues}" varStatus="i" >
                             <tr><td>${aCase.protectedValues.get(i.index).name}</td></tr>
            </c:forEach>
            </tbody>
            </table>
            </div>
            <div class="row">
                        <div class="col s6">
                            <table>
                            <thead>
                                <tr><th>Przedmiot kontroli </th></tr></thead>
                                <tbody>
                                    <tr><td>${aCase.controlSubject.name}</td></tr>
                                </tbody>
                                </table>
                                </div>

            </div>


            <div class="row">
            <div class="col s6">
            <table><thead><tr><th>Wzorce kontroli</th></tr></thead>
            <tbody>
            <c:forEach items="${aCase.controlPatterns}" varStatus="i" >
                 <tr><td>${aCase.controlPatterns.get(i.index).name}</td></tr>
            </c:forEach>
            </tbody>
            </table>
            </div>
            <div class="col s6">
            <table><thead><tr><th>Obiekty kontroli</th></tr></thead>
            <tbody>
            <c:forEach items="${aCase.controlObjects}" varStatus="i" >
                <tr><td>${aCase.controlObjects.get(i.index).name}</td></tr>
            </c:forEach>
            </tbody>
            </table>
            </div>
            </div>
            <br />
<h5 class="center-align">Skład orzekający</h5><br />
             <div class="row">
             <div class="col s6">
             <table><thead><tr><th>Przewodniczący</th></tr></thead>
             <tbody>
                <tr><td><a href="judge_info.php?id=${aCase.leader.judgeID}" >${aCase.leader.name} ${aCase.leader.lastname}</a></td></tr>
             </tbody>
             </table>

             <table><thead><tr><th>Sprawozdawcy</th></tr></thead>
             <tbody>
                <c:forEach items="${aCase.judgesReporter}" varStatus="i" >
                <tr><td><a href="<spring:url value="/judge_info?id=${aCase.judgesReporter.get(i.index).judgeID}" />">${aCase.judgesReporter.get(i.index).name} ${aCase.judgesReporter.get(i.index).lastname}</a></td></tr>
            </c:forEach>              </tbody>
             </table>

               <table><thead><tr><th>Zdania odrębne</th></tr></thead>
                          <tbody>
                            <c:forEach items="${votums}" varStatus="i" >
                               <tr><td>${votums.get(i.index)}</td></tr>
                         </c:forEach>              </tbody>
                          </table>


             </div>
             <div class="col s6">
             <table><thead><tr><th>Skład</th></tr></thead>
             <tbody>
             <c:forEach items="${aCase.judgesTeam}" varStatus="i" >
                <tr><td><a href="<spring:url value="/judge_info?id=${aCase.judgesTeam.get(i.index).judgeID}" />">${aCase.judgesTeam.get(i.index).name} ${aCase.judgesTeam.get(i.index).lastname}</a></td></tr>
            </c:forEach>

            </tbody>
            </table>
            </div>
            </div>





		</div>
	</div>
</div>

<!-- end content -->
    </div>
  </div>



</div>
<%@ include file="scripts.jsp" %>
<%@ include file="footer.jsp" %>
