package pl.dweb.separatum.controller;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dweb.separatum.dto.NominationDTO;
import pl.dweb.separatum.model.Nomination;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.Valid;
import java.util.List;

@Controller
public class NominationController {
    @RequestMapping(value = "/nominations", method = RequestMethod.GET)
    public String nominations(Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

//        createCase();
//        em.getTransaction().begin();
        Session session = em.unwrap(Session.class);
        Query query = session.createQuery("from Nomination");
        List<Nomination> nominations = query.list();
//        em.getTransaction().commit();
        em.close();
        emf.close();

        model.addAttribute("label", "Nominacje");
        model.addAttribute("nominations", nominations);
        return "nominations";
    }

    @RequestMapping(value = "/add_nomination", method = RequestMethod.GET)
    public String addNominationForm(Model model) {
        model.addAttribute("label", "Dodaj partię nominującą");
        model.addAttribute("nomination", new NominationDTO());
        return "add_nomination";
    }

    @RequestMapping(value = "/add_nomination", method = RequestMethod.POST)
    public String addNomination(@ModelAttribute("nomination") @Valid NominationDTO nomination, BindingResult result) {
        if (result.hasErrors()) {
            return "add_nomination";
        } else {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
            EntityManager em = emf.createEntityManager();

            em.getTransaction().begin();
            em.persist(new Nomination(nomination.getName()));
            em.getTransaction().commit();
            em.close();
            emf.close();
            return "redirect:/nominations";
        }
    }

    @RequestMapping(value = "/nomination_info", method = RequestMethod.GET)
    public String caseInfo(@RequestParam("id")Integer id, Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

        Nomination nom = em.find(Nomination.class, id);
        Hibernate.initialize(nom);
        Hibernate.initialize(nom.getJudges());

        em.close();
        emf.close();
        model.addAttribute("label", "Nominacje " + nom.getPartyName());
        model.addAttribute("nom", nom);

        return "nomination_info";
    }
}
