package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by md on 2/18/17.
 */
@Entity
@Table(name = "judges")
public class Judge {

    @Id
    @GeneratedValue
    @Column(name = "judge_id")
    private Long judgeID;

    @Column(name = "name")
    private String name;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "birth_year")
    private Integer birthYear;

    @Column(name ="death_year")
    private Integer deathYear;

    @Column(name = "begin_year")
    private Integer beginYear;

    @Column(name = "end_year")
    private Integer endYear;

    @OneToMany(mappedBy = "leader")
    private List<Case> casesLeader;

    @OneToMany(mappedBy = "judge")
    private List<Separatum> separatums;

    @ManyToMany(mappedBy = "judgesTeam")
    private List<Case> casesTeam;

    @ManyToMany(mappedBy = "judgesReporter")
    private List<Case> casesReporter;

    @ManyToMany
    @JoinTable(name = "judge_spec", joinColumns = {@JoinColumn(name = "judge_id")}, inverseJoinColumns = {@JoinColumn(name = "specialization_id")})
    List<Specialization> specializations;

    @ManyToMany
    @JoinTable(name = "judge_nominat", joinColumns = {@JoinColumn(name = "judge_id")}, inverseJoinColumns = {@JoinColumn(name = "nomination_id")})
    List<Nomination> nominations;

    public Judge() {}

    public Judge(Long judgeID, String name, String lastname, Integer birthYear, Integer deathYear, Integer beginYear, Integer endYear) {
        this.judgeID = judgeID;
        this.name = name;
        this.lastname = lastname;
        this.birthYear = birthYear;
        this.deathYear = deathYear;
        this.beginYear = beginYear;
        this.endYear = endYear;
    }

    public Long getJudgeID() {
        return judgeID;
    }

    public void setJudgeID(Long judgeID) {
        this.judgeID = judgeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public Integer getDeathYear() {
        return deathYear;
    }

    public void setDeathYear(Integer deathYear) {
        this.deathYear = deathYear;
    }

    public Integer getBeginYear() {
        return beginYear;
    }

    public void setBeginYear(Integer beginYear) {
        this.beginYear = beginYear;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public List<Case> getCasesTeam() {
        return casesTeam;
    }

    public void setCasesTeam(List<Case> casesTeam) {
        this.casesTeam = casesTeam;
    }

    public List<Case> getCasesReporter() {
        return casesReporter;
    }

    public void setCasesReporter(List<Case> casesReporter) {
        this.casesReporter = casesReporter;
    }

    public List<Case> getCasesLeader() {
        return casesLeader;
    }

    public void setCasesLeader(List<Case> casesLeader) {
        this.casesLeader = casesLeader;
    }

    public List<Specialization> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(List<Specialization> specializations) {
        this.specializations = specializations;
    }

    public List<Nomination> getNominations() {
        return nominations;
    }

    public void setNominations(List<Nomination> nominations) {
        this.nominations = nominations;
    }

    public List<Separatum> getSeparatums() {
        return separatums;
    }

    public void setSeparatums(List<Separatum> separatums) {
        this.separatums = separatums;
    }

    @Override
    public String toString() {
        return "Judge{" +
                "judgeID=" + judgeID +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthYear=" + birthYear +
                ", deathYear=" + deathYear +
                ", beginYear=" + beginYear +
                ", endYear=" + endYear +
                '}';
    }
}
