package pl.dweb.separatum.controller;

import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Controller
public class MainController {
    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String dashboard(Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

//        em.getTransaction().begin();
        Session session = em.unwrap(Session.class);
        Long cases = (Long) session.createQuery("select count(*) from  Case").uniqueResult();
        Long judges = (Long) session.createQuery("select count(*) from  Judge").uniqueResult();
        Long nominations = (Long) session.createQuery("select count(*) from  Nomination").uniqueResult();
        Long specializations = (Long) session.createQuery("select count(*) from  Specialization").uniqueResult();
//        em.getTransaction().commit();

        em.close();
        emf.close();
        model.addAttribute("cases", cases);
        model.addAttribute("judges", judges);
        model.addAttribute("nominations", nominations);
        model.addAttribute("specializations", specializations);
        model.addAttribute("label", "Strona główna");
        return "dashboard";
    }
}
