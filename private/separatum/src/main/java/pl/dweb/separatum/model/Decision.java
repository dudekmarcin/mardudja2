package pl.dweb.separatum.model;

import javax.persistence.*;

@Entity
@Table(name = "decisions")
public class Decision {

    @Id
    @GeneratedValue
    @Column(name = "decision_id")
    private Integer decisionID;

    @Column(name = "decision_name")
    private String name;



    public Decision() {}


}
