package pl.dweb.separatum.controller;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dweb.separatum.dto.CaseCoreDTO;
import pl.dweb.separatum.model.Case;
import pl.dweb.separatum.model.Instance;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.Valid;
import java.util.List;

@Controller
public class CaseController {

    @RequestMapping(value = "/cases", method = RequestMethod.GET)
    public String cases(Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

//        em.getTransaction().begin();
        Session session = em.unwrap(Session.class);
        Query query = session.createQuery("from Case");
        List<Case> cases = query.list();
//        em.getTransaction().commit();

        em.close();
        emf.close();

        model.addAttribute("label", "Orzeczenia");
        model.addAttribute("cases", cases);
        return "cases";
    }

    @RequestMapping(value = "/case_info", method = RequestMethod.GET)
    public String caseInfo(@RequestParam("id")Long id, Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

//        em.getTransaction().begin();
        Session session = em.unwrap(Session.class);
        Case aCase = (Case)session.load(Case.class, id);
        Hibernate.initialize(aCase);
        Hibernate.initialize(aCase.getJudgesTeam());
        Hibernate.initialize(aCase.getSeparatums());
        Hibernate.initialize(aCase.getJudgesReporter());
        Hibernate.initialize(aCase.getControlObjects());
        Hibernate.initialize(aCase.getControlPatterns());
        Hibernate.initialize(aCase.getProtectedValues());
        List<String> votums = aCase.getVotum();

//        List<Case> team = judge.getCasesTeam();
//        Hibernate.initialize(team);
//        List<Case> reporter = judge.getCasesReporter();
//        Hibernate.initialize(reporter);
//        List<Case> leader = judge.getCasesLeader();
//        Hibernate.initialize(leader);
//        List<Specialization> specializations = judge.getSpecializations();
//        Hibernate.initialize(specializations);
//        List<Nomination> nominations = judge.getNominations();
//        Hibernate.initialize(nominations);

//        em.getTransaction().commit();
        em.close();
        emf.close();
        model.addAttribute("label", "Sprawa " + aCase.getSignature());
        model.addAttribute("aCase", aCase);
        model.addAttribute("votums", votums);

//        model.addAttribute("team", team);
//        model.addAttribute("reporter", reporter);
//        model.addAttribute("leader", leader);
//        model.addAttribute("nomination", nominations);
//        model.addAttribute("specialization", specializations);

        return "case_info";
    }

    @RequestMapping(value = "/add_case", method = RequestMethod.GET)
    public String addCaseCoreForm(Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

//        em.getTransaction().begin();
        Session session = em.unwrap(Session.class);
        Query query = session.createQuery("from Instance");
        List<Instance> instances = query.list();
//        em.getTransaction().commit();

        em.close();
        emf.close();

        model.addAttribute("label", "Dodaj orzeczenie");
        model.addAttribute("caseCoreInfo", new CaseCoreDTO());
        model.addAttribute("instanceList", instances);
        return "add_case";
    }

    @RequestMapping(value = "/add_case", method = RequestMethod.POST)
    public String addCaseCore(@ModelAttribute("caseCoreInfo") @Valid CaseCoreDTO caseCore, BindingResult result) {
        if (result.hasErrors()) {
            return "add_case";
        } else {
            System.out.println(caseCore);
            return "redirect:/cases";
        }
    }


}
