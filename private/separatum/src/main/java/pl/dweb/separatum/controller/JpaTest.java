package pl.dweb.separatum.controller;


import pl.dweb.separatum.model.ProtectedValue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;

/**
 * Created by md on 2/23/17.
 */
public class JpaTest {

    private static EntityManager em;


    public static void main(String[] args) throws ParseException {
        // TODO Auto-generated method stub

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        em = emf.createEntityManager();

        em.getTransaction().begin();
        em.persist(new ProtectedValue("wartość testowa 2", null));
        em.getTransaction().commit();


        em.close();
        emf.close();
//
    }
//
//    private static void createCase() {
//        em.getTransaction().begin();
//      //  Case emp = new Case("K 10/10", new Date(), 1, 10, 1, 1L, 2L, 1L, 1, "www");
////        em.persist(emp);
//        em.getTransaction().commit();
//    }
}
