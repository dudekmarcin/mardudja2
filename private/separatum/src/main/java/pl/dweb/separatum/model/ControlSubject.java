package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "control_subjects")
public class ControlSubject {

    @Id
    @GeneratedValue
    @Column(name = "control_id")
    private Integer subjectID;

    @Column(name = "control_name")
    private String name;

    @OneToMany(mappedBy = "controlSubject")
    private List<Case> cases;

    public ControlSubject() {}

    public ControlSubject(String name) {
        this.name = name;
    }

    public Integer getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(Integer subjectID) {
        this.subjectID = subjectID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Case> getCases() {
        return cases;
    }

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }

    @Override
    public String toString() {
        return "ControlSubject{" +
                "subjectID=" + subjectID +
                ", name='" + name + '\'' +
                ", cases=" + cases +
                '}';
    }
}
