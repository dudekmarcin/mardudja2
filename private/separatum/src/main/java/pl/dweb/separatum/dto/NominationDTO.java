package pl.dweb.separatum.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by md on 2/26/17.
 */
public class NominationDTO {

    @NotEmpty(message = "Nie można dodać partii bez nazwy")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
