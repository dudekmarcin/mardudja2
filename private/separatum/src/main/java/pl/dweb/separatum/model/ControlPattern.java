package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "control_patterns")
public class ControlPattern {

    @Id
    @GeneratedValue
    @Column(name = "pattern_id")
    private Long patternID;

    @Column(name = "pattern_name")
    private String name;

    @ManyToMany(mappedBy = "controlPatterns")
    private List<Case> cases;

    public ControlPattern() {}

    public ControlPattern(String name) {
        this.name = name;
    }

    public Long getPatternID() {
        return patternID;
    }

    public void setPatternID(Long patternID) {
        this.patternID = patternID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Case> getCases() {
        return cases;
    }

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }

    @Override
    public String toString() {
        return "ControlPattern{" +
                "patternID=" + patternID +
                ", name='" + name + '\'' +
                ", cases=" + cases +
                '}';
    }
}
