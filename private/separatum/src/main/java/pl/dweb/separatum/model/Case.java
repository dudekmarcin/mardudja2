package pl.dweb.separatum.model;

import javax.persistence.*;
import java.sql.Date;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Entity
@Table(name = "cases")
public class Case {

    @Id
    @GeneratedValue
    @Column(name = "case_id")
    private Long caseID;

    @Column(name = "signature")
    private String signature;

    @Column(name = "date")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "instance")
    private Instance instance;

    @Column(name = "number_of_judges")
    private Integer judgesNumber;

    @Column(name = "number_of_votsep")
    private Integer separatumNumber;

    @ManyToOne
    @JoinColumn(name = "leader_id")
    private Judge leader;

    @ManyToOne
    @JoinColumn(name = "control_subject")
    private ControlSubject controlSubject;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "case_protected", joinColumns = {@JoinColumn(name = "case_id")}, inverseJoinColumns = {@JoinColumn(name = "value_id")})
    List<ProtectedValue> protectedValues;

    @Column(name = "sentence_point_number")
    private Integer pointsNumber;

    @Column(name = "sentence_url")
    private String sentenceURL;

    @ManyToMany
    @JoinTable(name = "case_control_pattern", joinColumns = {@JoinColumn(name = "case_id")}, inverseJoinColumns = {@JoinColumn(name = "pattern_id")})
    List<ControlPattern> controlPatterns;

    @ManyToMany
    @JoinTable(name = "case_object", joinColumns = {@JoinColumn(name = "case_id")}, inverseJoinColumns = {@JoinColumn(name = "object_id")})
    List<ControlObject> controlObjects;

    @ManyToMany
    @JoinTable(name = "teams", joinColumns = {@JoinColumn(name = "case_id")}, inverseJoinColumns = {@JoinColumn(name = "judge_id")})
    List<Judge> judgesTeam;

    @ManyToMany
    @JoinTable(name = "reporters", joinColumns = {@JoinColumn(name = "case_id")}, inverseJoinColumns = {@JoinColumn(name = "judge_id")})
    List<Judge> judgesReporter;

    @OneToMany(mappedBy = "sCase")
    private List<Separatum> separatums;

    @OneToMany(mappedBy = "pCase")
    private List<Point> points;

    public Case() {}

    public Case(String signature, Date date, Instance instance, Integer judgesNumber,
                Integer separatumNumber, Judge leader, ControlSubject controlSubject, List<ProtectedValue> protectedValues, Integer pointsNumber,
                String sentenceURL) {
        this.signature = signature;
        this.date = date;
        this.instance = instance;
        this.judgesNumber = judgesNumber;
        this.separatumNumber = separatumNumber;
        this.leader = leader;
        this.controlSubject = controlSubject;
        this.protectedValues = protectedValues;
        this.pointsNumber = pointsNumber;
        this.sentenceURL = sentenceURL;
    }

    public Long getCaseID() {
        return caseID;
    }

    public void setCaseID(Long caseID) {
        this.caseID = caseID;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Instance getInstance() {
        return instance;
    }

    public void setInstance(Instance instance) {
        this.instance = instance;
    }

    public Integer getJudgesNumber() {
        return judgesNumber;
    }

    public void setJudgesNumber(Integer judgesNumber) {
        this.judgesNumber = judgesNumber;
    }

    public Integer getSeparatumNumber() {
        return separatumNumber;
    }

    public void setSeparatumNumber(Integer separatumNumber) {
        this.separatumNumber = separatumNumber;
    }

    public Judge getLeader() {
        return leader;
    }

    public void setLeader(Judge leader) {
        this.leader = leader;
    }

    public ControlSubject getControlSubject() {
        return controlSubject;
    }

    public void setControlSubject(ControlSubject controlSubject) {
        this.controlSubject = controlSubject;
    }

    public List<ProtectedValue> getProtectedValues() {
        return protectedValues;
    }

    public void setProtectedValues(List<ProtectedValue> protectedValues) {
        this.protectedValues = protectedValues;
    }

    public Integer getPointsNumber() {
        return pointsNumber;
    }

    public void setPointsNumber(Integer pointsNumber) {
        this.pointsNumber = pointsNumber;
    }

    public String getSentenceURL() {
        return sentenceURL;
    }

    public void setSentenceURL(String sentenceURL) {
        this.sentenceURL = sentenceURL;
    }

    public List<Judge> getJudgesTeam() {
        return judgesTeam;
    }

    public void setJudgesTeam(List<Judge> judgesTeam) {
        this.judgesTeam = judgesTeam;
    }

    public List<Judge> getJudgesReporter() {
        return judgesReporter;
    }

    public void setJudgesReporter(List<Judge> judgesReporter) {
        this.judgesReporter = judgesReporter;
    }

    public List<Separatum> getSeparatums() {
        return separatums;
    }

    public void setSeparatums(List<Separatum> separatums) {
        this.separatums = separatums;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public String getLocalDate() {
        return date.toLocalDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    @Override
    public String toString() {
        return "Case{" +
                "caseID=" + caseID +
                ", signature='" + signature + '\'' +
                ", date=" + date +
                ", instance=" + instance +
                ", judgesNumber=" + judgesNumber +
                ", separatumNumber=" + separatumNumber +
                ", leader=" + leader.getName() + " " + leader.getLastname() +
//                ", controlSubject=" + controlSubject +
//                ", protectedValues=" + protectedValues +
                ", pointsNumber=" + pointsNumber +
                ", sentenceURL='" + sentenceURL + '\'' +
                '}';
    }

    public List<ControlPattern> getControlPatterns() {
        return controlPatterns;
    }

    public void setControlPatterns(List<ControlPattern> controlPatterns) {
        this.controlPatterns = controlPatterns;
    }

    public List<ControlObject> getControlObjects() {
        return controlObjects;
    }

    public void setControlObjects(List<ControlObject> controlObjects) {
        this.controlObjects = controlObjects;
    }

    public void addProtectedValue(ProtectedValue value) {
        if(protectedValues == null) {
            protectedValues = new ArrayList<>();
        }
        protectedValues.add(value);
    }

    public List<String> getVotum() {
        Map<Judge, String> judges = new HashMap<>();
        List<String> result = new ArrayList<>();
        for(Separatum s : separatums) {
            if(!judges.containsKey(s.getJudge())) {
                String line = s.getJudge().getName() + " " + s.getJudge().getLastname() + " do " + s.getCause().getCauseName() + " ";
                line += (s.getCause().getCauseID() == 2) ? s.getPoint().getPointNumber().toString() : "";
                judges.put(s.getJudge(), line);
            } else {
                judges.put(s.getJudge(), judges.get(s.getJudge()) + ", " + s.getPoint().getPointNumber() );
            }
        }

        judges.forEach((j, l) -> result.add(l));

        return result;
    }
}
