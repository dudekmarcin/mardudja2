package pl.dweb.separatum.dto;

import org.hibernate.validator.constraints.NotEmpty;


public class SpecializationDTO {

    @NotEmpty(message = "Nie można dodać specializacji bez nazwy")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
