package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "instances")
public class Instance {

    @Id
    @GeneratedValue
    @Column(name = "instance_id")
    private Integer instanceID;

    @Column(name = "instance_name")
    private String name;

    @OneToMany(mappedBy = "instance")
    private List<Case> cases;
    public Instance() { }

    public Instance(String name) {
        this.name = name;
    }

    public Integer getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(Integer instanceID) {
        this.instanceID = instanceID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Case> getCases() {
        return cases;
    }

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }

    @Override
    public String toString() {
        return "Instance{" +
                "name='" + name + '\'' +
                '}';
    }
}
