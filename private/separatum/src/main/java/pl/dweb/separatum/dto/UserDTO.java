package pl.dweb.separatum.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by md on 2/27/17.
 */
public class UserDTO {

    @NotEmpty(message = "Błędny login")
    private String mail;
    @NotEmpty (message = "Błędne hasło")
    private String pass;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
