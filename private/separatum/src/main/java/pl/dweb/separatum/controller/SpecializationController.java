package pl.dweb.separatum.controller;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dweb.separatum.dto.SpecializationDTO;
import pl.dweb.separatum.model.Specialization;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.Valid;
import java.util.List;

@Controller
public class SpecializationController {

    @RequestMapping(value = "/specializations", method = RequestMethod.GET)
    public String specializations(Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

//        createCase();
//        em.getTransaction().begin();
        Session session = em.unwrap(Session.class);
        Query query = session.createQuery("from Specialization");
        List<Specialization> specializations = query.list();
//        em.getTransaction().commit();
        em.close();
        emf.close();

        model.addAttribute("label", "Specializacje");
        model.addAttribute("specializations", specializations);
        return "specializations";
    }

    @RequestMapping(value = "/add_specialization", method = RequestMethod.GET)
    public String addSpecializationForm(Model model) {
        model.addAttribute("label", "Dodaj specializację");
        model.addAttribute("specialization", new SpecializationDTO());
        return "add_specialization";
    }

    @RequestMapping(value = "/add_specialization", method = RequestMethod.POST)
    public String addSpecialization(@ModelAttribute("specialization") @Valid SpecializationDTO specialization, BindingResult result) {
        if (result.hasErrors()) {
            return "add_specialization";
        } else {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
            EntityManager em = emf.createEntityManager();

            em.getTransaction().begin();
            em.persist(new Specialization(specialization.getName()));
            em.getTransaction().commit();
            em.close();
            emf.close();
            return "redirect:/specializations";
        }
    }


    @RequestMapping(value = "/specialization_info", method = RequestMethod.GET)
    public String caseInfo(@RequestParam("id")Integer id, Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

        Specialization spec = em.find(Specialization.class, id);
        Hibernate.initialize(spec);
        Hibernate.initialize(spec.getJudges());

        em.close();
        emf.close();
        model.addAttribute("label", "Specializacja " + spec.getSpecializationName());
        model.addAttribute("spec", spec);

        return "specialization_info";
    }


}
