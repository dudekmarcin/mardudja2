package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "control_objects")
public class ControlObject {

    @Id
    @GeneratedValue
    @Column(name = "object_id")
    private Long objectID;

    @Column(name = "object_name")
    private String name;

    @ManyToMany(mappedBy = "controlObjects")
    private List<Case> cases;

    public ControlObject() {}

    public ControlObject(String name) {
        this.name = name;
    }

    public Long getObjectID() {
        return objectID;
    }

    public void setObjectID(Long objectID) {
        this.objectID = objectID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Case> getCases() {
        return cases;
    }

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }

    @Override
    public String toString() {
        return "ControlObject{" +
                "objectID=" + objectID +
                ", name='" + name + '\'' +
                '}';
    }
}
