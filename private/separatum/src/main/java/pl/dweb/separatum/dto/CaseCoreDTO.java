package pl.dweb.separatum.dto;

/**
 * Created by md on 2/26/17.
 */
public class CaseCoreDTO {

    private String date;
    private String signature;
    private Integer instance;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Integer getInstance() {
        return instance;
    }

    public void setInstance(Integer instance) {
        this.instance = instance;
    }

    @Override
    public String toString() {
        return "CaseCoreDTO{" +
                "date='" + date + '\'' +
                ", signature='" + signature + '\'' +
                ", instance=" + instance +
                '}';
    }
}
