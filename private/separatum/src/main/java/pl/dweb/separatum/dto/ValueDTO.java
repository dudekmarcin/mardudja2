package pl.dweb.separatum.dto;

import java.util.List;

/**
 * Created by md on 2/26/17.
 */
public class ValueDTO {

    private Long caseID;
    private List<Long> valueID;
    private String name;

    public Long getCaseID() {
        return caseID;
    }

    public String getName() {
        return name;
    }


    public void setCaseID(Long caseID) {
        this.caseID = caseID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getValueID() {
        return valueID;
    }

    public void setValueID(List<Long> valueID) {
        this.valueID = valueID;
    }


}
