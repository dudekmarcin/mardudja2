package pl.dweb.separatum.controller;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dweb.separatum.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

@Controller
public class JudgeController {

    @RequestMapping(value = "/judges", method = RequestMethod.GET)
    public String judges(Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

//        em.getTransaction().begin();
        Session session = em.unwrap(Session.class);
        Query query = session.createQuery("from Judge");
        List<Judge> judges = query.list();
        judges.forEach(j -> {
            Hibernate.initialize(j.getNominations());
            Hibernate.initialize(j.getSpecializations());
        });
//        em.getTransaction().commit();
        em.close();
        emf.close();

        model.addAttribute("label", "Sędziowie");
        model.addAttribute("judges", judges);
        return "judges";
    }

    @RequestMapping(value = "/judge_info", method = RequestMethod.GET)
    public String judgesInfo(@RequestParam("id")Long id, Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

//        em.getTransaction().begin();
        Session session = em.unwrap(Session.class);
        Judge judge = (Judge)session.load(Judge.class, id);
        Hibernate.initialize(judge);
        List<Case> team = judge.getCasesTeam();
        Hibernate.initialize(team);
        List<Case> reporter = judge.getCasesReporter();
        Hibernate.initialize(reporter);
        List<Case> leader = judge.getCasesLeader();
        Hibernate.initialize(leader);
        List<Specialization> specializations = judge.getSpecializations();
        Hibernate.initialize(specializations);
        List<Nomination> nominations = judge.getNominations();
        Hibernate.initialize(nominations);
        List<Separatum> separatums = judge.getSeparatums();
        Hibernate.initialize(separatums);
//        em.getTransaction().commit();
        em.close();
        emf.close();
        model.addAttribute("label", judge.getName() + " " + judge.getLastname());
        model.addAttribute("judge", judge);
        model.addAttribute("team", team);
        model.addAttribute("reporter", reporter);
        model.addAttribute("leader", leader);
        model.addAttribute("nomination", nominations);
        model.addAttribute("specialization", specializations);
        model.addAttribute("separatum", separatums);

        return "judge_info";
    }

}
