package pl.dweb.separatum.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.dweb.separatum.dto.UserDTO;
import pl.dweb.separatum.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * Created by md on 2/11/17.
 */
@Controller
public class LoginController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String loginForm(Model model) {
        model.addAttribute("user", new UserDTO());
        return "login";
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("user") UserDTO user, Model model) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();
        User u = null;

        try {
            TypedQuery<User> query = em.createQuery("select u from User u where u.userMail = '" + user.getMail() + "'", User.class);
            u = query.getSingleResult();
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

        if(u != null && user.getPass().equals(u.getUserPass())) {
            return "redirect:/dashboard";
        } else {
            model.addAttribute("error", "Błędny login lub hasło");
            return "login";
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model) {

        return "redirect:/";
    }





}
