package pl.dweb.separatum.controller;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dweb.separatum.dto.ValueDTO;
import pl.dweb.separatum.model.Case;
import pl.dweb.separatum.model.ProtectedValue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.Valid;
import java.util.List;

@Controller
public class ValuesController {


    @RequestMapping(value = "/add_protected_value", method = RequestMethod.GET)
    public String addValueForm(@RequestParam("id")Long id, Model model) {
        ValueDTO value = new ValueDTO();
        model.addAttribute("protectedValue", value);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
        EntityManager em = emf.createEntityManager();

        Session session = em.unwrap(Session.class);
        Query query = session.createQuery("from ProtectedValue");
        List<ProtectedValue> values = query.list();
       Case aCase = em.find(Case.class, id);
        Hibernate.initialize(aCase.getControlPatterns());
        Hibernate.initialize(aCase.getControlObjects());
        model.addAttribute("aCase", aCase);
        model.addAttribute("label", "Dodaj chronioną wartość konstytucyjną");
        model.addAttribute("values", values);
        em.close();
        emf.close();
        return "add_protected_value";
    }

    @RequestMapping(value = "/add_protected_value", method = RequestMethod.POST)
    public String addValue(@ModelAttribute("protectedValue") @Valid ValueDTO value, BindingResult result) {
        if (result.hasErrors()) {
            return "add_protected_value";
        } else {
            System.out.println(value.getCaseID());
            System.out.println(value.getName());

            EntityManagerFactory emf = Persistence.createEntityManagerFactory("CasePU");
            EntityManager em = emf.createEntityManager();

            Case aCase = em.find(Case.class, value.getCaseID());
            ProtectedValue v = null;
            List<ProtectedValue> tmp = null;
            List<Case> caseTmp = null;
            for(Long id : value.getValueID()) {
                v = em.find(ProtectedValue.class, id);
                if(!aCase.getProtectedValues().contains(v)) {
                    em.getTransaction().begin();
                    tmp = aCase.getProtectedValues();
                    tmp.add(v);
                    caseTmp = v.getCases();
                    aCase.setProtectedValues(tmp);
                    caseTmp.add(aCase);
                    em.merge(v);
                    em.merge(aCase);
                    em.getTransaction().commit();
                }
            }

            if(value.getName().length() > 0) {
                String[] newValues = value.getName().split(";");
                for (int i = 0; i < newValues.length; i++) {
                    System.out.println("!!! new value: " + newValues[i]);
                    v = new ProtectedValue(newValues[i].trim(), aCase);
                    if(!aCase.getProtectedValues().contains(v)) {
                        em.getTransaction().begin();
                        tmp = aCase.getProtectedValues();
                        tmp.add(v);
                        aCase.setProtectedValues(tmp);
                        em.persist(v);
                        em.merge(aCase);
                        em.getTransaction().commit();
                    }
                }
            }
            em.close();
            emf.close();
            return "redirect:/case_info?id=" + value.getCaseID();
        }
    }
}
