package pl.dweb.separatum.model;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private Integer userID;

    @Column(name = "mail")
    private String userMail;

    @Column(name = "pass")
    private String userPass;

    @Column(name = "name")
    private String userName;

    @Column(name = "lastname")
    private String userLastname;

    public User() {}

    public User(String userMail, String userPass) {
        this.userMail = userMail;
        this.userPass = userPass;
    }

    public User(String userMail, String userPass, String userName, String userLastname) {
        this.userMail = userMail;
        this.userPass = userPass;
        this.userName = userName;
        this.userLastname = userLastname;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }
}
