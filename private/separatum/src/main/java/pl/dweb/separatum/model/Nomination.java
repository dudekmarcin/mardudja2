package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by md on 2/24/17.
 */
@Entity
@Table(name = "nominations")
public class Nomination {

    @Id
    @GeneratedValue
    @Column(name = "nomination_id")
    private Integer nominationID;

    @Column(name = "party_name")
    private String partyName;

    @ManyToMany(mappedBy = "nominations")
    List<Judge> judges;

    public Nomination() {}

    public Nomination(String partyName) {
        this.partyName = partyName;
    }

    public Integer getNominationID() {
        return nominationID;
    }

    public void setNominationID(Integer nominationID) {
        this.nominationID = nominationID;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public List<Judge> getJudges() {
        return judges;
    }

    public void setJudges(List<Judge> judges) {
        this.judges = judges;
    }

    @Override
    public String toString() {
        return "Nomination{" +
                "nominationID=" + nominationID +
                ", partyName='" + partyName + '\'' +
                '}';
    }
}
