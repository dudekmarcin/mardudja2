package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "votum_causes")
public class Cause {

    @Id
    @GeneratedValue
    @Column(name = "cause_id")
    private Integer causeID;

    @Column( name = "cause_name")
    private String causeName;

    @OneToMany(mappedBy = "cause")
    private List<Separatum> separatums;

    public Cause() {}

    public Cause(String causeName) {
        this.causeName = causeName;
    }

    public Integer getCauseID() {
        return causeID;
    }

    public void setCauseID(Integer causeID) {
        this.causeID = causeID;
    }

    public String getCauseName() {
        return causeName;
    }

    public void setCauseName(String causeName) {
        this.causeName = causeName;
    }

    @Override
    public String toString() {
        return "Cause{" +
                "causeID=" + causeID +
                ", causeName='" + causeName + '\'' +
                '}';
    }
}
