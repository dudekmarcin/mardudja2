package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "points")
public class Point {

    @Id
    @GeneratedValue
    @Column(name = "point_id")
    private Long pointID;

    @ManyToOne
    @JoinColumn(name = "case_id")
    private Case pCase;

    @OneToMany(mappedBy = "point")
    private List<Separatum> separatums;

    @Column(name = "point_number")
    private Integer pointNumber;


    public Point() { }

    public Point(Case pCase, List<Separatum> separatums, Integer pointNumber) {
        this.pCase = pCase;
        this.separatums = separatums;
        this.pointNumber = pointNumber;
    }

    public Long getPointID() {
        return pointID;
    }

    public void setPointID(Long pointID) {
        this.pointID = pointID;
    }

    public Case getpCase() {
        return pCase;
    }

    public void setpCase(Case pCase) {
        this.pCase = pCase;
    }

    public List<Separatum> getSeparatums() {
        return separatums;
    }

    public void setSeparatums(List<Separatum> separatums) {
        this.separatums = separatums;
    }

    public Integer getPointNumber() {
        return pointNumber;
    }

    public void setPointNumber(Integer pointNumber) {
        this.pointNumber = pointNumber;
    }

    @Override
    public String toString() {
        return "Point{" +
                "pointID=" + pointID +
                ", pCase=" + pCase.getCaseID() +
                ", separatums=" + separatums.size() +
                ", pointNumber=" + pointNumber +
                '}';
    }
}
