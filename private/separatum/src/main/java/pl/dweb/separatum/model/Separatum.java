package pl.dweb.separatum.model;

import javax.persistence.*;

@Entity
@Table(name = "votum_separatum")
public class Separatum {

    @Id
    @GeneratedValue
    @Column(name = "votum_id")
    private Long votumID;

    @ManyToOne
    @JoinColumn(name = "case_id")
    private Case sCase;


    @ManyToOne
    @JoinColumn(name = "sentence_point")
    private Point point;

    @ManyToOne
    @JoinColumn(name = "judge_id")
    private Judge judge;

    @ManyToOne
    @JoinColumn(name = "votum_cause")
    private Cause cause;

    public Separatum() {}

    public Separatum(Case sCase, Point point, Judge judge, Cause cause) {
        this.sCase = sCase;
        this.point = point;
        this.judge = judge;
        this.cause = cause;
    }

    public Long getVotumID() {
        return votumID;
    }

    public void setVotumID(Long votumID) {
        this.votumID = votumID;
    }

    public Case getsCase() {
        return sCase;
    }

    public void setsCase(Case sCase) {
        this.sCase = sCase;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Judge getJudge() {
        return judge;
    }

    public void setJudge(Judge judge) {
        this.judge = judge;
    }

    public Cause getCause() {
        return cause;
    }

    public void setCause(Cause cause) {
        this.cause = cause;
    }

    @Override
    public String toString() {
        String p = (point != null) ? point.getPointID().toString() : "-";

        return "Separatum{" +
                "votumID=" + votumID +
                ", sCase=" + sCase.getCaseID() +
                ", point=" + p +
                ", judge=" + judge.getJudgeID() +
                ", cause=" + cause.getCauseID() +
                '}';
    }
}
