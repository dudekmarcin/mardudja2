package pl.dweb.separatum.model;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "protected_values")
public class ProtectedValue {

    @Id
    @GeneratedValue
    @Column(name = "value_id")
    private Long valueID;

    @Column(name = "protected_name")
    private String name;

    @ManyToMany(mappedBy = "protectedValues", cascade = CascadeType.ALL)
    @Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE )
    private List<Case> cases;

    public ProtectedValue() {}

    public ProtectedValue(String name, Case vCase) {
        this.name = name;
        cases = new ArrayList<>();
        cases.add(vCase);
    }

    public Long getValueID() {
        return valueID;
    }

    public void setValueID(Long valueID) {
        this.valueID = valueID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Case> getCases() {
        return cases;
    }

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }

    public void addCase(Case pCase) {
        if(cases == null) {
            cases = new ArrayList<>();
        }
        cases.add(pCase);
    }

    @Override
    public String toString() {
        return "ProtectedValue{" +
                "valueID=" + valueID +
                ", name='" + name + '\'' +
                ", cases=" + cases +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProtectedValue that = (ProtectedValue) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

}
