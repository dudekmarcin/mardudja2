package pl.dweb.separatum.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by md on 2/18/17.
 */
@Entity
@Table(name = "specializations")
public class Specialization{

    @Id
    @GeneratedValue
    @Column(name = "specialization_id")
    private Integer specializationID;

    @Column(name = "specialization_name")
    private String specializationName;

    @ManyToMany(mappedBy = "specializations")
    List<Judge> judges;

    public Specialization() {}

    public Specialization(String specializationName) {
        this.specializationName = specializationName;
    }

    public Integer getSpecializationID() {
        return specializationID;
    }

    public void setSpecializationID(Integer specializationID) {
        this.specializationID = specializationID;
    }

    public String getSpecializationName() {
        return specializationName;
    }

    public void setSpecializationName(String specializationName) {
        this.specializationName = specializationName;
    }

    public List<Judge> getJudges() {
        return judges;
    }

    public void setJudges(List<Judge> judges) {
        this.judges = judges;
    }

    @Override
    public String toString() {
        return "Specialization{" +
                "specializationID=" + specializationID +
                ", specializationName='" + specializationName + '\'' +
                '}';
    }
}
