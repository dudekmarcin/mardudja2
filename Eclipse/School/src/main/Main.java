package main;

import java.util.List;

public class Main {

	public static void main(String[] args) {

		School school = new School();
		
		school.addStudent(new Student("Janek", "Kowalski", 2004, 04102100643, 34), new StudentData("Katarzyna", "Władysław"));
		school.addStudent(new Student("Maciek", "Nowak", 2003, 03112103643, 35), new StudentData("Anna", "Radosław"));
		school.addStudent(new Student("Tomek", "Boski", 2002, 02122100624, 32), new StudentData("Monika", "Wiesław"));
		school.addStudent(new Student("Kamil", "Waśniewski", 2004, 041111643, 31), new StudentData("Maria", "Tomasz"));
		school.addStudent(new Student("Paweł", "Kozak", 2003, 03102100222, 33), new StudentData("Jolanta", "Wacław"));
		school.addStudent(new Student("Marcin", "Tomczyk", 2004, 04102111131, 36), new StudentData("Maryla", "Tadeusz"));

		StudentData sd = school.getSDFromPesel(02122100624);
		List<Student> ls = school.getStudentPerClass(5);
		System.out.println(sd);
		System.out.println("------");
		ls.forEach(s -> System.out.println(s));
		System.out.println("------");
		ls = school.getStudentPerClass(6);
		ls.forEach(s -> System.out.println(s));
	}

}
