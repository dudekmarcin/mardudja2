package main;

public class StudentData {

	private String momsName;
	private String dadsName;
	
	public StudentData(String momsName, String dadsName) {
		this.momsName = momsName;
		this.dadsName = dadsName;
	}
	
	public String getMomsName() {
		return this.getMomsName();
	}
	
	public String getDadsName() {
		return this.getDadsName();
	}
	
	@Override
	public String toString() {
		return "Imię matki: " + this.momsName + ", imię ojca: " + this.dadsName;
	}
	
}
