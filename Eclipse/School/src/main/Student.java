package main;

public class Student {

	private String name;
	private String surname;
	private int yearOfBirth;
	private int pesel;
	private int shoeSize;
	
	public Student(String name, String surname, int year, int pesel, int shoeSize) {
		this.name = name;
		this.surname = surname;
		this.yearOfBirth = year;
		this.pesel = pesel;
		this.shoeSize = shoeSize;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public int getYearOfBirth() {
		return yearOfBirth;
	}

	public int getPesel() {
		return pesel;
	}

	public int getShoeSize() {
		return shoeSize;
	}

	@Override
	public String toString() {
		
		return this.name + " " + this.surname + ", rocznik: " + this.yearOfBirth + ", PESEL: " + this.pesel + ", nr buta: " + this.shoeSize; 
		
	}
	
}
