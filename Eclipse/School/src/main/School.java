package main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class School {

	private List<Student> studentList = new ArrayList<>();
	private Map<Integer, StudentData> studentDataMap = new TreeMap<>(); 
	
	public void addStudent(Student student, StudentData sData) {
		this.studentList.add(student);
		this.studentDataMap.put(student.getPesel(), sData);
	}
	
	public List<Student> getStudentPerClass(int classNum) {
		return this.studentList.stream().filter(s -> s.getYearOfBirth() == this.getBirthYear(classNum)).collect(Collectors.toList());
	}
	
	public StudentData getSDFromPesel(int pesel) {
		return this.studentDataMap.get(pesel);
	}
	
	private int getBirthYear(int classNum) {
		return Calendar.getInstance().get(Calendar.YEAR) - 7 - classNum;
	}
	
}
