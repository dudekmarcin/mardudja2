package Zad1;

import java.util.Arrays;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Zad1 {

//	Napisz metodę, która utworzy obiekt JSONObject. Następnie dodaj do obiektu następujące klucze
//	i wartości:
//	"pierwszy" : "przykład",
//	"drugi" : "inny przykład",
//	"liczba" : 5,
//	"tablica" : [1, 2, 3, 4, 5],
//	"obiekt" : { "x" : 5, "y" : 17 }
//	i wydrukuj obiekt w formacie JSON w konsoli.
	
	public static void main(String[] args) {
		
		JSONObject jo = new JSONObject();
		JSONArray ja = new JSONArray();
		ja.add(1);
		ja.add(2);
		ja.add(3);
		ja.add(4);
		ja.add(5);
		
		JSONObject jo2 = new JSONObject();
		jo2.put("x", 5);
		jo2.put("y", 17);
		
		jo.put("pierwszy", "przykład");
		jo.put("drugi", "inny przykład");
		jo.put("liczba",  5);
		jo.put("tablica", ja);
		jo.put("obiekt", jo2);
	//	System.out.println(jo);
	
	
	}	
}
