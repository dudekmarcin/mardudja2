package Zad2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

import org.json.simple.JSONArray;

public class Zad2 {

//	Napisz metodę, która wygeneruje tablicę losowej długości (0, 100) i uzupełni ją losowymi liczbami z
//	zakresu (0, 100), a następnie stwórz obiekt JSONArray, przekonwertuj wygenerowaną tablicę na
//	tablicę JSON i zapisz plik wynikowy jako randomInts.json w pakiecie resources.

	
	
	public JSONArray randomNumbers() {
		Random random = new Random();
		int[] array = new int[random.nextInt(101)];
		
		for(int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(101);
		}
	
		JSONArray ja = new JSONArray();
		for(int arr: array) {
			ja.add(arr);
		}
		return ja;
	}
	
	public void writeJSON(JSONArray array) {
		File f = new File("src//resources//randomInts.json");
		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			pw.println(array);
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
