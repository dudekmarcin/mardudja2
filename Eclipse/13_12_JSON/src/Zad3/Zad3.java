package Zad3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import org.json.simple.JSONObject;

public class Zad3 {

//	Korzystając z pliku words.txt utwórz JSONObject, który będzie zawierał nieparzyste linie jako
//	klucze, a parzyste jako wartości tych kluczy. Następnie zapisz wynik w pliku words.json. Jeżeli w
//	pliku words.txt ilość linii jest nieparzysta, pomiń ostatni wyraz.

	private String path;
	private String name;
	
	public Zad3() {
		this.path = "src//resources//";
		this.name = "words";
	}
	
	public Zad3(String path, String name) {
		this.path = path;
		this.name = name;
	}
	
	public JSONObject readFile() {
		JSONObject jsonObject = new JSONObject();
		try {
			File f = new File(this.path + this.name + ".txt");
			Scanner scan = new Scanner(f);
			String key;
			while(scan.hasNextLine()) {
				key = scan.nextLine();
				if(scan.hasNextLine()) {
					jsonObject.put(key, scan.nextLine());
				}
			}
			scan.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonObject;
		
	}
	
	public void writeJSON(JSONObject map) {
		try {
			File f = new File(this.path + this.name + ".json");
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			pw.println(map);
			pw.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
