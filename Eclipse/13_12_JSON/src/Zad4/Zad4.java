package Zad4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.json.simple.JSONObject;

public class Zad4 {
//	Napisz metodę, która odczyta plik CSV, a następnie utworzy obiekt JSONObject, w którym
//	kluczami będą poszczególne elementy nagłówka, a wartościami poszczególne wartości. Zwróć uwagę,
//	że w pliku CSV może być więcej rekordów niż 1, dlatego konieczne może być zwrócenie tablicy
//	obiektów (JSONArray).
	
	private String path;
	private String name;
	
	public Zad4() {
		this.path = "src//resources//";
		this.name = "students";
	}
	
	public Zad4(String path, String name) {
		this.path = path;
		this.name = name;
	}
	
	public JSONObject[] csvToJSONObject() {
		JSONObject[] jArray= new JSONObject[this.countFileLines() - 1];
		try {
			File f = new File(this.path + this.name + ".csv");
			Scanner scan = new Scanner(f);
			String[] head = scan.nextLine().split(";");
			String[] values;
			int i = 0;
			while(scan.hasNextLine()) {
				values = scan.nextLine().split(";");
				jArray[i] = new JSONObject();
				for(int j = 0; j < values.length; j++) {
					jArray[i].put(head[j], values[j]);
				}
				i++;
			}
			scan.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jArray;
	}
	
	private int countFileLines() {
		int counter = 0;
		try {
			File f = new File(this.path + this.name + ".csv");
			Scanner scan = new Scanner(f);
			while(scan.hasNextLine()) {
				scan.nextLine();
				counter++;
			}
			scan.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return counter;
	}
	
}
