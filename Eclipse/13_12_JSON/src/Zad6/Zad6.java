package Zad6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Zad6 {

	// Napisz klasę, która pozwoli na odczytanie pliku words.json, a następnie
	// wypisze wszystkie jego
	// klucze i wartości przypisane do tych kluczy.

	private String path;
	private String name;

	public Zad6() {
		this.path = "src//resources//";
		this.name = "words.json";
	}

	public Zad6(String path, String name) {
		this.path = path;
		this.name = name;
	}

	public void run() {
		this.readJSON(this.getJSON());
	}
	
	private String getJSON() {
		String json = "";
		try {
			File f = new File(this.path + this.name);
			Scanner scan = new Scanner(f);
			while (scan.hasNextLine()) {
				json = scan.nextLine();
			}
			scan.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	
	private void readJSON(String json) {
		JSONParser parser = new JSONParser();
		try {
			JSONObject jsonMap = (JSONObject) parser.parse(json);
			
			jsonMap.forEach((k, v) -> System.out.println(k + " : " + v));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
}
