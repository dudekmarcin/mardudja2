package Zad5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class Zad5 {

	// Napisz metodę, która odczyta plik file.json, a następnie wydrukuj z niego
	// do konsoli poszczególne wartości kluczy (lista, asd, qweqe). Następnie
	// dopisz do klasy metodę, która pozwoli na zapisanie poszczególnych
	// wartości jako plik settings.ini zachowując odpowiednią strukturę pliku
	// INI.

	private String filepath = "src//resources//file";
	private List<String> lines = new LinkedList<>();

	public Object readJSON() {
		String jsonContent = "";
		Object obj = new Object();
		try {
			File f = new File(this.filepath+".json");
			Scanner scan = new Scanner(f);
			while (scan.hasNextLine()) {
				jsonContent += scan.nextLine();
			}
			scan.close();
			JSONParser parser = new JSONParser();
			obj = parser.parse(jsonContent);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return obj;
	}

	public void printJSONValues() {
		Object obj = this.readJSON();
		if (obj instanceof JSONArray) {
			JSONArray jFile = (JSONArray) obj;
			jFile.forEach(j -> System.out.println(j));
		} else if (obj instanceof JSONObject) {
			JSONObject jFile = (JSONObject) obj;
			jFile.keySet().forEach(k -> System.out.println(k + ": " + jFile.get(k)));
			JSONArray jArray = (JSONArray) jFile.get("lista");

		}

	}

	public String createINI(Object json) {
		switch (json.getClass().getSimpleName()) {
		case "JSONArray":

			JSONArray array = (JSONArray) json;
			String arrayValues = "";
			for (Object item : array) {

				if (item.getClass().getSimpleName().equals("JSONArray")
						|| item.getClass().getSimpleName().equals("JSONObject")) {

					this.createINI(item);
				} else {
					arrayValues += item + ",";
				}
			}
			arrayValues = arrayValues.substring(0, arrayValues.length() - 1);
//			this.lines.add(arrayValues);
			return arrayValues;

		case "JSONObject":

			JSONObject object = (JSONObject) json;
			Iterator itrObj = object.keySet().iterator();
			String objValue = "";
			while (itrObj.hasNext()) {
				Object k = itrObj.next();

				if (object.get(k).getClass().getSimpleName().equals("JSONObject")) {
					this.createINI(object.get(k));
				} else if (object.get(k).getClass().getSimpleName().equals("JSONArray")) {
					objValue = k + "=" + this.createINI(object.get(k));
					this.lines.add(objValue);

				} else {
					objValue = k + "=" + object.get(k);
					this.lines.add(objValue);

				}
			}
			return objValue;
		}
		return null;
	}
	
	public void writeJSONtoINI() {
		this.createINI(this.readJSON());
		try {
			File f = new File(this.filepath + ".ini");
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			this.lines.forEach(l -> pw.println(l));
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<String> getLines() {
		return this.lines;
	}
}
