package Zad7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Zad7 {

	private String path;
	private String name;

	public Zad7(String name) {
		this.path = "src//resources//";
		this.name = (name.length() > 5 && name.substring(name.length() - 5, name.length()).equals(".json"))
				? name.substring(0, name.length() - 5) : name;
	}

	public Zad7(String path, String name) {
		this.path = path;
		this.name = (name.length() > 5 && name.substring(name.length() - 5, name.length()).equals(".json"))
				? name.substring(0, name.length() - 5) : name;
	}

	public void createXML() throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();

		Element root = doc.createElement("root");
		Node node = this.readJSON(doc, root);
		root.appendChild(node);
		doc.appendChild(root);
		try {
			this.saveXML(doc);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Node readJSON(Document doc, Element parent) {
		Node node = null;

		try {
			String json = "";
			File f = new File(this.path + this.name + ".json");
			Scanner scan = new Scanner(f);
			while (scan.hasNextLine()) {
				json += scan.nextLine();
			}
			scan.close();
			JSONParser parser = new JSONParser();
			Object root = parser.parse(json);
			node = addNodeToXML(doc, root, parent);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return node;

	}

	private Node addNodeToXML(Document doc, Object json, Node parent) {
		switch (json.getClass().getSimpleName()) {
		case "JSONArray":
			JSONArray array = (JSONArray) json;
			Node arrNode = doc.createElement("array");
			for (Object item : array) {
				if (item instanceof JSONArray || item instanceof JSONObject) {

					arrNode.appendChild(this.addNodeToXML(doc, item, arrNode));
				} else {
					arrNode.appendChild(this.createNode(doc, item, "val"));
				}
			}

			return arrNode;

		case "JSONObject":
			JSONObject object = (JSONObject) json;
			Iterator itrObj = object.keySet().iterator();
			Node objNode = doc.createElement("object");

			while (itrObj.hasNext()) {
				Object k = itrObj.next();

				if (object.get(k) instanceof JSONArray || object.get(k) instanceof JSONObject) {

					objNode.appendChild(this.addNodeToXML(doc, object.get(k), objNode));
				} else {
					objNode.appendChild(this.createNode(doc, object.get(k), k.toString()));
				}
			}
			return objNode;
		}
		return null;
	}

	private Node createNode(Document doc, Object obj, String type) {
		Node node = doc.createElement(type);
		node.appendChild(doc.createTextNode(obj.toString()));
		return node;
	}

	public void saveXML(Document doc) throws TransformerException {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + this.name + ".xml"));
		t.transform(source, sr);
	}

}
