
public class Zad5 {

	public void printNumbers(int a, int b) {
		if(((a > 0 && a < 254) && (b > 1 && b < 255))) {
			System.out.println("Liczby poza zakresem.");
			return;
		}
			
			for(int i = (a%2==0) ? a+1 : a; i<=b;i+=2) {
				System.out.print(i+" ");
			}
			
			for(int i = (b%2==0) ? b : b-1; i>=a; i-=2) {
				System.out.print(i+" ");
			}
			
			System.out.println();

	}
}
