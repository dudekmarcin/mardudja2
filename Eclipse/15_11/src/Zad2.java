
public class Zad2 {

	public String returnDayName(int x) {
		String result;
		
		switch(x) {
		case 1: result = "Poniedzia�ek"; break;
		case 2: result = "Wtorek"; break;
		case 3: result = "�roda"; break;
		case 4: result = "Czwartek"; break;
		case 5: result = "Pi�tek"; break;
		case 6: result = "Sobota"; break;
		case 7: result = "Niedziela"; break;
		default: result = null;
		}
		
		return result;
	}
	
	public void getDayOfTheWeek(int x) {		
		switch(x) {
		case 1: System.out.println("Poniedzia�ek"); break;
		case 2: System.out.println("Wtorek"); break;
		case 3: System.out.println("�roda"); break;
		case 4: System.out.println("Czwartek"); break;
		case 5: System.out.println("Pi�tek"); break;
		case 6: System.out.println("Sobota"); break;
		case 7: System.out.println("Niedziela"); break;
		}
	}
}
