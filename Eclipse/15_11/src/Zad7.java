
public class Zad7 {

	public float countMoney(float sum, int years) {
		double rate = 1.194;
		for(int i=0;i<years;i++) {
			sum *= rate;
		}
		
		return sum;
	}
}
