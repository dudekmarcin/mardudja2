
public class Zad6 {

	public int squares(int number) {
		if(number < 0 || number > 10) {
			System.out.print("Liczba "+number+" jest poza dopuszczalnym zakresem. Kod b��du: ");
			return -1;
		}
		
		int result = 0;
		for(int i = 1; i<=number;i++) {
			result+=i*i;
		}
		return result;
	}
}
