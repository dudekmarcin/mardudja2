
public class Homework {

	public String secsToHours(int s) {
		int hours = s/3600;
		int tmp = s%3600;
		int mins = tmp/60;
		int secs = tmp%60;
		
		return s+" sekund(y) to "+hours+" godzin, "+mins+" minut, "+secs+" sekund";
	}
}
