package czwartek1711;

public class Zad4 {

	//4. Napisz program obliczający iloczyn elementów nieparzystych tablicy dwuwymiarowej.
	public int multiplyOddElements(int[][] arr) {
		int result = 1;
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] % 2 == 1) {
					result *= arr[i][j];
				}
			}
		}
		
		return result;
	}

}
