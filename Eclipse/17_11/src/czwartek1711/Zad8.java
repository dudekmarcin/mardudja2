package czwartek1711;

public class Zad8 {

	// 8. Napisz program sumujący elementy parzyste w każdym wierszu tablicy
	// dwuwymiarowej. Funkcja powinna zwracać tablicę jednowymiarową zawierającą
	// sumy poszczególnych wierszy w indeksach tablicy.
	
	public int[] sumEvenNumbersInRows(int[][] arr) {
		
		int[] result = new int[arr.length];
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] % 2 == 0) {
					result[i] += arr[i][j];
				}
			}
		}
		
		return result;
	}
}
