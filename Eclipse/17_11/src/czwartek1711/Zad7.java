package czwartek1711;

public class Zad7 {

	//7. Napisz program znajdujący maksymalny element tablicy dwuwymiarowej.
	public int find2DArrayMax(int[][] arr) {
		int max = arr[0][0];
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(max < arr[i][j]) {
					max = arr[i][j];
				}
			}
		}
		
		return max;
		
	}
}
