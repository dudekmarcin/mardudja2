package czwartek1711;

public class Zad3 {

	//3. Napisz program obliczający iloczyn elementów parzystych tablicy dwuwymiarowej.
	public int multiplyEvenElements(int[][] arr) {
		int result = 1;
		
		for(int i=0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] % 2 == 0) {
					result *= arr[i][j];
				}
			}
		}
		
		return result;
	}
}
