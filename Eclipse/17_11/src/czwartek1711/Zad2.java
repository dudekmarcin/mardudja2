package czwartek1711;

public class Zad2 {

	//2. Napisz program obliczający iloczyn elementów tablicy dwuwymiarowej.
	public int multiplyElements(int[][] arr) {
		int result = 1;
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				result *= arr[i][j];
			}
		}
		
		return result;
	}

}
