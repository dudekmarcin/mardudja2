package czwartek1711;

public class Zad5 {

	//5. Napisz program obliczający iloczyn elementów podzielnych przez 3 tablicy dwuwymiarowej.
	public int multiplyElementsDividedByThree(int[][] arr) {
		int result = 1;
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if( arr[i][j] % 3 == 0) {
					result *= arr[i][j];
				}
			}
		}
		
		return result;
	}

}
