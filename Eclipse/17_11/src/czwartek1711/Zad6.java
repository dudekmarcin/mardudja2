package czwartek1711;

public class Zad6 {

	//6. Napisz program znajdujący minimalny element tablicy dwuwymiarowej.
	public int find2DArrayMin(int[][] arr) {
		int result = arr[0][0];
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(result > arr[i][j]) {
					result = arr[i][j];
				}
			}
		}
		
		return result;
	}

}
