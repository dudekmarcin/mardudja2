package Main;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import RandomUsers.CreateUser;
import RandomUsers.User;
import Main.HTTPConnector;

public class Main {

	public static void main(String[] args) {
	
		CreateUser user = new CreateUser();
	//	User random = user.newUser();
	//	System.out.println(random);
//	//	System.out.println(random.toJSON());
//		JSONArray users = new JSONArray();
//		
//		for(int i = 0; i < 10; i++) {
//			users.add(user.newUser().toJSON());
//		}
//		System.out.println(users);
	//	System.out.println(users);
	//	users.forEach(l -> System.out.println(l));
	
	
	//User from rug API
	
	HTTPConnector con = new HTTPConnector();
	Map<String, String> params = new HashMap<>();
	params.put("results", "1");
	try {
		String result = con.send("post", params);
		JSONParser parser = new JSONParser();
		Object jsonObj = parser.parse(result); 
		JSONObject resultsJSON = (JSONObject) jsonObj;
		JSONArray usersJSON = (JSONArray) resultsJSON.get("results");
		JSONObject userJSON = (JSONObject) usersJSON.get(0);
		User test = user.newUser(userJSON);
		System.out.println(test);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	}
}
