package main;

import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Zad4 implements Closeable {
	// - dodawanie alfabetyczne słów (1 literka alfabetycznie, reszta losowo).
	// Dla zainteresowanych dodawanie ze kompletnym sprawdzeniem kolejności
	// alfabetycznej (całe słowa).
	
//	Set<String> words;
//	
//	public Zad4() {
//		 words = new TreeSet<>();
//	}
//	
//	public void addWord(String word) {
//		words.add(word);
//	}
//	
	Scanner scan = new Scanner(System.in);
	Map<Character, Set<String>> words = new TreeMap<>();
	
	public void addWord() {
		
		String line = "";
		while(!line.equals("q")) {
			System.out.println("Podaj słowo:");
			line = scan.nextLine().toLowerCase();
			char firstLetter = line.charAt(0);
			if(words.containsKey(firstLetter)) {
				Set<String> gotFromMap = words.get(firstLetter);
				gotFromMap.add(line);
			} else {
				Set<String> toPut = new TreeSet<>();
				toPut.add(line);
				words.put(firstLetter, toPut);
			}
		}
		
	}
	
	public void print() {
		System.out.println(words);
	}
//	

	@Override
	public void close() throws IOException {
		scan.close();
		
	}
}
