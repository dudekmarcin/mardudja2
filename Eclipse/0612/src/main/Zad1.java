package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Zad1 {

	// - stworzenie przykładowego zbioru liczb i znalezienie najwyższego oraz
	// najniższego elementu. Którą kolekcję lepiej tutaj użyć?

	List<Integer> numberList = new ArrayList<>(100);
	Random random = new Random();

	public Zad1() {
		
	for(int i = 0; i < 100; i++) {
		numberList.add(random.nextInt() % 99999);
		}
	}

	public void getMax() {
		int max = this.numberList.get(0);
		int index = 0;
		for (int i = 0; i < this.numberList.size(); i++) {
			if (this.numberList.get(i) > max) {
				max = this.numberList.get(i);
				index = i;
			}
		}
	//	int max1 = Collections.max(this.numberList);
		
		System.out.println("Maksymalna wartość: " + max + " pod indeksem: " + index);

	}
	
	public void getMin() {
		int min = this.numberList.get(0);
		int index = 0;
		for (int i = 0; i < this.numberList.size(); i++) {
			if (this.numberList.get(i) < min) {
				min = this.numberList.get(i);
				index = i;
			}
		}
		
		//int min1 = Collections.min(this.numberList);

		System.out.println("Minimalna wartość: " + min + " pod indeksem: " + index);

	}
}
