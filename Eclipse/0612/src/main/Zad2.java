package main;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Zad2 implements Closeable {
//	- pobieranie z wejścia (Scanner) jednej linii zawierającej liczby oddzielone średnikami. Policzyć średnią i wypisać.
	
	Scanner scan = new Scanner(System.in);
	
	public double countAvg() {
		System.out.println("Wprowadź liczby oddzielone średnikami:");
		String[] numbers = scan.nextLine().split(";");
		List<Double> lista = new ArrayList<>();
		double avg = 0.0;
		for(int i = 0; i < numbers.length; i++) {
			lista.add(Double.parseDouble(numbers[i].replace(",", ".")));
			avg += lista.get(i);
		}
		
		return avg/numbers.length;
		
	}
	
	

	@Override
	public void close() throws IOException {
		scan.close();		
	}
	
	
}
