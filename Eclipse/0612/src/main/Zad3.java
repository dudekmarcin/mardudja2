package main;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Zad3 implements Closeable {
	// - obliczenie i dodanie do listy kolejnych liczb ciągu fibonacciego. Po
		// każdym kliknięciu "ENTER" powinna się (obliczyć i) dodać kolejna liczba.
		
	
	Scanner scan = new Scanner(System.in);
	List<Integer> fibonacci;
	
	public Zad3() {
		fibonacci = new LinkedList<>();
		fibonacci.add(1);
		fibonacci.add(1);
	}
	
	public void increaseFibonacci() {
		int i = 0;
		System.out.println("Wciskaj Enter, aby zwiekszać ciąg Fibonacciego");
		System.out.println(fibonacci.get(0));
		System.out.print(fibonacci.get(1));

		while(scan.nextLine().equals("")) {
			i++;
			fibonacci.add(fibonacci.get(i-1) + fibonacci.get(i));
			System.out.print(fibonacci.get(i+1));
		}
	}
	
	public void removeNumbers() {
//		- do poprzedniego zadania - ze zbioru usunąć wszystkie liczby podzielne przez 2 i 3.
		Iterator<Integer> iterator = fibonacci.iterator();
		while(iterator.hasNext()) {
			int temp = iterator.next();
			if(temp % 2 == 0 || temp % 3 ==0) {
				iterator.remove();
			}
		}
	}
	
	public void print() {
		for(int i = 0; i < fibonacci.size(); i++) {
			System.out.println(fibonacci.get(i));
		}
	}
	
	public void printToFile() {
		
//		- do poprzedniego zadania - wynik listy zapisać do pliku (linia po linii kolejna liczba)
		
		File f = new File("src//main//fibonacci");
		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			fibonacci.forEach(s -> pw.println(s));
			pw.close();
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
	@Override
	public void close() throws IOException {
		scan.close();
	}
	
	
}
