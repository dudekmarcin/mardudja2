package pl.dweb.hibernate_votum;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import pl.dweb.votum.model.Case;

public class JpaTest {
    private static EntityManager em;
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 EntityManagerFactory emf = Persistence
	                .createEntityManagerFactory("CasePU");
	        em = emf.createEntityManager();
	        
	        createCase();
	       
	}
	
	private static void createCase() {
        em.getTransaction().begin();
        Case emp = new Case(new Long(3), "K 10/10", new Date(), 1, 10, 1, 1L, 2L, 1L, 1, "www");
        em.persist(emp);
        em.getTransaction().commit();
    }

}
