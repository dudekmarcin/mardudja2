package pl.dweb.votum.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cases")
public class Case {

	@Id
	@Column(name = "case_id")
	private Long caseID;
	
	@Column(name = "signature")
    private String signature;
	
	@Column(name = "date")
    private Date date;
	
	@Column(name = "instance")
    private Integer instanceID;
	
	@Column(name = "number_of_judges")
    private Integer judgesNumber;
	
	@Column(name = "number_of_votsep")
    private Integer separatumNumber;
	
	@Column(name = "leader_id")
    private Long leaderID;
	
	@Column(name = "control_subject")
    private Long controlSubjectID;
	
	@Column(name = "protected_value")
    private Long protectedValueID;
	
	@Column(name = "sentence_point_number")
    private Integer pointsNumber;
	
	@Column(name = "sentence_url")
    private String sentenceURL;
	
	public Case() {}

	public Case(Long caseID, String signature, Date date, Integer instanceID, Integer judgesNumber,
			Integer separatumNumber, Long leaderID, Long controlSubjectID, Long protectedValueID, Integer pointsNumber,
			String sentenceURL) {
		this.caseID = caseID;
		this.signature = signature;
		this.date = date;
		this.instanceID = instanceID;
		this.judgesNumber = judgesNumber;
		this.separatumNumber = separatumNumber;
		this.leaderID = leaderID;
		this.controlSubjectID = controlSubjectID;
		this.protectedValueID = protectedValueID;
		this.pointsNumber = pointsNumber;
		this.sentenceURL = sentenceURL;
	}

	public Long getCaseID() {
		return caseID;
	}

	public void setCaseID(Long caseID) {
		this.caseID = caseID;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getInstanceID() {
		return instanceID;
	}

	public void setInstanceID(Integer instanceID) {
		this.instanceID = instanceID;
	}

	public Integer getJudgesNumber() {
		return judgesNumber;
	}

	public void setJudgesNumber(Integer judgesNumber) {
		this.judgesNumber = judgesNumber;
	}

	public Integer getSeparatumNumber() {
		return separatumNumber;
	}

	public void setSeparatumNumber(Integer separatumNumber) {
		this.separatumNumber = separatumNumber;
	}

	public Long getLeaderID() {
		return leaderID;
	}

	public void setLeaderID(Long leaderID) {
		this.leaderID = leaderID;
	}

	public Long getControlSubjectID() {
		return controlSubjectID;
	}

	public void setControlSubjectID(Long controlSubjectID) {
		this.controlSubjectID = controlSubjectID;
	}

	public Long getProtectedValueID() {
		return protectedValueID;
	}

	public void setProtectedValueID(Long protectedValueID) {
		this.protectedValueID = protectedValueID;
	}

	public Integer getPointsNumber() {
		return pointsNumber;
	}

	public void setPointsNumber(Integer pointsNumber) {
		this.pointsNumber = pointsNumber;
	}

	public String getSentenceURL() {
		return sentenceURL;
	}

	public void setSentenceURL(String sentenceURL) {
		this.sentenceURL = sentenceURL;
	}
	
	
	
}
