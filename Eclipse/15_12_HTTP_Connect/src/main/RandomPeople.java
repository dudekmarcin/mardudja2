package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class RandomPeople {
	// Pod adresem http://randomuser.me/api/?nat=US&results=10 znajduje się API
	// zwracające losowych ludzi. Połącz się z serwerem, pobierz dane w formacie
	// JSON, a następnie wyświetl imiona i nazwiska, maila oraz płeć pobranych
	// użytkowników w konsoli w formacie:
	//
	// Imię Naziwsko (M/K) - m@il

	private Map<String, String> params;
	private String url;
	private Map<String, String> values;
	private List<String> vallist;
	
	public RandomPeople() {
		this.params = new HashMap<>();
		this.values = new HashMap<>();
		this.vallist = new ArrayList<>();
		this.url = "https://randomuser.me/api/";
		params.put("nat", "US");
		params.put("results", "10");
	}
	
	public void getValues(List<String> keys) {
		
			try {
				this.readJSON(this.getData());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
	}
	
	public void run() throws IOException {
		this.readJSON(this.getData());
		this.vallist.forEach(l -> System.out.println(l));
	}
	
	private String getData() throws IOException {
		HTTPConnector rs = new HTTPConnector(this.url);
		return rs.send("GET", params);
	}

	private void readJSON(String json) {
		JSONObject jsonObj = null;
		try {
			JSONParser parser = new JSONParser();
			jsonObj = (JSONObject) parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<String> keys = new ArrayList<>();
		
		keys.add("first");
		keys.add("last");
		keys.add("gender");
		keys.add("email");
		this.getValues(keys);
		JSONArray arr = (JSONArray) jsonObj.get("result");
		for(Object item : arr) {
			this.encodeJSON(item, keys);
		}
	}

	public Object encodeJSON(Object json, List<String> keys) {
		String result = "";
		switch (json.getClass().getSimpleName()) {
		case "JSONArray":
			JSONArray array = (JSONArray) json;
			for (Object item : array) {
				if (item instanceof JSONArray || item instanceof JSONObject) {
					result += this.encodeJSON(item, keys).toString();
				} 
			}
			break;

		case "JSONObject":
			JSONObject object = (JSONObject) json;
			Iterator itrObj = object.keySet().iterator();
			while (itrObj.hasNext()) {
				Object k = itrObj.next();
				if (object.get(k) instanceof JSONArray || object.get(k) instanceof JSONObject) {
					result += this.encodeJSON(object.get(k), keys).toString();
				} else {
 					for(String key : keys) {
					if(k.equals(key)) {
						result += (k.toString() + ":" + object.get(k).toString() + " ");
					}
 					}
 					this.vallist.add(result);
				}
			}
		break;	
		}
	
		return result;
	}

}
