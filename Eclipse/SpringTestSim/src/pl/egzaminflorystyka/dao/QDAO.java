package pl.egzaminflorystyka.dao;

import java.util.List;

import pl.egzaminflorystyka.model.Question;

public interface QDAO {

	
	public List<Question> findAllQuestion();
	public Question findQuestionById(int id);
	public int getMaxId();
}
