<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="header.jsp" %>

<body>
<div id="container">
<div id="wrapper">
<%@ include file="menu.jsp" %>

<div class=info><p>Strona powstała jako odpowiedź na brak dobrej i darmowej bazy testów dla technika florysty, jakie istnieją dla innych zawodów. 
Strona została napisana jako projekt "po godzinach" dla starogardzkich florstek, przygotowujących się do testu R26 :) </p>

<p>Szkoda, aby praca włożona w stworzenie strony została zmarnowana, dlatego postanowiłem udostępnić ją innym osobom, które przygotowują się do egzaminu. 
W bazie znajdują się pytania z arkuszy egzaminacyjnych z lat 2010 - 2016. &nbsp; Łączna liczba pytań w bazie to obecnie &nbsp; ${how_many}.</p>
<p>Jeżeli znajdziesz na stronie jakiś błąd, będę wdzięczny, jeżeli zgłosisz do mailowo na adres <a href="mailto:marcin.a.dudek@gmail.com">marcin.a.dudek@gmail.com</a>. 
Przy opisie błędu podaj numer pytania, który znajduje się w szarym pasku pod każdym pytaniem. Pomoże mi to szybko i bezproblemowo poprawić wszystkie niedociągnięcia.</p>

	 </div>
</div>
<%@ include file="footer.jsp" %>


