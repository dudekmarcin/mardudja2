<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="Content-Language" content="pl" />
<title> Testy zawodowe technik florysta</title>
<c:url value="/resources/css/pdf.css" var="styleCSS" />
<link href="${styleCSS}" rel="stylesheet" type="text/css"/>
<script src="<c:url value="/resources/js/jspdf.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery-2.1.3.js"/>"></script>
	<script src="<c:url value="/resources/js/main.js"/>"></script>
	
	<script src="<c:url value="/resources/js/pdfFromHTML.js"/>"></script>
</head>
<body>
<a href="#" onclick="HTMLtoPDF()">Download PDF</a>
<div id="msg"></div>
<div id="container">

<div id="wrapper">
<p style="text-align: right;">www.egzamin-florystyka.pl</p>	
<h1>Kwalifikacja zawodowa R.26</h1>
<h2>Wykonywanie kompozycji florystycznych (test próbny)</h2>

<c:forEach items="${questions}" var="question" varStatus="i" >
<div class="quest">

${i.count}. ${question.content}
<c:if test="${question.img != null}"><img src="<c:url value="/resources/quest_img/${question.img}" />" /></c:if>

</div>

<div class="answer">
	A. ${question.getAnswerContent(0)}
</div> 

<div class="answer">
	B. ${question.getAnswerContent(1)}
</div> 

<div class="answer">
	C. ${questions.get(i.index).getAnswerContent(2)}
</div> 

<div class="answer">
	D. ${question.getAnswerContent(3)}
</div> 

<div class="qid">Pyt. ${question.id}</div>
</c:forEach>

<div class="keytable">;
	<p style="text-align: right;">www.egzamin-florystyka.pl</p>
	<h1>Kwalifikacja zawodowa R.26</h1>
	<h2>Wykonywanie kompozycji florystycznych (test próbny)</h2>
	<table>
	<tr><th colspan="4"><span>Klucz poprawnych odpowiedzi</span></th></tr>
	<tr><th>Pytanie</th><th>Odpowiedź</th><th>Pytanie</th><th>Odpowiedź</th></tr>
	<c:forEach var="k" begin="0" end="19">
		<tr><td><strong> ${k+1} </strong></td><td> ${questions.get(k).getCorrect()}</td>
		<td><strong>${k + 21} </strong></td><td> ${questions.get(k+5).getCorrect()}</td></tr>
	</c:forEach>	
	</table></div>	

</div>
</div>

	
	
</body>
</html>
