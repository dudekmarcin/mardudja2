package pl.dweb.users;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;

import org.json.simple.JSONObject;

public class CreateUser {

	private String path;
	private Random random;

	public CreateUser() {
		this.path = "pl//dweb//resources//";
		this.random = new Random();
	}

	public User newUser(JSONObject json) {
		JSONObject addr = (JSONObject) json.get("address");
		Address address = new Address(addr.get("street").toString(),
				Integer.parseInt(addr.get("houseNumber").toString()), addr.get("city").toString(),
				addr.get("postalCode").toString());
		User user = new User(json.get("name").toString(), json.get("lastname").toString(), address,
				Integer.parseInt(json.get("phone").toString()), json.get("cardNumber").toString(),
				json.get("pesel").toString());
		return user;
	}
	
	public User newUser() {
		Gender gender = this.randomGender();
		String pesel = this.randomPesel(gender);
		String name = this.randomName(gender);
		String lastname = this.randomLastname(gender);
		return new User(name, lastname, this.newAddress(), (this.random.nextInt(888888888) + 111111111), this.randomCard() , pesel);
	}
	
		
	public Address newAddress() {
		String[] city = this.randomCity();
		return new Address(this.randomStreet(), this.randomNumber(), city[0].trim(), city[1].trim());
	}

	private String randomCard() {
		String result = "";
		for (int i = 0; i < 4 ; i++){
			result += this.random.nextInt(10000) + " ";
		}
		return result.trim();
	}
	
	private String randomName(Gender gender) {
		String name = "";
		switch(gender) {
		case MALE: name = this.randomData("name_m.txt"); break;
		case FEMALE: name = this.randomData("name_f.txt"); break;
		}
		return name;
	}

	private String randomLastname(Gender gender) {
		String lastname = this.randomData("lastname.txt");
		if(gender.equals(Gender.FEMALE) && 
				((lastname.substring(lastname.length()-3, lastname.length()).equals("ski")) 
						|| (lastname.substring(lastname.length()-3, lastname.length()).equals("cki")))) {
				lastname = lastname.substring(0, lastname.length()-1) + "a";
			
		}
		return lastname;
	}

	public String randomPesel(Gender gender) {
		Calendar cal = this.randomBirthDate();
		String pesel = "";
		pesel += ((cal.get(Calendar.YEAR) % 1900) < 10) ? "0" + cal.get(Calendar.YEAR) % 1900
				: cal.get(Calendar.YEAR) % 1900;
		pesel += ((cal.get(Calendar.MONTH) + 1) < 10) ? "0" + (cal.get(Calendar.MONTH) + 1)
				: (cal.get(Calendar.MONTH) + 1);
		pesel += (cal.get(Calendar.DAY_OF_MONTH) < 10) ? "0" + cal.get(Calendar.DAY_OF_MONTH)
				: cal.get(Calendar.DAY_OF_MONTH);
		for(int i = 0; i < 3; i++) {
			pesel += (random.nextInt(10));
		}
		int sex = -1;
		switch (gender) {
		case MALE:
			do {
				sex = random.nextInt(10);
			} while (sex > 0 && sex % 2 == 0);
		case FEMALE: 
			do {
				sex = random.nextInt(10);
			} while (sex > 0 && sex % 2 == 1);
		}
		pesel += sex;
		int cn = this.getPESELControlNumber(pesel);
		pesel += cn;
		return pesel;
	}

	private int getPESELControlNumber(String pesel) {
		String[] tmp = pesel.split("");
		int controlNumber = 0;
		for(int i = 0, j = 9, k = 0; i < tmp.length; i++, j--, k++) {
			if(j > k && (j-k) != 5) {
				controlNumber += (Integer.parseInt(tmp[i]) * (j-k)) ;
			} else {
				if(j < k ){
					j = 10;
					k = -1;
				} 
					i--;
			} 
		}
		return controlNumber%10;
	}
	
	private Calendar randomBirthDate() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, (random.nextInt(99) + 1901));
		cal.set(Calendar.MONTH, (random.nextInt(12) + 1));
		int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, this.random.nextInt(daysInMonth));
		return cal;
	}

	private Gender randomGender() {
		Gender gender = null;
		switch (this.random.nextInt(2)) {
		case 0:
			gender = Gender.MALE;
			break;
		case 1:
			gender = Gender.FEMALE;
			break;
		}
		return gender;
	}

	private String[] randomCity() {
		String[] arr = this.randomData("city.txt").split("\t");
		return arr;
	}

	private String randomStreet() {
		return this.randomData("street.txt");
	}

	private int randomNumber() {
		return this.random.nextInt(300);
	}

	private String randomData(String filename) {
		int line = this.random.nextInt(this.countFileLines(this.path + filename) + 1);
		//File f = new File(this.path + filename);
		//InputStream is = getClass().getResourceAsStream(this.path + filename);
		ClassLoader classLoader = getClass().getClassLoader();
		File f = new File(classLoader.getResource(this.path + filename).getFile());
		int i = 1;
		String result = "";
		Scanner scan;
		try {
			scan = new Scanner(f);
	
		while (scan.hasNextLine()) {
			if (i == line) {
				result = scan.nextLine();
				break;
			}
			scan.nextLine();
			i++;
		}
		scan.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	private int countFileLines(String path) {
		//File f = new File(path);
		//InputStream is = getClass().getResourceAsStream(path) ;
		ClassLoader classLoader = getClass().getClassLoader();
		File f = new File(classLoader.getResource(path).getFile());
		int i = 0;
		Scanner file;
		try {
			file = new Scanner(f);
		
		while (file.hasNextLine()) {
			i++;
			file.nextLine();
		}
		file.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return i;
	}

}
