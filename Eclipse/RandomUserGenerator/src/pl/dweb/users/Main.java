package pl.dweb.users;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.Context;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import pl.dweb.users.CreateUser;

public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		this.returnUsers(request, response);

	}
	
	public void doPost(HttpServletRequest request,
	        HttpServletResponse response)   throws ServletException, IOException {
		this.returnUsers(request, response);
	}
	
	private void returnUsers(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType ("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		JSONArray users = new JSONArray();
		JSONObject result = new JSONObject();
		
		CreateUser user = new CreateUser();
		
		int res;
		if(request.getParameter("results") != null) {
			res = Integer.parseInt(request.getParameter("results"));
		} else {
			res = 1;
		}
			for (int i = 0; i < res; i++) {
				users.add(user.newUser().toJSON());
			}
			result.put("results", users);
			out.print(result);
	}
	
}
