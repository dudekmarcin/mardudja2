package pl.dweb.users;

import org.json.simple.JSONObject;

public class Address {
 
	private String street;
	private int number;
	private String city;
	private String postalCode;
	public Address(String street, int number, String city, String postalCode) {
		super();
		this.street = street;
		this.number = number;
		this.city = city;
		this.postalCode = postalCode;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	@Override
	public String toString() {
		return this.street + " " + this.number + ", " + this.postalCode + " " + this.city;
	}
	
	public JSONObject toJSON() {
		JSONObject addr = new JSONObject();
		addr.put("street", this.street);
		addr.put("houseNumber", this.number);
		addr.put("postalCode", this.postalCode);
		addr.put("city", this.city);
		return addr;
	}
}
