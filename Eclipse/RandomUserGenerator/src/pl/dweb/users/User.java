package pl.dweb.users;

import java.util.Calendar;

import org.json.simple.JSONObject;

public class User  {

	// Zaprojektuj strukturę danych dla przykładowego użytkownika. Struktura
	// powinna deklarować pola tj. imię, nazwisko, adres, telefon, numer karty
	// kredytowej, pesel. Przygotuj plik imiona.txt, nazwiska.txt, ulice.txt,
	// które będą zawierały przykładowe dane. Następnie w klasie dostarcz
	// metody, które wygenerują losowe dane, losowego użytkownika. Generator
	// numeru pesel powinien generować pesel, który przedzie walidację (tj.
	// poprawny numer).

	private String name;
	private String lastname;
	private Gender gender;
	private Address address;
	private int phone;
	private String cardNumber;
	private String pesel;
	private Calendar birthDate;
	public User(String name, String lastname, Address address, int phone, String cardNumber, String pesel) {
		this.name = name;
		this.lastname = lastname;
		this.address = address;
		this.phone = phone;
		this.cardNumber = cardNumber;
		this.pesel = pesel;
		this.birthDate = Calendar.getInstance();
		this.gender = (Integer.parseInt(pesel.substring(9,10)) % 2 == 0) ? Gender.FEMALE : Gender.MALE;
		this.birthDate.set(Calendar.YEAR, (Integer.parseInt(pesel.substring(0,2)) + 1900));
		this.birthDate.set(Calendar.MONTH, (Integer.parseInt(pesel.substring(2,4))-1));
		this.birthDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(pesel.substring(4,6)));
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLastname() {
		return lastname;
	}
	
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public int getPhone() {
		return phone;
	}
	
	public void setPhone(int phone) {
		this.phone = phone;
	}
	
	public String getCardNumber() {
		return cardNumber;
	}
	
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	
	public String getPesel() {
		return pesel;
	}
	
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	
	@Override
	public String toString() {
		return this.name + " " + this.lastname + ", PESEL:  " + this.pesel + ", Data urodzenia: " + this.birthDate.get(Calendar.YEAR) + "-" + (this.birthDate.get(Calendar.MONTH) +1 )+ "-" + this.birthDate.get(Calendar.DAY_OF_MONTH) + " , adres: " + this.address + ", nr karty: " + this.cardNumber + ", nr telefonu: " + this.phone;
	}
	
	public JSONObject toJSON() {
		JSONObject user = new JSONObject();
		user.put("name", this.name);
		user.put("lastname", this.lastname);
		user.put("pesel", this.pesel);
		user.put("birthDate", (this.birthDate.get(Calendar.YEAR) + "-" + (this.birthDate.get(Calendar.MONTH) + 1)+ "-" + this.birthDate.get(Calendar.DAY_OF_MONTH)));
		user.put("address", this.address.toJSON());
		user.put("cardNumber", this.cardNumber);
		user.put("phone", this.phone);
		return user;
	}
	
}
