package com.sda;

import java.util.Optional;
import java.util.function.Function;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class OptionMapTest {

    @Test
    public void example() {
        Optional<Integer> theAnswer = Optional.of(42);
        Optional<Integer> squareTheAnswer = this.square(theAnswer);
    }



    /**
     * Uzupelnij metode pomocnicza "getDigitsOption", ktora jako argument bierze
     * Optional<Integer>, zwraca rowniez Optional<Integer> reprezentujacy mozliwa
     * liczbe cyfr wystepujaca w liczbie.
     * tj:
     * Optional.of(123) -> Optional.of(3)
     * Optional.of(0) -> Optional.of(1)
     * Optional.of(-5) -> Optional.of(1)
     * 
     * Nastepnie przetestuj stworzona przez siebie metode.
     */
    @Test
    public void digitsTest() {
        Optional<Integer> minusTen = Optional.of(-10);
        
        Optional<Integer> digitsMinusten = getDigitsOption(minusTen);
        
        assertThat(getDigitsOption(minusTen), is(Optional.of(2)));

        Optional<Integer> zero = Optional.of(0);
        assertThat(getDigitsOption(zero), is(Optional.of(1)));

        Optional<Integer> ten = Optional.of(10);
        assertThat(getDigitsOption(ten), is(Optional.of(2)));

        Optional<Integer> nullableOption = Optional.ofNullable(null);
        assertThat(getDigitsOption(nullableOption), is(Optional.empty()));
    }

    private Optional<Integer> getDigitsOption(Optional<Integer> opt) {
        Function<Integer, Integer> getDigitsLambda = value -> {
          Integer absValue = Math.abs(value);
          String stringValue = absValue.toString();
          int valueResult = stringValue.length();
          return valueResult;
        };
        return intMap(opt, getDigitsLambda);
    }

    private Optional<Integer> square(Optional<Integer> opt) {
        return intMap(opt, v -> v * v);
    }

    /**
     * Spojrz na metody #getDigitsOption oraz #square. So do siebie podobne:
     * Kazda z nich powinna sprawdzic czy wartosc opcji istnieje, jesli tak
     * powina zwrocic nowa opcje zawierajaca wynik pewnej operacji (hint: lambdy/funkcji)
     * (dla square - kwadratu liczby, dla getDigitsOption - liczby cyfr),
     * jezeli nie pusta opcje. Sprobuj uzupenic metode intMap, w taki sposob
     * by przy jej pomocy mozna bylo zapisac #getDigitsOption oraz #square
     *
     * Nastepnie przy pomocy testow sprawdz, czy metody intMap (wraz z odpowiednia lambda)
     * oraz square i getDigitsOption zwracaja zawsze te same wyniki.
     */
    @Test
    public void intMapTest() {
        
    }

    private Optional<Integer> intMap(Optional<Integer> option, Function<Integer, Integer> func) {
        Optional<Integer> result;
        if (option.isPresent()) {
            int value = option.get();
            //start
            int valueResult = func.apply(value);
            //koniec
            result = Optional.of(valueResult);
        } else {
            result = Optional.empty();
        }
        return result;
    }

    /**
     * Spojrz na stringLengthOption. Ma ona z przekazanej opcji Stringa zwracac opcje
     * reprezentujaca dlugosc tego Stringa. Sprobuj ja uzupelnic oraz napisac test.
     */
    @Test
    public void stringLengthOptionTest() {
        
    }

    private Optional<Integer> stringLengthOption(Optional<String> opt) {
        // TODO: uzupelnij.
        return null;
    }

    /**
     * Okej teraz czas wysylic sporo szarych komorek. Przypomnij sobie interface QuadraFunction i
     * TriFunction z poprzenich zajec. Jak pamietasz dzieki tym interfaceom moglismy stworzyc lambde
     * dla dowolnych typow.
     * 
     * Teraz poprzez zebrana wiedze z poprzednich zadan oraz powyzszego faktu sprobuj stworzyc metode
     * #ourMap, ktora rowniez bedzie generyczna, tj:
     *  - Jako pierwszy argument bedzie brala opcje dowolnego typu
     *  - Jako drugi argument bedzie brala lambde (Function), ktora przyjmuje arugment typu pierwszego
     *    argumentu, z zwraca typ wyniku.
     *  - Jako wynik bedzie zwracaja opcje dowolnego typu
     * 
     * Jest to zadanie trudniejsze i moga pojawiac sie problemu - zarowno ze strony IDE/kompilatora jak
     * czysto koncepcyjne. Pytajcie.
     */
    @Test
    public void testOurMap() {
        
    }
    
    private <T, R> Optional<R> ourMap(Optional<T> opt, Function<T, R> function) {
        Optional<R> result;
        if (opt.isPresent()) {
            T value = opt.get();
            R resultValue = function.apply(value);
            result = Optional.of(resultValue);
        } else {
            result = Optional.empty();
        }
        return result;
    }
}
