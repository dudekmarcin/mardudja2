package com.sda;

import java.util.Optional;
import org.junit.Test;

public class FlatMapTest {

    /**
     * Oczywiscie nie trzeba takiej mapy tworzyc za kazdym razem :)
     */
    @Test
    public void mapExample() {
        Optional<Integer> theAnswer = Optional.of(42);
        Optional<Integer> eighteenAnswers = theAnswer.map(i -> i * 18);

        Optional<Integer> chainOfPerations =
                        theAnswer.map(i -> i * i)
                        .map(i -> Math.sqrt(i))
                        .map(i -> i.toString())
                        .map(s -> s.length());
    }

    /**
     * Mapa ma natomiast pewne smutne ograniczenia :(.
     * Sprobuj przy pomocy {@link Optional#map(java.util.function.Function)}
     * zapisac nastepujaca operacje - Jesli w opcji znajduje sie liczba parzysta
     * zwroc jej kwadrat, jezeli nieparzysta nie zwracaj nic konczac obliczenia.
     * Jest natomiast jeden haczyk - opcja ma na celu eliminacje nulla, wiec i Ty
     * nie mozesz jego uzywac.
     */
    @Test
    public void flatMapIntro() {
    }

    /**
     * Tutaj stworzymy wlasna implementacje flatMapy :)
     */

    /**
     * Tutaj napiszesz implementacje filtrowania przy pomocy naszej flatMapy :)
     */

    /**
     * Tutaj przy pomocy naszej flatMapy stworzysz mape! :)
     */
}
