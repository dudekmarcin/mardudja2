package com.sda;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.junit.Test;

public class BasicOptionalOperationsTest {

    @Test
    public void constructor() {
        Optional<Integer> optional = Optional.of(10);
        System.out.println(optional);
        System.out.println(optional.get());

//         optional = Optional.of(null);
//         System.out.println(optional);
//         System.out.println(optional.get());

        Optional<String> nullableOption = Optional.ofNullable("Hello");
        System.out.println("no: " + nullableOption);
        System.out.println("no: " + nullableOption.get());
//
        nullableOption = Optional.ofNullable(null);
        System.out.println("no: " + nullableOption);
        System.out.println("no: " + nullableOption.get());
    }

    @Test
    public void basics() {
        Optional<String> maybeJohnny = Optional.of("Johnny Cash");
//        maybeJohnny.ifPresent(johnny -> System.out.println(johnny));

        Optional<String> maybeJimmy = Optional.ofNullable(null);
//        maybeJimmy.ifPresent(jimmy -> System.out.println(jimmy));
//
        if (maybeJohnny.isPresent()) {
            System.out.println("I am present!");
        }
//
        if (maybeJimmy.isPresent()) {
            System.out.println("Jimmy is also present!");
        } else {
            System.out.println("Jimmy is not here!");
        }
//
        String jimmyValue = maybeJohnny.orElse("Nie mam imienia");
        System.out.println(jimmyValue);
//        
        String anotherJimmyValue = maybeJimmy.orElseGet(() -> "W lambdzie tez nie mam imienia");
        System.out.println(anotherJimmyValue);
//
        try {
            String yetAnotherJimmyValue = maybeJimmy.orElseThrow(() -> new IllegalStateException("Blad! brak imienia"));
        } catch (IllegalStateException ise) {
            System.out.println(ise);
        }
//        
//maybeJohnny.filter(name -> name.equals("hhgfhgfxhdh"));
  //      System.out.println(maybeJohnny);

        Optional<String> filteredJohnny =
            maybeJohnny.filter(name -> name.contains("Johnny"));
        System.out.println(filteredJohnny);
        Optional<String> notJohnny =
            maybeJohnny.filter(name -> !name.contains("Johnny"));
        System.out.println(notJohnny);
//
//        //regular way to do
        maybeJohnny
            .filter(name -> name.contains("Johnny"))
            .ifPresent(name -> System.out.println(name));
    }

    /**
     * Wykonaj polecenia i napisz testy - o ile to mozliwe.
     */
    @Test
    public void basicExercises() {
        /**
         * Stworz opcje StartingPoint, ktora bedzie mogla przechowywac wartosci typu Integer.
         * A nastepnie na bazie tej opcji:
         *  - Stworz opcje, ktora przechowuje wynik predykatu sprawdzajacego,
         *     czy wartosc w StartingPoint jest parzysta.
         *  - Stworz opcje, ktora - jesli w StartingPoint jest przechowywana wartosc - wypisze 
         *   do System.out Set posiadajacy wszystkie jej dzielniki naturalne.
         *  - Do zmiennej endPoint przypisz wartosc przechowywana w StartingPoint. Jesli w starting point
         *  nie ma zadnej wartosci powinna przypisywac wartosc odpowiedzi na wszystko - liczbe 42.
         */
        Optional<Integer> startingPoint = Optional.of(213);
        Optional<Integer> isEvenStartingPoint = startingPoint.filter(value -> value % 2 == 0);
        System.out.println(isEvenStartingPoint);

        startingPoint.ifPresent(value -> {
            Set<Integer> dividors = new HashSet<Integer>();
            for (int i = 1; i <= value; i++) {
                if (value % i == 0) {
                    dividors.add(i);
                }
            }
            System.out.println(dividors);
        });

        int endPoint = startingPoint.orElse(42);
    }

}
