package com.travelerservice.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class TravelService {

	Map<String, List<TravelOffer>> offers;

	public TravelService() {
		offers = new TreeMap<>();
	}

	public void addOffer(TravelOffer offer) {
		if (this.offers.containsKey(offer.getCountry())) {
			List<TravelOffer> offerList = this.offers.get(offer.getCountry());
			offerList.add(offer);
			this.offers.put(offer.getCountry(), offerList);
		} else {
			List<TravelOffer> offerList = new ArrayList<>();
			offerList.add(offer);
			this.offers.put(offer.getCountry(), offerList);
		}
	}

	public TravelResponse getProposal(TravelRequest request) {
		TravelPreferences pref = request.getPreferences();
		List<TravelOffer> proposalOffers = this.offers.get(pref.getCountry());
		proposalOffers = proposalOffers.stream()
				.filter(o -> (o.getCost() >= pref.getMinCost() && o.getCost() <= pref.getMaxCost())
						&& (o.getTemp() >= pref.getMinTemp() && o.getTemp() <= pref.getMaxTemp())
						&& (o.getAttraction() >= pref.getMinAtr() && o.getAttraction() <= pref.getMaxAtr()))
				.collect(Collectors.toList());
		int toIndex = (proposalOffers.size() > (request.getPageNumber() + 1) * request.getOffersPerPage())
						? (request.getPageNumber() + 1) * request.getOffersPerPage()
						: proposalOffers.size();
						
		int pages = (proposalOffers.size() % request.getOffersPerPage() != 0 ) 
						? (proposalOffers.size() / request.getOffersPerPage()) + 1 
						: proposalOffers.size() / request.getOffersPerPage();
		
		return new TravelResponse(
				proposalOffers
						.subList(request.getPageNumber() * request.getOffersPerPage(), toIndex),
							request.getOffersPerPage(), request.getPageNumber(), pages);

	}

	// - serwis powinien pozwalać na sortowanie ofert i wyświetlanie stronnicowe
	// - serwis powinien udostępniać możliwość filtrowania na podstawie
	// możliwości finansowych lub preferencji użytkownika
	//
	//
	// Budowa klas:
	//
	// TravelService przyjmuje zgłoszenia.
	// Preferencje są obiektami klasy TravelRequest
	// Dostępne oferty są obiektami klasy TravelPorposals
	//
	// Request ma kilka pól dotyczących informacji filtrowania;
	// Porposals posiada listę wycieczek które spełniają nasze wymagania oraz
	// zwraca request wysłany przez klienta w odpowiedzi

}
