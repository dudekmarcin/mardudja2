package com.travelerservice.app;

import java.util.List;

public class TravelResponse {
	
	private List<TravelOffer> offers;
	private int offersPerPage;
	private int pageNumber;
	private int maxPage;
	public TravelResponse(List<TravelOffer> offers, int offersPerPage, int pageNumber, int maxPage) {
		this.offers = offers;
		this.offersPerPage = offersPerPage;
		this.pageNumber = pageNumber;
		this.maxPage = maxPage;
	}
	public List<TravelOffer> getOffers() {
		return offers;
	}
	public void setOffers(List<TravelOffer> offers) {
		this.offers = offers;
	}
	public int getOffersPerPage() {
		return offersPerPage;
	}
	public void setOffersPerPage(int offersPerPage) {
		this.offersPerPage = offersPerPage;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	public int getMaxPage() {
		return this.maxPage;
	}
	public void setMaxPage(int max) {
		this.maxPage = max;
	}
	@Override
	public String toString() {
		return "TravelResponse offers=" + offers + ", offersPerPage=" + offersPerPage + ", pageNumber=" + pageNumber
				+ "]";
	}
	
	
	
	
}
