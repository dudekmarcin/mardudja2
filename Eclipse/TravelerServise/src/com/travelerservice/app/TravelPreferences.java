package com.travelerservice.app;

public class TravelPreferences {

	private double minCost;
	private double maxCost;
	private String country;
	private double minTemp;
	private double maxTemp;
	private int minAtr;
	private int maxAtr;
	public TravelPreferences(double minCost, double maxCost, String country, double minTemp, double maxTemp, int minAtr,
			int maxAtr) {
		this.minCost = minCost;
		this.maxCost = maxCost;
		this.country = country;
		this.minTemp = minTemp;
		this.maxTemp = maxTemp;
		this.minAtr = minAtr;
		this.maxAtr = maxAtr;
	}
	public double getMinCost() {
		return minCost;
	}
	public void setMinCost(double minCost) {
		this.minCost = minCost;
	}
	public double getMaxCost() {
		return maxCost;
	}
	public void setMaxCost(double maxCost) {
		this.maxCost = maxCost;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public double getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(double minTemp) {
		this.minTemp = minTemp;
	}
	public double getMaxTemp() {
		return maxTemp;
	}
	public void setMaxTemp(double maxTemp) {
		this.maxTemp = maxTemp;
	}
	public int getMinAtr() {
		return minAtr;
	}
	public void setMinAtr(int minAtr) {
		this.minAtr = minAtr;
	}
	public int getMaxAtr() {
		return maxAtr;
	}
	public void setMaxAtr(int maxAtr) {
		this.maxAtr = maxAtr;
	}
	@Override
	public String toString() {
		return "TravelPreferences [minCost=" + minCost + ", maxCost=" + maxCost + ", country=" + country + ", minTemp="
				+ minTemp + ", maxTemp=" + maxTemp + ", minAtr=" + minAtr + ", maxAtr=" + maxAtr + "]";
	}
	
	
	
	
}
