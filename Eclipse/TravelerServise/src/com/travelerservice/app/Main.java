package com.travelerservice.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		TravelService service = new TravelService();
		List<TravelOffer> listOffer = new ArrayList<>();
		for(int i = 1; i < 100; i++) {
			listOffer.add(new TravelOffer("Polska", "Gdańsk"+i, 18, 999, 10));
			listOffer.add(new TravelOffer("Polska", "Gdynia"+i, 16, 1050.99, 8));
			listOffer.add(new TravelOffer("Polska", "Zakopane"+i, 10, 1999, 12));
			listOffer.add(new TravelOffer("Polska", "Poznań"+i, 14, 899, 9));
			listOffer.add(new TravelOffer("Polska", "Warszawa"+i, 15, 1999, 11));
			listOffer.add(new TravelOffer("Polska", "Wrocław"+i, 13, 1599, 20));
			listOffer.add(new TravelOffer("Polska", "Toruń"+i, 14, 1250.99, 10));
			listOffer.add(new TravelOffer("Polska", "Kraków"+i, 12, 999, 10));
			listOffer.add(new TravelOffer("Polska", "Sosnowiec"+i, 8, 99.99, 0));
		}
		
		
		listOffer.forEach(o -> service.addOffer(o));

		Scanner scan = new Scanner(System.in);
		System.out.println("Podaj kraj, do którego chcesz jechać: ");
		String country = scan.nextLine().toLowerCase();
		System.out.println("Podaj minimalną cenę: ");
		double minCost = scan.nextDouble();
		System.out.println("Podaj maksymalną cenę: ");
		double maxCost = scan.nextDouble();
		System.out.println("Podaj minimalną temperaturę: ");
		double minTemp = scan.nextDouble();
		System.out.println("Podaj maksymalną temperaturę: ");
		double maxTemp = scan.nextDouble();
		System.out.println("Podaj minimalną liczbę atrakcji: ");
		int minAtr = scan.nextInt();
		System.out.println("Podaj maksymalną liczbę atrakcji: ");
		int maxAtr = scan.nextInt();
		System.out.println("Ile wyników wyświetlić na jednej stronie?");
		int offertsPerPage = scan.nextInt();
		int i = 0;
		TravelRequest request = new TravelRequest(offertsPerPage, i, new TravelPreferences(minCost, maxCost, country, minTemp, maxTemp, minAtr, maxAtr));
		
		TravelResponse response = service.getProposal(request);
		do {
			if(i == -1) {
				break;
			} else if(i >= response.getMaxPage()) {
				System.out.println("Nie ma tylu stron. Jeszcze raz.");
				continue;
			}
			request = new TravelRequest(offertsPerPage, i, new TravelPreferences(minCost, maxCost, country, minTemp, maxTemp, minAtr, maxAtr));

			response = service.getProposal(request);
			response.getOffers().forEach(o -> System.out.println(o));
			System.out.println("-----");
			
			
			System.out.println("Strona " + i + "/" + response.getMaxPage() + " Podaj stronę, do której chcesz przejść lub -1 aby wyjść: ");
			i = scan.nextInt();
		} while (response.getMaxPage() > i);
		
		scan.close();
	}

}
