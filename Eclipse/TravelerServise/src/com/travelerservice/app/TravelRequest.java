package com.travelerservice.app;

public class TravelRequest {

	private int offersPerPage;
	private int pageNumber;
	private TravelPreferences preferences;
	public TravelRequest(int offersPerPage, int pageNumber, TravelPreferences preferences) {
		this.offersPerPage = offersPerPage;
		this.pageNumber = pageNumber;
		this.preferences = preferences;
	}
	public int getOffersPerPage() {
		return offersPerPage;
	}
	public void setOffersPerPage(int offersPerPage) {
		this.offersPerPage = offersPerPage;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public TravelPreferences getPreferences() {
		return preferences;
	}
	public void setPreferences(TravelPreferences preferences) {
		this.preferences = preferences;
	}
	@Override
	public String toString() {
		return "TravelRequest [offersPerPage=" + offersPerPage + ", pageNumber=" + pageNumber + ", preferences="
				+ preferences + "]";
	}
	
	
	
}
