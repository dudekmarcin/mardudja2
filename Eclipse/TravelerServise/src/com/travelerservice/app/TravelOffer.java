package com.travelerservice.app;

public class TravelOffer {

	private String country;
	private String city;
	private double temp;
	private double cost;
	private int attraction;
	
	public TravelOffer(String country, String city, double temp, double cost, int atr) {
		this.country = country.toLowerCase().trim();
		this.city = city.toLowerCase().trim();
		this.temp = temp;
		this.cost = cost;
		this.attraction = atr;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public double getTemp() {
		return temp;
	}

	public void setTemp(float temp) {
		this.temp = temp;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public int getAttraction() {
		return attraction;
	}

	public void setAttraction(int attraction) {
		this.attraction = attraction;
	}
	
	@Override
	public String toString() {
		return this.country + " - " + this.city + ", cena: " + this.cost + " [śr temp: " + this.temp + ", ilość atrakcji: " + this.attraction + "]";
	}
	
}
