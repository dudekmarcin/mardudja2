package main;

public class Patient {

	public String name;
	public String surname;
	
	public Patient(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}
	
	@Override
	public String toString() {
		return name + " " + surname;
	}
	
}
