package main;

import java.util.List;

public class MyLinkedList {

	public ListElement first;

	public MyLinkedList() {
		this.first = null;
	}

	public boolean isEmpty() {
		return first == null;
	}

	public void AddElementToList(ListElement element) {
		if (this.isEmpty()) {
			first = element;
		} else {
			ListElement temp = first;
			while (temp.next != null) {
				temp = temp.next;
			}

			temp.next = element;
			element.previous = temp;
		}
	}

	public int getSize() {
		int result = 0;
		if (!isEmpty()) {
			result++;
			ListElement temp = first;
			while (temp.next != null) {
				temp = temp.next;
				result++;
			}
		}

		return result;
	}

	public void removeElement() {
		if (!isEmpty()) {
			if (first.next == null) {
				first = null;
				return;
			}

			ListElement temp = first;
			while (temp.next != null) {
				temp = temp.next;
			}
			if (temp.previous != null) {
				temp = temp.previous;
				temp.next = null;
			}

		} else {
			System.out.println("List is empty.");
		}
	}
	
	public void clear() {
		first = null;
	}
	
	public void removeElement(int element) {
		if(getSize() > element) {
			
			if(element == 0) {
				first = first.next;
				return;
			}
			
			if(element == getSize()-1) {
				this.removeElement();
				return;
			}
			
			int i = 0;
			ListElement temp = first;
			while(i != element) {
				temp = temp.next;
				i++;
			}
			
			temp.previous.next = temp.next;
			temp.next.previous = temp.previous;
			
//		ListElement tempNext = temp.next;
//		tempNext.previous = temp.previous;
//		temp = temp.previous;
//		temp.next = tempNext;
		
		} else
			System.out.println("There is not enough elements in our list.");
	}

	public void PrintOutList() {
		if(!isEmpty()) {
			ListElement temp = first;
			System.out.println(temp);

			while(temp.next != null) {
				temp = temp.next;
				System.out.println(temp);
			}
		}
	}
	
}
