package main;

public class ListElement {

	public Object value;
	public ListElement next;
	public ListElement previous;
	
	public ListElement(Object givenValue) {
		this.next = null;
		this.previous = null;
		this.value = givenValue;
	}
	
	@Override
	public String toString() {
		return this.value.toString();
	}
	
}
