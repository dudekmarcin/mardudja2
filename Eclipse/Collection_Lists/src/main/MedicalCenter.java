package main;

import java.util.ArrayList;
import java.util.List;

public class MedicalCenter {

	public List<Patient> listOfPatients;
	
	public MedicalCenter() {
		this.listOfPatients = new ArrayList<>();
	}
	
	public void registerPatient(Patient toRegister) {
		this.listOfPatients.add(toRegister);
	}
	
	public void curePatient() {
		System.out.println("Patient cured: "+ this.listOfPatients.get(0));
		this.listOfPatients.remove(0);
	}
	
	public static void main(String[] arr) {
		MedicalCenter center = new MedicalCenter();
		center.registerPatient(new Patient("Pawel", "Gawel"));
		center.registerPatient(new Patient("Jan", "Kowalski"));
		center.registerPatient(new Patient("Tadeusz", "Nowak"));
		center.registerPatient(new Patient("Marian", "Mocny"));
		
		center.curePatient();
		


	}
}
