package main;

public class Main {

	public static void main(String[] args) {
		
		MyLinkedList list = new MyLinkedList();
		
		ListElement el1 = new ListElement(1);
		ListElement el2 = new ListElement(2);
		ListElement el3 = new ListElement(3);
		ListElement el4 = new ListElement(4);
		ListElement el5 = new ListElement(5);
		ListElement el6 = new ListElement(6);

		
		list.AddElementToList(el1);
		list.AddElementToList(el2);
		list.AddElementToList(el3);
		list.AddElementToList(el4);
		list.AddElementToList(el5);
		list.AddElementToList(el6);

		
		System.out.println(list.getSize());
		System.out.println("------------");
		list.PrintOutList();
		System.out.println("------------");
		list.removeElement(3);
		list.PrintOutList();


	}

}
