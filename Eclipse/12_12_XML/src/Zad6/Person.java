package Zad6;

public class Person {

	private String name;
	private String lastname;
	private String company;
	private Double salary;
	private String department;
	private Integer yearOfBorn;

	public Person() {
		this.name = "";
		this.lastname = "";
		this.company ="";
		this.salary = 0.0;
		this.department = "";
		this.yearOfBorn = 0;
	}
	
	public Person(String name, String lastname, String company, Double salary, String department, Integer yearOfBorn) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.company = company;
		this.salary = salary;
		this.department = department;
		this.yearOfBorn = yearOfBorn;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public Integer getYearOfBorn() {
		return yearOfBorn;
	}
	public void setYearOfBorn(Integer yearOfBorn) {
		this.yearOfBorn = yearOfBorn;
	}
	
	
}
