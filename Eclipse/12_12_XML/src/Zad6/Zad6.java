package Zad6;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Zad6 {
	//
	// Utwórz klasę Person, która będzie zawierała następująca pola prywatne,
	// gettery,
	// settery oraz konstruktory:
	// -name:String
	// -lastname:String
	// -company:String
	// -salary:double
	// -department:String
	// -yearOfBorn:int
	// Następnie, dodaj przykładowych 5 instancji tego obiektu do listy
	// (ArrayList) i dostarcz metodę:
	// +savePeople(ArrayList<Person> people):void
	// która zapisze dane użytkowników do pliku XML pod nazwą people.xml


	private String path;
	private String name;

	public Zad6() {
		this.path = "src//resources//";
		this.name = "people.xml";
	}

	public Zad6(String path, String name) {
		this.path = path;
		this.name = name;
	}

	public void savePeople(ArrayList<Person> people) throws TransformerException, ParserConfigurationException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();

		Element root = doc.createElement("ROOT");
		Element person;
		Element name;
		Element lastname;
		Element company;
		Element salary;
		Element department;
		Element yearOfBirth;

		for (Person p : people) {
			
			person = doc.createElement("PERSON");
			name = doc.createElement("NAME");
			lastname = doc.createElement("LASTNAME");
			company = doc.createElement("COMPANY");
			salary = doc.createElement("SALARY");
			department = doc.createElement("DEPARTMENT");
			yearOfBirth = doc.createElement("YEAR_OF_BIRTH");

			name.appendChild(doc.createTextNode(p.getName()));
			lastname.appendChild(doc.createTextNode(p.getLastname()));
			company.appendChild(doc.createTextNode(p.getCompany()));
			salary.appendChild(doc.createTextNode(p.getSalary().toString()));
			department.appendChild(doc.createTextNode(p.getDepartment()));
			yearOfBirth.appendChild(doc.createTextNode(p.getYearOfBorn().toString()));
			
			person.appendChild(name);
			person.appendChild(lastname);
			person.appendChild(company);
			person.appendChild(salary);
			person.appendChild(department);
			person.appendChild(yearOfBirth);
			root.appendChild(person);

		}

		doc.appendChild(root);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + this.name));
		t.transform(source, sr);

	}
}
