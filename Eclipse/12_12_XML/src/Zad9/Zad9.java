package Zad9;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Zad9 {
//	Napisz program, który sprawdzi, czy w pliku XML (staff.xml) nie występują elementy o takim samym
//	ID. Jeżeli występują, program powinien utworzyć kopię pliku staff_copy.xml, a w nim zapisać
//	niepowtarzające się elementy. Przyjmij, że tylko pierwsze napotkane unikalne ID jest poprawne i je
//	zapisuj do pliku kopii. Resztę duplikatów pomiń.
	
	private String path;
	private String name;
	
	public Zad9() {
		this.path = "src//resources//";
		this.name = "staff.xml";
	}
	
	public Zad9(String path, String name) {
		this.path = path;
		this.name = name;
	}
	
	public void removeDuplicates() throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException {
		File f = new File(this.path + this.name);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		
		NodeList nodeList = doc.getElementsByTagName("root");
		
		List<Integer> id = new ArrayList<>();
		List<Node> elements = new ArrayList<>();
		boolean isDuplicate = false;
		Node node = nodeList.item(0).getFirstChild();
		
		for(int i =0; i < nodeList.item(0).getChildNodes().getLength(); i++) {

			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				if(!id.contains(Integer.parseInt(element.getAttribute("id")))) {
					id.add(Integer.parseInt(element.getAttribute("id")));
					elements.add(element);
				} else {
					isDuplicate = true;
				}
			}
			node = node.getNextSibling();
			
		}
		
		if(isDuplicate) {
			try {
				this.createXML(elements);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void createXML(List<Node> elements) throws ParserConfigurationException, TransformerException, SAXException, IOException {
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
	
		Element root = doc.createElement("root");
		
		for(Node el : elements) {
			doc.adoptNode(el);
			root.appendChild(el);
		}
	
		doc.appendChild(root);

		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + "staff_copy.xml"));
		t.transform(source, sr);
	}
	
}
