package Zad5;

public class Company {

	private String name;
	private Integer starts;
	private Integer employees;
	private Integer vat;
	
	
	
	public Company(String name, Integer starts, Integer employees, Integer vat) {
		super();
		this.name = name;
		this.starts = starts;
		this.employees = employees;
		this.vat = vat;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStarts() {
		return starts;
	}
	public void setStarts(Integer starts) {
		this.starts = starts;
	}
	public Integer getEmployees() {
		return employees;
	}
	public void setEmployees(Integer employees) {
		this.employees = employees;
	}
	public Integer getVat() {
		return vat;
	}
	public void setVat(Integer vat) {
		this.vat = vat;
	}
	
	
	
	
}
