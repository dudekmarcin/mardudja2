package Zad5;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;


public class Zad5 {
//	Napisz metodę, która pozwoli na zapisanie pliku XML według następującej struktury:
//		ROOT
//			COMPANY(name="Testowa")
//				STARTS: 2008
//				EMPLOYEES: 345
//				VAT: 23
//			COMPANY(name="Testowa 2")
//				STARTS: 1979
//				EMPLOYEES: 34345
//				VAT: 40
//			COMPANY(name="Testowa 3")
//				STARTS: 1999
//				EMPLOYEES: 5
//				VAT: 8
//		Plik zapisz w pakiecie resources pod nazwą companies.xml
	
		
private List<Company> companyList;
	
	private String path;
	private String name;
	
	public Zad5() {
		this.path = "src//resources//";
		this.name = "companies.xml";
		this.companyList  = new ArrayList<>();
	}

	public Zad5(String path, String name) {
		this.path = path;
		this.name = name;
		this.companyList  = new ArrayList<>();
	}
	
	public void writeToXML() throws ParserConfigurationException, SAXException, IOException, TransformerException {
		
		companyList.add(new Company("Testowa", 2008, 345, 23));
		companyList.add(new Company("Testowa 2", 1979, 34345, 40));
		companyList.add(new Company("Testowa 3", 1999, 5, 8));
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element root = doc.createElement("ROOT");
		Element company;
		Element starts;
		Element employees;
		Element vat;
		
		
		for(int i = 0; i < this.companyList.size(); i++) {
			company = doc.createElement("COMPANY");
			starts = doc.createElement("STARTS");
			employees = doc.createElement("EMPLOYEES");
			vat = doc.createElement("VAT");
			
			company.setAttribute("name", this.companyList.get(i).getName());
			
			starts.appendChild(doc.createTextNode(this.companyList.get(i).getStarts().toString()));
			employees.appendChild(doc.createTextNode(this.companyList.get(i).getEmployees().toString()));
			vat.appendChild(doc.createTextNode(this.companyList.get(i).getVat().toString()));

			company.appendChild(starts);
			company.appendChild(employees);
			company.appendChild(vat);
			root.appendChild(company);

			
		}

			doc.appendChild(root);


		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + this.name));
		t.transform(source, sr);
		
		
		
	}
	
}
