package Zad10;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Zad10 {

//	Napisz program, który pozwoli na realizację odczytu pliku XML. Następnie dostarcz metodę, która do
//		drugiego pliku XML przepisze tylko te tagi, które zawierają w nazwie literę 'a'.
	
	//jeszcze nie działa :(

	
	private String path;
	public String name;
	
	public Zad10() {
		this.path = "src//resources//";
		this.name = "companies.xml";
	}
	
	public Zad10(String path, String name) {
		this.path = path;
		this.name = (name.length() > 4 && name.substring(name.length()-4, name.length()).equals(".xml")) ? name : name + ".xml";
	}
	
	
	public void run() {
		Document doc;
		try {
			doc = this.readXML();
			NodeList listRoot = doc.getFirstChild().getChildNodes();
			this.childNodeList(listRoot);
			this.createXML(doc);
		} catch (TransformerConfigurationException | ParserConfigurationException | SAXException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Document readXML() throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException {
		File f = new File(this.path + this.name);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		
		return doc;	
	}
	
	private NodeList childNodeList(NodeList parent) {
		
		for(int i = 0; i < parent.getLength(); i++) {
			if(parent.item(i).hasChildNodes()) {
				this.childNodeList(parent.item(i).getChildNodes());
			
			} else {
				if(!parent.item(i).getNodeName().contains("a"));
				
				parent.item(i).getParentNode().removeChild(parent.item(i));
			}

		}

		
		return null;
	
	
		
	}
	
private void createXML(Document doc) throws ParserConfigurationException, TransformerException, SAXException, IOException {
				
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + "staff_copy.xml"));
		t.transform(source, sr);
	}
	
}
