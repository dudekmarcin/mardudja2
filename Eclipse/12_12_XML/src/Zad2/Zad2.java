package Zad2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Zad2 {
//	Napisz metodę, która odczyta plik XMl, sparsuje go, a następnie zwróci listę imion i nazwisk. Wykorzystaj plik staff.xml:
//		+getNames():ArrayList<String>
//		W metodzie main() wypisz do konsoli wszystkie odczytane imiona i nazwiska.
	
	private String path = "src//resources//";
	
		
	public List<String> getNames() throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + "staff.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		
		NodeList nodeList = doc.getElementsByTagName("staff");
		
		List<String> names = new ArrayList<>();
		
		for(int i =0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				names.add((element.getElementsByTagName("firstname").item(0).getTextContent()) + " " + (element.getElementsByTagName("lastname").item(0).getTextContent()));
			}
		}
	
		return names;
		
	}
	
}
