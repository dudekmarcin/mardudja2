package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import Zad10.Zad10;
import Zad2.Zad2;
import Zad3.Zad3;
import Zad4.Zad4;
import Zad5.Zad5;
import Zad6.Person;
import Zad6.Zad6;
import Zad7.Zad7;
import Zad8.Zad8;
import Zad9.Zad9;

public class Main {
	public static void main(String[] args) {
//		XMLExercise xml = new XMLExercise();
//		try {
//			xml.readXMLFile("staff.xml");
//			xml.writeXMLFile("staff2.xml");
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
		
//		Zad1 z1 = new Zad1();
//		System.out.println(z1.getAvgSalary());
		
//		Zad2 z2 = new Zad2();
//		try {
//			List<String> names = z2.getNames();
//			names.forEach(s -> System.out.println(s));
//		} catch (ParserConfigurationException | SAXException | IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		Zad3 z3 = new Zad3();
//		try {
//			z3.getMinMaxStaffID();
//		} catch (ParserConfigurationException | SAXException | IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		Zad4 z4 = new Zad4();
//		try {
//			z4.writeToXML();
//		} catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		Zad5 z5 = new Zad5();
//		try {
//			z5.writeToXML();
//		} catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		Zad6 z6 = new Zad6();
//		ArrayList<Person> personList = new ArrayList<>();
//		
//		personList.add(new Person("Adam", "Kowalski", "Google", 5000.0, "Marketing", 1980));
//		personList.add(new Person("Jan", "Nowak", "Microsoft", 9000.0, "Nowe Technologie", 1978));
//		personList.add(new Person("Janusz", "Komorowski", "Apple", 10000.0, "Przejściówki", 1982));
//		personList.add(new Person("Włodek", "Markowicz", "youTube", 6500.0, "Łączenie kropek", 1980));
//		personList.add(new Person("Andrzej", "Duda", "Internet", 15000.0, "Memy", 1972));
//
//		try {
//			z6.savePeople(personList);
//		} catch (TransformerException | ParserConfigurationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		Zad7 z7 = new Zad7();
//		ArrayList<Integer> numbers = new ArrayList<>();
//		numbers.add(19);
//		numbers.add(73);
//		numbers.add(54);
//		try {
//			System.out.println(z7.countAverage("file", 4, 6));
//			z7.saveXMLFile("file_7", numbers);
//		} catch (ParserConfigurationException | SAXException | IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		Zad8 z8 = new Zad8();
//		try {
//			z8.csvToXML("students.csv", "csvToXml.xml");
//		} catch (ParserConfigurationException | TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		Zad9 z9 = new Zad9();
//		try {
//			z9.removeDuplicates();
//		} catch (ParserConfigurationException | SAXException | IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		Zad10 z10 = new Zad10();
		z10.run();
		
		
	}
}
