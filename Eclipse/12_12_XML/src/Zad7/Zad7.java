package Zad7;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Zad7 {
//	Przygotuj pliki XML oraz napisz program, który zliczy statystyki dla kilku plików XML. Przykładowo
//	metoda:
//	+countAverage(String filename, int a, int b):double
//	zliczy średnią arytmetyczną z plików plik_[a].xml ... plik_[b].xml, gdzie a i b to liczby
//	nieujemne całkowite i a < b.Pliki powinny mieć strukturę:
//	<root>
//	<date>2016-12-12</date>
//	<values>
//	<val>123</val>
//	<val>523</val>
//	<val>53</val>
//	<val>134</val>
//	<val>1563</val>
//	<val>973</val>
//	<val>7673</val>
//	</values>
//	</root>
//	Dostarcz także metodę, do zapisu plików według przekazanych na liście wartości, np.
//	+saveXMLFile(String filename, ArrayList<Integer>);

	private String path;
	
	public Zad7() {
		this.path = "src//resources//";
	}
	
	public Zad7(String path) {
		this.path = path;
	}

	private double countAveragesSum(String filename, int a, int b) throws ParserConfigurationException, SAXException, IOException {
		
		double avg = 0.0;
		File f = new File(this.path + filename + "_" + a + ".xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		doc.getDocumentElement().normalize();
		
		NodeList nl = doc.getElementsByTagName("val");

		for(int i =0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				avg += Double.parseDouble(element.getTextContent());
			}
			
		}
		
		avg /= nl.getLength();
		if(b > a) {
			avg += countAveragesSum(filename, a+1, b);
		}
		
		return avg;
	}

	public double countAverage(String filename, int a, int b) throws ParserConfigurationException, SAXException, IOException {
		double avg = 0.0;
		avg += countAveragesSum(filename, a, b);
		
		return avg/(b+1-a);
	}
	
	public void saveXMLFile(String filename, ArrayList<Integer> numbers) throws TransformerException, ParserConfigurationException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();

		Element root = doc.createElement("root");
		Element date = doc.createElement("date");
		Element values = doc.createElement("values");
		Element val;
		
		Calendar cal = Calendar.getInstance();
		
		
		date.appendChild(doc.createTextNode(cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH)+1) + "-" + cal.get(Calendar.DAY_OF_MONTH)));
		
		for (Integer number : numbers) {
			
			val = doc.createElement("val");
			val.appendChild(doc.createTextNode(number.toString()));
			values.appendChild(val);

		}
		root.appendChild(date);
		root.appendChild(values);
		doc.appendChild(root);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + filename+".xml"));
		t.transform(source, sr);

	}

	
}
