package Zad1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Zad1 {

	private String path = "src//resources//";
	private List<String> salaryList;
	
//	Napisz metodę, która zliczy średnią arytmetyczną pensji pracowników:
//		+getAvgSalary():double
//		Wykorzystaj plik staff.xml
	
	public Zad1() {
		salaryList = new ArrayList<>();
	}
	
	public double getAvgSalary() {
		try {
			this.readSalaryXML();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double avg = 0.0;
		for(int i = 0; i < this.salaryList.size(); i++) {
			avg += Double.parseDouble(this.salaryList.get(i));
		}
		
		return avg/this.salaryList.size();
	}
	
	private void readSalaryXML() throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + "staff.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		
		NodeList nodeList = doc.getElementsByTagName("staff");
		
		for(int i =0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				this.salaryList.add(element.getElementsByTagName("salary").item(0).getTextContent());
			}
		}
		
	}
	
}
