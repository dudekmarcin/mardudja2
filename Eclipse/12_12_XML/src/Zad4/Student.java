package Zad4;

public class Student {

	private String name;
	private String lastname;
	private Integer year;
	
	public Student(String name, String lastname, Integer year) {
		this.name = name;
		this.lastname = lastname;
		this.year = year;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	
	
}
