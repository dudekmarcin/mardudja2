package Zad4;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class Zad4 {
//
//	Napisz metodę, która pozwoli na zapisanie pliku XML według następującej struktury:
//		ROOT
//			STUDENTS
//				STUDENT
//					NAME: John
//					LASTNAME: Simple
//					YEAR: 3
//				STUDENT
//					NAME: Jane
//					LASTNAME: Doe
//					YEAR: 1
//		Plik zapisz w pakiecie resources pod nazwą students.xml
//	

	private List<Student> studentList;
	
	private String path;
	private String name;
	
	public Zad4() {
		this.path = "src//resources//";
		this.name = "students.xml";
		this.studentList  = new ArrayList<>();
	}

	public Zad4(String path, String name) {
		this.path = path;
		this.name = name;
		this.studentList  = new ArrayList<>();
	}
	
	public void writeToXML() throws ParserConfigurationException, SAXException, IOException, TransformerException {
		
		studentList.add(new Student("John", "Simple", 3));
		studentList.add(new Student("Jane", "Doe", 1));
		studentList.add(new Student("Peter", "Smith", 2));
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element root = doc.createElement("root");
		Element students = doc.createElement("students");
		Element student;
		Element name;
		Element lastname;
		Element year;
		
		
		for(int i = 0; i < this.studentList.size(); i++) {
			student = doc.createElement("student");
			name = doc.createElement("name");
			lastname = doc.createElement("lastname");
			year = doc.createElement("year");
			name.appendChild(doc.createTextNode(this.studentList.get(i).getName()));
			lastname.appendChild(doc.createTextNode(this.studentList.get(i).getLastname()));
			year.appendChild(doc.createTextNode(this.studentList.get(i).getYear().toString()));
			student.appendChild(name);
			student.appendChild(lastname);
			student.appendChild(year);
			students.appendChild(student);

			
		}

			root.appendChild(students);
			doc.appendChild(root);


		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + this.name));
		t.transform(source, sr);
		
		
		
	}

}
