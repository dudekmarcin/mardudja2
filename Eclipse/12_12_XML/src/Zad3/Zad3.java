package Zad3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Zad3 {
//	Napisz metodę, która sprawdzi i wypisze do konsoli maksymalny oraz minimalny ID użytkownika w pliku XML (staff.xml).
//	+getMinMaxStaffID():void

	private String path = "src//resources//";
	
	
	public void getMinMaxStaffID() throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + "staff.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		
		NodeList nodeList = doc.getElementsByTagName("staff");
		List<Integer> ids = new ArrayList<>();
		
		for(int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			
			Element element = (Element) node;
			
			
			ids.add(Integer.parseInt(element.getAttribute("id")));
		
		}
		int min = ids.get(0);
		int max = ids.get(0);
		for(int i = 1; i < ids.size(); i++) {
			min = (ids.get(i) < min) ? ids.get(i) : min;
			max = (ids.get(i) > min) ? ids.get(i) : max;
		}
		
		System.out.println("Min ID: " + min);
		System.out.println("Max ID: " + max);
		
	}
	
}
