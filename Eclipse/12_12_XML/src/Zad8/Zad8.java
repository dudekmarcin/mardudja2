package Zad8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Zad8 {
//	Posiadając plik CSV (Comma Separated Values) - czyli wartości oddzielone wybranym
//	delimeterem o nagówku:
//	ID;Imię;Nazwisko;Kierunek;Rok;Średnia
//	np.:
//	ID;Imię;Nazwisko;Kierunek;Rok;Średnia
//	1;Paweł;Testowy;Architektura;1;4.25
//	Napisz metodę, która przekonwertuje taki plik z nagłówkiem na plik XML (bez nagłówka).

	private String path;
	
	public Zad8() {
		this.path = "src//resources//";
	}
	
	public Zad8(String path) {
		this.path = path;
	}

	public void csvToXML(String csvFilename, String xmlFilename) throws ParserConfigurationException, TransformerException {
		csvFilename = (csvFilename.length() > 4 && csvFilename.substring(csvFilename.length()-4, csvFilename.length()).equals(".csv")) ? csvFilename : csvFilename + ".csv";
		xmlFilename = (xmlFilename.length() > 4 && xmlFilename.substring(xmlFilename.length()-4, xmlFilename.length()).equals(".xml")) ? xmlFilename : xmlFilename + ".xml";

		List<String[]> csv = this.readCSV(csvFilename);
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();

		Element root = doc.createElement("ROOT");
		Element student;
		Element name;
		Element lastname;
		Element subject;
		Element year;
		Element average;
		
		for(int i = 1; i < csv.size(); i++) {
			student = doc.createElement("STUDENT");
			student.setAttribute(csv.get(0)[0], csv.get(i)[0]);
			name = doc.createElement(csv.get(0)[1]);
			lastname = doc.createElement(csv.get(0)[2]);
			subject = doc.createElement(csv.get(0)[3]);
			year = doc.createElement(csv.get(0)[4]);
			average = doc.createElement(csv.get(0)[5]);

			name.appendChild(doc.createTextNode(csv.get(i)[1]));
			lastname.appendChild(doc.createTextNode(csv.get(i)[2]));
			subject.appendChild(doc.createTextNode(csv.get(i)[3]));
			year.appendChild(doc.createTextNode(csv.get(i)[4]));
			average.appendChild(doc.createTextNode(csv.get(i)[5]));
			
			student.appendChild(name);
			student.appendChild(lastname);
			student.appendChild(subject);
			student.appendChild(year);
			student.appendChild(average);
			root.appendChild(student);
			
		}

		doc.appendChild(root);
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + xmlFilename));
		t.transform(source, sr);
		
	}
	
	public List<String[]> readCSV(String csvFilename) {
		File f = new File(this.path + csvFilename);
		List<String[]> csv = new LinkedList<>();
		try {
			Scanner scan = new Scanner(f);
			while(scan.hasNextLine()) {
				csv.add(scan.nextLine().split(";"));
			}
			scan.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return csv;
	}
	

}

