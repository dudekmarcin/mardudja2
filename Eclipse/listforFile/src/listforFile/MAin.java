package listforFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class MAin {

	public static void main(String[] args) throws FileNotFoundException {
		File f = new File("src//resources//name_f.txt");
		File f2 = new File("src//resources//name_f_list.txt");
		FileOutputStream fos = new FileOutputStream(f2);
		PrintWriter pw = new PrintWriter(fos);
		Scanner s = new Scanner(f);
		while(s.hasNextLine()) {
			pw.println("name_f.add(\"" + s.nextLine() + "\");");
		}
		pw.close();
		s.close();
	}

}
