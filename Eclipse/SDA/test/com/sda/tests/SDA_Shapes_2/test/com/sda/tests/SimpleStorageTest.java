package com.sda.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.data.structures.SimpleStorage;
import com.sda.shapes.Circle;
import com.sda.shapes.Rectangle;
import com.sda.shapes.Shape;
import com.sda.shapes.ShapesCountHolder;
import com.sda.shapes.Square;

public class SimpleStorageTest {

	@Test
	public void testAddingShapeToList(){
		SimpleStorage simpleStorage = new SimpleStorage();
		
		Shape myCircle = new Circle(5);
		
		simpleStorage.addShapeToList(myCircle);
		
		assertFalse(simpleStorage.isEmpty());
	}
	
	@Test
	public void testRemovingFromList(){
		SimpleStorage simpleStorage = new SimpleStorage();
		
		Shape myRectangle = new Rectangle(2,5);
		simpleStorage.addShapeToList(myRectangle);
		assertFalse(simpleStorage.isEmpty());
		
		simpleStorage.printEntries();
	
		simpleStorage.deleteShapeFromList(10);
		assertTrue(simpleStorage.isEmpty());
	}
	
	@Test
	public void testCountShapesOccurences(){
		SimpleStorage simpleStorage = new SimpleStorage();
		
		Shape myRectangle = new Rectangle(2,5);
		Shape myRectangle2 = new Rectangle(2,5);
		Shape myRectangle3 = new Rectangle(2,5);
		
		Shape myCircle = new Circle(5);
		Shape myCircle2 = new Circle(7);
		
		Shape mySquare = new Square(2);
		
		
		
		simpleStorage.addShapeToList(myRectangle);
		simpleStorage.addShapeToList(myRectangle2);
		simpleStorage.addShapeToList(myRectangle3);
		simpleStorage.addShapeToList(myCircle);
		simpleStorage.addShapeToList(myCircle2);
		simpleStorage.addShapeToList(mySquare);
		
		ShapesCountHolder sch = simpleStorage.countShapeTypesOccurences();
		
		System.out.println(sch.countRectangles);
		System.out.println(sch.countCircles);
		System.out.println(sch.countSquares);
		
	}
}
