package com.sda.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.shapes.Circle;

public class CircleTest {
	
	@Test
	public void testArea(){
		int a = 2;
		
		Circle circle = new Circle(a);
		assertTrue(circle.area() == (a*a*Math.PI));
	}

	@Test
	public void testPerimeter(){
		int a = 2;
		
		Circle circle = new Circle(a);
		assertTrue(circle.perimeter() == (2*a*Math.PI));
		
	}
}
