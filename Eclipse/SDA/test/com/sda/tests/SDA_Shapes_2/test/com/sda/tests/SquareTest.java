package com.sda.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.shapes.Square;

public class SquareTest {
	
	@Test
	public void testArea(){
		int a = 2;
		
		Square square = new Square(a);
		assertTrue(square.area() == (a*a));
	}

	@Test
	public void testPerimeter(){
		int a = 2;
		
		Square square = new Square(a);
		assertTrue(square.perimeter() == (4*a));
		
	}
}
