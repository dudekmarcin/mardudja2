package com.sda.data.structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sda.shapes.Rectangle;
import com.sda.shapes.Shape;
import com.sda.shapes.ShapesCountHolder;
import com.sda.shapes.Square;

public class SimpleStorage implements AbstractStorage{
	private List<Shape> storage;
	
	public SimpleStorage(){
		storage = new ArrayList<>();
	}
	
	@Override
	public void addShapeToList(Shape shape) {
		storage.add(shape);
	}

	@Override
	public void deleteShapeFromList(double area) {
		Iterator<Shape> iterator = storage.iterator();
		
		while(iterator.hasNext()){
			Shape tmp = iterator.next();
			
			if(tmp.area() == area){
				System.out.println("I am about to remove: "+tmp.getClass().getSimpleName());
				iterator.remove();
			}
		}
		
	}

	@Override
	public boolean isEmpty() {
		return storage.isEmpty();
	}

	@Override
	public void printEntries() {
		for(Shape shape : storage){
			System.out.println(shape.getClass().getSimpleName()+" "+shape.area());
		}
		
	}

	@Override
	public int countRectangleTypes() {
		int counter = 0;
		
		for(Shape shape : storage){
			if(shape instanceof Rectangle){
				counter++;
			}
		}
		
		return counter;
	}
	
	public ShapesCountHolder countShapeTypesOccurences(){
		ShapesCountHolder shc = new ShapesCountHolder();

		for(Shape shape : storage){
			if(shape.getClass().equals(Rectangle.class)){
				shc.countRectangles++;
			} else if(shape.getClass().equals(Square.class)){
				shc.countSquares++;
			} else {
				shc.countCircles++;
			}
		}
		
		
		
		return shc;
	}
}
