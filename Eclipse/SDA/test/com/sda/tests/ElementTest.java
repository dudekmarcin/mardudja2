package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import static com.sda.apps.NWW.*;


public class ElementTest {

	@Test
	public void testCompareTo() {
		
		Element number1 = new Element(4);
		Element number2 = new Element(5);
		
		
		assertTrue(number1.compareTo(number2) == -1);
		
		Element number3 = new Element(12);
		assertFalse(number3.compareTo(number2) == -1);
		
		Element number4 = new Element(4);
		assertTrue(number1.compareTo(number4) ==0);
			
	}
	@Test 
	public void testGetNWW() {
		Element[] array = new Element[5];
		array[0] = new Element(48);
		array[1] = new Element(72);
		array[2] = new Element(40);
		array[3] = new Element(36);
		array[4] = new Element(26);
		int result = getNWW(array);
		assertTrue(result == 9360);
	}
	
	
}
