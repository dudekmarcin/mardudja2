package com.sda.apps;

import java.util.Scanner;

public class LatinDecimal {

	public static int latinDecimal(String latin) {
		int result=0;
		int length = latin.length();
		int[] array = new int[length];
		for(int i=0;i<length;i++) {
			String temp = latin.substring(i,i+1);
			switch(temp) {
			case "I": array[i] = 1; break;
			case "V": array[i] = 5; break;
			case "X": array[i] = 10; break;
			case "L": array[i] = 50; break;
			case "C": array[i] = 100; break;
			case "D": array[i] = 500; break;
			case "M": array[i] = 1000; break;
			default: System.out.println("Podana liczba nie jest rzymska!");
			}
		}
		
		
		
		
		for(int i=length-1;i>=0;i--) {
			if(i>0) {
				if(array[i]>array[i-1]) {
					result += array[i] - array[i-1];
					i--;
				} else {
					result += array[i];
			} 
			}else {
				result += array[i];
		}
	
		}
		return result;
	}
	
	public static String decimalLatin(int decimal) {
		String result = "";
		String[] array = new String[] {"I", "V", "X", "L", "C", "D", "M"};
		int[] dec_array = new int[7];
		
			
			dec_array[6] = decimal/1000;
			decimal -= (dec_array[6]*1000);
			dec_array[5] = decimal/500;
			decimal -= (dec_array[5]*500);
			dec_array[4] = decimal/100;
			decimal -= (dec_array[4]*100);
			dec_array[3] = decimal/50;
			decimal -= (dec_array[3]*50);
			dec_array[2] = decimal/10;
			decimal -= (dec_array[2]*10);
			dec_array[1] = decimal/5;
			decimal -= (dec_array[1]*5);
			dec_array[0] = decimal/1;
			decimal -= (dec_array[1]*1);
		
			for(int i=6;i>=0;i--)
			{
				for(int j=dec_array[i];j>0;j--) {
					if(dec_array[i] == 1 && dec_array[i-1]==4) {
					/*int length=result.length();
					result=result.substring(0, length-1);*/
					result = result+array[i-1]+array[i+1];
					i-=1;
					j-=1;
					} else {
					result= result+array[i];
				
					}
			}
			
			
			
			}
		
		return result;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj liczb� rzymsk�:");
		String number = scanner.next(); 
		System.out.println("Podaj drug� liczb� rzymsk�:");
		String number2 = scanner.next(); 
		
		System.out.println("--------------");
		
		int result = latinDecimal(number) + latinDecimal(number2);
		System.out.println(result);
		System.out.println("--------------");
		System.out.println(decimalLatin(result));
		System.out.println("--------------");
		scanner.close();
	}

}
