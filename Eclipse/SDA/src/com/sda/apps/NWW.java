package com.sda.apps;

import java.util.Arrays;
import java.util.Scanner;

public class NWW {

	public static class Element implements Comparable<Element> {
		private int start = 0;
		private int current = 0;
		
		public Element(int number) {
			current = number;
			start = number;
		}
		
		public int getElement() {
			return current;
		}
		
		public int increaseElement() {
			return current += start;
		}
		
		public int compareTo(Element number) {
			if(this.current > number.current) {
				return 1;
			} else if(this.current < number.current) {
				return -1;
			} else {
				return 0;
			}
		}
		
	}
	
	public static int getNWW(Element[] array) {
		
		Arrays.sort(array);
		
		while(array[0].getElement() != array[4].getElement()) {
			array[0].increaseElement();
			Arrays.sort(array);
		}
		
		return array[0].getElement();
	
	}
	
	public static int getNWW() {
	return 1;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Element[] numbers = new Element[5];
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Podaj 5 liczb naturalnych: ");
		numbers[0] = new Element(scan.nextInt());
		numbers[1] = new Element(scan.nextInt());
		numbers[2] = new Element(scan.nextInt());
		numbers[3] = new Element(scan.nextInt());
		numbers[4] = new Element(scan.nextInt());
		scan.close();
		System.out.println("-------------------");
		
		int result = getNWW(numbers);
		System.out.println("NWW: "+result);
		
		
	}

}
