package com.sda.apps;

public class Array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] myNumbers = new int[5];

		for (int i = 0; i < 5; i++) {
			myNumbers[i] = i + 2;
			System.out.println(myNumbers[i]);

		}
		System.out.println("-------------------");
		for (int i = 4; i >= 0; i--) {
			System.out.println(myNumbers[i]);

		}
		System.out.println("-------------------");

		for (int i = 0; i <= 10; i++) {
			System.out.println("Marcin");
		}
		System.out.println("-------------------");

		int a = 0;
		while (a < 10) {
			System.out.println("Marcin");
			a++;
		}

	}

}
