package sroda;



public class Zad2 {
	// 2. Mamy dwie tablice liczb całkowitych, należy
	// a. wyświetlić te liczby, które występują w obydwu tablicach
	// b. wyświetlić liczby z obu tablic, które się nie powtarzają

	public void duplicatedValues(int[] arr1, int[] arr2) {
		Zad0 z0 = new Zad0();
		// Set<Integer> duplicate = new HashSet<>();
		int tempArrayLength = (arr1.length >= arr2.length) ? arr1.length : arr2.length;
		int[] tempArray = new int[tempArrayLength];
		int tempIndex = 0;

		System.out.println("Liczby powtarzające się w tablicach: ");
		for (int i = 0; i < arr1.length; i++) {
			if (z0.indexOf(arr2, arr1[i]) >= 0) {
				if (z0.indexOf(tempArray, arr1[i]) == -1) {
					tempArray[tempIndex] = arr1[i];
					tempIndex++;
				}
			}
		}
		for (int i = 0; i < tempIndex; i++) {
			System.out.print(tempArray[i] + " ");
		}
		System.out.println();
	}

	public void uniqueValues(int[] arr1, int[] arr2) {
		Zad0 z0 = new Zad0();
		int tempArrayLength = arr1.length + arr2.length;
		int[] tempArray = new int[tempArrayLength];
		int tempIndex = 0;
		System.out.println("Liczby nie powatarzające się w tablicach: ");
		int[] longerArray = (arr1.length >= arr2.length) ? arr1 : arr2;
		int[] shorterArray = (arr1.length < arr2.length) ? arr1 : arr2;
		
		for (int i = 0; i <shorterArray.length; i++) {
			if (z0.indexOf(tempArray, shorterArray[i])== -1) {
				tempArray[tempIndex] = shorterArray[i];
				tempIndex++;
			}
		}

		for (int i = 0; i < longerArray.length; i++) {
			if (z0.indexOf(shorterArray, longerArray[i]) == -1) {
				if (z0.indexOf(tempArray, longerArray[i]) == -1) {
					;
					tempArray[tempIndex] = longerArray[i];
					tempIndex++;
				}
			}
		}
		
		for (int i = 0; i < tempIndex; i++) {
			System.out.print(tempArray[i] + " ");
		}
		System.out.println();
	}
	
//	private int[] fillingTempArray(int[] arr, int[] resultArr, int maxIndex) {
//		Zad0 z0 = new Zad0();
//		for (int i = 0; i <arr.length; i++) {
//			if (z0.indexOf(resultArr, arr[i])== -1) {
//				resultArr[maxIndex] = arr[i];
//				maxIndex++;
//			}
//		}
//		
//		return resultArr;
//		
//	}
	
}