package sroda;

public class Zad4 {
	// Napisz funkcję, która przyjmie dwa argumenty: liczbę znaków oraz zdanie.
	// Funkcja powinna wypisać słowa, które są dłuższe niż zadana liczba w
	// pierwszym argumencie.
	
	public void longWords(int length, String str) {
		
		String[] words = str.split("\\W+");
		for(int i=0; i<words.length; i++) {
			if(words[i].length() > length) {
				System.out.println(words[i]);
			}
		}
		
	}
	
}
