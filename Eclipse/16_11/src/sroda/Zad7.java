package sroda;

public class Zad7 {

	// Napisz funkcję, która jako argument przyjmuje dodatnią liczbę całkowitą
	// większą od zera. Dla podanego zakresu wydrukuj kolejne wartości pomijając
	// te, które są podzielne przez 3 lub przez 4
	
	public void  indivisibleNumbers(int number) {
		for(int i = 0; i<=number; i++) {
			if(!(i % 4 == 0) && !(i % 3 == 0))
			System.out.println(i);
		}
	}

}
