package sroda;

public class Zad5 {
	// Napisz funkcję, która zamienia dla zadanego ciągu znaków pierwszy znak z
	// ostatnim
	
	public String replaceFirstAndLast(String str) {
		if(str == null || str.isEmpty()) {
			return "Nie da się przerobić pustego stringa";
		}

		return str.substring(str.length()-1) + str.substring(1, str.length()-1) + str.substring(0,1);
	}
}
