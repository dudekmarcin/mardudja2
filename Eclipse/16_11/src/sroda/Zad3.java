package sroda;

public class Zad3 {

	// 3. Napisz funkcję, w której dla zadanego łańcucha znaków, wszystkie znaki
	// - takie same jak pierwsza litera ciągu znaków zostaną zamienione na znak
	// '_', wyjątkiem jednak jest pierwszy znak. Dla przykładu:
	// Wejście: oksymoron
	// Wyjście: oksym_r_n
	
	public String replaceSing(String start) {
		if(start == null || start.isEmpty()) {
			return "Nie da się przerobić pustego stringa";
		}
		
		start = start.toLowerCase();
		
		return start.substring(0,1) + start.substring(1).replace(start.substring(0, 1), "_");
		
	}
	
}
