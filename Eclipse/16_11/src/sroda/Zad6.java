package sroda;

public class Zad6 {
	// Napisz funkcję, która dla wczytanego od użytkownika słowa, wyświetla jego
	// litery w kolejności odwrotnej.
	public void reverse(String str) {
		String[] stringArr = str.split("");
		for(int i=stringArr.length-1; i >= 0 ; i--) {
			System.out.print(stringArr[i]);
		}
		
	}
}
