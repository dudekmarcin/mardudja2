package sroda;

public class Zad1 {
	// 1. Napisz funkcję, która przyjmuje dwie tablice jako argumenty.
	// Jeżeli na obu tablicach występuje ten sam element, funkcja powinna
	// zwracać true, w przeciwnym wypadku false.

	public boolean compareTwoArrays(int[] arr1, int[] arr2) {
		Zad0 z0 = new Zad0();
		for (int i = 0; i < arr1.length; i++) {
			if (z0.indexOf(arr2, arr1[i]) >= 0) {
				return true;
			}
		}
		return false;
	}

	public boolean checkForDupicate(int[] arr1, int[] arr2) {
		for (int i = 0; i < arr1.length; i++) {
			for (int j = 0; j < arr2.length; j++) {
				if (arr1[i] == arr2[j]) {
					return true;
				}
			}
		}

		return false;
	}
}
