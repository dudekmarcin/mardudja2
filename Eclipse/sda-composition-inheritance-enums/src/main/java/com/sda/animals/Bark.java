package com.sda.animals;

public class Bark extends AnimalSound {

	public Bark(int count) {
		super(count, "Bark");
	}
}
