package com.sda.animals.inheritance;

public class FrenchBulldog extends Dog {

	
	public int barkCount() {
		return super.bark().getCount();
	}
}
