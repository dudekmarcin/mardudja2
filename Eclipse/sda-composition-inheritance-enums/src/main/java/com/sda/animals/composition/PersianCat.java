package com.sda.animals.composition;

public class PersianCat {

    private Cat cat = new Cat();

    public int meow() {
        return cat.meow().getCount();
    }

}
