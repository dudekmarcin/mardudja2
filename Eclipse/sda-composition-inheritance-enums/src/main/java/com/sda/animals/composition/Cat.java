package com.sda.animals.composition;

import com.sda.animals.Meow;

public class Cat {

    public Meow meow() {
        Meow meow = new Meow(1);
        return meow;
    }

}
