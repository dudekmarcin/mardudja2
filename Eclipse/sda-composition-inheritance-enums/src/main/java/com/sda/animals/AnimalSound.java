package com.sda.animals;

public abstract class AnimalSound {
	private int count;
	private String sound;

	public AnimalSound(int count, String sound) {
		this.count = count;
		this.sound = sound;
	}

	public int getCount() {
		return count;
	}

	public String getSound() {
		return sound;
	}

	
	/*public void makeSound() {
		String createSound = sound;
		for(int i=1;i<=count;i++) {
			createSound +=" "+sound;
		}
		System.out.println(sound);
	}*/

}
