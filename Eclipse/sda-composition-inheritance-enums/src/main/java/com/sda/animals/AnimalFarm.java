package com.sda.animals;

import com.sda.animals.composition.PersianCat;
import com.sda.animals.inheritance.FrenchBulldog;

public class AnimalFarm {

    public static void main(String[] args) {
        FrenchBulldog gacek = new FrenchBulldog();
        int barks = gacek.barkCount();
        System.out.println(barks);
        
        PersianCat filemon = new PersianCat();
        	int meows = filemon.meow();
        	System.out.println(meows);
    }

}
