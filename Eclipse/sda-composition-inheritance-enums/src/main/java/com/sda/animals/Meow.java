package com.sda.animals;

public class Meow extends AnimalSound {
	
	public Meow(int count) {
		super(count, "Meow");
	}
}
