package com.sda.animals.inheritance;

import com.sda.animals.Bark;

public class Dog {

    public Bark bark() {
       Bark bark = new Bark(2);
       return bark;
    }

}
