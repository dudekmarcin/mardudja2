package main;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListBenchmark {

	public LinkedList<Integer> linked;
	public ArrayList<Integer> array;
	
	public ListBenchmark() {
		linked = new LinkedList<>();
		array = new ArrayList<>();
	}
	
	public void LinkedListTime(int numberOfElements) {
		Long startTime = System.nanoTime();
		
		for(int i=0; i < numberOfElements;i++) {
			linked.add(i);
		}
		
		Long stopTime = System.nanoTime();

		System.out.println("LINKED It took: " + (stopTime - startTime) + " nanoseconds");
		
	}
	
	public void ArrayListTime(int numberOfElements) {
		Long startTime = System.nanoTime();
		
		for(int i=0; i < numberOfElements;i++) {
			array.add(i);
		}
		
		Long stopTime = System.nanoTime();

		System.out.println("ARRAY It took: " + (stopTime - startTime) + " nanoseconds");
		
	}
	
	
	
	public void ArrayListTimeGET(int numberOfElements, int elementNumber) {
		Long startTime = System.nanoTime();
		
		for(int i=0; i < numberOfElements;i++) {
			array.get(elementNumber);
		}
		
		Long stopTime = System.nanoTime();

		System.out.println("ARRAY GET took: " + (stopTime - startTime) + " nanoseconds");
		
	}
	
	public void LinkedListTimeGET(int numberOfElements, int elementNumber) {
		Long startTime = System.nanoTime();
		
		for(int i=0; i < numberOfElements;i++) {
			linked.get(elementNumber);
		}
		
		Long stopTime = System.nanoTime();

		System.out.println("LINKED GET took: " + (stopTime - startTime) + " nanoseconds");
		
	}
	
	public void ArrayListTimeREMOVE(int numberOfElements, int elementNumber) {
		Long startTime = System.nanoTime();
		
		for(int i=0; i < numberOfElements;i++) {
			array.remove(elementNumber);
		}
		
		Long stopTime = System.nanoTime();

		System.out.println("ARRAY REMOVE took: " + (stopTime - startTime) + " nanoseconds");
		
	}
	
	public void LinkedListTimeREMOVE(int numberOfElements, int elementNumber) {
		Long startTime = System.nanoTime();
		
		for(int i=0; i < numberOfElements;i++) {
			linked.remove(elementNumber);
		}
		
		Long stopTime = System.nanoTime();

		System.out.println("LINKED REMOVE took: " + (stopTime - startTime) + " nanoseconds");
		
	}
	
	
	
	
	
	public static void main(String[] args) {
		
		ListBenchmark bench = new ListBenchmark();
		bench.LinkedListTime(100000);
		bench.ArrayListTime(100000);
		
		bench.LinkedListTimeGET(10000, 50000);
		bench.ArrayListTimeGET(10000, 50000);
		
		bench.LinkedListTimeREMOVE(10000, 50000);
		bench.ArrayListTimeREMOVE(10000, 50000);
		
	}

}
