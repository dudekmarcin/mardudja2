package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.shapes.Circle;

public class CircleTest {
	
	@Test
	public void testArea(){
		int r = 4;
		Circle circle = new Circle(r);
		System.out.println(circle.area());
		assertTrue(circle.area() == r*r*Math.PI);
	}
	
	@Test
	public void testPerimeter() {
		int r = 4;
		Circle circle = new Circle(r);
		System.out.println(circle.perimeter());
		assertTrue(circle.perimeter() == 2*r*Math.PI);
	}

}
