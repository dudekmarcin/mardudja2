package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.data.structures.SimpleStorage;
import com.sda.shapes.Circle;
import com.sda.shapes.Rectangle;
import com.sda.shapes.Shape;
import com.sda.shapes.Square;

public class SimpleStorageTest {
	
	@Test
	public void testAddingShapeToList() {
		SimpleStorage simple = new SimpleStorage();
		Shape myCircle = new Circle(5);
		simple.addShapeToList(myCircle);
		assertFalse(simple.isEmpty());
	}
	
	@Test
	public void tesDeleteShapeFromList() {
		SimpleStorage simple = new SimpleStorage();
		Shape mySquare  =new Square(5);
		simple.addShapeToList(mySquare);
		assertFalse(simple.isEmpty());
		simple.printEntries();
		simple.deleteShapeFromList(25);
		assertTrue(simple.isEmpty());
	}
	
	@Test
	public void testCountRectangleTypes() {
		SimpleStorage storage = new SimpleStorage();
		Shape c1 = new Circle(2);
		Rectangle r1 = new Rectangle(3,4);
		Shape s1 = new Square(5);
		
		storage.addShapeToList(c1);
		assertTrue(storage.countRectangleTypes() == 0);
		
		storage.addShapeToList(r1);
		storage.addShapeToList(s1);
		
		
		assertTrue(storage.countRectangleTypes() == 2);
	}
	
}
