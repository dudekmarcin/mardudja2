package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.shapes.Square;

public class SquareTest {

	@Test
	public void testArea() {
		int side = 5;
		Square sq = new Square(side);
		assertTrue(sq.area() == side*side);
	}
	
	@Test
	public void testPerimeter() {
		int side = 5;
		Square sq = new Square(side);
		assertTrue(sq.perimeter() == 4*side);
	}
}
