package com.sda.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.shapes.Rectangle;

public class RectangleTest {
	
	@Test
	public void testArea() {
		int a=2;
		int b=5;
		
		Rectangle rect = new Rectangle(a,b);
		assertTrue(rect.area() == a*b);
	}
	
	@Test
	public void testPerimeter() {
		int a=2;
		int b=5;
		
		Rectangle rect = new Rectangle(2,5);
		assertTrue(rect.perimeter() == 2*a+2*b);
	}
	
}
