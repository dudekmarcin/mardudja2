package com.sda.data.structures;

import java.util.Arrays;
import java.util.Scanner;

public class Nwd {

	public static int obliczNwd(int[] array) {
		Arrays.sort(array);
		while(array[0] != array[4])
		{
			array[4]-=array[0];
			Arrays.sort(array);
		}
		return array[0];
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] numbers = new int[5];
		Scanner scan = new Scanner(System.in);
		System.out.println("Podaj 5 liczb naturalnych: ");
		numbers[0] = scan.nextInt();
		numbers[1] = scan.nextInt();
		numbers[2] = scan.nextInt();
		numbers[3] = scan.nextInt();
		numbers[4] = scan.nextInt();
		scan.close();
		System.out.println("---------------");
		int result = obliczNwd(numbers);
		System.out.println("Najwi�kszy wsp�lny dzielnik to: "+result);
		
	}

}
