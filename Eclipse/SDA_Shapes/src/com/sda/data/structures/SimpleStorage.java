package com.sda.data.structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sda.shapes.Circle;
import com.sda.shapes.Rectangle;
import com.sda.shapes.Shape;
import com.sda.shapes.Square;

public class SimpleStorage implements AbstractStorage {

	/*
	 * 1. Klasa SimpleStorage powinna zawiera� prywatne pole List o typie <Shape>. 
	 * Zaimplementuj dodawanie i usuwanie dla tej mapy poprzez metody klasy SimpleStorage. 
	 * Dodaj inicjalizacj� listy w konstruktorze; Zaimlementuj pozosta�e metody.
	 */
	
	private List<Shape> storage;
	
	public SimpleStorage() {
		storage = new ArrayList<>();
	}
	
	
	@Override
	public void addShapeToList(Shape shape) {
		storage.add(shape);
	}
	
	@Override
	public void deleteShapeFromList(double area) {
		/*	
		 * 	int listSize = storage.size();

		for(int i=0;i<listSize;i++) {
			if(storage.get(i).area() == area) {
				storage.remove(i);
				break;
			}
		}*/
		
		Iterator <Shape> iterator = storage.iterator();
		while(iterator.hasNext()) {
			Shape tmp = iterator.next();
			if(tmp.area() == area) {
				iterator.remove();
			}
		}
		
	}
	
	/*
	 * Dodaj do klasy SimpleStorage metod� typu void countShapeTypesOccurrences, 
	 * kt�ra dla ka�dego kszta�tu wy�wietli w konsoli ( w obr�bie tej metody ) ile 
	 * poszczeg�lnych obiekt�w danego kszta�tu znajduje si� w Twoim SimpleStorage. 
	 * Napisz odpowiedni test jednostkowy. BONUS: ShapesCountHolder jako struktura
	 *  przechowuj�ca pola z informacj� o zliczonych kszta�tach
	(Podpowied�: por�wnanie klas mo�emy zrealizowa� za pomoc� if(myObject.getClass().equals(SomeClass.class))
	 */
	public class ShapesCountHolder {
		public int cC=0;
		public int cR=0;
		public int cS=0;
	}
	
	public ShapesCountHolder countShapeTypesOccurrences() {
		ShapesCountHolder shc = new ShapesCountHolder();
		for(Shape shape : storage) {
			if(shape.getClass().equals(Circle.class)) {
				shc.cC++;
			} else if(shape instanceof Rectangle) {
				shc.cR++;
				if(shape.getClass().equals(Square.class)) {
				shc.cS++; }
			} 
		}
		return shc;
	}
	

	
	@Override
	public boolean isEmpty() {
		return storage.isEmpty();
	}

	@Override
	public void printEntries() {
		for(Shape shape : storage) {
			System.out.println(shape.getClass().getSimpleName()+" "+shape.area());
		}
	}
	
	@Override
	public int countRectangleTypes() {
		
		int rectangles = 0;
		for(Shape shape : storage) {
			if(shape instanceof Rectangle) {
				rectangles++;
				
			}
		}
		
		
		
		return rectangles;
	}
}
