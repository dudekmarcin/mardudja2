package com.sda.shapes;

public class Circle implements Shape {
	private int r;
	
	public Circle(int r) {
		this.r = r;
	}
	
	@Override 
	public double area() {
		return this.r * this.r * Math.PI;
	}
	
	@Override
	public double perimeter() {
		return 2*this.r*Math.PI;
	}
}
