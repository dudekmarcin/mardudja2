package generics;


public class Main {

	public static void main(String[] args) {
//		List<String> someB = new LinkedList<String>();
//		List<String> someA = new ArrayList<String>();
//		ListComparatorUtility<LinkedList<String>> compB = new ListComparatorUtility(someB);
//		ListComparatorUtility<LinkedList<String>> compA = new ListComparatorUtility(someA);
//			
//		compB.compareWithOtherList(compA);
		
		HighSchoolClass hsc = new HighSchoolClass();
		ElementaryClass ec = new ElementaryClass();
		ElementaryClass ec2 = new ElementaryClass();

		hsc.addGrades("Math", 5);
		hsc.addGrades("Math", 5);
		hsc.addGrades("Math", 5);
		hsc.addGrades("Math", 5);
		hsc.addGrades("Math", 5);
		hsc.addGrades("Math", 5);

		hsc.addGrades("Chemistry", 5);
		hsc.addGrades("Chemistry", 5);
		hsc.addGrades("Art", 5);
		hsc.addGrades("Art", 5);
		hsc.addGrades("Art", 5);
		hsc.addGrades("Art", 5);
		System.out.println("hsc getAverage: " + hsc.getAverage());
		System.out.println("hsc getSubjectAverage(art): " + hsc.getSubjectAverage("art"));
		System.out.println("hsc.getSubjectAverage(chemistry): " + hsc.getSubjectAverage("chemistry"));
		System.out.println("hsc.getSubjectAverage(math): " + hsc.getSubjectAverage("math"));
		ec.addGrades("Math", 4);
		ec.addGrades("Math", 4);
		ec.addGrades("Math", 2);
		ec.addGrades("Math", 5);
		ec.addGrades("Math", 4);
		ec.addGrades("Natural Science", 3);
		ec.addGrades("Natural Science", 5);
		ec.addGrades("Art", 5);
		ec.addGrades("Art", 6);
		ec.addGrades("Art", 4);
		
		
		ec2.addGrades("Math", 4);
		ec2.addGrades("Math", 4);
		ec2.addGrades("Math", 5);
		ec2.addGrades("Math", 4);
		//ec2.addGrades("Chemistry", 3);
		ec2.addGrades("Natural Science", 5);
		ec2.addGrades("Art", 6);
		ec2.addGrades("Art", 4);
		
		
		System.out.println(hsc.getAverage());
		System.out.println(ec.getAverage());

		ClassCompareUtility<ElementaryClass> ec_utility = new ClassCompareUtility<ElementaryClass>(ec);
		
		System.out.println(ec_utility.compareSubjects(ec2));

		


		
	}
}
