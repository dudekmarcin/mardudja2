package generics;

import java.util.List;

public class ListComparatorUtility<T extends List<String>> {

	private T someList;

	public ListComparatorUtility(T list) {
		someList = list;
	}

	public boolean compareWithOtherList(ListComparatorUtility<?> otherUtility) {
		if (this.getListSize() == otherUtility.getListSize()) {
			return true;
		}

		return false;
	}

	public int getListSize() {
		return someList.size();
	}
}
