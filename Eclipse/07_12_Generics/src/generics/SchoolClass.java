package generics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public abstract class SchoolClass {

	protected Map<String, List<Integer>> gradeList;
	private double avg = 0;

	public SchoolClass() {
		this.gradeList = new HashMap<>();
	}

	public SchoolClass(Map<String, List<Integer>> gradeList) {
		this.gradeList = gradeList;
	}

	public void addGrades(String subject, int grade) {
		if (this.gradeList.containsKey(subject.toLowerCase())) {
			this.gradeList.get(subject.toLowerCase()).add(grade);	
		} else {
			List<Integer> tempGrade = new ArrayList<>();
			tempGrade.add(grade);
			this.gradeList.put(subject.toLowerCase(), tempGrade);
		}
	}

	public double getAverage() {
		this.avg = 0;

		this.gradeList.forEach((s, g) -> this.avg += this.getSubjectAverage(s));
	
		double avg = this.avg / gradeList.size();

		return avg;
	}
	
	public double getSubjectAverage(String subject) {
		double avg = 0;
		this.avg = 0.0;

		if(this.gradeList.containsKey(subject.toLowerCase())) {
			this.gradeList.get(subject.toLowerCase()).forEach(g -> this.avg += g);
			avg = this.avg / this.gradeList.get(subject).size();
		}

		return avg;
	}
	
	public abstract List<String> getSubjects();
	
}
