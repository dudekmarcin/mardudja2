package generics;

import java.util.ArrayList;
import java.util.List;

public class ElementaryClass extends SchoolClass {

	@Override
	public List<String> getSubjects() {
		List<String> subjects = new ArrayList<>();
		this.gradeList.forEach((sub, gra) -> subjects.add(sub));
		return subjects;
	}

}
