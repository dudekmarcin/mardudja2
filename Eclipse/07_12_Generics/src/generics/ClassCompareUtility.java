package generics;

import java.util.List;

public class ClassCompareUtility<T extends SchoolClass> {
	
	private T schoolClass;
	
	public ClassCompareUtility(T schoolClass) {
		this.schoolClass = schoolClass;
	}
	
	public int compareAverage(ClassCompareUtility<?> other) {
		
		return (this.getAvg() <= other.getAvg()) ? ((this.getAvg() == other.getAvg()) ? 0 : -1) : 1; 
	
	}
	
	public boolean compareSubjects(T other) {
		return (this.schoolClass.getSubjects().equals(other.getSubjects()));
	}
	
	public boolean compareSubjects(ClassCompareUtility<T> other) {
		return (this.schoolClass.getSubjects().equals(other.getSubjects()));
	}
	
		
	public double getAvg() {
		return this.schoolClass.getAverage();
	}
	
	public List<String> getSubjects() {
		return this.schoolClass.getSubjects();
	}

}
