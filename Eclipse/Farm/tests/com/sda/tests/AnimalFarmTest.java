package com.sda.tests;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.animals.AnimalKinds;
import com.sda.animals.Cow;
import com.sda.animals.Duck;
import com.sda.animals.Feed;
import com.sda.animals.Goat;
import com.sda.exceptions.NoFeedException;
import com.sda.exceptions.NoMoneyException;
import com.sda.exceptions.NoProductException;
import com.sda.exceptions.NoThatAnimalException;
import com.sda.farm.AnimalFarm;
import com.sda.products.ComposedProducts;
import com.sda.products.Products;

public class AnimalFarmTest {

	@Test
	public void initializeFarmTest() {
		AnimalFarm farm = new AnimalFarm(100);
		assertEquals(farm.getBalance(), 100);
	}

	@Test
	public void BuyAnimalTest() throws NoMoneyException {
		AnimalFarm farm = new AnimalFarm(100);
		farm.buyAnimal(new Cow());
		assertEquals(farm.getBalance(), 82);
		assertThat(farm.getAnimalsList().get(0).getKind(), is(AnimalKinds.COW));
	}

	@Test(expected = NoMoneyException.class)
	public void BuyAnimalWithOutMoneyTest() throws NoMoneyException {
		AnimalFarm farm = new AnimalFarm(10);
		farm.buyAnimal(new Cow());
	}

	@Test
	public void buyFeedTest() throws NoMoneyException {
		AnimalFarm farm = new AnimalFarm(10);
		buyFeed(farm, Feed.GREAT);
		buyFeed(farm, Feed.REGULAR);
	}

	@Test(expected = NoMoneyException.class)
	public void buyFeedFailTest() throws NoMoneyException {
		AnimalFarm farm = new AnimalFarm(1);
		buyFeed(farm, Feed.GREAT);
	}

	@Test
	public void sellAnimalTest() throws NoMoneyException, NoThatAnimalException {
		AnimalFarm farm = new AnimalFarm(100);
		farm.buyAnimal(new Cow());
		farm.buyAnimal(new Goat());
		farm.buyAnimal(new Duck());

		System.out.println(farm.getAnimalsList().get(0).getId());
		System.out.println(farm.getAnimalsList().get(1).getId());
		System.out.println(farm.getAnimalsList().get(2).getId());
		
		int uid = farm.getAnimalsList().get(1).getId();
		farm.sellAnimal(uid);
		assertThat(farm.getAnimalsList().size(), is(1));
		assertThat(farm.getBalance(), is(72));
	}

	@Test(expected = NoThatAnimalException.class)
	public void sellAnimalFailTest() throws NoMoneyException, NoThatAnimalException {
		AnimalFarm farm = new AnimalFarm(100);
		farm.buyAnimal(new Cow());
		int uid = -1;
		farm.sellAnimal(uid);
	}

	@Test
	public void eatFeedTest() throws Exception {
		AnimalFarm farm = new AnimalFarm(100);
		farm.buyAnimal(new Cow());
		farm.buyAnimal(new Goat());
		buyFeed(farm, Feed.GREAT);
		buyFeed(farm, Feed.REGULAR);
		feedAnimal(farm, Feed.GREAT, 0);
		feedAnimal(farm, Feed.REGULAR, 1);
	}
	
	@Test
	public void makeComposedProductTest() throws NoFeedException, NoProductException, NoMoneyException {
		AnimalFarm farm = new AnimalFarm(100);
		farm.buyAnimal(new Cow());
		for (int i = 0; i < 5; i++) {
			farm.buyFeed(Feed.GREAT);
			farm.eatFeed(farm.getAnimalsList().get(0), Feed.GREAT);
		}
		farm.makeComposedProduct(ComposedProducts.COWSCHEESE);
		assertThat(farm.getComposedProductsList().size(), is(1));
		assertThat(farm.getProductsList().size(), is(7));
	}


	@Test
	public void sellProductTest() throws NoMoneyException, NoProductException, NoFeedException  {
		AnimalFarm farm = new AnimalFarm(100);
		farm.buyAnimal(new Goat());
		farm.buyFeed(Feed.GREAT);
		farm.eatFeed(farm.getAnimalsList().get(0), Feed.GREAT);
		sellingProduct(farm, Products.GOATSMILK);
		
		for (int i = 0; i < 5; i++) {
			farm.buyFeed(Feed.GREAT);
			farm.eatFeed(farm.getAnimalsList().get(0), Feed.GREAT);
		}
		
		farm.makeComposedProduct(ComposedProducts.GOATSCHEESE);
		sellingProduct(farm, ComposedProducts.GOATSCHEESE);
	}

	@Test(expected = NoProductException.class)
	public void sellProductFailTest() throws NoMoneyException, NoProductException, NoFeedException {
		AnimalFarm farm = new AnimalFarm(100);
		farm.buyAnimal(new Cow());
		farm.buyFeed(Feed.GREAT);
		farm.eatFeed(farm.getAnimalsList().get(0), Feed.GREAT);
		farm.sellProduct(Products.GOATSMILK);
	}

	@Test
	public void updateTimelifeTest() throws IllegalArgumentException, NoMoneyException, NoFeedException {
		AnimalFarm farm = new AnimalFarm(100);
		farm.buyAnimal(new Cow());
		diedAnimal(farm, Feed.GREAT, 0);
	}
	

	private void buyFeed(AnimalFarm farm, Feed feedType) throws NoMoneyException {
		int balance = farm.getBalance();
		farm.buyFeed(feedType);

		if (feedType.equals(Feed.GREAT)) {
			assertThat(farm.getBalance(), is(balance - 2));
			assertEquals(farm.getSuperFeedBalance(), 1);
		} else if (feedType.equals(Feed.REGULAR)) {
			assertThat(farm.getBalance(), is(balance - 1));
			assertEquals(farm.getRegularFeedBalance(), 1);
		}
	}
	
	private void feedAnimal(AnimalFarm farm, Feed feedType, int index) throws NoFeedException {
		int listSize = farm.getProductsList().size();
		int timeLife = farm.getAnimalsList().get(index).getTimelife();
		farm.eatFeed(farm.getAnimalsList().get(index), feedType);
		
		if (feedType.equals(Feed.GREAT)) {
			assertThat(farm.getProductsList().size(), is(listSize+2));
		} else if(feedType.equals(Feed.REGULAR)){
			assertThat(farm.getProductsList().size(), is(listSize+1));
		}
		assertThat(farm.getAnimalsList().get(index).getTimelife(), is(timeLife-1));
	}
	
	private void sellingProduct(AnimalFarm farm, Products product) throws NoProductException {
		int balance = farm.getBalance();
		int listSize = farm.getProductsList().size();
		farm.sellProduct(product);
		assertThat(farm.getBalance(), is(balance+product.getValue()));
		assertThat(farm.getProductsList().size(), is(listSize-1));
	}
	
	private void sellingProduct(AnimalFarm farm, ComposedProducts product) throws NoProductException {
		int balance = farm.getBalance();
		int listSize = farm.getComposedProductsList().size();
		farm.sellProduct(product);
		assertThat(farm.getBalance(), is(balance+product.getValue()));
		assertThat(farm.getComposedProductsList().size(), is(listSize-1));
	}

	private void diedAnimal(AnimalFarm farm, Feed feedType, int index) throws NoFeedException, NoMoneyException {
		int listSize = farm.getProductsList().size();
		int animalListSize = farm.getAnimalsList().size();
		for (int i = 0; i < 10; i++) {
			farm.buyFeed(feedType);
			farm.eatFeed(farm.getAnimalsList().get(0), feedType);
		}
		assertThat(farm.getProductsList().size(), is(listSize+(10*feedType.getMultiplier())));
		assertThat(farm.getAnimalsList().size(), is(animalListSize-1));
	}
	
	

}
