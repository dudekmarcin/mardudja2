package com.sda.products;

public enum ComposedProducts {
		GOATSCHEESE(4, Products.GOATSMILK, 25, "Ser kozi"),
		COWSCHEESE(3, Products.COWSMILK, 15, "Ser krowi"),
		DUCKSOMLET(3, Products.DUCKSEGG, 11, "Omlet kaczy"),
		CHICKENSOMLET(3, Products.CHICKENSEGG, 7, "Omlet kurzy");
		
		private final int value;
		private final int components;
		private final String name;
		private final Products product;
		
		private ComposedProducts(int components, Products product, int value, String name) {
			this.components = components;
			this.product = product;
			this.value = value;
			this.name = name;
		}
		
		public int getComponents() {
			return components;
		}
		
		public Products getProduct() {
			return product;
		}
		
		public int getValue() {
			return value;
		}
		
		public String getName() {
			return name;
		}
}
