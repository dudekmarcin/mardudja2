package com.sda.products;

public enum Products {
		GOATSMILK(5, "Mleko kozie"),
		COWSMILK(4, "Mleko krowie"),
		DUCKSEGG(3, "Jajko kacze"),
		CHICKENSEGG(2, "Jajko kurze");
		
		private final int value;
		private final String name;
		
		private Products(int value, String name) {
			this.value = value;
			this.name = name;
		}
		
		public int getValue() {
			return value;
		}
		
		public String getName() {
			return name;
		}
}
