package com.sda.exceptions;

public class NoProductException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8380276146375760209L;

	public NoProductException() {
		super("You don't have this product. Sell something else.");
	}
}
