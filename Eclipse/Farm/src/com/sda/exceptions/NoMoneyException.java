package com.sda.exceptions;

public class NoMoneyException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8062522758982991565L;

	public NoMoneyException() {
        super("Not enough money. Sell something");
    }
}
