package com.sda.exceptions;

public class NoFeedException extends Exception {

	private static final long serialVersionUID = 8157154615757311967L;

	public NoFeedException() {
		 super("Not enough feed.Buy some feed.");
	}
}
