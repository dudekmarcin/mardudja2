package com.sda.exceptions;

public class NoThatAnimalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6633246874797103695L;
	
	public NoThatAnimalException() {
		super("You don't have that animal on your farm.");
	}
}
