package com.sda.animals;

import com.sda.products.ComposedProducts;
import com.sda.products.Products;

public enum AnimalKinds {
		GOAT(20, Products.GOATSMILK, ComposedProducts.GOATSCHEESE),
		COW(18, Products.COWSMILK, ComposedProducts.COWSCHEESE),
		DUCK(12, Products.DUCKSEGG, ComposedProducts.DUCKSOMLET),
		CHICKEN(6, Products.CHICKENSEGG, ComposedProducts.CHICKENSOMLET);

	private final int value;
	private final Products product;
	private final ComposedProducts composedProduct;
	
	private AnimalKinds(int value, Products product, ComposedProducts composedProduct) {
		this.value = value;
		this.product = product;
		this.composedProduct = composedProduct;
	}
	
	public int getValue() {
		return value;
	}
	
	public Products getProduct() {
		return product;
	}
	
	public ComposedProducts getComposedProduct() {
		return composedProduct;
	}

}
