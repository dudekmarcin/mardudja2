package com.sda.animals;

import com.sda.products.Products;

public class Animal {
	
	private static int COUNT = 0;

	private final AnimalKinds kind;
	private int timelife = 10;
	private final Products product;
	private int value;
	private int uid = COUNT++;
	
	public Animal(AnimalKinds kind) {
		this.value = kind.getValue();
		this.product = kind.getProduct();
		this.kind = kind;
	}
	
	public int getId() {
		return uid;
	}
	
	public AnimalKinds getKind() {
		return kind;
	}
	
	public int getValue() {
		return value;
	}
	
	public Products makeProduct() {
		return product; 
	}
	
		public int getTimelife() {
		return timelife;
	}
	
	public void increaseTimelife() {
		timelife--;
	}

}
