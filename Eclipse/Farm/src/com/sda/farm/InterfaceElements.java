package com.sda.farm;

import java.util.Scanner;

import com.sda.animals.Animal;
import com.sda.animals.AnimalKinds;
import com.sda.animals.Chicken;
import com.sda.animals.Cow;
import com.sda.animals.Duck;
import com.sda.animals.Feed;
import com.sda.animals.Goat;
import com.sda.exceptions.NoFeedException;
import com.sda.exceptions.NoMoneyException;
import com.sda.exceptions.NoProductException;
import com.sda.exceptions.NoThatAnimalException;
import com.sda.products.ComposedProducts;
import com.sda.products.Products;

public class InterfaceElements {

	private static Scanner scanner = new Scanner(System.in);

	public static void farmStatus(AnimalFarm farm) {
		System.out.println("-----------------------------------------------------------");
		System.out.println("Stan farmy:");
		System.out.println("Pieniądze: " + farm.getBalance());
		System.out.println("------ ZWIERZĘTA ------");
		System.out.println("Kozy: " + farm.howManyAnimals(AnimalKinds.GOAT));
		System.out.println("Krowy: " + farm.howManyAnimals(AnimalKinds.COW));
		System.out.println("Kaczki: " + farm.howManyAnimals(AnimalKinds.DUCK));
		System.out.println("Kury: " + farm.howManyAnimals(AnimalKinds.CHICKEN));
		System.out.println("------ PASZA------");
		System.out.println("Zwykła pasza: " + farm.getRegularFeedBalance());
		System.out.println("Super pasza: " + farm.getSuperFeedBalance());
		System.out.println("------ PRODUKTY ------");
		System.out.println("Kozie mleko: " + farm.howManyProducts(Products.GOATSMILK));
		System.out.println("Krowie mleko: " + farm.howManyProducts(Products.COWSMILK));
		System.out.println("Kacze jaja: " + farm.howManyProducts(Products.DUCKSEGG));
		System.out.println("Kurze jaja: " + farm.howManyProducts(Products.CHICKENSEGG));
		System.out.println("------ PRZETWORY ------");
		System.out.println("Kozi ser: " + farm.howManyProducts(ComposedProducts.GOATSCHEESE));
		System.out.println("Krowi ser: " + farm.howManyProducts(ComposedProducts.COWSCHEESE));
		System.out.println("Kaczy omlet: " + farm.howManyProducts(ComposedProducts.DUCKSOMLET));
		System.out.println("Kurzy omlet: " + farm.howManyProducts(ComposedProducts.CHICKENSOMLET));
		System.out.println("-----------------------------------------------------------");
	}

	public static void farmActions(AnimalFarm farm) {
		System.out.println("");
		System.out.println("Kup zwierzę (1)");
		System.out.println("Nakarm zwierzę (2)");
		System.out.println("Sprzedaj zwierzę (3)");
		System.out.println("Sprzedaj produkty (4)");
		System.out.println("Zrób przetwory: (5)");
		System.out.println("Sprzedaj przetwory: (6)");
		System.out.println("Kup paszę: (7)");
		System.out.println("Wyświetl stan farmy: (8)");
		System.out.println("Zakończ: (0)");
		System.out.println("");
	}

	public static void animalsToBuy(AnimalFarm farm) {
		System.out.println("------ Zwierzęta do kupienia: -------");
		System.out.println("Koza [$20]: (1)");
		System.out.println("Krowa [$18]: (2)");
		System.out.println("Kaczka [$12]: (3)");
		System.out.println("Kura [$6]: (4)");
		boolean isNotOk = true;
		try {
			while (isNotOk) {
				switch (scanner.nextInt()) {
				case 1:
					farm.buyAnimal(new Goat());
					System.out.println("Koza kupiona.");
					isNotOk = false;
					break;
				case 2:
					farm.buyAnimal(new Cow());
					System.out.println("Krowa kupiona.");
					isNotOk = false;
					break;
				case 3:
					farm.buyAnimal(new Duck());
					System.out.println("Kaczka kupiona.");
					isNotOk = false;
					break;
				case 4:
					farm.buyAnimal(new Chicken());
					System.out.println("Kura kupiona.");
					isNotOk = false;
					break;
				default:
					System.out.println("Niedozwolony wybór, spróbuj ponownie.");
				}
			}
		} catch (NoMoneyException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void feedToBuy(AnimalFarm farm) {

		System.out.println("------ Pasza do kupienia: -------");
		System.out.println("Zwykła pasza [$1]: (1)");
		System.out.println("Super pasza (daje 2x więcej produktów) [$2]: (2)");
		boolean isNotOk = true;
		try {
			while (isNotOk) {
				switch (scanner.nextInt()) {
				case 1:
					farm.buyFeed(Feed.REGULAR);
					System.out.println("Zwykła pasza kupiona.");
					isNotOk = false;
					break;
				case 2:
					farm.buyFeed(Feed.GREAT);
					System.out.println("Super pasza kupiona.");
					isNotOk = false;
					break;
				default:
					System.out.println("Niedozwolony wybór, spróbuj ponownie.");
				}
			}
		} catch (NoMoneyException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void animalsToFeed(AnimalFarm farm) {
		int feed = -1;
		AnimalKinds animal = choiceAnimal(farm);

		while (feed < 1 || feed > 2) {
			if (farm.getRegularFeedBalance() > 0)
				System.out.println("Zwykła pasza: " + farm.getRegularFeedBalance() + " (1)");
			if (farm.getSuperFeedBalance() > 0)
				System.out.println("Super pasza: " + farm.getSuperFeedBalance() + " (2)");
			feed = scanner.nextInt();
			if (feed < 1 || feed > 2)
				System.out.println("Niedozwolony wybór. Spróbuj jeszcze raz.");
		}
		Feed feedType = (feed == 1) ? Feed.REGULAR : Feed.GREAT;

		int index = -1;
		for (int i = 0; i < farm.getAnimalsList().size(); i++) {
			if (farm.getAnimalsList().get(i).getKind() == animal) {
				index = i;
				break;
			}
		}

		try {
			if (index < 0) {
				throw new NoThatAnimalException();
			} else {
				farm.eatFeed(farm.getAnimalsList().get(index), feedType);
				System.out.println("Zwierze nakarmione.");
			}

		} catch (NoFeedException e) {
			System.out.println(e.getMessage());
		} catch (NoThatAnimalException e1) {
			System.out.println(e1.getMessage());

		}

	}

	public static void animalsToSell(AnimalFarm farm) {
		AnimalKinds animal = choiceAnimal(farm);
		int uid = -1;
		for (Animal a : farm.getAnimalsList()) {
			if (a.getKind() == animal) {
				uid = a.getId();
				break;
			}
		}

		try {
			farm.sellAnimal(uid);
			System.out.println("Zwierze sprzedane.");
		} catch (NoThatAnimalException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void productsToSell(AnimalFarm farm) {
		Products product = choiceProduct(farm);
		try {
			farm.sellProduct(product);
			System.out.println(product.getName() + " sprzedane.");
		} catch (NoProductException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void makeComposedProds(AnimalFarm farm) {
		ComposedProducts product = showComposedProduct(farm);
		try {
			farm.makeComposedProduct(product);
			System.out.println(product.getName() + " wytworzony.");

		} catch (NoProductException e) {
			System.out.println("You don't have needed components.");
		}
	}

	public static void composedProdsToSell(AnimalFarm farm) {
		ComposedProducts product = choiceComposedProduct(farm);
		try {
			farm.sellProduct(product);
			System.out.println(product.getName() + " sprzedany.");
		} catch (NoProductException e) {
			System.out.println(e.getMessage());
		}

	}

	private static AnimalKinds choiceAnimal(AnimalFarm farm) {
		int animal = -1;
		AnimalKinds animalType = null;

		while (animal < 1 || animal > 4) {
			System.out.println("------ WYBIERZ ZWIERZĘ ------");
			if (farm.howManyAnimals(AnimalKinds.GOAT) > 0)
				System.out.println("Kozy: " + farm.howManyAnimals(AnimalKinds.GOAT) + " (1)");
			if (farm.howManyAnimals(AnimalKinds.COW) > 0)
				System.out.println("Krowy: " + farm.howManyAnimals(AnimalKinds.COW) + " (2)");
			if (farm.howManyAnimals(AnimalKinds.DUCK) > 0)
				System.out.println("Kaczki: " + farm.howManyAnimals(AnimalKinds.DUCK) + " (3)");
			if (farm.howManyAnimals(AnimalKinds.CHICKEN) > 0)
				System.out.println("Kury: " + farm.howManyAnimals(AnimalKinds.CHICKEN) + " (4)");
			animal = scanner.nextInt();
			if (animal < 1 || animal > 4)
				System.out.println("Niedozwolony wybór. Spróbuj jeszcze raz.");
		}

		switch (animal) {
		case 1:
			animalType = AnimalKinds.GOAT;
			break;
		case 2:
			animalType = AnimalKinds.COW;
			break;
		case 3:
			animalType = AnimalKinds.DUCK;
			break;
		case 4:
			animalType = AnimalKinds.CHICKEN;
			break;
		}

		return animalType;
	}

	private static Products choiceProduct(AnimalFarm farm) {
		int product = -1;
		Products productType = null;
		while (product < 1 || product > 4) {
			System.out.println("------ WYBIERZ PRODUKT ------");
			if (farm.howManyProducts(Products.GOATSMILK) > 0)
				System.out.println("Kozie mleko: " + farm.howManyProducts(Products.GOATSMILK) + " (1)");
			if (farm.howManyProducts(Products.COWSMILK) > 0)
				System.out.println("Krowie mleko: " + farm.howManyProducts(Products.COWSMILK) + " (2)");
			if (farm.howManyProducts(Products.DUCKSEGG) > 0)
				System.out.println("Kacze jaja: " + farm.howManyProducts(Products.DUCKSEGG) + " (3)");
			if (farm.howManyProducts(Products.CHICKENSEGG) > 0)
				System.out.println("Kurze jaja: " + farm.howManyProducts(Products.CHICKENSEGG) + " (4)");
			product = scanner.nextInt();
			if (product < 1 || product > 4)
				System.out.println("Niedozwolony wybór. Spróbuj jeszcze raz.");
		}

		switch (product) {
		case 1:
			productType = Products.GOATSMILK;
			break;
		case 2:
			productType = Products.COWSMILK;
			break;
		case 3:
			productType = Products.DUCKSEGG;
			break;
		case 4:
			productType = Products.CHICKENSEGG;
			break;
		}

		return productType;
	}

	private static ComposedProducts choiceComposedProduct(AnimalFarm farm) {
		int product = -1;
		ComposedProducts productType = null;
		while (product < 1 || product > 4) {
			System.out.println("------ WYBIERZ PRODUKT ------");
			if (farm.howManyProducts(ComposedProducts.GOATSCHEESE) > 0)
				System.out.println("Kozi ser: " + farm.howManyProducts(ComposedProducts.GOATSCHEESE) + " (1)");
			if (farm.howManyProducts(ComposedProducts.COWSCHEESE) > 0)
				System.out.println("Krowi ser: " + farm.howManyProducts(ComposedProducts.COWSCHEESE) + " (2)");
			if (farm.howManyProducts(ComposedProducts.DUCKSOMLET) > 0)
				System.out.println("Kaczy omlet: " + farm.howManyProducts(ComposedProducts.DUCKSOMLET) + " (3)");
			if (farm.howManyProducts(ComposedProducts.CHICKENSOMLET) > 0)
				System.out.println("Kurzy omlet: " + farm.howManyProducts(ComposedProducts.CHICKENSOMLET) + " (4)");
			product = scanner.nextInt();
			if (product < 1 || product > 4)
				System.out.println("Niedozwolony wybór. Spróbuj jeszcze raz.");
		}

		switch (product) {
		case 1:
			productType = ComposedProducts.GOATSCHEESE;
			break;
		case 2:
			productType = ComposedProducts.COWSCHEESE;
			break;
		case 3:
			productType = ComposedProducts.DUCKSOMLET;
			break;
		case 4:
			productType = ComposedProducts.CHICKENSOMLET;
			break;
		}

		return productType;
	}

	private static ComposedProducts showComposedProduct(AnimalFarm farm) {
		int product = -1;
		ComposedProducts productType = null;
		while (product < 1 || product > 4) {
			System.out.println("------ WYBIERZ PRODUKT ------");
			System.out.println("Kozi ser: " + farm.howManyProducts(ComposedProducts.GOATSCHEESE) + " (1)");
			System.out.println("Krowi ser: " + farm.howManyProducts(ComposedProducts.COWSCHEESE) + " (2)");
			System.out.println("Kaczy omlet: " + farm.howManyProducts(ComposedProducts.DUCKSOMLET) + " (3)");
			System.out.println("Kurzy omlet: " + farm.howManyProducts(ComposedProducts.CHICKENSOMLET) + " (4)");
			product = scanner.nextInt();
			if (product < 1 || product > 4)
				System.out.println("Niedozwolony wybór. Spróbuj jeszcze raz.");
		}

		switch (product) {
		case 1:
			productType = ComposedProducts.GOATSCHEESE;
			break;
		case 2:
			productType = ComposedProducts.COWSCHEESE;
			break;
		case 3:
			productType = ComposedProducts.DUCKSOMLET;
			break;
		case 4:
			productType = ComposedProducts.CHICKENSOMLET;
			break;
		}

		return productType;
	}
}
