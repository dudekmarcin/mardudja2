package com.sda.farm;

import java.util.Scanner;

import static com.sda.farm.InterfaceElements.*;

public class MainFarm {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Witaj na farmie!");
		System.out.println("Aby kierować swoją farmą wybieraj numery opcji podane w nawiasach ().");
		System.out.println("Podaj kwotę, z jaką chcesz rozpocząć:");
		int money = scan.nextInt();
		AnimalFarm farm = new AnimalFarm(money);
		System.out.println("Twoja farma została utworzona.");
		farmStatus(farm);
		int i = -1;
		while (i != 0) {
			farmActions(farm);
			switch (scan.nextInt()) {
			case 1:
				animalsToBuy(farm);
				break;
			case 2:
				animalsToFeed(farm);
				break;
			case 3:
				animalsToSell(farm);
				break;
			case 4:
				productsToSell(farm);
				break;
			case 5:
				makeComposedProds(farm);
				break;
			case 6:
				composedProdsToSell(farm);
				break;
			case 7:
				feedToBuy(farm);
				break;
			case 8:
				farmStatus(farm);
				break;
			case 0:
				System.out.println("Do zobaczenia!");
				i = 0;
				break;
			default:
				System.out.println("Niedozwolony wybór. Spróbuj jeszcze raz");
				break;
			}
		}
		
		scan.close();
	}

}
