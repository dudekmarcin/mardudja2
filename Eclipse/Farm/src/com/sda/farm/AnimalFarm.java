package com.sda.farm;

import java.util.ArrayList;
import java.util.List;

import com.sda.animals.Animal;
import com.sda.animals.AnimalKinds;
import com.sda.animals.Feed;
import com.sda.exceptions.NoFeedException;
import com.sda.exceptions.NoMoneyException;
import com.sda.exceptions.NoProductException;
import com.sda.exceptions.NoThatAnimalException;
import com.sda.products.ComposedProducts;
import com.sda.products.Products;

public class AnimalFarm {
	private int money;
	private List<Animal> animals = new ArrayList<>();
	private int regularFeedBalance;
	private int superFeedBalance;
	private List<Products> products = new ArrayList<>();;
	private List<ComposedProducts> composedProducts = new ArrayList<>();;

	public AnimalFarm(int money) {
		this.money = money;
	}

	public int getBalance() {
		return money;
	}

	public List<Animal> getAnimalsList() {
		List<Animal> copy = this.animals;
		return copy;
	}

	public List<Products> getProductsList() {
		List<Products> copy = this.products;
		return copy;
	}
	
	public List<ComposedProducts> getComposedProductsList() {
		List<ComposedProducts> copy = this.composedProducts;
		return copy;
	}

	public int getRegularFeedBalance() {
		return regularFeedBalance;
	}

	public int getSuperFeedBalance() {
		return superFeedBalance;
	}

	public void removeDeadAnimals() {
		for (int i = 0; i < animals.size(); i++) {
			if (animals.get(i).getTimelife() == 0) {
				animals.remove(i);
			}
		}
	}

	public void buyAnimal(Animal animal) throws NoMoneyException {
		if (money >= animal.getValue()) {
			this.animals.add(new Animal(animal.getKind()));
			money -= animal.getValue();
		} else {
			throw new NoMoneyException();
		}
	}

	public void sellAnimal(int uid) throws NoThatAnimalException {
		boolean isAnimal = false;
		for(int i = 0; i < animals.size(); i++) {
			if (animals.get(i).getId() == uid) {
				money += (animals.get(i).getValue() / 2);
				animals.remove(i);
				isAnimal = true;
				break;
			} 
		} 
		if(isAnimal == false) {
				throw new NoThatAnimalException();
		}
	}

	public void buyFeed(Feed feedType) throws NoMoneyException {
		if (money >= feedType.getPrice()) {
			switch (feedType) {
			case GREAT:
				this.money -= 2;
				this.superFeedBalance++;
				break;
			case REGULAR:
				this.money--;
				this.regularFeedBalance++;
				break;
			}
		} else
			throw new NoMoneyException();
	}

	public void eatFeed(Animal animal, Feed feedType) throws NoFeedException {
		switch (feedType) {
		case GREAT:
			if (superFeedBalance > 0) {
				superFeedBalance--;
				for (int i = 0; i < 2; i++) {
					products.add(animal.makeProduct());
				}
			} else
				throw new NoFeedException();

			break;
		case REGULAR:
			if (regularFeedBalance > 0) {
				regularFeedBalance--;
				products.add(animal.makeProduct());
			} else
				throw new NoFeedException();

			break;
		default:
			throw new IllegalArgumentException("Unknown type of feed");
		}
		animal.increaseTimelife();
		removeDeadAnimals();

	}
	
	public void makeComposedProduct(ComposedProducts composedProduct) throws NoProductException {
		int productsNeeded = 0;
		int[] productsNeededIndex = new int[composedProduct.getComponents()];
		for(int i=0;i<products.size();i++) {
			if(products.get(i).equals(composedProduct.getProduct()) && productsNeeded < composedProduct.getComponents()) {
				productsNeeded++;
				productsNeededIndex[productsNeeded-1] = i;
			}
		}
		if(productsNeeded == composedProduct.getComponents()) {
			while(productsNeeded > 0) {
				int i = --productsNeeded;
				products.remove(productsNeededIndex[i]);
			}
			composedProducts.add(composedProduct);
		} else 
			throw new NoProductException();
		
	}

	public void sellProduct(Products product) throws NoProductException {
		if (products.contains(product)) {
			money += (product.getValue());
			products.remove(product);
		} else
			throw new NoProductException();
	}
	//Overloaded
	public void sellProduct(ComposedProducts product) throws NoProductException {
		if (composedProducts.contains(product)) {
			money += (product.getValue());
			composedProducts.remove(product);
		} else
			throw new NoProductException();
	}	
	
	public int howManyAnimals(AnimalKinds animalType) {
		int result = 0;
		for(Animal animal : animals) {
			if(animal.getKind().equals(animalType))
					result++;
		}
		return result;
	}
	
	public int howManyProducts(Products productType) {
		int result = 0;
		for(Products product : products) {
			if(product.equals(productType))
					result++;
		}
		
		return result;
	}
	
	//Overloaded
	public int howManyProducts(ComposedProducts productType) {
		int result = 0;
		for(ComposedProducts product : composedProducts) {
			if(product.equals(productType))
					result++;
		}
		
		return result;
	}
	
}
