package com.sda;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;

public class BasicOptionalOperationsTest {

    @Test
    public void constructor() {
        Optional<Integer> optional = Optional.of(10);
        System.out.println(optional);
        System.out.println(optional.get());

        // optional = Optional.of(null);
        // System.out.println(optional);
        // System.out.println(optional.get());

        Optional<String> nullableOption = Optional.ofNullable("Hello");
        System.out.println("no: " + nullableOption);
        System.out.println("no: " + nullableOption.get());

        nullableOption = Optional.ofNullable(null);
        System.out.println("no: " + nullableOption);
        System.out.println("no: " + nullableOption.get());
    }

    @Test
    public void basics() {
        Optional<String> maybeJohnny = Optional.of("Johnny Cash");
        maybeJohnny.ifPresent(johnny -> System.out.println(johnny));

        Optional<String> maybeJimmy = Optional.ofNullable(null);
        maybeJimmy.ifPresent(johnny -> System.out.println(johnny));

        if (maybeJohnny.isPresent()) {
            System.out.println("I am present!");
        }

        if (maybeJimmy.isPresent()) {
            System.out.println("Jimmy is also present!");
        } else {
            System.out.println("Jimmy is not here!");
        }

        String jimmyValue = maybeJimmy.orElse("Nie mam imienia");
        System.out.println(jimmyValue);
        
        String anotherJimmyValue = maybeJimmy.orElseGet(() -> "W lambdzie tez nie mam imienia");
        System.out.println(anotherJimmyValue);

        try {
            String yetAnotherJimmyValue = maybeJimmy.orElseThrow(() -> new IllegalStateException("Blad! brak imienia"));
        } catch (IllegalStateException ise) {
            System.out.println(ise);
        }
        

        maybeJohnny.filter(name -> name.equals("Johnny"));
        System.out.println(maybeJohnny);

        Optional<String> filteredJohnny = maybeJohnny.filter(name -> name.contains("Johnny"));
        System.out.println(filteredJohnny);
        Optional<String> notJohnny = maybeJohnny.filter(name -> !name.contains("Johnny"));
        System.out.println(notJohnny);

        //regular way to do
        maybeJohnny
            .filter(name -> name.contains("Johnny"))
            .ifPresent(name -> System.out.println(name));
    }

    /**
     * Wykonaj polecenia i napisz testy - o ile to mozliwe.
     */
    @Test
    public void basicExercises() {
      	int number = 10;
		Set<Integer> set = new HashSet<>();
		Set<Integer> setTest = new HashSet<>(Arrays.asList(1,2,5,10));

    	Optional<Integer> startingPoint = Optional.ofNullable(number);
    	assertThat(startingPoint.get(), is(10));
    	Optional<Integer> isEven = startingPoint.filter(n -> n%2==0);
    	startingPoint.ifPresent(n -> {
    		for(int i=n;i>0;i--){
    			if(n%i==0) {
    				set.add(i);
    			}
    		}
    		System.out.println(set.toString());
    	});
    	
    	assertTrue(set.containsAll(setTest));
    	
    	int endPoint = startingPoint.orElseGet(() -> 42);
    	assertThat(endPoint, is(10));
    	startingPoint = Optional.ofNullable(null);
    	endPoint = startingPoint.orElseGet(() -> 42);
    	assertThat(endPoint, is(42));

    	
    	/**
         * Stworz opcje StartingPoint, ktora bedzie mogla przechowywac wartosci typu Integer.
         * A nastepnie na bazie tej opcji:
         *  - Stworz opcje, ktora przechowuje wynik predykatu sprawdzajacego,
         *     czy wartosc w StartingPoint jest parzysta.
         *  - Stworz opcje, ktora - jesli w StartingPoint jest przechowywana wartosc - wypisze 
         *   do System.out Set posiadajacy wszystkie jej dzielniki naturalne.
         *  - Do zmiennej EndPoint przypisz wartosc przechowywana w StartingPoint. Jesli w starting point
         *  nie ma zadnej wartosci powinna przypisywac wartosc odpowiedzi na wszystko - liczbe 42.
         */
    }

}
