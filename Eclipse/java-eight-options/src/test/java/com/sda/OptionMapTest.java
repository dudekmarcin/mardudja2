package com.sda;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import java.util.Optional;
import java.util.function.Function;
import org.junit.Test;

public class OptionMapTest {

	@Test
	public void example() {
		Optional<Integer> theAnswer = Optional.of(42);
		Optional<Integer> squareTheAnswer = this.square(theAnswer);
	}

	private Optional<Integer> square(Optional<Integer> opt) {
		Optional<Integer> result = Optional.empty();
		if (opt.isPresent()) {
			int value = opt.get();
			result = Optional.of(value * value);
		}
		return result;
	}

	/**
	 * Uzupelnij metode pomocnicza "getDigitsOption", ktora jako argument bierze
	 * Optional<Integer>, zwraca rowniez Optional<Integer> reprezentujacy
	 * mozliwa liczbe cyfr wystepujaca w liczbie. tj: Optional.of(123) ->
	 * Optional.of(3) Optional.of(0) -> Optional.of(1) Optional.of(-5) ->
	 * Optional.of(1)
	 * 
	 * Nastepnie przetestuj stworzona przez siebie metode.
	 */
	@Test
	public void digitsTest() {
		Optional<Integer> someNumber = Optional.ofNullable(null);
		Optional<Integer> result = getDigitsOption(someNumber);
		assertFalse(result.isPresent());
		someNumber = Optional.ofNullable(-123);
		result = getDigitsOption(someNumber);
		assertThat(result.get(), is(3));
		someNumber = Optional.ofNullable(0);
		result = getDigitsOption(someNumber);
		assertThat(result.get(), is(1));
		someNumber = Optional.ofNullable(12345);
		result = getDigitsOption(someNumber);
		assertThat(result.get(), is(5));

	}

	private Optional<Integer> getDigitsOption(Optional<Integer> opt) {
		return intMap(opt, n -> {
			Integer temp = Math.abs(n);
			String string = temp.toString();
			return string.length();
		});

		/*
		 * Optional<Integer> result = Optional.empty(); if(opt.isPresent()) {
		 * int number = opt.get(); Integer absNumber = Math.abs(number); String
		 * numberOfNumbers = absNumber.toString();
		 * 
		 * result = Optional.of(numberOfNumbers.length()); } return result;
		 */
	}

	/**
	 * Spojrz na metody #getDigitsOption oraz #square. So do siebie podobne:
	 * Kazda z nich powinna sprawdzic czy wartosc opcji istnieje, jesli tak
	 * powina zwrocic nowa opcje zawierajaca wynik pewnej operacji (hint:
	 * lambdy/funkcji) (dla square - kwadratu liczby, dla getDigitsOption -
	 * liczby cyfr), jezeli nie pusta opcje. Sprobuj uzupenic metode intMap, w
	 * taki sposob by przy jej pomocy mozna bylo zapisac #getDigitsOption oraz
	 * #square
	 *
	 * Nastepnie przy pomocy testow sprawdz, czy metody intMap (wraz z
	 * odpowiednia lambda) oraz square i getDigitsOption zwracaja zawsze te same
	 * wyniki.
	 */
	@Test
	public void intMapTest() {
		intMapTestHelper(Optional.ofNullable(13));
		intMapTestHelper(Optional.ofNullable(1));
		intMapTestHelper(Optional.ofNullable(-3));
		intMapTestHelper(Optional.ofNullable(0));
		intMapTestHelper(Optional.ofNullable(null));
	}

	private void intMapTestHelper(Optional<Integer> opt) {
		Function<Integer, Integer> squareFunction = n -> n * n;
		Function<Integer, Integer> digitsFunction = n -> {
			Integer temp = Math.abs(n);
			String string = temp.toString();
			return string.length();
		};

		Optional<Integer> squareIntMap = intMap(opt, squareFunction);
		Optional<Integer> digitsIntMap = intMap(opt, digitsFunction);
		if (opt.isPresent()) {
			assertThat(squareIntMap.get(), is(square(opt).get()));
			assertThat(digitsIntMap.get(), is(getDigitsOption(opt).get()));
		} else {
			assertThat(squareIntMap, is(square(opt)));
			assertThat(digitsIntMap, is(getDigitsOption(opt)));
		}
	}

	private Optional<Integer> intMap(Optional<Integer> option, Function<Integer, Integer> func) {
		Optional<Integer> result = Optional.empty();
		if (option.isPresent()) {
			result = Optional.of(func.apply(option.get()));
		}

		return result;
	}

	/**
	 * Spojrz na stringLengthOption. Ma ona z przekazanej opcji Stringa zwracac
	 * opcje reprezentujaca dlugosc tego Stringa. Sprobuj ja uzupelnic oraz
	 * napisac test.
	 */
	@Test
	public void stringLengthOptionTest() {
		Optional<String> stringOpt = Optional.ofNullable("Some string");
		Optional<Integer> stringLength = stringLengthOption(stringOpt);
		assertThat(stringLength.get(), is(11));
		stringOpt = Optional.ofNullable(null);
		stringLength = stringLengthOption(stringOpt);
		assertThat(stringLength, is(Optional.empty()));
		stringOpt = Optional.ofNullable("");
		stringLength = stringLengthOption(stringOpt);
		assertThat(stringLength.get(), is(0));

	}

	private Optional<Integer> stringLengthOption(Optional<String> opt) {
		Optional<Integer> result = Optional.empty();
		if(opt.isPresent()) {
		Function<String, Optional<Integer>> stringLength = s -> Optional.ofNullable(s.length());
		result = stringLength.apply(opt.get());
		}
		return result;
	}

	/**
	 * Okej teraz czas wysylic sporo szarych komorek. Przypomnij sobie interface
	 * QuadraFunction i TriFunction z poprzenich zajec. Jak pamietasz dzieki tym
	 * interfaceom moglismy stworzyc lambde dla dowolnych typow.
	 * 
	 * Teraz poprzez zebrana wiedze z poprzednich zadan oraz powyzszego faktu
	 * sprobuj stworzyc metode #ourMap, ktora rowniez bedzie generyczna, tj: -
	 * Jako pierwszy argument bedzie brala opcje dowolnego typu - Jako drugi
	 * argument bedzie brala lambde (Function), ktora przyjmuje arugment typu
	 * pierwszego argumentu, z zwraca typ wyniku. - Jako wynik bedzie zwracaja
	 * opcje dowolnego typu
	 * 
	 * Jest to zadanie trudniejsze i moga pojawiac sie problemu - zarowno ze
	 * strony IDE/kompilatora jak czysto koncepcyjne. Pytajcie.
	 */
	
	
	
	@Test
	public void testOurMap() {
		Function<Integer, Integer> squareFunction = n -> n * n;
		Function<Integer, Integer> digitsFunction = n -> {
			Integer temp = Math.abs(n);
			String string = temp.toString();
			return string.length();
		//Function<String, Optional<Integer>> stringLenght = s -> Optional.of(s.length());
		};
		Optional<Integer> opt = Optional.ofNullable(13);
		Optional<Integer> squareIntMap = ourMap(opt, squareFunction);
		//Optional<Integer> squareIntMap = ourMap<Integer>(Optional.of(12), squareFunction);
		System.out.println(squareIntMap.get());
		Optional<Integer> digitsIntMap = intMap(opt, digitsFunction);
		if (opt.isPresent()) {
			assertThat(squareIntMap.get(), is(square(opt).get()));
			assertThat(digitsIntMap.get(), is(getDigitsOption(opt).get()));
		} else {
			assertThat(squareIntMap, is(square(opt)));
			assertThat(digitsIntMap, is(getDigitsOption(opt)));
		}
		
		Optional<String> stringOpt = Optional.ofNullable("Some string");
		Optional<Integer> stringLength = stringLengthOption(stringOpt);
		Optional<Integer> stringOurMap = ourMap(stringOpt, s -> s.length());
		assertThat(stringLength.get(), is(stringOurMap.get()));
		
	}
	
	private <T, R> Optional<R> ourMap(Optional<T> opt, Function<T, R> func) {
        Optional<R> result = Optional.empty();
        if (opt.isPresent()) {
            result = Optional.of(func.apply(opt.get()));
        }
        return result;
    }
}
