package zad4;

public class Hybrid extends Bus implements Electric, LiquidFuel {

	private double range;
	private int emissionTier;
	
	public Hybrid(int id, int capacity, int cost, double range, int emissionTier) {
		super(id, capacity, cost);
		this.range = range;
		this.emissionTier = emissionTier;
	}

	@Override
	public double getRange() {
		return this.range;
	}

	@Override
	public int getEmissionTier() {
		return this.emissionTier;
	}

	@Override
	public int getVoltage() {
		return 0;
	}

	@Override
	public int getAccel() {
		// TODO Auto-generated method stub
		return 0;
	}

}
