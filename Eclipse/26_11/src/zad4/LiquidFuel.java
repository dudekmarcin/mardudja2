package zad4;

public interface LiquidFuel {

	public double getRange();
	public int getEmissionTier();
	
}
