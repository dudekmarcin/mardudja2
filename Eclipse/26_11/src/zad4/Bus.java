package zad4;

public abstract class Bus {

	private int id;
	private int capacity;
	private int cost;
	
	public Bus(int id, int capacity, int cost) {
		this.id = id;
		this.capacity = capacity;
		this.cost = cost;
	}

	public abstract int getAccel();
	
}
