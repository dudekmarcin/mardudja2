package zad4;

public interface Electric {

	public final int HIGH_VOLTAGE = 10000;
	public final int LOW_VOLTAGE = 100;
	
	public int getVoltage();
}
