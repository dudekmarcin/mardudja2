package zad4;

public enum Voltage {
	LOW_VOLTAGE(100),
	HIGH_VOLTAGE(1000);
	
	private final int voltage;
	
	private Voltage(int voltage) {
		this.voltage = voltage;
	}
	
	public int getVoltage() {
		return this.voltage;
	}
}
