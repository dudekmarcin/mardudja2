package zad3;

public class MovableCircle implements Movable {

	private int radius;
	private MovablePoint center;

	public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius) {
		this.center = new MovablePoint(x, y, xSpeed, ySpeed);
		this.radius = radius;
	}

	@Override
	public void moveUp() {
		this.center.moveUp();
	}

	@Override
	public void moveDown() {
		this.center.moveDown();

	}

	@Override
	public void moveLeft() {
		this.center.moveLeft();
	}

	@Override
	public void moveRight() {
		this.center.moveRight();
	}

//	@Override
//	public String toString() {
////		return "Circle[Center[x=" + this.center. + ", y" + this.center.y + ", xSpeed=" + this.center.xSpeed
////				+ ", ySpeed=" + this.center.ySpeed + "], radius=" + this.radius + "]";

//	}

}
