package zad3;

public class Point {

	protected double x;
	protected double y;
	
	public Point() {
		this.x = 0.0;
		this.y = 0.0;
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return this.x;
	}

	public void setX(double d) {
		this.x = d;
	}

	public double getY() {
		return this.y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public double[] getXY() {
		double[] result = new double[] {this.x, this.y};
		return result;
	}

	public void setXY(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "("+this.x+","+this.y+")";
	}
	
}

