package zad3;

public class MovablePoint3D extends Point3D implements Movable {

	protected int xSpeed;
	protected int ySpeed;
	protected int zSpeed;
	
	public MovablePoint3D(double x, double y, double z, int xSpeed, int ySpeed, int zSpeed) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;
		this.zSpeed = zSpeed;
	}
	
	public int getXSpeed() {
		return this.xSpeed;
	}
	
	public int getYSpeed() {
		return this.ySpeed;
	}
	
	public int getZSpeed() {
		return this.zSpeed;
	}
	
	@Override
	public String toString() {
		return "MovablePoint[x="+this.x+", y="+this.y+ ", z="+this.z+", xSpeed="+this.xSpeed+", ySpeed="+this.ySpeed+", zSpeed="+this.ySpeed+"]";
	}

	@Override
	public void moveUp() {
		++this.y;		
	}

	@Override
	public void moveDown() {
		--this.y;
	}

	@Override
	public void moveLeft() {
		--this.x;
	}

	@Override
	public void moveRight() {
		++this.x;
	}
	
	public void moveFront() {
		--this.z;
	}
	
	public void moveBack() {
		++this.z;
	}
	
	public void move(double x, double y, double z) {
		this.x += x;
		this.y += y;
		this.z += z;
	}
	
	public double getHeight(double y) {
		return Math.abs(y - this.y);
	}

}
