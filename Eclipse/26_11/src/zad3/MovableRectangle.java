package zad3;

public class MovableRectangle implements Movable {

	private MovablePoint topLeft;
	private MovablePoint bottomRight;

	public MovableRectangle(int x1, int y1, int x2, int y2, int xSpeed, int ySpeed) {
		this.topLeft = new MovablePoint(x1, y1, xSpeed, ySpeed);
		this.bottomRight = new MovablePoint(x2, y2, xSpeed, ySpeed);
	}

	@Override
	public void moveUp() {
		this.topLeft.moveUp();
		this.bottomRight.moveUp();
	}

	@Override
	public void moveDown() {
		this.topLeft.moveDown();
		this.bottomRight.moveDown();
	}

	@Override
	public void moveLeft() {
		this.topLeft.moveLeft();
		this.bottomRight.moveLeft();
	}

	@Override
	public void moveRight() {
		this.topLeft.moveRight();
		this.bottomRight.moveRight();
	}

	@Override
	public String toString() {
		return "Rectangle[TopLeft[x=" + this.topLeft.x + ", y=" + this.topLeft.y + "], BottomRight[x="
				+ this.bottomRight.x + ", y=" + this.bottomRight.y + "], xSpeed=" + this.topLeft.xSpeed + ", ySpeed="
				+ this.topLeft.ySpeed + "]";
	}

	public double getWidth() {
		return Math.abs(this.bottomRight.x - this.topLeft.x);
	}

	public double getHeight() {
		return Math.abs(this.topLeft.y - this.bottomRight.y);
	}
	
	public double[] getSize() {
		double[] size = new double[] {this.getWidth(), this.getHeight() };
		return size;
	}
	
	public double getArea() {
		return this.getHeight() * this.getWidth();
	}
	
	public double getPerimeter() {
		return 2 * this.getHeight() + 2 * this.getWidth();
	}
	
}
