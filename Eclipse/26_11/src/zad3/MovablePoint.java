package zad3;

public class MovablePoint extends Point implements Movable {

	protected int xSpeed;
	protected int ySpeed;
	
	public MovablePoint(double x, double y, int xSpeed, int ySpeed) {
		this.x = x;
		this.y = y;
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;
	}
	
	public int getXSpeed() {
		return this.xSpeed;
	}
	
	public int getYSpeed() {
		return this.ySpeed;
	}
	
	@Override
	public String toString() {
		return "MovablePoint[x="+this.x+", y"+this.y+", xSpeed="+this.xSpeed+", ySpeed="+this.ySpeed+"]";
	}

	@Override
	public void moveUp() {
		++this.y;		
	}

	@Override
	public void moveDown() {
		--this.y;
	}

	@Override
	public void moveLeft() {
		--this.x;
	}

	@Override
	public void moveRight() {
		++this.x;
	}
	
	public void move(double x, double y) {
		this.x += x;
		this.y += y;
	}
}
