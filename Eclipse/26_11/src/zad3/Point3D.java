package zad3;

public class Point3D extends Point {

	protected double z;
	
	public Point3D() {
		super();
		this.z = 0.0;
	}
	
	public Point3D(double x, double y, double z) {
			super(x, y);
			this.z = z;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public void setZ(double z) {
		this.z = z;
	}
	
	public double[] getXYZ() {
		double[] result = new double[] {this.x, this.y, this.z};
		return result;
	}

	public void setXYZ(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public String toString() {
		return "("+this.x+","+this.y+", "+this.z+")";
	}
	
}
