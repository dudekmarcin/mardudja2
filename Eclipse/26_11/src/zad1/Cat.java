package zad1;

public class Cat implements Animal, Soundable {

	@Override
	public void makeSounds() {
		System.out.println("Meow");		
	}

	@Override
	public int getNumOfLegs() {
		return 4;
	}
	
	@Override
	public String getEnv() {
		return "Ląd";
	}
	
	public void climb() {
		System.out.println("Kot wszedł na drzewo");
	}

}
