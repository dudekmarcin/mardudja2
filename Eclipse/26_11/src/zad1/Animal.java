package zad1;

public interface Animal {
	
	public int getNumOfLegs();
	
	public String getEnv();
}
