package zad1;

public class Dog implements Animal, Soundable {

	@Override
	public void makeSounds() {
		System.out.println("Bark");
	}

	@Override
	public int getNumOfLegs() {
		return 4;
	}

	@Override
	public String getEnv() {
		return "Ląd";
	}
	
	public void aport() {
		System.out.println("Pies zaaportował");
	}
	
	public void swim() {
		System.out.println("Pies pływa");
	}
	
}
