package zad2;

public class Circle implements GeometricObject {

	protected double radius = 1.0;
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	@Override
	public double getPerimeter() {
		return 2*Math.PI*this.radius;
	}

	@Override
	public double getArea() {
		return Math.PI*this.radius*this.radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}

	
}
