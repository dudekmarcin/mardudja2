package reponame;

public class RepoName {

	private String name;
	private String secondName;

	public RepoName(String name, String secondName) {
		this.name = name.toLowerCase();
		this.secondName = secondName.toLowerCase();
	}
	
	public String getName() {
		return this.name;
	}

	public String getSecondName() {
		return this.secondName;
	}

	public void setName(String name) {
		this.name = name.toLowerCase();
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName.toLowerCase();
	}

	public String getRepoName() {
		return (this.name.substring(2, 3).equals(this.secondName.substring(2, 3)))
				? this.name.substring(0, 1).toUpperCase() + this.name.substring(1, 4)
						+ this.secondName.substring(0, 1).toUpperCase() + this.secondName.substring(1, 4) + "JA2"
				: this.name.substring(0, 1).toUpperCase() + this.name.substring(1, 3)
						+ this.secondName.substring(0, 1).toUpperCase() + this.secondName.substring(1, 3) + "JA2";
	}
	
	public static String getRepoName(String name, String secondName) {
		return (name.substring(2, 3).equals(secondName.substring(2, 3)))
				? name.substring(0, 1).toUpperCase() + name.substring(1, 4)
						+ secondName.substring(0, 1).toUpperCase() + secondName.substring(1, 4) + "JA2"
				: name.substring(0, 1).toUpperCase() + name.substring(1, 3)
						+ secondName.substring(0, 1).toUpperCase() + secondName.substring(1, 3) + "JA2";
	}
}
