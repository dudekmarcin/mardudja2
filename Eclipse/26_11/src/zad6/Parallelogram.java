package zad6;

import zad3.MovablePoint;
import zad3.Point;

public class Parallelogram extends Trapeze implements Figure {

	public Parallelogram(Point p1, Point p2, Point p3, Point p4) {
		super(p1, p2, p3, p4);
	}
	
	public Parallelogram(MovablePoint p1, MovablePoint p2, MovablePoint p3, MovablePoint p4) {
		super(p1, p2, p3, p4);
	}
	
	public Parallelogram(double a, double b, double h) {
		super(a, a, b, b, h);
	}

	public Parallelogram(double a, double b) {
		super(a, a, b, b);
	}

	@Override
	public String toString() {
		return "Równoległobok[\n bok a: "+ this.a
				+"\n bok b: " + this.b
				+"\n bok c: " + this.c 
				+"\n bok d: " + this.d
				+"\n punkt AC: " + this.p1
				+"\n punkt AD: " + this.p2
				+"\n puntk BC: " + this.p3
				+"\n punkt BD: " + this.p4
				+"\n wysokość: " + this.h
				+"\n pole: " + this.getArea()
				+"\n obwód: " + this.getPeremiter()
				+"]";
	}


}
