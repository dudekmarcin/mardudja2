package zad6;

import zad3.MovablePoint;

public class Circle implements Figure {
	private MovablePoint center;
	private double radius;
	
	public Circle(double radius) {
		this.radius = radius;
		this.center = new MovablePoint(0, 0, 0, 0);
	}
	
	public Circle(int x, int y, double radius) {
		this.radius = radius;
		this.center = new MovablePoint(x,y,0,0);
	}
	
	public Circle(MovablePoint center, double radius) {
		this.radius = radius;
		this.center = center;
	}
		
	public double getRadius() {
		return this.radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public MovablePoint getCenter() {
		return this.center;
	}
	
	public void setCenter(MovablePoint center) {
		this.center = center;
	}
	
	public void setCenter(int x, int y) {
		this.center = new MovablePoint(x,y,0,0);
	}
		
	@Override
	public double getArea() {
		return Math.PI * this.radius * this.radius;
	}

	@Override
	public double getPeremiter() {
		return 2 * Math.PI * this.radius;
	}
	
	@Override
	public int getSidesNumber() {
		return 0;
	}
	
	@Override
	public int getNodesNumber() {
		return 0;
	}
	
	public void moveUp() {
		this.center.moveUp();	
	}

	public void moveDown() {
		this.center.moveDown();	
	}

	public void moveLeft() {
		this.center.moveLeft();
	}

	public void moveRight() {
		this.center.moveRight();
	}
	
	public void move(int x, int y) {
		this.center.move(x, y);
	}

	@Override
	public String toString() {
		return "Koło[\n promień a: "+ this.radius
				+"\n środek: " + this.center
				+"\n pole: " + this.getArea()
				+"\n obwód: " + this.getPeremiter()
				+"]";
	}
	
}
