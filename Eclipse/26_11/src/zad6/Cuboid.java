package zad6;

import zad3.MovablePoint;
import zad3.MovablePoint3D;
import zad3.Point;
import zad3.Point3D;

public class Cuboid implements Figure, Figure3D {

	protected MovablePoint3D frontTopLeft;
	protected MovablePoint3D frontTopRight;
	protected MovablePoint3D frontBottomRight;
	protected MovablePoint3D frontBottomLeft;
	protected MovablePoint3D backTopRight;
	protected MovablePoint3D backTopLeft;
	protected MovablePoint3D backBottomRight;
	protected MovablePoint3D backBottomLeft;
	
	protected Rectangle base;
	protected Rectangle front;
	protected Rectangle side;
	
	public Cuboid(MovablePoint3D frontTopLeft, MovablePoint3D frontTopRight, MovablePoint3D frontBottomRight, MovablePoint3D backTopRight) {
		this.frontTopLeft = frontTopLeft;
		this.frontTopRight = frontTopRight;
		this.frontBottomRight = frontBottomRight;
		this.backTopRight = backTopRight;
		this.frontBottomLeft = new MovablePoint3D(frontTopLeft.getX(), frontBottomRight.getY(), frontBottomRight.getZ(), 0, 0, 0);
		this.backTopLeft = new MovablePoint3D(frontTopLeft.getX(), frontTopLeft.getY(), backTopRight.getZ(), 0, 0, 0);
		this.backBottomRight = new MovablePoint3D(frontBottomRight.getX(), frontBottomRight.getY(), backTopRight.getZ(), 0, 0, 0);
		this.backBottomLeft = new MovablePoint3D(frontBottomLeft.getX(), frontBottomLeft.getY(), backTopRight.getZ(), 0, 0, 0);
		this.base = new Rectangle(
				new MovablePoint(frontBottomLeft.getX(), frontBottomLeft.getY(), frontBottomLeft.getXSpeed(), frontBottomLeft.getYSpeed()), 
				new MovablePoint(frontBottomRight.getX(), frontBottomRight.getY(), frontBottomRight.getXSpeed(), frontBottomRight.getYSpeed()),
				new MovablePoint(backBottomLeft.getX(), backBottomLeft.getY(), backBottomLeft.getXSpeed(), backBottomLeft.getYSpeed()),
				new MovablePoint(backBottomRight.getX(), backBottomRight.getY(), backBottomRight.getXSpeed(), backBottomRight.getYSpeed()));
		this.front = new Rectangle(
				new MovablePoint(frontBottomLeft.getX(), frontBottomLeft.getY(), frontBottomLeft.getXSpeed(), frontBottomLeft.getYSpeed()), 
				new MovablePoint(frontBottomRight.getX(), frontBottomRight.getY(), frontBottomRight.getXSpeed(), frontBottomRight.getYSpeed()),
				new MovablePoint(frontTopLeft.getX(), frontTopLeft.getY(), frontTopLeft.getXSpeed(), frontTopLeft.getYSpeed()), 
				new MovablePoint(frontTopRight.getX(), frontTopRight.getY(), frontTopRight.getXSpeed(), frontTopRight.getYSpeed()));
		this.side = new Rectangle(
				new MovablePoint(frontBottomRight.getX(), frontBottomRight.getY(), frontBottomRight.getXSpeed(), frontBottomRight.getYSpeed()),
				new MovablePoint(frontTopRight.getX(), frontTopRight.getY(), frontTopRight.getXSpeed(), frontTopRight.getYSpeed()),
				new MovablePoint(backBottomRight.getX(), backBottomRight.getY(), backBottomRight.getXSpeed(), backBottomRight.getYSpeed()),
				new MovablePoint(backTopRight.getX(),backTopRight.getY(), backTopRight.getXSpeed(), backTopRight.getYSpeed())
				
				);
	
	
	}
	
	public Cuboid(Rectangle base, Rectangle front, Rectangle side) {
		this.base = base;
		this.front = front;
		this.side = side;
	}
	
	@Override
	public double getArea() {
		return 2 * this.base.getArea() + 2 * front.getArea() + 2 * side.getArea();
	}
	
	@Override
	public double getPeremiter() {
		return this.base.getPeremiter() + this.side.getPeremiter() + this.front.getPeremiter();
	}
	
	@Override
	public int getSidesNumber() {
		return 12;
	}
	
	@Override
	public int getNodesNumber() {
		return 8;
	}
	
	
	@Override
	public double getVolume() {
		// TODO Auto-generated method stub
		return this.side.getArea() * this.front.getB();
	}
	
	public void moveUp() {
		this.frontTopLeft.moveUp();
		this.frontTopRight.moveUp();
		this.frontBottomRight.moveUp();
		this.frontBottomLeft.moveUp();
		this.backTopRight.moveUp();
		this.backTopLeft.moveUp();
		this.backBottomRight.moveUp();
		this.backBottomLeft.moveUp();
	}

	public void moveDown() {
		this.frontTopLeft.moveDown();
		this.frontTopRight.moveDown();
		this.frontBottomRight.moveDown();
		this.frontBottomLeft.moveDown();
		this.backTopRight.moveDown();
		this.backTopLeft.moveDown();
		this.backBottomRight.moveDown();
		this.backBottomLeft.moveDown();
	}

	public void moveLeft() {
		this.frontTopLeft.moveLeft();
		this.frontTopRight.moveLeft();
		this.frontBottomRight.moveLeft();
		this.frontBottomLeft.moveLeft();
		this.backTopRight.moveLeft();
		this.backTopLeft.moveLeft();
		this.backBottomRight.moveLeft();
		this.backBottomLeft.moveLeft();
	}

	public void moveRight() {
		this.frontTopLeft.moveRight();
		this.frontTopRight.moveRight();
		this.frontBottomRight.moveRight();
		this.frontBottomLeft.moveRight();
		this.backTopRight.moveRight();
		this.backTopLeft.moveRight();
		this.backBottomRight.moveRight();
		this.backBottomLeft.moveRight();
	}
	
	public void moveFront() {
		this.frontTopLeft.moveFront();
		this.frontTopRight.moveFront();
		this.frontBottomRight.moveFront();
		this.frontBottomLeft.moveFront();
		this.backTopRight.moveFront();
		this.backTopLeft.moveFront();
		this.backBottomRight.moveFront();
		this.backBottomLeft.moveFront();
	}
	
	public void moveBack() {
		this.frontTopLeft.moveBack();
		this.frontTopRight.moveBack();
		this.frontBottomRight.moveBack();
		this.frontBottomLeft.moveBack();
		this.backTopRight.moveBack();
		this.backTopLeft.moveBack();
		this.backBottomRight.moveBack();
		this.backBottomLeft.moveBack();
	}
	
	public void move(int x, int y, int z) {
		this.frontTopLeft.move(x, y, z);
		this.frontTopRight.move(x, y, z);
		this.frontBottomRight.move(x, y, z);
		this.frontBottomLeft.move(x, y, z);
		this.backTopRight.move(x, y, z);
		this.backTopLeft.move(x, y, z);
		this.backBottomRight.move(x, y, z);
		this.backBottomLeft.move(x, y, z);
	}

	@Override
	public void rotate(double angleXY, double angleXZ) {
		
//		double r = Math.sqrt((base.getA() * base.getA()) + (front.getB() * front.getB()) + (base.getB() * base.getB()));
//		this.frontTopLeft.setX(r * Math.cos(angleXY) * Math.sin(angleXZ));
//		this.frontTopLeft.setY(r * Math.sin(angleXY) * Math.sin(angleXZ));
//		this.frontTopLeft.setZ(r * Math.cos(angleXZ));
//
//		this.frontTopRight.setX(r * Math.cos(angleXY) * Math.sin(angleXZ));
//		this.frontTopRight.setY(r * Math.sin(angleXY) * Math.sin(angleXZ));
//		this.frontTopRight.setZ(r * Math.cos(angleXZ));
//
//		this.frontBottomRight.setX(r * Math.cos(angleXY) * Math.sin(angleXZ));
//		this.frontBottomRight.setY(r * Math.sin(angleXY) * Math.sin(angleXZ));
//		this.frontBottomRight.setZ(r * Math.cos(angleXZ));
//
//		
//		this.frontBottomLeft.move(x, y, z);
//		this.backTopRight.move(x, y, z);
//		this.backTopLeft.move(x, y, z);
//		this.backBottomRight.move(x, y, z);
//		this.backBottomLeft.move(x, y, z);
//		
//		p1.setX(p1.getX() * Math.cos(angle));
//		p1.setY(p1.getY() * Math.sin(angle));
//		
//		p2.setX(p2.getX() * Math.cos(angle));
//		p2.setY(p2.getY() * Math.sin(angle));
//		
//		p3.setX(p3.getX() * Math.cos(angle));
//		p3.setY(p3.getY() * Math.sin(angle));
//		
//		p4.setX(p4.getX() * Math.cos(angle));
//		p4.setY(p4.getY() * Math.sin(angle));

	}
	
	@Override
	public String toString() {
		return "Prostopadłościan[\n podstawa a: "+ this.base.getA()
				+"\n podstawa b: " + this.base.getB()
				+"\n wysokość c: " + this.front.getB() 
				+"\n pole: " + this.getArea()
				+"\n obwód: " + this.getPeremiter()
				+"]";
	}
	
	
}
