package zad6;

public interface Figure {

	public double getArea();
	public double getPeremiter();
	public int getSidesNumber();
	public int getNodesNumber();
}
