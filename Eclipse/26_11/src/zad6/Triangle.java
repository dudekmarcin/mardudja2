package zad6;

import zad3.MovablePoint;
import zad3.Point;

public class Triangle implements Figure, Rotable {

	private double a;
	private double b;
	private double c;
	private double height;

	private MovablePoint p1;
	private MovablePoint p2;
	private MovablePoint p3;

	private TriangleType type;

	public Triangle(Point p1, Point p2, Point p3) {
		this.p1 = new MovablePoint(p1.getX(), p1.getY(), 0, 0);
		this.p2 = new MovablePoint(p2.getX(), p2.getY(), 0, 0);
		this.p3 = new MovablePoint(p3.getX(), p3.getY(), 0, 0);
		this.a = countSide(this.p1, this.p2);
		this.b = countSide(this.p1, this.p3);
		this.c = countSide(this.p2, this.p3);
		type = getType();
		this.height = countH();
	}

	public Triangle(MovablePoint p1, MovablePoint p2, MovablePoint p3) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.a = countSide(this.p1, this.p2);
		this.b = countSide(this.p1, this.p3);
		this.c = countSide(this.p2, this.p3);
		type = getType();
		this.height = countH();
	}

	public Triangle(double a, double b, double c, double h) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.height = h;
		this.p1 = new MovablePoint(0, 0, 0, 0);
		this.p2 = new MovablePoint(this.a, 0, 0, 0);
		double p3_x = this.a - Math.sqrt((this.height * this.height) - (this.c * this.c));
		this.p3 = new MovablePoint(p3_x, this.height, 0, 0);
		type = getType();

	}

	public Triangle(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
		type = getType();
		this.height = countH();
		this.p1 = new MovablePoint(0, 0, 0, 0);
		this.p2 = new MovablePoint(this.a, 0, 0, 0);
		double p3_x = this.a - Math.sqrt((this.height * this.height) - (this.c * this.c));
		double p3_y = countH();
		this.p3 = new MovablePoint(p3_x, p3_y, 0, 0);

	}

	private TriangleType getType() {
		return (a == b || b == c || a == c) ? ((a == b && b == c) ? TriangleType.EQUILATERAL : TriangleType.ISOSCELES)
				: TriangleType.SCALENE;
	}

	private double getWidth(MovablePoint a, MovablePoint b) {
		return Math.abs(b.getX() - a.getX());
	}

	private double getHeight(MovablePoint a, MovablePoint b) {
		return Math.abs(a.getY() - b.getY());
	}

	private double countSide(MovablePoint a, MovablePoint b) {
		return Math.sqrt(Math.pow(getWidth(a, b), 2) + Math.pow(getHeight(a, b), 2));
	}

	private double countH() {
		double h = 0;
		switch (this.type) {
		case EQUILATERAL:
			h = (this.a * Math.sqrt(3)) / 2;
			break;
		case ISOSCELES:
			double base = (this.a != this.b) ? ((this.a != this.c) ? this.a : this.b) : this.c;
			double arm = (base == this.a) ? this.b : this.a;
			h = Math.sqrt((arm * arm) - ((base / 2) * (base / 2)));
			break;
		case SCALENE:
			h = (2 * this.getArea()) / 2;
			break;
		}

		return h;

	}

	public double getSideA() {
		return this.a;
	}

	public void setSideA(double sideA) {
		this.a = sideA;
	}

	public double getSideB() {
		return this.b;
	}

	public void setSideB(double sideB) {
		this.b = sideB;
	}

	public double getSideC() {
		return this.c;
	}

	public void setSideC(double sideC) {
		this.c = sideC;
	}

	@Override
	public double getArea() {
		double area = 0;
		if (this.height == 0) {
			double p = 0.5 * (this.a + this.b + this.c);
			area = Math.sqrt((p * (p - this.a) * (p - this.b) * (p - this.c)));
		} else {
			switch (this.type) {
			case EQUILATERAL:
				area = (this.a * this.a * Math.sqrt(3)) / 4;
				break;
			case ISOSCELES:
				double base = (this.a != this.b) ? ((this.a != this.c) ? this.a : this.b) : this.c;
				area = (this.height * base)/2;
			case SCALENE:
				area = (this.height)/2;
			default:
				break;
			}
		}
		return area;

	}

	@Override
	public double getPeremiter() {
		return this.a + this.b + this.c;
	}

	@Override
	public int getSidesNumber() {
		return 3;
	}

	@Override
	public int getNodesNumber() {
		return 3;
	}

	@Override
	public String toString() {
		return "Trójkąt[\n bok a: " + this.a + "\n bok b: " + this.b + "\n bok c: " + this.c + "\n punkt AB: " + this.p1
				+ "\n punkt AC: " + this.p2 + "\n puntk BC: " + this.p3 + "\n wysokość: " + this.height + "\n pole: "
				+ this.getArea() + "\n obwód: " + this.getPeremiter() + "]";
	}
	
	public void moveUp() {
		this.p1.moveUp();	
		this.p2.moveUp();		
		this.p3.moveUp();		
	}

	public void moveDown() {
		this.p1.moveDown();	
		this.p2.moveDown();		
		this.p3.moveDown();		
	}

	public void moveLeft() {
		this.p1.moveLeft();
		this.p2.moveLeft();
		this.p3.moveLeft();
	}

	public void moveRight() {
		this.p1.moveRight();
		this.p2.moveRight();
		this.p3.moveRight();
	}
	
	public void move(int x, int y) {
		this.p1.move(x, y);
		this.p2.move(x, y);
		this.p3.move(x, y);
	}

	@Override
	public void rotate(double angle) {
		p1.setX(p1.getX() * Math.cos(angle));
		p1.setY(p1.getY() * Math.sin(angle));
		
		p2.setX(p2.getX() * Math.cos(angle));
		p2.setY(p2.getY() * Math.sin(angle));
		
		p3.setX(p3.getX() * Math.cos(angle));
		p3.setY(p3.getY() * Math.sin(angle));
	}

}
