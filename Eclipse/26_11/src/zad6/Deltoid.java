package zad6;

import zad3.MovablePoint;
import zad3.Point;

public class Deltoid implements Figure, Rotable {

	private double a;
	private double b;
	private double c;
	private double d;
	private double angleABC;
	
	private MovablePoint p1;
	private MovablePoint p2;
	private MovablePoint p3;
	private MovablePoint p4;

	public Deltoid(Point p1, Point p2, Point p3, Point p4) {
		this.p1 = new MovablePoint(p1.getX(), p1.getY(), 0, 0);
		this.p2 = new MovablePoint(p2.getX(), p2.getY(), 0, 0);
		this.p3 = new MovablePoint(p3.getX(), p3.getY(), 0, 0);
		this.p4 = new MovablePoint(p4.getX(), p4.getY(), 0, 0);
		this.a = countSide(this.p1, this.p2);
		this.b = countSide(this.p3, this.p4);
		this.c = countSide(this.p1, this.p3);
		this.d = countSide(this.p2, this.p4);
	}
	
	public Deltoid(MovablePoint p1, MovablePoint p2, MovablePoint p3, MovablePoint p4) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.p4 = p4;
		this.a = countSide(this.p1, this.p2);
		this.b = countSide(this.p3, this.p4);
		this.c = countSide(this.p1, this.p3);
		this.d = countSide(this.p2, this.p4);
		
	}
	

	public Deltoid(double a, double b, double angleABC) {
		this.a = a;
		this.b = b;
		this.c = a;
		this.d = b;
		this.angleABC = angleABC;
		this.p1 = new MovablePoint(0, 0, 0, 0);
		this.p2 = new MovablePoint(this.a, 0, 0, 0);
		double p3_x = (this.b * Math.sin(90 -(180-angleABC))) / Math.sin(90);
		double p3_y = (this.b * Math.sin((180-angleABC))) / Math.sin(90);
		this.p3 = new MovablePoint(this.a + p3_x, p3_y, 0, 0);
		double p4_x = (this.c * Math.sin(90 -(180-angleABC))) / Math.sin(90);
		double p4_y = (this.c * Math.sin((180-angleABC))) / Math.sin(90);
		this.p4 = new MovablePoint(p4_x, p4_y, 0, 0);
	}
	
	protected double getWidth(MovablePoint a, MovablePoint b) {
		return Math.abs(b.getX() - a.getX());
	}

	protected double getHeight(MovablePoint a, MovablePoint b) {
		return Math.abs(a.getY() - b.getY());
	}
	
	
	protected double countSide(MovablePoint a, MovablePoint b) {
		return Math.sqrt(Math.pow(getWidth(a,b), 2) + Math.pow(getHeight(a,b), 2));
	}


	@Override
	public double getArea() {
		return this.a * this.b * Math.sin(this.angleABC);
	}

	@Override
	public double getPeremiter() {
		return this.a + this.b + this.c + this.d;
	}

	@Override
	public int getSidesNumber() {
		return 4;
	}

	@Override
	public int getNodesNumber() {
		return 4;
	}
	public void moveUp() {
		this.p1.moveUp();	
		this.p2.moveUp();		
		this.p3.moveUp();		
		this.p4.moveUp();		

	}

	public void moveDown() {
		this.p1.moveDown();	
		this.p2.moveDown();		
		this.p3.moveDown();		
		this.p4.moveDown();		

	}

	public void moveLeft() {
		this.p1.moveLeft();
		this.p2.moveLeft();
		this.p3.moveLeft();
		this.p4.moveLeft();

	}

	public void moveRight() {
		this.p1.moveRight();
		this.p2.moveRight();
		this.p3.moveRight();
		this.p4.moveRight();

	}
	
	public void move(int x, int y) {
		this.p1.move(x, y);
		this.p2.move(x, y);
		this.p3.move(x, y);
		this.p4.move(x, y);
	}

	@Override
	public void rotate(double angle) {
		p1.setX(p1.getX() * Math.cos(angle));
		p1.setY(p1.getY() * Math.sin(angle));
		
		p2.setX(p2.getX() * Math.cos(angle));
		p2.setY(p2.getY() * Math.sin(angle));
		
		p3.setX(p3.getX() * Math.cos(angle));
		p3.setY(p3.getY() * Math.sin(angle));
		
		p4.setX(p4.getX() * Math.cos(angle));
		p4.setY(p4.getY() * Math.sin(angle));

	}
	
	@Override
	public String toString() {
		return "Deltoid[\n bok a: "+ this.a
				+"\n bok b: " + this.b
				+"\n bok c: " + this.c 
				+"\n bok d: " + this.d
				+"\n punkt AC: " + this.p1
				+"\n punkt AD: " + this.p2
				+"\n puntk BC: " + this.p3
				+"\n punkt BD: " + this.p4
				+"\n pole: " + this.getArea()
				+"\n obwód: " + this.getPeremiter()
				+"]";
	}
	
}
