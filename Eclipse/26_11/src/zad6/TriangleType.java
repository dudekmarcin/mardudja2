package zad6;

public enum TriangleType {
	EQUILATERAL,
	ISOSCELES,
	SCALENE;
}
