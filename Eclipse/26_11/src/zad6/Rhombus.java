package zad6;

import zad3.MovablePoint;
import zad3.Point;

public class Rhombus extends Parallelogram {
	
	public Rhombus(Point p1, Point p2, Point p3, Point p4) {
		super(p1, p2, p3, p4);
	}
	
	public Rhombus(MovablePoint p1, MovablePoint p2, MovablePoint p3, MovablePoint p4) {
		super(p1, p2, p3, p4);
	}
	
	public Rhombus(double a, double h) {
		super(a, a, h);
	}

	public Rhombus(double a) {
		super(a, a);
	}
	
	@Override
	public String toString() {
		return "Romb[\n bok a: "+ this.a
				+"\n bok b: " + this.b
				+"\n bok c: " + this.c 
				+"\n bok d: " + this.d
				+"\n wysokość: " + this.h
				+"\n punkt AC: " + this.p1
				+"\n punkt AD: " + this.p2
				+"\n puntk BC: " + this.p3
				+"\n punkt BD: " + this.p4
				+"\n pole: " + this.getArea()
				+"\n obwód: " + this.getPeremiter()
				+"]";
	}
	

}
