package zad6;

import zad3.MovablePoint;
import zad3.Point;

public class Square extends Rectangle {
	
	public Square(Point p1, Point p2, Point p3, Point p4) {
		super(p1, p2, p3, p4);
	}
	
	public Square(MovablePoint p1, MovablePoint p2, MovablePoint p3, MovablePoint p4) {
		super(p1, p2, p3, p4);
	}
	
	public Square(double a, double h) {
		super(a, a, h);
	}

	public Square(double a) {
		super(a, a);
	}

	@Override
	public String toString() {
		return "Kwadrat[\n bok a: "+ this.a
				+"\n bok b: " + this.b
				+"\n bok c: " + this.c 
				+"\n bok d: " + this.d
				+"\n punkt AC: " + this.p1
				+"\n punkt AD: " + this.p2
				+"\n puntk BC: " + this.p3
				+"\n punkt BD: " + this.p4
				+"\n pole: " + this.getArea()
				+"\n obwód: " + this.getPeremiter()
				+"]";
	}
	
}
