package zad6;

import zad3.MovablePoint;
import zad3.Point;

public class Trapeze implements Figure, Rotable {

	protected double a;
	protected double b;
	protected double c;
	protected double d;
	protected double h;
	
	protected MovablePoint p1;
	protected MovablePoint p2;
	protected MovablePoint p3;
	protected MovablePoint p4;

	
	public Trapeze(Point p1, Point p2, Point p3, Point p4) {
		this.p1 = new MovablePoint(p1.getX(), p1.getY(), 0, 0);
		this.p2 = new MovablePoint(p2.getX(), p2.getY(), 0, 0);
		this.p3 = new MovablePoint(p3.getX(), p3.getY(), 0, 0);
		this.p4 = new MovablePoint(p4.getX(), p4.getY(), 0, 0);
		this.a = countSide(this.p1, this.p2);
		this.b = countSide(this.p3, this.p4);
		this.c = countSide(this.p1, this.p3);
		this.d = countSide(this.p2, this.p4);
		this.h = countH();
	}
	
	public Trapeze(MovablePoint p1, MovablePoint p2, MovablePoint p3, MovablePoint p4) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.p4 = p4;
		this.a = countSide(this.p1, this.p2);
		this.b = countSide(this.p3, this.p4);
		this.c = countSide(this.p1, this.p3);
		this.d = countSide(this.p2, this.p4);
		this.h = countH();
		
	}
	
	public Trapeze(double a, double b, double c, double d, double h) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.h = h;
		this.p1 = new MovablePoint(0, 0, 0, 0);
		this.p2 = new MovablePoint(this.a, 0, 0, 0);
		double ah_c = Math.sqrt((this.c * this.c) - (this.h * this.h));
		double ah_d = Math.sqrt((this.d * this.d) - (this.h * this.h));
		this.p3 = new MovablePoint(ah_c, this.h, 0, 0);
		this.p4 = new MovablePoint((this.a - ah_d), this.h, 0, 0);
	}

	public Trapeze(double a, double b, double c, double d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.h = countH();
		this.p1 = new MovablePoint(0, 0, 0, 0);
		this.p2 = new MovablePoint(this.a, 0, 0, 0);
		double ah_c = Math.sqrt((this.c * this.c) - (this.h * this.h));
		double ah_d = Math.sqrt((this.d * this.d) - (this.h * this.h));
		this.p3 = new MovablePoint(ah_c, this.h, 0, 0);
		this.p4 = new MovablePoint((this.a - ah_d), this.h, 0, 0);

		
	}
	
	public double getA() {
		return this.a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return this.b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return this.c;
	}

	public void setC(double c) {
		this.c = c;
	}

	public double getD() {
		return this.d;
	}

	public void setD(double d) {
		this.d = d;
	}

	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}

	public MovablePoint getP1() {
		return this.p1;
	}

	public void setP1(MovablePoint p1) {
		this.p1 = p1;
	}

	public MovablePoint getP2() {
		return this.p2;
	}

	public void setP2(MovablePoint p2) {
		this.p2 = p2;
	}

	public MovablePoint getP3() {
		return this.p3;
	}

	public void setP3(MovablePoint p3) {
		this.p3 = p3;
	}

	public MovablePoint getP4() {
		return this.p4;
	}

	public void setP4(MovablePoint p4) {
		this.p4 = p4;
	}

	protected double getWidth(MovablePoint a, MovablePoint b) {
		return Math.abs(b.getX() - a.getX());
	}

	protected double getHeight(MovablePoint a, MovablePoint b) {
		return Math.abs(a.getY() - b.getY());
	}
	
	
	protected double countSide(MovablePoint a, MovablePoint b) {
		return Math.sqrt(Math.pow(getWidth(a,b), 2) + Math.pow(getHeight(a,b), 2));
	}

	protected double countH() {
		return (2 * this.getArea()) / (this.a + this.b);
	}

	@Override
	public double getArea() {
		return (this.h != 0) ? ((this.a + this.b) / 2) * this.h
				: 1 / 4 * ((this.a + this.b) / (this.a - this.b)) * Math.sqrt((a - b) + c + d)
						* Math.sqrt((a - b) + c - d) * Math.sqrt((a - b) - c + d) * Math.sqrt(-(a - b) + c + d);
	}

	@Override
	public double getPeremiter() {
		return this.a + this.b + this.c + this.d;
	}

	@Override
	public int getSidesNumber() {
		return 4;
	}

	@Override
	public int getNodesNumber() {
		return 4;
	}

	public void moveUp() {
		this.p1.moveUp();	
		this.p2.moveUp();		
		this.p3.moveUp();		
		this.p4.moveUp();		

	}

	public void moveDown() {
		this.p1.moveDown();	
		this.p2.moveDown();		
		this.p3.moveDown();		
		this.p4.moveDown();		

	}

	public void moveLeft() {
		this.p1.moveLeft();
		this.p2.moveLeft();
		this.p3.moveLeft();
		this.p4.moveLeft();

	}

	public void moveRight() {
		this.p1.moveRight();
		this.p2.moveRight();
		this.p3.moveRight();
		this.p4.moveRight();

	}
	
	public void move(int x, int y) {
		this.p1.move(x, y);
		this.p2.move(x, y);
		this.p3.move(x, y);
		this.p4.move(x, y);
	}

	@Override
	public void rotate(double angle) {
		p1.setX(p1.getX() * Math.cos(angle));
		p1.setY(p1.getY() * Math.sin(angle));
		
		p2.setX(p2.getX() * Math.cos(angle));
		p2.setY(p2.getY() * Math.sin(angle));
		
		p3.setX(p3.getX() * Math.cos(angle));
		p3.setY(p3.getY() * Math.sin(angle));
		
		p4.setX(p4.getX() * Math.cos(angle));
		p4.setY(p4.getY() * Math.sin(angle));

	}
	
	@Override
	public String toString() {
		return "Trapez[\n podstawa a: "+ this.a
				+"\n podstawa b: " + this.b
				+"\n ramię c: " + this.c 
				+"\n ramię d: " + this.d
				+"\n punkt AC: " + this.p1
				+"\n punkt AD: " + this.p2
				+"\n puntk BC: " + this.p3
				+"\n punkt BD: " + this.p4
				+"\n wysokość: " + this.h
				+"\n pole: " + this.getArea()
				+"\n obwód: " + this.getPeremiter()
				+"]";
	}
	
}
