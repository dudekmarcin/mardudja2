package zad6;

public interface Figure3D {

	public double getVolume();

	void rotate(double angleXY, double angleXZ);
}
