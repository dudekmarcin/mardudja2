package zad6;

public class Pyramid implements Figure, Figure3D {

	private double h;
	private Figure base;
	
	public Pyramid(Figure base, double height) {
		this.base = base;
		this.h = height;
	}
	
	@Override
	public double getVolume() {
		return (this.base.getArea()/3) * this.h;
	}

	@Override
	public void rotate(double angleXY, double angleXZ) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getArea() {
		double area = this.base.getArea();
//		
//		for(int i = 0; i < base.getSidesNumber(); i++) {
//			Triangle temp = new Triangle() 
//		}
		
		return area;
	}

	@Override
	public double getPeremiter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getSidesNumber() {
		// TODO Auto-generated method stub
		return base.getSidesNumber() * 2;
	}

	@Override
	public int getNodesNumber() {
		// TODO Auto-generated method stub
		return base.getNodesNumber() + 1;
	}
	
	public void moveUp() {
		
	}

	public void moveDown() {
		
	}

	public void moveLeft() {
		
	}

	public void moveRight() {
	
	}
	
	public void moveFront() {
		
	}
	
	public void moveBack() {
		
	}
	
	public void move(int x, int y, int z) {
		
	}
	

}
