package zad6;

import zad3.MovablePoint;
import zad3.MovablePoint3D;

public class Cylinder implements Figure, Figure3D {

	private Circle base;
	private double height;
	private MovablePoint3D pH;
	
	public Cylinder(Circle base, double height) {
		this.base = base;
		this.height = height;
		this.pH = new MovablePoint3D(this.base.getCenter().getX(), this.base.getCenter().getY() + this.height, 0, this.base.getCenter().getXSpeed(), this.base.getCenter().getYSpeed(), 0); 
	}
	
	public Cylinder(MovablePoint3D center, double radius, double height) {
		this.base = new Circle(new MovablePoint(center.getX(), center.getY(), center.getXSpeed(), center.getYSpeed()), radius);
		this.height = height;
		this.pH = new MovablePoint3D(center.getX(), center.getY() + this.height, center.getZ(), center.getXSpeed(), center.getYSpeed(), center.getZSpeed()); 

	}
	
	public Cylinder(MovablePoint3D center, double radius, MovablePoint3D height) {
		this.base = new Circle(new MovablePoint(center.getX(), center.getY(), center.getXSpeed(), center.getYSpeed()), radius);
		this.height = height.getHeight(base.getCenter().getY());
		this.pH = height; 

	}
	
	
	@Override
	public double getVolume() {
		return this.base.getArea() * this.height;
	}

	@Override
	public void rotate(double angleXY, double angleXZ) {

	}

	@Override
	public double getArea() {
		return 2 * this.base.getArea() + 2 * Math.PI * this.base.getRadius() * this.height;
	}

	@Override
	public double getPeremiter() {
		return 2 * this.getPeremiter() ;
	}

	@Override
	public int getSidesNumber() {
		return 0;
	}

	@Override
	public int getNodesNumber() {
		return 0;
	}

	public void moveUp() {
		this.base.moveUp();
		this.pH.moveUp();
	}

	public void moveDown() {
		this.base.moveDown();
		this.pH.moveDown();
	}

	public void moveLeft() {
		this.base.moveLeft();
		this.pH.moveLeft();
	}

	public void moveRight() {
		this.base.moveRight();
		this.pH.moveRight();
	}
	
	public void moveFront() {
		this.base.moveFront();
		this.pH.moveFront();
	}
	
	public void moveBack() {
		this.base.moveBack();
		this.pH.moveBack();
	}
	
	public void move(int x, int y, int z) {
		this.base.move(x, y, z);
		this.pH.move(x, y, z);
	}
	
	

}
