package zad6;

import zad3.MovablePoint;
import zad3.Point;

public class Rectangle extends Parallelogram {

	public Rectangle(Point p1, Point p2, Point p3, Point p4) {
		super(p1, p2, p3, p4);
	}
	
	public Rectangle(MovablePoint p1, MovablePoint p2, MovablePoint p3, MovablePoint p4) {
		super(p1, p2, p3, p4);
	}
	
	public Rectangle(double a, double b, double h) {
		super(a, b, h);
	}

	public Rectangle(double a, double b) {
		super(a, b);
	}

	@Override
	public String toString() {
		return "Prostokąt[\n bok a: "+ this.a
				+"\n bok b: " + this.b
				+"\n bok c: " + this.c 
				+"\n bok d: " + this.d
				+"\n punkt AC: " + this.p1
				+"\n punkt AD: " + this.p2
				+"\n puntk BC: " + this.p3
				+"\n punkt BD: " + this.p4
				+"\n pole: " + this.getArea()
				+"\n obwód: " + this.getPeremiter()
				+"]";
	}
	
//	private double sideA;
//	private double sideB;
//	
//	public Rectangle(double a, double b) {
//		this.sideA = a;
//		this.sideB = b;
//	}
//	
//	public double getSideA() {
//		return this.sideA;
//	}
//
//	public void setSideA(double sideA) {
//		this.sideA = sideA;
//	}
//
//	public double getSideB() {
//		return this.sideB;
//	}
//
//	public void setSideB(double sideB) {
//		this.sideB = sideB;
//	}
//
//	@Override
//	public double getArea() {
//		return this.sideA * this.sideB;
//	}
//
//	@Override
//	public double getPeremiter() {
//		return 2 * this.sideA + 2 * this.sideB;
//	}
//
//	@Override
//	public int getSidesNumber() {
//		return 4;
//	}
//
//	@Override
//	public int getNodesNumber() {
//		return 4;
//	}

}
