package pl.dweb.hibtut.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cases")
public class Case {

	private Long caseID;
    private String signature;
    private Date date;
    private Integer instanceID;
    private Integer judgesNumber;
    private Integer separatumNumber;
    private Long leaderID;
    private Long controlSubjectID;
    private Long protectedValueID;
    private Integer pointsNumber;
    private String sentenceURL;

    public Case() {}
    
    
}
