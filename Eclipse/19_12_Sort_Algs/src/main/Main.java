package main;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		int[] arr;	
		
		System.out.println("Bubble");
		arr = randomize(10);
		printArray(arr);
		Sorting sort = new Sorting();
		sort.bubbleSort(arr);
		printArray(arr);
		System.out.println();

		System.out.println("Insert");
		arr = randomize(10);
		printArray(arr);
		sort.insertSort(arr);
		printArray(arr);
		System.out.println();

		System.out.println("Selection");
		arr = randomize(10);
		printArray(arr);		
		sort.selectionSort(arr);
		printArray(arr);
		System.out.println();

		System.out.println("Merge");		
		arr = randomize(10);
		printArray(arr);
		int[] merge = sort.mergeSort(arr);
		printArray(merge);
		//printArray(arr);

		System.out.println();

		System.out.println("Shell");
		arr = randomize(10);
		printArray(arr);
		sort.shellSort(arr);
		printArray(arr);
		System.out.println();

		System.out.println("Quick");
		arr = randomize(10);
		printArray(arr);
		sort.quickSort(arr);
		printArray(arr);
	}
	
	public static void printArray(int[] arr) {
		String array = "[";
		for(int i = 0; i < arr.length; i++) {
			array += arr[i];
			if(i < arr.length-1){
				array += ",";
			} else {
				array += "]";
			}
		}
		System.out.println(array);
	}
	
	public static int[] randomize(int size) {
		Random random = new Random();
		int[] arr = new int[size];
		for(int i = 0; i < size; i++) {
			arr[i] = random.nextInt(100);
		}
		return arr;
	}
	
}
