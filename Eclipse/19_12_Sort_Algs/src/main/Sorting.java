package main;

public class Sorting {

	public Sorting() {

	}

	public void bubbleSort(int[] arr) {
		int j = 1;
		while (j < arr.length) {
			int tmp = 0;
			for (int i = 0; i < arr.length - 1; i++) {
				if (arr[i] > arr[i + 1]) {
					tmp = arr[i + 1];
					arr[i + 1] = arr[i];
					arr[i] = tmp;
				}
			}
			j++;
		}
	}

	public void insertSort(int[] arr) {
		int tmp, i, j = arr.length - 2;
		while (j >= 0) {
			tmp = arr[j];
			i = j + 1;
			while (i < arr.length) {
				if (tmp <= arr[i]) {
					break;
				} else {

					arr[i - 1] = arr[i];
					i++;
				}
			}
			arr[i - 1] = tmp;
			j--;
		}
	}

	public void selectionSort(int[] arr) {
		int j = 0, min, i, tmp;
		while (j < arr.length) {
			min = j;
			i = j + 1;
			while (i < arr.length) {
				if (arr[i] < arr[min]) {
					min = i;
				}
				i++;
			}
			tmp = arr[min];
			arr[min] = arr[j];
			arr[j] = tmp;
			j++;
		}
	}

	public int[] mergeSort(int[] arr) {
		if (arr.length > 1) {
			int center = (arr.length % 2 == 0) ? (arr.length / 2) - 1 : arr.length / 2;
			int[] l = new int[center + 1];
			int[] r = new int[arr.length - (center + 1)];
			for (int i = 0, j = 0; i < arr.length; i++) {
				if (i <= center) {
					l[i] = arr[i];
				} else {
					r[j] = arr[i];
					j++;
				}
			}
			l = this.mergeSort(l);
			r = this.mergeSort(r);
			arr = this.scale(l, r);

		}
		return arr;
	}

	private int[] scale(int[] l, int[] r) {
		int[] arr = new int[l.length + r.length];
		int i = 0, j = 0, k = 0;

		while (k < arr.length) {
			if ((i < l.length && j < r.length && l[i] <= r[j]) || (i < l.length && (j == r.length))) {
				arr[k] = l[i];
				i++;
			} else if ((i < l.length && j < r.length && l[i] > r[j]) || (i == l.length && j < r.length)) {
				arr[k] = r[j];
				j++;
			}
			k++;
		}
		return arr;
	}

	public void shellSort(int[] arr) {
		
		int h = arr.length / 2;
		while (h > 0) {
			int j = h;
			while (j < arr.length) {
				int tmp = arr[j];
				int i = 0;

				while (i < arr.length) {
					if (arr[i] > arr[j]) {
						tmp = arr[j];
						arr[j] = arr[i];
						arr[i] = tmp;
						if(i > h){
						i -= h;
						}
					}
					else {
						i+=h;
					}
				}
				j++;

			}
				h /= 2;
		}

	}

	public void quickSort(int[] arr) {
		int left = 0;
		int right = arr.length - 1;
		this.quickSort(arr, left, right);

	}

	private void quickSort(int[] arr, int left, int right) {

		if (left != right) {

			int pivot = arr[(left + right) / 2];
			int i = left;
			int j = right;
			int tmp;

			do {
				while (arr[i] < pivot) {
					i++;
				}

				while (arr[j] > pivot) {
					j--;
				}

				if (i <= j) {
					tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;
					i++;
					j--;
				}
			} while (i <= j);

			if (left < j) {
				this.quickSort(arr, left, j);
			}

			if (i < right) {
				this.quickSort(arr, i, right);
			}
		}
	}

	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

}
