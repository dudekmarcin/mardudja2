package com.sda.tests;

import static com.sda.utils.HeroCreator.*;
import static org.junit.Assert.*;


import org.junit.Test;

import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;
import com.sda.teams.Team;
import com.sda.teams.TeamType;

public class TeamTest {

	@Test
	public void testAddHeroToTeam() {
		Villain jocker = createVillainWithDefaultStats(3, TeamType.RED);
		SuperHero batman = createHeroWithDefaultStats(4, TeamType.BLUE);
		SuperHero superman = createHeroWithDefaultStats(4, TeamType.RED);

		
		Team red = new Team(TeamType.RED);
		assertTrue(red.addHeroToTeam(jocker));
		assertFalse(red.addHeroToTeam(batman));
		assertFalse(red.addHeroToTeam(superman));
		
	}
	
}
