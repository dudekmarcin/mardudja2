package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.superheroes.HeroStatistics;

public class HeroStatisticsTest {

	@Test
	public void testAddToStats() {
		HeroStatistics hero = new HeroStatistics(15, 10, 8);
		hero.addToAttack(10);
		hero.addToHealth(15);
		hero.addToDefense(8);
		assertTrue(hero.getAttack() == 20);
		assertTrue(hero.getHealth() == 30);
		assertTrue(hero.getDefense() == 16);
	}
	
}
