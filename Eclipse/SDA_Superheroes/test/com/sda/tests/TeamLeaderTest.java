package com.sda.tests;

import static com.sda.utils.HeroCreator.createHeroWithDefaultStats;
import static com.sda.utils.HeroCreator.createSuperHero;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.teams.Team;
import com.sda.teams.TeamType;

public class TeamLeaderTest {

	@Test
	public void testFindTeamLeader() {
		SuperHero jocker = createHeroWithDefaultStats(1, TeamType.RED);
		SuperHero batman = createHeroWithDefaultStats(7, TeamType.RED);
		HeroStatistics stats = new HeroStatistics(100,190,100);
		SuperHero superman = createSuperHero("Superman", stats, TeamType.RED);
		
		Team red = new Team(TeamType.RED);
		assertTrue(red.addHeroToTeam(jocker));
		assertTrue(red.addHeroToTeam(batman));
		assertTrue(red.addHeroToTeam(superman));
		
		AbstractHero leader = red.findTeamLeader();
		assertTrue(leader.getName().equals("Batman"));
		System.out.println(red.toString());
	}
}
