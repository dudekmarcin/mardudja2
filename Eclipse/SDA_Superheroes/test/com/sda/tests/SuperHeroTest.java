package com.sda.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.teams.TeamType;

public class SuperHeroTest {

	@Test
	public void testCreateVillain() {
		HeroStatistics batmanStats = new HeroStatistics(40,25,20);
		SuperHero batman = new SuperHero("Batman", batmanStats, TeamType.BLUE);
		
		double result = (batman.getStats().getDefense()+batman.getStats().getAttack())*batman.getStats().getHealth();
		assertTrue(batman.getPower() == result);
	}
	
}
