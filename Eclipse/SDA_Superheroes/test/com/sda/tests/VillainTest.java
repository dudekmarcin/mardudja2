package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.Villain;
import com.sda.teams.TeamType;

public class VillainTest {

	@Test
	public void testCreateVillain() {
		HeroStatistics jockerStats = new HeroStatistics(40,25,20);
		Villain jocker = new Villain("Jocker", jockerStats, TeamType.RED);
		
		double result = (jocker.getStats().getHealth()+jocker.getStats().getAttack())*jocker.getStats().getDefense();
		assertTrue(jocker.getPower() == result);
	}
	
}
