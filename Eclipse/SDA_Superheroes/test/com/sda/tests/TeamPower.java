package com.sda.tests;

import static com.sda.utils.HeroCreator.createHeroWithDefaultStats;
import static com.sda.utils.HeroCreator.createVillainWithDefaultStats;
import static com.sda.utils.HeroCreator.createSuperHero;
import static com.sda.utils.HeroCreator.createVillain;
import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;
import com.sda.teams.Team;
import com.sda.teams.TeamType;

public class TeamPower {

	@Test
	public void testTeamPower() {
		
		//   RED TEAM 
		SuperHero flash = createHeroWithDefaultStats(3, TeamType.RED);
		SuperHero batman = createHeroWithDefaultStats(0, TeamType.RED);
		HeroStatistics supermanStats = new HeroStatistics(100,190,100);
		SuperHero superman = createSuperHero("Superman", supermanStats, TeamType.RED);
		HeroStatistics aquamanStats = new HeroStatistics(80,120,110);
		SuperHero aquaman = createSuperHero("Aquaman", aquamanStats, TeamType.RED);
		HeroStatistics wonderwomanStats = new HeroStatistics(120,140,90);
		SuperHero wonderwoman = createSuperHero("Wonder Woman", wonderwomanStats, TeamType.RED);
		
		Team red = new Team(TeamType.RED);
		red.addHeroToTeam(flash);
		red.addHeroToTeam(batman);
		red.addHeroToTeam(superman);
		red.addHeroToTeam(aquaman);
		red.addHeroToTeam(wonderwoman);
		//  END RED TEAM
		
		Villain jocker = createVillainWithDefaultStats(9, TeamType.BLUE);
		Villain scarecrow = createVillainWithDefaultStats(6, TeamType.BLUE);
		HeroStatistics baneStats = new HeroStatistics(110,170,120);
		Villain bane = createVillain("Bane", baneStats, TeamType.BLUE);
		HeroStatistics badassStats = new HeroStatistics(80,120,150);
		Villain baddass = createVillain("Badass", badassStats, TeamType.BLUE);
		HeroStatistics zorgStats = new HeroStatistics(190,150,100);
		Villain zorg = createVillain("Zorg", zorgStats, TeamType.BLUE);
		
		Team blue = new Team(TeamType.BLUE);
		blue.addHeroToTeam(jocker);
		blue.addHeroToTeam(scarecrow);
		blue.addHeroToTeam(bane);
		blue.addHeroToTeam(baddass);
		blue.addHeroToTeam(zorg);
		System.out.println("RED: "+red.getTeamPower());
		System.out.println("BLUE: "+blue.getTeamPower());
		assertTrue(red.getTeamPower() > blue.getTeamPower());
		

		
		
	}
	
}
