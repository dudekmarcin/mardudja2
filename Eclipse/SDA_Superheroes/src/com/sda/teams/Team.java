package com.sda.teams;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.Villain;

public class Team {

	private TeamType team;
	private List<AbstractHero> heroesList;
	private AbstractHero teamLeader;
	private Side side = Side.UNKNOWN;

	public Team(TeamType team) {
		this.team = team;
		heroesList = new ArrayList<AbstractHero>();
	}

	public boolean addHeroToTeam(AbstractHero hero) {
		if ((heroesList.size() == 0 && this.team.equals(hero.getTeam()))
				|| (heroesList.size() > 0 && hero.getClass().equals(heroesList.get(0).getClass()))) {
			this.heroesList.add(hero);
			this.teamLeader = findTeamLeader();
			this.updateTeamSide(hero);

			return true;
		} else {
			System.out.println("Nie można dodać postaci przeznaczonej do innej drużyny!");
			return false;
		}
	}

	private void updateTeamSide(AbstractHero hero) {
		if (hero instanceof Villain) {
			this.side = Side.EVIL;
		} else {
			this.side = Side.GOOD;
		}
	}

	public Side getSide() {
		return this.side;
	}

	public TeamType getTeamType() {
		return this.team;
	}
	
	public boolean isTeamAlive() {
		boolean isAlive = false;
		for(AbstractHero hero : heroesList) {
			if(hero.getIsAlive()) {
				isAlive = true;
			}
		}
		
		return isAlive;
	}

	public AbstractHero getTeamLeader() {
		return teamLeader;
	}

	public AbstractHero getTeamMember(int index) {
		return heroesList.get(index);
	}

	public AbstractHero findTeamLeader() {
		Collections.sort(this.heroesList);
		int max = this.heroesList.size();
		max--;
		int batman = -1;
		for (AbstractHero hero : heroesList) {
			if (hero.getName().equals("Batman")) {
				batman = heroesList.indexOf(hero);
			}
		}
		if (batman >= 0) {
			return this.heroesList.get(batman);
		} else {
			return this.heroesList.get(max);
		}
	}

	@Override
	public String toString() {
		String list = "";
		for (AbstractHero hero : heroesList) {
			if (hero.getName().equals(getTeamLeader().getName())) {
				list += "-------------------- \n ";
				list += " " + hero.getName() + " (Leader) \n";
				list += "-------------------- \n ";

			} else {
				list += hero.getName() + " \n";
			}

		}

		return list;
	}

	public String aliveToString() {
		String list ="";
		int i=1;
		for(AbstractHero hero : heroesList) {
			if(hero.getIsAlive()) {
			if(hero.getName().equals(getTeamLeader().getName())) {
				list += "-------------------- \n ";
				list += " "+i+ " "+ hero.getName() + " (Leader) \n";
				list += "-------------------- \n ";

			} else {
				list += +i+ " "+ hero.getName() + " \n";
			}
			}
			i++;
		}
		
		return list;
	}

	public double getTeamPower() {
		double totalPower = 0;
		for (AbstractHero hero : heroesList) {
			totalPower += hero.getPower();
		}
		return totalPower;
	}

	public void increaseTeamPowerWithAdditionalBuff() {
		for (AbstractHero hero : heroesList) {
			if (hero.getIsIncrease() == false) {
				if (hero.getClass().equals("Villain")) {
					hero.getStats().addToHealth(10);
					hero.setIsIncrease(true);
				} else if (hero.getClass().equals("SuperHero")) {
					hero.getStats().addToDefense(10);
					hero.setIsIncrease(true);
				}
			}
		}

	}

	public void attackEnemy(AbstractHero hero, AbstractHero enemy) {

		int attack = (int) (hero.getPower() / enemy.getStats().getDefense()) / 10;

		enemy.getStats().substractHealth(attack);
		double enemyHealth = enemy.getStats().getHealth();

		if (enemyHealth <= 0) {
			enemy.setDead();
			System.out.println(hero.getName() + " zabił " + enemy.getName());
			System.out.println(hero.getName() + " HP: " + hero.getStats().getHealth());
			System.out.println("");
		} else {
			System.out.println(hero.getName() + " zadał " + enemy.getName() + " obrażenia w wysokości: " + attack);
			System.out.println(hero.getName() + " HP: " + hero.getStats().getHealth() + "  |  " + enemy.getName()
					+ " HP: " + enemy.getStats().getHealth());
			System.out.println("");

		}

	}

}
