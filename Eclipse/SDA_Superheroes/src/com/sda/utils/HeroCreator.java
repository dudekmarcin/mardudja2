package com.sda.utils;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;
import com.sda.teams.TeamType;



public class HeroCreator {

	public static SuperHero createSuperHero(String name, HeroStatistics stats, TeamType team) {
		return new SuperHero(name, stats, team);
		
	}
	
	public static Villain createVillain(String name, HeroStatistics stats, TeamType team) {
		return new Villain(name, stats, team);
	}
	
	public static SuperHero createHeroWithDefaultStats(int i, TeamType team) {
		PropertyReader.loadHeroValues();
		String name = System.getProperty("config."+(i-1)+".Name");
		String health = System.getProperty("config."+(i-1)+".BaseHealth");
		String attack = System.getProperty("config."+(i-1)+".BaseAttack");
		String defense = System.getProperty("config."+(i-1)+".BaseDefense");

		HeroStatistics stats = new HeroStatistics(Integer.parseInt(health), Integer.parseInt(attack), Integer.parseInt(defense));
		return createSuperHero(name, stats, team);
	
		}
	
	public static Villain createVillainWithDefaultStats(int i, TeamType team)	{
		PropertyReader.loadVillainValues();
		String name = System.getProperty("config."+(i-1)+".Name");
		String health = System.getProperty("config."+(i-1)+".BaseHealth");
		String attack = System.getProperty("config."+(i-1)+".BaseAttack");
		String defense = System.getProperty("config."+(i-1)+".BaseDefense");
		
		HeroStatistics stats = new HeroStatistics(Integer.parseInt(health), Integer.parseInt(attack), Integer.parseInt(defense));
		return createVillain(name, stats, team);
	}
	
}
