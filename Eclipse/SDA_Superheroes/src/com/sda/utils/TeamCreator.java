package com.sda.utils;

import java.util.Random;
import java.util.Scanner;

import com.sda.teams.Side;
import com.sda.teams.Team;
import com.sda.teams.TeamType;

import static com.sda.superheroes.Villain.getVillainList;
import static com.sda.superheroes.SuperHero.getSuperHeroList;
import static com.sda.utils.HeroCreator.createVillainWithDefaultStats;
import static com.sda.utils.HeroCreator.createHeroWithDefaultStats;

public class TeamCreator {
	
	private static boolean validateChoice(int choice, int[] array) {
		 boolean isOk = false;		
		if(choice > 0 && choice <= 10){
			for(int i=0;i<array.length;i++) {
				if(array[i] == choice) {
					isOk = false;
					break;
				} else {
					isOk = true;
				}
			}
		}
			
		return isOk;
	}

	public static Team initializeTeam() {

		Scanner scan = new Scanner(System.in);
		int sideChoice = 0;
		String teamChoice = "";
		TeamType type = TeamType.NONE;
		Side side = Side.UNKNOWN;
		do {
			System.out.println("Wybierz stronę (1 - bohaterzy, 2 - łotrzy): ");

			sideChoice = scan.nextInt();
			if (sideChoice == 1) {
				side = Side.GOOD;
			} else if (sideChoice == 2) {
				side = Side.EVIL;
			} else {
				System.out.println("Niedozwolony wybór");
				System.out.println("--------------------");
			}
		} while (sideChoice != 1 && sideChoice != 2);

		do {
			System.out.println("Wybierz kolor drużyny (R - czerwony, B - niebieski, G - zielony): ");

			teamChoice = scan.next();
			teamChoice = teamChoice.toUpperCase();
			switch (teamChoice) {
			case "R":
				type = TeamType.RED;
				break;
			case "B":
				type = TeamType.BLUE;
				break;
			case "G":
				type = TeamType.GREEN;
				break;
			default:
				System.out.println("Niedozwolony wybór");
				System.out.println("--------------------");
			}

		} while (!(teamChoice.equals("R") || teamChoice.equals("G") || teamChoice.equals("B")));

		System.out.println("--------------------");

		Team userTeam = new Team(type);

		System.out.println("Wybierz 5 jednostek do swojej drużyny: ");
		if (side.equals(Side.EVIL)) {
			getVillainList();
		} else {
			getSuperHeroList();
		}
		
		int[] heroes = new int[5];
		int j = 0;
		
		do {
			int temp = scan.nextInt();
			boolean isOk = validateChoice(temp, heroes);
			if(isOk == true) {
				heroes[j] = temp;
				j++;
			} else {
				System.out.println("Wartość niedozwolona lub zdublowany bohater, spróbuj jeszcze raz");
			}
		} while(j < 5);
		
	//	scan.close();
		if (side.equals(Side.GOOD)) {
			for (int i = 0; i < 5; i++) {
				userTeam.addHeroToTeam(createHeroWithDefaultStats(heroes[i], type));
			}
		} else {
			for (int i = 0; i < 5; i++) {
				userTeam.addHeroToTeam(createVillainWithDefaultStats(heroes[i], type));
			}

		}

		return userTeam;

	}

	public static Team initializeEnemyTeam(Side userSide, TeamType userType) {

		Random generator = new Random();
		Side side = (userSide.equals(Side.GOOD)) ? Side.EVIL : Side.GOOD;
		TeamType type = TeamType.NONE;
		int rTeam = generator.nextInt(100);
		switch (userType) {
		case RED:
			type = (rTeam % 2 == 0) ? TeamType.BLUE : TeamType.GREEN;
			break;
		case BLUE:
			type = (rTeam % 2 == 0) ? TeamType.RED : TeamType.GREEN;
			break;
		case GREEN:
			type = (rTeam % 2 == 0) ? TeamType.BLUE : TeamType.RED;
			break;
		default:
			type = TeamType.NONE;
			break;
		}
		
		Team enemyTeam = new Team(type);
		
		int[] heroes = new int[5];
		int j = 0;
		do {
			int temp = (generator.nextInt(1000) % 10);
			boolean isOk = validateChoice(temp, heroes);
			if(isOk == true) {
				heroes[j] = temp;
				j++;
			} 
		} while (j < 5);
		
		
		if (side.equals(Side.GOOD)) {
			for (int i = 0; i < 5; i++) {
				enemyTeam.addHeroToTeam(createHeroWithDefaultStats(heroes[i], type));
			}
		} else {
			for (int i = 0; i < 5; i++) {
				enemyTeam.addHeroToTeam(createVillainWithDefaultStats(heroes[i], type));
			}

		}

		return enemyTeam;
	}

}
