package com.sda.utils;

import com.sda.teams.Team;

public class TeamUtils {
	
	public static int compareTwoTeams(Team first, Team second) {
		if(first.getTeamPower() > second.getTeamPower()) {
			return 1;
		} else if(first.getTeamPower() < second.getTeamPower()) {
			return -1;
		} else {		
		return 0;
		}
	}

}
