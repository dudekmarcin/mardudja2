package com.sda.superheroes;


import com.sda.teams.TeamType;



public class Villain extends AbstractHero {
	
	private static String[] villainList = new String[]{"Loki", "MODOK", "Red Skull", "Dr Octopus", "Ultron", "Thanos", "Mandarin", "Kang", "Dormammu", "Arnim Zola" };
	

	public Villain(String name, HeroStatistics stats, TeamType team) {
		super(name, stats, team);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getPower() {
		return ( this.getStats().getHealth() + this.getStats().getAttack() ) * this.getStats().getDefense();
		
	}

	public static void getVillainList() {
		int length=villainList.length;
		for(int i=0;i<length;i++) {
			System.out.println("("+(i+1)+") "+villainList[i]);
		}
	}
	
}
