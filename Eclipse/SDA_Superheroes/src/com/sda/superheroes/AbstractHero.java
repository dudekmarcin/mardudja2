package com.sda.superheroes;

import com.sda.teams.TeamType;


public abstract class AbstractHero implements Comparable<AbstractHero> {

	private String name;
	private HeroStatistics stats;
	private TeamType team;
	private boolean isIncrease = false;
	private boolean isAlive = true;
	
	public AbstractHero(String name, HeroStatistics stats, TeamType team) {
		this.name = name;
		this.stats = stats;
		this.team = team;
	
		switch(team) {
		case RED: stats.addToHealth(50); break;
		case BLUE: stats.addToAttack(50); break;
		case GREEN: stats.addToDefense(50); break;
		case NONE: break;
		}
	
	}
	
	public String getName() {
		return name;
	}
	
	public boolean getIsIncrease() {
		return isIncrease;
	}
	
	public void setIsIncrease(boolean flag) {
		isIncrease = flag;
	}
	
	public HeroStatistics getStats() {
		return stats;
	}
	
	public TeamType getTeam() {
		return team;
	}
	
	public boolean getIsAlive() {
		return isAlive;
	}
	
	public void setDead() {
		this.isAlive = false;
	}
	
	public void setAlive() {
		this.isAlive = 	true;
	}
	
	public abstract double getPower();
	
	@Override
    public int compareTo(AbstractHero hero) {
        if(this.getStats().getAttack() > hero.getStats().getAttack()) {
            return 1;
        } else if(this.getPower() < hero.getPower()) {
            return -1;
        } else {
            return 0;
        }
        
    }
	
}