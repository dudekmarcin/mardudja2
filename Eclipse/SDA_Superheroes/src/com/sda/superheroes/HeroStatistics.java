package com.sda.superheroes;

public class HeroStatistics {
	private int health;
	private int attack;
	private int defense;
	
	public HeroStatistics(int health, int attack, int defense) {
		this.health = health;
		this.attack = attack;
		this.defense = defense; 
	}

	public int getHealth() {
		return health;
	}
	
	public int getAttack() {
		return attack;
	}
	
	public int getDefense() {
		return defense;
	}
	
	public void addToHealth(int add) {
		health += add;
	}
	
	public void addToAttack(int add) {
		attack += add;
	}
	
	public void addToDefense(int add) {
		defense += add;
	}
	
	public void substractHealth(int add) {
		health -= add;
	}
	
	public void substractAttack(int add) {
		attack -= add;
	}
	
	public void substractDefense(int add) {
		defense -= add;
	}
}
