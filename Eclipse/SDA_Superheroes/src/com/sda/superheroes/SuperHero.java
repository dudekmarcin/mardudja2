package com.sda.superheroes;

import com.sda.teams.TeamType;

public class SuperHero extends AbstractHero {
	
	private static String[] superheroList = new String[]{"Thor", "Hawkeye", "Captain America", "Spiderman", "Ironman", "Vision", "Black Panther", "Scarlet Witch", "Dr Strange", "Black Widow" };


	public SuperHero(String name, HeroStatistics stats, TeamType team) {
		super(name, stats, team);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getPower() {
		
		return (this.getStats().getDefense()+this.getStats().getAttack())*this.getStats().getHealth();
				
	}
	
	public static void getSuperHeroList() {
		int length=superheroList.length;
		for(int i=0;i<length;i++) {
			System.out.println("("+(i+1)+") "+superheroList[i]);
		}
	}
	
}
