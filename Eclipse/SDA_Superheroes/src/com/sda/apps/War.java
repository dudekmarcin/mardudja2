package com.sda.apps;

import static com.sda.utils.TeamCreator.*;

import java.util.Random;
import java.util.Scanner;

import com.sda.teams.Team;

public class War {
	
	public static Scanner scan = new Scanner(System.in);
	
	public static int choiceHero() {
		System.out.println("----------- Wybierz bohatera do walki -------------");
		
		
		int choice = 0;
		do {
				int temp = scan.nextInt();
				if(temp > 0 && temp <= 5){
					choice = temp;
				} else {
					System.out.println("Nieprawidlowa wartosc");
				}
				
		} while(choice == 0);
		
		return choice;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		Team userTeam = initializeTeam();
		Team enemyTeam = initializeEnemyTeam(userTeam.getSide(), userTeam.getTeamType());
		while(userTeam.isTeamAlive() && enemyTeam.isTeamAlive() ) {
		System.out.println("----------- YOUR TEAM -----------");
		System.out.println(userTeam.aliveToString());
		System.out.println("----------- ENEMY TEAM -----------");
		System.out.println(enemyTeam.aliveToString());
		System.out.println("");
		
		int choice = choiceHero();
		System.out.println("");
		System.out.println("----------- FIGHT -------------");
		System.out.println("");
		Random generator = new Random();
		int random;
		do {
			random = generator.nextInt(1000) % 5;
		} while(userTeam.getTeamMember(random).getIsAlive() == false);
		
		while(userTeam.getTeamMember(choice-1).getStats().getHealth() > 0 && enemyTeam.getTeamMember(random).getStats().getHealth() > 0) {
			userTeam.attackEnemy(userTeam.getTeamMember(choice-1), enemyTeam.getTeamMember(random));
			if(enemyTeam.getTeamMember(random).getStats().getHealth() > 0) {
				userTeam.attackEnemy(enemyTeam.getTeamMember(random), userTeam.getTeamMember(choice-1));
			}
		}

	}
		scan.close();
	}
	
}
