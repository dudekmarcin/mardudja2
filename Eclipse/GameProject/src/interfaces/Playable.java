package interfaces;

public interface Playable extends Settable {

	public void run();
	
	public void game();
	
	public String getName();
	
}
