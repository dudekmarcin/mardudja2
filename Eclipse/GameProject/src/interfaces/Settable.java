package interfaces;

import java.util.Date;

public interface Settable {

	public int getTotalGames();
	public void setTotalGames();
	public void getDefaultConfig();
	public Date getLastGame();
	public void setLastGame();
	public void showStats();
}
