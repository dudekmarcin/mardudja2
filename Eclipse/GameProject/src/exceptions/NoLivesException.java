package exceptions;

public class NoLivesException extends Exception {

	public NoLivesException() {
		super("Wszystkie szanse zostały stracone. Koniec gry.");
	}
}
