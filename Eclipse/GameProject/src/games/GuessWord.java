package games;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import exceptions.NoLivesException;
import interfaces.Playable;
import interfaces.Settable;
import utility.Configurator;
import utility.IniConfig;
import utility.ProperPrinter;

public class GuessWord implements Playable, Closeable, Settable {

	private double lives;
	private final String name;
	private String word;
	private String[] result;
	private Set<String> usedLetters;
	private Scanner scan;
	private ProperPrinter pp;
	private IniConfig config;
	private Configurator configurator;

	public GuessWord() {
		this.name = "GuessWord";
		this.usedLetters = new LinkedHashSet<>();
		this.scan = new Scanner(System.in);
		this.pp = new ProperPrinter(50);
		this.config = new IniConfig();
		this.getDefaultConfig();
		this.configurator = new Configurator(this.name, this.config);
		try {
			this.config = this.configurator.readSettings();
		} catch (FileNotFoundException e) {
			pp.println("Config file not found. Used default values.");
		}
	}

	private void selectWord() {
		Random random = new Random();
		do {
			word = readWord(random.nextInt(countFileLines() + 1));
		} while (word.equals(this.getLastWord()));
		this.setLastWord();
		lives = Math.ceil(1.25 * word.length());
		result = new String[word.length()];
		for (int i = 0; i < word.length(); i++) {
			result[i] = "_ ";
		}
	}

	public void getDefaultConfig() {
		Calendar date = Calendar.getInstance();
		String currentDate = "" + date.getTimeInMillis();
		this.config.setValue("general", "totalGames", "0");
		this.config.setValue("general", "lastGame", currentDate);
		this.config.setValue("general", "lastWord", null);
		this.config.setValue("tips", "showUsedLetters", "true");
		this.config.setValue("general", "DictPath", "src//resources//words.txt");
	}

	public void setConfigFolder() {
		String path = "";
		do {
			pp.delim();
			pp.println("Input config folder path:");
			System.out.print("| ");
			path = scan.next();
			File f = new File(path);
			if(f.exists() && f.isDirectory()) {
					break;
			} else {
				pp.println("Folder doesn't exist. Input correct path or press Enter to cancel.");
			}

		} while (path.length() > 0);
		pp.delim();
		this.configurator.setPath(path);
		try {
			this.config = configurator.readSettings();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		pp.println("New config folder saved.");
		pp.delim();
	}

	public String readWord(int word) {
		int i = 1;
		String temp = "";

		try {
			File f = new File(this.config.getValue("general", "DictPath"));
			Scanner file_scan = new Scanner(f);
			while (file_scan.hasNextLine()) {
				if (i == word) {
					temp = file_scan.nextLine();
				} else {
					file_scan.nextLine();
				}
				i++;
			}
			file_scan.close();
		} catch (FileNotFoundException e) {
			this.pp.println(e.getMessage());
		}
		return temp;

	}

	public int countFileLines() {
		File f = new File(this.config.getValue("general", "DictPath"));
		int i = 0;
		try {
			Scanner file = new Scanner(f);
			while (file.hasNextLine()) {
				i++;
				file.nextLine();
			}
			file.close();

		} catch (FileNotFoundException e) {
			this.pp.println(e.getMessage());
		}

		return i;
	}

	public String getLetter() {
		this.pp.println("Podaj literę (lub 0, aby zakończyć):");
		System.out.print("| ");
		return scan.next().trim().substring(0, 1);
	}

	@Override
	public void game() {
		selectWord();
		pp.delim();
		pp.println(this.name);
		String forResult = "";
		String letter;

		this.pp.spacer();
		for (int i = 0; i < word.length(); i++) {
			forResult += result[i];
		}
		this.pp.println("Wylosowano słowo");
		this.pp.println(forResult);

		while (lives >= 0) {
			this.pp.spacer();
			if (lives == 0) {
				try {
					throw new NoLivesException();
				} catch (NoLivesException e) {
					this.pp.println(e.getMessage());
					break;
				}
			}
			if (showUsedLetters()) {
				this.pp.print("Użyte litery: ");
				String tip ="";
				Iterator<String> it = this.usedLetters.iterator();
				while(it.hasNext()) {
					tip += it.next() + " ";
				}
				pp.println(tip);
			}
			this.pp.println("Ilość prób: " + lives + "/" + Math.ceil(1.25 * word.length()));
			letter = getLetter();
			if (letter.equals("0")) {
				pp.delim();
				pp.println("ZAKOŃCZYŁEŚ GRĘ");
				break;
			}
			this.usedLetters.add(letter.toUpperCase());

			for (int i = 0; i < word.length(); i++) {
				if (word.substring(i, i+1).toUpperCase().equals(letter.toUpperCase())) {
					result[i] = letter.toUpperCase() + " ";
				}
			}

			forResult = "";

			for (int i = 0; i < word.length(); i++) {
				forResult += result[i];
			}
			this.pp.println("" + forResult);
			--this.lives;

			boolean isWin = true;
			for (int i = 0; i < word.length(); i++) {
				if ((result[i].equals("_ "))) {
					isWin = false;
				}
			}

			if (isWin) {
				this.pp.println("WYGRAŁEŚ!");
				break;
			}

		}

		
		this.setTotalGames();
		this.setLastGame();

	}

	@Override
	public void run() {
		int choice = -1;
		do {
			pp.delim();
			pp.println("Choose action");
			pp.println("1. Start game");
			pp.println("2. Show statistics");
			pp.println("3. Show/Hide UsedLetters");
			pp.println("4. Set dictionary filepath");
			pp.println("5. Show config file");
			pp.println("6. Set folder with config file");
			pp.spacer();
			pp.println("0. Exit");
			System.out.println("| ");
			choice = scan.nextInt();
			pp.delim();

			switch (choice) {
			case 1:
				this.game();
				break;
			case 2:
				this.showStats();
				break;
			case 3:
				this.setShowingUsedLetters();
				break;
			case 4:
				this.setDictPath();
				break;
			case 5:
				this.configurator.printSettings();
				break;
			case 6:
				this.setConfigFolder();

				break;
			case 0:
				this.configurator.saveSettings(this.config);
				pp.println("Goodbye!");
				break;
			default:
				pp.println("Incorrect value. Try again.");
			}

		} while (choice != 0);
	}

	public void showStats() {
		pp.println("Total Games: " + this.getTotalGames());
		pp.println("Last Game: " + this.getLastGame());
		pp.println("Last word in game: " + this.getLastWord());
		pp.println("Show UsedLetters: " + this.showUsedLetters());
		pp.println("Dictionary path: " + this.getDictPath());
	}

	public String getLastWord() {
		return this.config.getValue("general", "lastWord");
	}
	
	public void setLastWord() {
		this.config.setValue("general", "lastWord", this.word);
		this.configurator.saveSettings(this.config);
	}

	public String getDictPath() {
		return this.config.getValue("general", "DictPath");
	}

	@Override
	public void close() {
		scan.close();

	}

	@Override
	public void setTotalGames() {
		int games = Integer.parseInt(this.config.getValue("general", "totalGames"));
		String totalGames = "" + (games + 1);
		this.config.setValue("general", "totalGames", totalGames);
		this.configurator.saveSettings(this.config);
	}

	public int getTotalGames() {
		return Integer.parseInt(this.config.getValue("general", "totalGames"));
	}

	@Override
	public Date getLastGame() {
		Calendar lastGame = Calendar.getInstance();
		lastGame.setTimeInMillis(Long.parseLong(this.config.getValue("general", "lastGame")));
		return lastGame.getTime();
	}

	@Override
	public void setLastGame() {
		Calendar lastGameDate = Calendar.getInstance();
		String lastGame = "" + lastGameDate.getTimeInMillis();
		this.config.setValue("general", "lastGame", lastGame);
		this.configurator.saveSettings(this.config);
	}

	@Override
	public String getName() {
		return this.name;
	}

	public boolean showUsedLetters() {
		return Boolean.parseBoolean(this.config.getValue("tips", "showUsedLetters"));
	}

	public void setShowingUsedLetters() {
		String tip = "";
		do {
			pp.delim();
			pp.println("Wyświetlać wykorzystane litery? (true/false):");
			System.out.print("| ");
			tip = scan.next();
			if (!tip.equals("true") && !tip.equals("false")) {
				pp.delim();
				pp.println("Argument przyjmuje tylko wartości true/false");
			}
		} while (!tip.equals("true") && !tip.equals("false"));
		pp.delim();
		this.config.setValue("tip", "showUsedLetters", tip);
		this.configurator.saveSettings(this.config);
		pp.println("Zapisano nowe ustawienia podpowiedzi");
		pp.delim();
	}

	public void setDictPath() {
		String path = "";
		do {
			pp.delim();
			pp.println("Input new dictionary filepath:");
			System.out.print("| ");
			path = scan.next();
			File f = new File(path);
			try {
				Scanner file_scan = new Scanner(f);
				file_scan.close();
				break;
			} catch (FileNotFoundException e) {
				pp.println("File doesn't exist. Input correct filepath or press Enter to cancel.");
			}

		} while (path.length() > 0);
		pp.delim();
		this.config.setValue("general", "DictPath", path);
		this.configurator.saveSettings(this.config);
		pp.println("New settings saved.");
		pp.delim();
	}

}
