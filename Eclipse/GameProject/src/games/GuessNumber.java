package games;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import interfaces.Playable;
import utility.Configurator;
import utility.IniConfig;
import utility.IniSection;
import utility.ProperPrinter;

public class GuessNumber implements Playable, Closeable {

	private int number;
	private final String name;
	private Scanner scan;
	private ProperPrinter pp;
	private IniConfig config;
	private Configurator configurator;

	public GuessNumber() {
		this.name = "GuessNumber";
		this.scan = new Scanner(System.in);
		this.pp = new ProperPrinter(50);
		this.config = new IniConfig();
		this.getDefaultConfig();
		this.configurator = new Configurator(this.name, this.config);
		try {
			this.config = this.configurator.readSettings();
		} catch (FileNotFoundException e) {
			pp.println("Config file not found. Used default values.");
		}
		Random random = new Random();
		do {
			this.number = random.nextInt(this.getMaxNumber() + 1 - this.getMinNumber()) + this.getMinNumber();
		} while(this.number == this.getLastNumber());
	}

	public GuessNumber(int number) {
		this();
		this.number = number;
	}

	public void getDefaultConfig() {
		Calendar date = Calendar.getInstance();
		String currentDate = "" + date.getTimeInMillis();
		
		this.config.setValue("general", "totalGames", "0");
		this.config.setValue("general", "lastGame", currentDate);
		this.config.setValue("general", "lastNumber", "0");
		this.config.setValue("range", "minNumber", "1");
		this.config.setValue("range", "maxNumber", "100");
		this.config.setValue("tips", "showTips", "true");
	}

	public int readNumber() {
		System.out.print("| ");
		return scan.nextInt();
	}

	public boolean checkNumber(int number) {
		return (this.number == number);
	}

	public int getNumber() {
		return this.number;
	}

	public String getName() {
		return this.name;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	@Override
	public void run() {
		int choice = -1;
		do {
			pp.delim();
			pp.println("Choose action");
			pp.println("1. Start game");
			pp.println("2. Show statistics");
			pp.println("3. Show/Hide Tips");
			pp.println("4. Set min number");
			pp.println("5. Set max number");
			pp.println("6. Show config file");
			pp.spacer();
			pp.println("0. Exit to main menu");
			System.out.println("| ");
			choice = scan.nextInt();
			pp.delim();

			switch (choice) {
			case 1:
				this.game();
				break;
			case 2:
				this.showStats();
				break;
			case 3:
				this.setTips();
				break;
			case 4:
				this.setMinNumber();
				break;
			case 5:
				this.setMaxNumber();
				break;
			case 6: 
				this.configurator.printSettings(); break;
			case 0:
				this.configurator.saveSettings(this.config);
				pp.println("Goodbye!");
				break;
			default:
				pp.println("Incorrect value. Try again.");
			}

		} while (choice != 0);

	}

	@Override
	public void game() {

		this.pp.delim();
		this.pp.println(this.name);
		this.pp.delim();
		
		this.pp.println("Wylosowano liczbę z zakresu " + this.getMinNumber() + " do " + this.getMaxNumber()
				+ ". Spróbuj zgadnąć liczbę (lub wciśnij 0, aby zakończyć):");
		
		int tries = 0;
		int answer = readNumber();
		
		String tip;
		
		while (!checkNumber(answer) && answer > 0) {
			tries++;
			if (this.getTips()) {
				tip = (answer < this.number) ? "za małą" : "za dużą";
				this.pp.println("Nie zgadłeś :( Podałeś " + tip
						+ " liczbę. Spróbuj jeszcze raz (lub wciśnij 0, aby zakończyć):");
			} else {
				this.pp.println("Nie zgadłeś :( Spróbuj jeszcze raz (lub wciśnij 0, aby zakończyć):");
			}

			answer = readNumber();
			this.pp.spacer();
			this.pp.delim();
			this.pp.spacer();
		}
		if (answer == 0) {
			this.pp.delim();
			this.pp.println("Zakończyłeś grę.");
			this.pp.delim();
		} else {
			this.pp.delim();
			this.pp.println("Zgadłeś! Gratulacje :)");
			this.pp.delim();

		}
		this.setTotalGames();
		this.setLastGame();
		this.setLastNumber();
	}
	
//	public void checkHighscores(int score, String user) {
//		String scoreString = "" + score;
//		if(this.config.getSection("highscores").size() < 5) {
//			this.config.setValue("highscores", scoreString, user);
//		} else {
//			Set<Integer> scoreSet = new TreeSet<>();
//			this.config.getSection("highscores").forEach((key, value) -> scoreSet.add(Integer.parseInt(key)));
//			scoreSet
//		}
//	}

	@Override
	public void close() throws IOException {
		scan.close();
	}

	@Override
	public void setTotalGames() {
		int games = this.getTotalGames();
		String totalGames = "" + (games + 1);
		this.config.setValue("general", "totalGames", totalGames);
		this.configurator.saveSettings(this.config);

	}

	public int getTotalGames() {
		return Integer.parseInt(this.config.getValue("general", "totalGames"));
	}

	@Override
	public Date getLastGame() {
		Calendar lastGame = Calendar.getInstance();
		lastGame.setTimeInMillis(Long.parseLong(this.config.getValue("general", "lastGame")));
		return lastGame.getTime();
	}

	@Override
	public void setLastGame() {
		Calendar lastGameDate = Calendar.getInstance();
		String lastGame = "" + lastGameDate.getTimeInMillis();
		this.config.setValue("general", "lastGame", lastGame);
		this.configurator.saveSettings(this.config);
	}


	public void setLastNumber() {
		String lastNumber = "" + this.number;
		this.config.setValue("general", "lastNumber", lastNumber);
		this.configurator.saveSettings(this.config);
	}
	
	public int getLastNumber() {
		return Integer.parseInt(this.config.getValue("general", "lastNumber"));
	}
	
	public int getMinNumber() {
		return Integer.parseInt(this.config.getValue("range", "minNumber"));
	}

	public void setMinNumber() {
		int number = 0;
		do {
			pp.delim();
			pp.println("Podaj wartość minimalną:");
			System.out.print("| ");
			number = scan.nextInt();
			if (number < 1) {
				pp.delim();
				pp.println("Wartość minimalna musi być większa od 0");
			} else if (this.getMaxNumber() <= number) {
				pp.delim();
				pp.println("Wartość minimalna musi być mniejsza od wartości maksymalnej (" + this.getMaxNumber() + ")");
			}
		} while (number < 1);
		pp.delim();
		String minNumber = "" + number;
		this.config.setValue("range", "minNumber", minNumber);
		this.configurator.saveSettings(this.config);
		pp.println("Zapisano nową wartość minimalną");
		pp.delim();
	}

	public int getMaxNumber() {
		return Integer.parseInt(this.config.getValue("range", "maxNumber"));
	}

	public void setMaxNumber() {
		int number = 0;
		do {
			pp.delim();
			pp.println("Podaj wartość maksymalną:");
			System.out.print("| ");
			number = scan.nextInt();
			if (number < this.getMinNumber()) {
				pp.delim();
				pp.println("Wartość maksymalna musi być większa od wartości minimalnej (" + this.getMinNumber() + ")");
			}
		} while (number < this.getMinNumber());
		pp.delim();
		String maxNumber = "" + number;
		this.config.setValue("range", "maxNumber", maxNumber);
		this.configurator.saveSettings(this.config);
		pp.println("Zapisano nową wartość maksymalną");
		pp.delim();
	}

	public boolean getTips() {
		return Boolean.parseBoolean(this.config.getValue("tips", "showTips"));
	}

	public void setTips() {
		String tip = "";
		do {
			pp.delim();
			pp.println("Wyświetlać podpowiedzi? (true/false):");
			System.out.print("| ");
			tip = scan.next();
			if (!tip.equals("true") && !tip.equals("false")) {
				pp.delim();
				pp.println("Argument przyjmuje tylko wartości true/false");
			}
		} while (!tip.equals("true") && !tip.equals("false"));
		pp.delim();
		this.config.setValue("tips","showTips", tip);
		this.configurator.saveSettings(this.config);
		pp.println("Zapisano nowe ustawienia podpowiedzi");
		pp.delim();
	}

	public void showStats() {
		pp.println("Total Games: " + this.getTotalGames());
		pp.println("Last Game: " + this.getLastGame());
		pp.println("Last number: " + this.getLastNumber());
		pp.println("Min number : " + this.getMinNumber());
		pp.println("Max Number: " + this.getMaxNumber());
		pp.println("Show Tips: " + this.getTips());
	}
	

}
