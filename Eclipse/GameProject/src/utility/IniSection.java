package utility;

import java.util.HashMap;
import java.util.Map;

public class IniSection {

	private Map<String, String> sectionConfig;
	
	public IniSection() {
		this.sectionConfig = new HashMap<>();
	}
	
	public String getValue(String key) {
		return this.sectionConfig.get(key);
	}
	public void setValue(String key, String value) {
		if(this.sectionConfig.containsKey(key)) {
			this.sectionConfig.replace(key, value);
		} else {
			this.sectionConfig.put(key, value);
		}
	}
	
	public Map<String, String> getMap() {
		return this.sectionConfig;
	}
	
	
	
}
