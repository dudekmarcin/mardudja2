package utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.function.BiConsumer;

public class Configurator {

	private String game;
	private String path = "src//resources//";
	private IniConfig config;
	private ProperPrinter pp;

	public Configurator(String game, IniConfig config) {
		this.game = game;
		this.config = config;
		this.pp = new ProperPrinter(50);
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public IniConfig readSettings() throws FileNotFoundException {
		if(!this.path.endsWith("\\")) {
			this.path += "\\";
		}
		File f = new File(this.path + this.game + ".ini");
			Scanner scan = new Scanner(f);
			String tmp;
			String[] tmpSplit;
			String section = "";
			
			while(scan.hasNextLine()) {
				tmp = scan.nextLine().trim();				
				if (!tmp.equals("") && tmp.substring(0, 1).equals("[")) {
					section = tmp.substring(1, tmp.length()-1);
					this.config.setSection(section);
				} else if (tmp.contains("=")) {
					tmpSplit = tmp.split("=");
					this.config.setValue(section, tmpSplit[0], tmpSplit[1]);
				}
				
			}

			scan.close();

		return this.config;

	}

	public void saveSettings(IniConfig config) {
		this.config = config;
			try {
				File f = new File(this.path + this.game + ".ini");
				FileOutputStream fos = new FileOutputStream(f);
				PrintWriter pw = new PrintWriter(fos);
				
				BiConsumer<String, Section> saver = (section, settings) -> {
					pw.println("[" + section + "]");
					settings.getMap().forEach((key, value) -> pw.println(key + "=" + value));
					pw.println();
				};
				
				config.getConfig().forEach(saver);
				
				pw.close();
			} catch (FileNotFoundException e) {
				pp.println("Config file not found.");
			}
			
	}
 
	public void printSettings() {
		
		BiConsumer<String, Section> printer = (section, settings) -> {
			pp.println(section);
			settings.getMap().forEach((key, value) -> pp.println(key + "=" + value));
			pp.spacer();
		};
		
		this.config.getConfig().forEach(printer);
	}
	
}
