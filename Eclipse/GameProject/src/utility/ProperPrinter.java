package utility;

public class ProperPrinter {
	// -number:int
	// +delim():void;
	// +print():void;
	// +println():void;
	// +spacer();
	// Jako argument konstruktora, użytkownik podaje liczbę większą od zera,
	// klasa powinna dostarczać
	// metody drukujące ramkę oraz tekst wycentrowany:
	// delim():
	// +-----------------------------+
	// print() / println():
	// | Tekst |
	// spacer():
	// | |

	// Użyj powyższej klasy do drukowania przebiegu poszczególnych gier jak i
	// całej aplikacji tak, aby gra
	// miała ciągłość w konsoli.

	private int number;
	private String print_tmp = "";
	
	public ProperPrinter() {
		this.number = 30;
	}

	public ProperPrinter(int number) {
		this.number = number;
	}
	
	public void cleanTmp() {
		this.print_tmp = "";
	}

	public void delim() {
		String show = "+";
		for (int i = 0; i < this.number - 1; i++) {
			show += "-";
		}
		show += "+";
		System.out.println(show);
	}

	public void spacer() {
		String show = "|";
		for (int i = 1; i < this.number; i++) {
			show += " ";
		}
		show += "|";
		System.out.println(show);
	}
	

	public void println(String word) {
		this.print_tmp = "";
		String show = "|";
		int length = this.number - word.length() - 1;

		for (int i = 0; i < length / 2; i++) {
			show += " ";
		}
		if (word.length() > number - 2) {
			show += word.substring(0, (word.length() - (word.length() - number + 2))) + "-";
			this.print_tmp = "";
			show += "|";
			System.out.println(show);
			this.println(word.substring(word.length() - (word.length() - number + 2)));
		} else {
			show += word;

			for (int i = 0; i < length / 2; i++) {
				show += " ";
			}
			if (length % 2 == 1) {
				show += " ";
			}
			show += "|";
			System.out.println(show);
		}
	}

	public void print(String word) {
		String show = "|";
		int length = this.number - word.length() - 1;

		for (int i = 0; i < length / 2; i++) {
			show += " ";
		}
		if ((word.length() + this.print_tmp.length()) > number - 2) {
			show += word.substring(0, (word.length() - (word.length() - number + 2))) + "-";
			this.print_tmp = "";
			show += "|";
			System.out.println(show);
			this.print(word.substring(word.length() - (word.length() - number + 2)));
		} else {
			show += word;

			for (int i = 0; i < length / 2; i++) {
				show += " ";
			}
			if (length % 2 == 1) {
				show += " ";
			}
			show += "|";
			this.print_tmp += word;
			System.out.println(show);

		}

	}

}
