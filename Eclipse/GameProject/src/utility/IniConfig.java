package utility;

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;

public class IniConfig {

	private Map<String, Section> config;

	public IniConfig() {
		this.config = new LinkedHashMap<>();
	}

	public String getValue(String section, String key) {
		return this.config.get(section).getValue(key);
	}

	public void setValue(String section, String key, String value) {
		if (this.config.containsKey(section)) {
			if (this.config.get(section).getMap().containsKey(key)) {
				this.config.get(section).getMap().replace(key, value);
			} else {
				this.config.get(section).getMap().put(key, value);
			}
		} else {
			this.config.put(section, new Section(key, value));
		}
	}

	public void setSection(String section) {
		if(!this.config.containsKey(section)) {
			this.config.put(section, new Section());
		}
	}
	
	public Map<String, String> getSection(String section) {
		return this.config.get(section).getMap();
	}

	public Map<String, Section> getConfig() {
		return this.config;
	}
	
	
}

class Section {
	private Map<String, String> sectionConfig;

	public Section() {
		this.sectionConfig = new HashMap<>();			
	}
	
	public Section(String key, String value) {
		this();
		this.sectionConfig.put(key, value);
	}

	public String getValue(String key) {
		return this.sectionConfig.get(key);
	}

	public void setValue(String key, String value) {
		if (this.sectionConfig.containsKey(key)) {
			this.sectionConfig.replace(key, value);
		} else {
			this.sectionConfig.put(key, value);
		}
	}

	public Map<String, String> getMap() {
		return this.sectionConfig;
	}
}
