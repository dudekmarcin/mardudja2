package utility;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import interfaces.Playable;
import interfaces.Settable;

public class Game implements Closeable, Settable {

	private Set<String> games;
	private ProperPrinter pp;
	private String name;
	private IniConfig config;
	private Configurator configurator;
	private int choice = -1;
	Scanner scan;

	public Game() {
		this.name = "GameArena";
		this.scan = new Scanner(System.in);
		this.pp = new ProperPrinter(50);
		this.games  = new LinkedHashSet<>();
		this.config = new IniConfig();
		
		this.configurator = new Configurator(this.name, this.config);
		try {
			this.config = this.configurator.readSettings();
		} catch (FileNotFoundException e) {
			pp.println("Config file not found. Used default values.");
		}
	}
	
	@Override
	public void getDefaultConfig() {
		Calendar date = Calendar.getInstance();
		String currentDate = "" + date.getTimeInMillis();
		this.config.setValue("general",  "totalGames", "0");
		this.config.setValue("general", "lastGame", currentDate);
		this.config.setValue("general", "lastGameName", null);
	}
	
	public int start() {

		this.register("GuessNumber");
		this.register("GuessWord");
		this.buildMenu();
		try {
			this.makeChoice();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return this.choice;
	}

	public void register(String game) {
		this.games.add(game);
	}

	private void buildMenu() {
		pp.delim();
		pp.println("Games in collection:");
		pp.delim();
		List<String> list = new ArrayList<>(this.games);
		for (int i = 0; i < list.size(); i++) {
			pp.println((i + 1) + ". " + list.get(i));
		}
		pp.spacer();
		pp.println("0. Exit");
		pp.delim();
	}

	private void makeChoice() throws InstantiationException, IllegalAccessException, ClassNotFoundException {

		
		String className;
		
			pp.println("Choise game:");
			System.out.print("| ");
			this.choice = scan.nextInt();
			if (this.choice == 0) {
				pp.delim();
				pp.println("Goodbye");
				pp.delim();
			} else if (this.choice > 0 && choice <= this.games.size()) {
				List<String> list = new ArrayList<>(this.games);
				className = list.get(this.choice - 1);
				Playable play = (Playable) Class.forName("games." + className).newInstance();
			//	this.config.put("lastGameName", className);
				//this.configurator.saveSettings(this.config);
				play.run();
			} else {
				pp.delim();
				pp.println("Wrong number.Try again.");
				pp.delim();
			}

	}

	@Override
	public void close() throws IOException {
		scan.close();
	}
	
	@Override
	public int getTotalGames() {
		return Integer.parseInt(this.config.getValue("general", "totalGames"));
	}
	
	@Override
	public void setTotalGames() {
		int games = Integer.parseInt(this.config.getValue("general", "totalGames"));
		String totalGames = "" + games++;
		this.config.setValue("general", "totalGames", totalGames);
		this.configurator.saveSettings(this.config);
	}

	@Override
	public Date getLastGame() {
		Calendar lastGame = Calendar.getInstance();
		lastGame.setTimeInMillis(Long.parseLong(this.config.getValue("general", "lastGame")));
		return lastGame.getTime();	
	}
	
	@Override
	public void setLastGame() {
		Calendar lastGameDate = Calendar.getInstance();
		String lastGame = "" + lastGameDate.getTimeInMillis();
		this.config.setValue("general", "lastGame", lastGame);
		this.configurator.saveSettings(this.config);		
	}

	public String getLastGameName() {
		return this.config.getValue("general", "lastGameName");
	}
	
	@Override
	public void showStats() {
		pp.println("Total Games: " + this.getTotalGames());
		pp.println("Last Game Date: " + this.getLastGame());
		pp.println("Last Game Name: " + this.getLastGameName());
	}

}
