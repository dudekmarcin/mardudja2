package sobota1911;

public class Point {

	private int x;
	private int y;
	
	public Point() {
		x = 0;
		y = 0;
	}
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Point(int[] args) {
		this.x = args[0];
		this.y = args[1];
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getX() {
		return this.x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getY() {
		return this.y;
	}
	
	public void addX(int x) {
		this.x +=x;
	}
	
	public void addY(int y) {
		this.y += y;
	}
	
	public void move(int x, int y) {
		this.addX(x);
		this.addY(y);
	}
	
	public void moveUpLeft() {
		this.move(-1, 1);
	}
	
	public void moveUpRight() {
		this.move(1, 1);
	}
	
	public void moveDownLeft() {
		this.move(-1, -1);
	}
	
	public void moveDownRight() {
		this.move(1, -1);
	}
	
	public void moveLeft() {
		this.move(-1, 0);
	}
	
	public void moveRight() {
		this.move(1, 0);
	}
	
	public void moveUp() {
		this.move(0, 1);
	}
	
	public void moveDown() {
		this.move(0, -1);
	}
	
	public int[] getDistance(int newX, int newY) {
		int[] result = new int[2];
		result[0] = Math.abs(this.x - newX);
		result[1] = Math.abs(this.y - newY);
		return result;
	}
	
	public double getVectorDistance(int x, int y) {
		int[] tmp = this.getDistance(x, y);
		return Math.sqrt(tmp[0]*tmp[0] + tmp[1]*tmp[1]);
	} 
	
	public void showXY() {
		System.out.println("X: " + this.x + ", Y: " + this.y);
	}
	
	public String toString() {
		return "X: " + this.x + ", Y: " + this.y;
	}
	
}
