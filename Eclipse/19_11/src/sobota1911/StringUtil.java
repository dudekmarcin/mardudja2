package sobota1911;

import java.util.Random;
import java.util.Scanner;

public class StringUtil {

	// Napiszmy klasę StringUtil, która będzie posiadała konstruktor
	// sparametryzowany którego zadaniem będzie przypisanie stringa do pola
	// prywatnego. Następnie dodajmy metody:

	private String string;

	public StringUtil(String str) {
		this.string = str;
	}

	public StringUtil letterSpacing() {
		// rozdziela string spacjami
		String[] temp = this.string.split("");
		this.string = "";
		for (int i = 0; i < temp.length; i++) {
			this.string += temp[i] + " ";
		}
		return this;
	}

	public StringUtil reverse() {
		String temp = "";
		for (int i = this.string.length() - 1; i >= 0; i--) {
			temp += this.string.charAt(i);
		}
		this.string = temp;
		return this;
	}

	public StringUtil getAlphabet() {
		// zwraca string z alfabetem 'abcdefgh...z';
		this.string = "";
		for (char i = 97; i < 123; i++) {
			this.string += i;
		}
		return this;
	}

	public StringUtil getFirstLetter() {
		// zwraca pierwszą literę
		this.string = "" + this.string.charAt(0);
		return this;
	}

	public StringUtil limit(int n) {
		// ucina string we wskazanym miejscu
		String temp = "";
		for (int i = 0; i < n; i++) {
			temp += this.string.charAt(i);
		}
		this.string = temp;
		return this;
	}

	public StringUtil insertAt(String string, int n) {
		// umieszcza zadany string pod wybrana pozycja
		if (n > this.string.length()) {
			for (int i = 0; i < n - this.string.length(); i++) {
				this.string += " ";
			}
		}
		this.string = this.string.substring(0, n) + string + this.string.substring(n);
		return this;
	}

	public StringUtil readText() {
		// wczytuje tekst z klawiatury i nadpisuje zmienna
		Scanner scan = new Scanner(System.in);
		System.out.println("Wpisz stringa:");
		this.string = scan.next();
		scan.close();
		return this;
	}

	public StringUtil resetText() {
		// ustawia pusty ciag znakow
		this.string = "";
		return this;
	}

	public StringUtil swapLetters() {
		// zwraca ciąg znaków z zamienioną pierwszą i ostatnią literą.
		String[] temp = this.string.split("");
		this.string = temp[temp.length - 1];
		for (int i = 1; i < temp.length - 1; i++) {
			this.string += temp[i];
		}
		this.string += temp[0];
		return this;
	}

	public StringUtil createSentence() {
		// zwraca ciąg znaków z pierwszą wielką literą i z kropką na końcu.
		// Należy sprawdzić, czy kropka na końcu nie występowała wcześniej.

		this.string = this.string.substring(0, 1).toUpperCase() + this.string.substring(1);
		if (!(this.string.endsWith("."))) {
			this.string += ".";
		}
		return this;
	}

	public StringUtil cut(int from, int to) {
		// zwraca obcięty string od..do
		// (bez użycia metody substring z klasy String!)
		String[] temp = this.string.split("");
		this.string = "";
		if (to >= temp.length) {
			to = temp.length - 1;
		}
		for (int i = from; i < to; i++) {
			this.string += temp[i];
		}
		return this;
	}

	public StringUtil pokemon() {
		// ZwRaCa cIaG zNaKoW pIsMeM pOkEmOn ;)
		String[] temp = this.string.split("");
		this.string = "";
		for (int i = 0; i < temp.length; i++) {
			this.string += (i % 2 == 0) ? temp[i].toUpperCase() : temp[i].toLowerCase();
		}
		return this;
	}

	public StringUtil getRandomHash(int n) {
		// przypisuje do zmiennej n losowych znakow z zakresu [0-9a-f];
		this.string = "";
		int[] temp = new int[2];
		Random random = new Random();
		int number;
		int i = 0;
		int k = 0;
		while(i < n) {
			number = random.nextInt(2);
			if (number == 0 && (temp[0] != number || temp[1] != number)) {
				this.string += random.nextInt(10);
				temp[k] = number;
				k++;
				i++;
			} else if(number == 1 && (temp[0] != number || temp[1] != number)) {
				this.string += (char) (97 + random.nextInt(7));
				temp[k] = number;
				i++;
				k++;
			}
			if(k==2) {
				k = 0;
			}
		}
		return this;
	}

	// Oraz metody dodatkowe, nie operujące na wczytanym stringu:

	public String join(String[] arrayOfStrings, String delimeter) {
		// zwraca String rozdzielony delimeterem.
		String[] temp = this.string.split("");
		String result = temp[0];
		for(int i = 1; i < temp.length; i++) {
			result += delimeter + temp[i];
		}
		return result;
	}

	// Metody powinny zwracać instancję samą w sobie, tak abyśmy mogli użyć
	// chaining.

	// Nadpiszmy metodę toString() w taki sposób, aby zwracała zawsze stringa
	// przypisanego do pola prywatnego.

	public String toString() {
		return this.string;
	}

}
