package sobota1911;

public class Calculator {

	private double result;
	private String[] log = new String[10];

	private void addToLog(String str) {
	
			for (int i = log.length - 1; i > 0; i--) {
				log[i] = log[i - 1];
			}
			log[0] = str;
	}

	public void showLog() {
		System.out.println("Historia ostatnich operacji: ");
		for (int i = 0; i <log.length; i++) {
			if (log[i] != null)
				System.out.println(log[i]);
		}
	}

	public double getResult() {
		return this.result;
	}

	public double add(double n) {
		addToLog(this.result + " + " + n + " = " + (this.result + n));
		this.result += n;
		return this.result;
	}

	public double substract(double n) {
		addToLog(this.result + " - " + n + " = " + (this.result - n));
		this.result -= n;
		return this.result;
	}

	public double miltiply(double n) {
		addToLog(this.result + " * " + n + " = " + (this.result * n));
		this.result *= n;
		return this.result;
	}

	public double division(double n, double m) {
		addToLog(this.result + " / " + n + " = " + (this.result / n));
		this.result /= n;
		return this.result;
	}

	public double power(double n) {
		double temp = this.result;
		if (n == 0) {
			this.result = 1;
		} else if (n > 1) {
			for (int i = 2; i <= n; i++) {
				this.result *= temp;
			}
		}
		addToLog(temp + " ^ " + n + " = " + (this.result));
		return this.result;
	}

	public void clear() {
		result = 0;
	}

}
