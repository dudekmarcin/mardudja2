package sobota1911;

public class Circle {

	private int r;
	private Point center;
	
	public Circle() {
		this.r = 0;
		this.center = new Point();
	}
	
	public Circle(int r) {
		this.r = r;
		this.center = new Point();
	}
	
	public Circle(int r, Point center) {
		this.r = r;
		this.center = center;
	}
	
	public Circle(int r, int x, int y) {
		this.r = r;
		this.center = new Point(x,y);
	}
	
	public void setR(int r) {
		this.r = r;
	}
	
	public void setCenter(Point center) {
		this.center = center;
	}
	
	public void setCenter(int x, int y) {
		this.center = new Point(x, y);
	}
	
	public boolean isInCircle(Point p) {
		return (center.getVectorDistance(p.getX(), p.getY()) <= r);
	}
	
	public boolean isInCircle(int x, int y) {
		return (center.getVectorDistance(x, y) <= r);
	}
	
}
