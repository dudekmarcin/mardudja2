package zad9;

import java.io.Closeable;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Puzzle implements Closeable {

	// Napisz klasę Puzzle, która będzie zawierała predefiniowaną tablicę słów w
	// polu oraz metody pozwalające na zgadywanie litery oraz zakończenie gry.
	// Ilość możliwych prób zgadnięcia to Math.ceil(1.5 *liczba_liter_w_slowie);
	// Pomyśl, w których momentach można dodać obsługę
	// wyjątków (np. gry brak możliwości zgadywania, przekazany zły argument
	// etc.) i ewentualnie utwórz wyjątki i dodaj je do klasy Puzzle. Metoda
	// zgadująca powinna zgłaszać wyjątek gdy brak możliwości odgadywania bądź
	// prosić użytkownika o podanie litery. Przy podaniu litery metoda powinna
	// drukować zamaskowane słowo np.
	// Ilość prób: 8/12
	// O K S Y _ O _ O _
	// Podaj literę:

	private String[] wordsList = new String[] { "oksymoron", "kaczka", "sewastopol", "kangur", "kolczyki" };
	double lives;
	String[] word;
	String[] result;
	Scanner scan = new Scanner(System.in);

	public Puzzle() {
		Random random = new Random();
		word = wordsList[random.nextInt(10) % wordsList.length].split("");
		lives = Math.ceil(1.5 * word.length);
		result = new String[word.length];
		for (int i = 0; i < word.length; i++) {
			result[i] = "_ ";
		}
	}

	public void guessWord() throws NoLivesException {
		String forResult = "";
		String letter;
		for (int i = 0; i < word.length; i++) {
			forResult += result[i];
		}
		System.out.println("Wylosowano słowo");
		System.out.println(forResult);
		
		while (lives >= 0) {
			System.out.println();
			if (lives == 0) {
				throw new NoLivesException();
			}
			System.out.println("Ilość prób: "+ lives + "/" + Math.ceil(1.5 * word.length));
			System.out.println("Podaj literę: ");
			letter = scan.next();
			
			if(letter.length() != 1) {
				System.out.println("Musisz podać dokładnie jedną literę");
				
				continue;
			}
			
			for (int i = 0; i < word.length; i++) {
				if (word[i].toUpperCase().equals(letter.toUpperCase())) {
					result[i] = letter.toUpperCase() + " ";
				}
			}

			forResult = "";
			
			for (int i = 0; i < word.length; i++) {
				forResult += result[i];
			}
			System.out.println(""+forResult);
			--this.lives;
			
			boolean isWin = true;
			for (int i = 0; i < word.length; i++) {
				if((result[i].equals("_ "))) {
					isWin = false;
				}
			}
			
			if(isWin) {
				System.out.println("\nWYGRAŁEŚ!");
				break;
			}
			
		}
	
	}

	@Override
	public void close() throws IOException {
		scan.close();
		
	}

}
