package zad2;

public class Square {
	
	public static double square(int n) throws IllegalArgumentException {
		if(n < 0) {
			throw new IllegalArgumentException("Niedozwolona jest wartość ujemna");
		} else {
			return Math.sqrt(n);
		}
	}
}
