package zad5;

import java.util.Scanner;

public class QuadricEquation {

	private int a;
	private int b;
	private int c;
	
	private int getNumber() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Podaj liczbę całkowitą:");
		return scan.nextInt();
	}
	
	public double[] solve() {
		double result[] = new double[2];
		while(this.a == 0 && this.b == 0 && this.c == 0) {
			this.a = this.getNumber();
			this.b = this.getNumber();
			this.c = this.getNumber();
		}
		
		int delta = (this.b * this.b) - 4 * a * c;
		
		if(delta < 0) {
			throw new ArithmeticException();
		} else if(delta == 0) {
			result[0] = -this.b / 2 * this.a;
			result[1] = result[0];
		} else {
			result[0] = (-this.b - Math.sqrt(delta)) / 2 * this.a;
			result[1] = (-this.b + Math.sqrt(delta)) / 2 * this.a;
		}
		
		
		return result;
	}
	// Metoda
	//
	// solve() w przypadku braku zainicjalizowania zmiennych a, b, c (lub
	// podania 0 dla wszystkich trzech parametrów) powinna wywołać
	//
	// metodę getNumber() przed dokonaniem obliczeń. Metoda powinna zwracać
	// tablicę z rozwiązaniami [x1, x2] bądź [x1, x1] i zgłaszać wyjątek ArithmeticException w 
	// przypadku ujemnej delty. Zwróć uwagę na przypadek, kiedy a = 0, b !=0, c != 0.
}
