package zad4;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {

	public double readDouble() {
		Scanner scan = new Scanner(System.in);
		double result;
		System.out.println("Podaj double:");
		try {
			result = scan.nextDouble();
		} catch (InputMismatchException e) {
			scan.close();
			return 0.0;
		}
		scan.close();
		return result;
	}

	public int readInt() {
		Scanner scan = new Scanner(System.in);
		int result;
		System.out.println("Podaj int:");
		try {
			result = scan.nextInt();
		} catch (InputMismatchException e) {
			scan.close();
			return 0;
		}
		scan.close();
		return result;
	}

	public String readString() {
		Scanner scan = new Scanner(System.in);
		String result;
		System.out.println("Podaj String:");

		try {
			result = scan.next();
		} catch (InputMismatchException e) {
			scan.close();
			return "";
		}
		scan.close();
		return result;
	}

}
