package main;

import java.util.Scanner;

import zad1.zad1;
import zad2.Square;
import zad4.ReadNumbers;
import zad6.WrongAgeException;
import zad7.Person;
import zad9.NoLivesException;
import zad9.Puzzle;

public class Poniedzialek_28_11 {

	public static void main(String[] args) {
		
		//Zad1
		
//		zad1 z1 = new zad1();
//		try {
//			int[] array = z1.getIndex(11);
//		} catch(ArrayIndexOutOfBoundsException e) {
//			System.out.println("Index " + e.getMessage() + " nie istnieje");
//		}
	
		
		//Zad2
		
//		Square sq = new Square();
//		try {
//			System.out.println(sq.square(9));
//			System.out.println(sq.square(0));
//			System.out.println(sq.square(-1));
//		} catch( IllegalArgumentException e) {
//			System.out.println(e.getMessage());
//		}
		
		//Zad4
		
//		ReadNumbers z4 = new ReadNumbers();
//		double z4d = z4.readDouble();
//		System.out.println(z4d);
		
		//Zad7
		
//		try {
//			Person bob = new Person("Bob", "Budowniczy", 130, "Brązowe", "Niebieskie", 37.5);
//		} catch (WrongAgeException e) {
//			System.out.println(e.getMessage());
//		}
		
		Puzzle z9 = new Puzzle();
		try {
			z9.guessWord();
		} catch (NoLivesException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		
	}
		
}
