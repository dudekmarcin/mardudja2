package zad8;

import java.util.ArrayList;
import java.util.List;

import zad6.WrongAgeException;
import zad7.Person;

public class People {

	// Wykorzystując klasę z poprzedniego zadania utwórz klasę People, która
	// będzie przetrzymywała listę N osób (obiektów typu Person). Klasa powinna
	// zawierać konstruktor sparametryzowany, którego parametr będzie wyznaczał
	// ilość osób na liście oraz pola i metody:
	
	private List<Person> personList = new ArrayList<>();
	private int limit;
	private final String[] allowedEyes = { "green", "brown", "blue", "black" };
	private final String[] allowedHair = { "blond", "brown", "black", "red" };
	
	public People(int howMany) {
		limit = howMany;
	}

	public void addPerson(Person person) throws FullListException {
		if(personList.size() < limit) {
			personList.add(person);
		} else {
			throw new FullListException();
		}
	}

	public void addPerson(String name, String secondName, int age, String hair, String eyes, double shoe)
			throws BadHairException, BadEyesException, BadShoeException, FullListException, WrongAgeException {
		if(personList.size() < limit) {
			for(int i = 0; i < allowedHair.length; i++) {
				if(!(allowedHair[i].equals(hair))) {
					throw new BadHairException();
				}
			}
			
			for(int i = 0; i < allowedEyes.length; i++) {
				if(!(allowedEyes[i].equals(eyes))) {
					throw new BadEyesException();
				}
			}
			
			if(shoe % 0.5 != 0) {
				throw new BadShoeException();
			}
			
			Person person = new Person(name, secondName, age, hair, eyes, shoe);
			personList.add(person);
		} else {
			throw new FullListException();
		}
	}
	// Druga metoda powinna sprawdzać czy przekazane argumenty są poprawne tj.
	// przekazane parametry hair oraz eyes zawierają się w tablicach
	// predefiniowanych. Rozmiar buta może być wartością połówkową lub całkowitą
	// (np. może przyjąć wartości: 9, 9.5, 10.50, ale nie: 9.43). W przypadku
	// podania nieprawidłowych argumentów utwórz oraz zwróć wyjątki:
	// BadHairException, BadEyesException, BadShoeException.
	// Metody addPerson() dodatkowo powinny zwracać wyjątek FullListException
	// (który należy utworzyć) w przypadku braku miejsca na liście.
}

