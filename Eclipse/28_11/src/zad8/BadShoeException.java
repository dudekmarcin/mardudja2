package zad8;

public class BadShoeException extends Exception {

	public BadShoeException() {
		super("Niedozwolony rozmiar butów");
	}
}
