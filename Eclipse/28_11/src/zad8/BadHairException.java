package zad8;

public class BadHairException extends Exception {
	
	public BadHairException() {
		super("Niedozwolony kolor włosów");
	}
}
