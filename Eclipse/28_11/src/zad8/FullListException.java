package zad8;

public class FullListException extends Exception {

	public FullListException() {
		super("Brak miejsca na liście");
	}
}
