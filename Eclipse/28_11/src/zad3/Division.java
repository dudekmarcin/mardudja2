package zad3;

public class Division {

	public static double divide(int a, int b) throws IllegalArgumentException {
		if(b == 0) {
			throw new IllegalArgumentException("Dzielenie przez zero jest niemoralne");
		} else {
			return a/b;
		}
	}
	
	public static double divide(double a, double b) {
		if(b == 0.0) {
			throw new IllegalArgumentException("Dzielenie przez zero jest niemoralne");
		} else {
			return a/b;
		}
	}
}
