package main;

import java.math.BigInteger;

public class MathHelper implements Runnable {
	private BigInteger number;
	private long longNumber;
	
	public MathHelper(BigInteger number) {
		this.number = number;
	}
	
	public MathHelper(long number) {
		this.longNumber = number;
	}

	public MathHelper() {
		// TODO Auto-generated constructor stub
	}

	public boolean checkLong() {
		for(int i = 0; i < Math.sqrt(this.longNumber); i++ ) {
			if(this.longNumber % 2 == 0) {
				return false;
			}
		}
		return true;
	}
	
	public boolean check() {
		boolean isPrime = true;
		BigInteger i = new BigInteger("2");
		while(isPrime = true && i.compareTo(number) < 0) {
			if(number.mod(i).equals(0)) {
				isPrime = false;
				break;
			}
			i = i.add(new BigInteger("1"));
		}
		
		return isPrime;
	}
	
	public String convertTime(BigInteger time) {
		BigInteger s = time.divide(new BigInteger("1000"));
		BigInteger min = new BigInteger("0");
		String ret = "";
		
		if(s.compareTo(new BigInteger("60")) == 1) {
			min = s.divide(new BigInteger("60"));
			s = s.subtract(min.multiply(new BigInteger("60")));
		}
		
		if(min.compareTo(new BigInteger("0")) == 1) {
			ret += min + " min ";
		}
		
		if(s.compareTo(new BigInteger("0")) == 1) {
			ret += s + " sec ";
		} else {
			ret += time + " ms";
		}
		
		return ret.trim();
	}
	
	
	public BigInteger sumToBi(BigInteger n) {
		BigInteger i = new BigInteger("1");
		BigInteger sum = new BigInteger("0");
		while(true) {
			sum = sum.add(i);
			if(i.compareTo(n) == 0) {
				break;
			}
			i = i.add(new BigInteger("1"));
		}
		return sum;
	}

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		boolean result =  this.check();
		long stop = System.currentTimeMillis();
		System.out.println("Czy liczba " + this.number + " jest pierwsza: " + result);
		System.out.println("Czas wykonania to: " + ((stop-start)/1000) + " sekund");

	}
	
}
