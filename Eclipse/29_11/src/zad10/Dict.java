package zad10;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Dict {

	private Word[] dict;
	private String path = "src//zad10//";
	private String filename;

	public Dict() {
		this.filename = "dict.txt";
		try {
			this.dict = new Word[this.countLines(this.filename)];
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		this.readDict(this.filename);
		

	}

	public Dict(String filename) {
		this.filename = filename;
		try {
			this.dict = new Word[this.countLines(this.filename)];
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		this.readDict(this.filename);
	}

	public void readDict(String filename) {
		File f = new File(this.path + filename);
		try {
			Scanner scan = new Scanner(f);
			int i = 0;
			while (scan.hasNextLine()) {
				String words[] = scan.nextLine().split("\t");
				this.dict[i] = new Word(words[0].toLowerCase(), words[1].toLowerCase());
				i++;
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public void addWord(String polishWord, String englishWord) {
		polishWord = polishWord.toLowerCase();
		englishWord = englishWord.toLowerCase();
		
		if (!(this.isPolishWordExists(polishWord))) {
			File f = new File(this.path + this.filename);
			try {
				FileOutputStream fos = new FileOutputStream(f, true);
				PrintWriter pw = new PrintWriter(fos);
				pw.println(polishWord + "\t" + englishWord);
				pw.close();
				this.dict = new Word[this.countLines(this.filename)];
				this.readDict(this.filename);

			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		} else {
			System.out.println("Słowo " + polishWord + " jest już w słowniku.");
		}
	}

	public boolean isPolishWordExists(String polishWord) {
		boolean isExists = false;
		polishWord = polishWord.toLowerCase();
		
		for (int i = 0; i < this.dict.length; i++) {
			if (this.dict[i].getPolishWord().equals(polishWord)) {
				isExists = true;
				break;
			}
		}

		return isExists;
	}
	
	public boolean isEnglishWordExists(String englishWord) {
		boolean isExists = false;
		englishWord = englishWord.toLowerCase();
		
		for (int i = 0; i < this.dict.length; i++) {
			if (this.dict[i].getEnglishWord().equals(englishWord)) {
				isExists = true;
				break;
			}
		}

		return isExists;
	}

	public void removeWord(String polishWord) {
		polishWord = polishWord.toLowerCase();

		if (this.isPolishWordExists(polishWord)) {
			File f = new File(this.path + this.filename);
			try {
				FileOutputStream fos = new FileOutputStream(f);
				PrintWriter pw = new PrintWriter(fos);
				for (int i = 0; i < this.dict.length; i++) {
					if (!this.dict[i].getPolishWord().equals(polishWord)) {
						pw.println(this.dict[i].getPolishWord() + "\t" + this.dict[i].getEnglishWord());
					}
				}
				pw.close();
				this.dict = new Word[this.countLines(this.filename)];
				this.readDict(this.filename);
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
			
		} else {
			System.out.println("Słowa " + polishWord + " nie ma w słowniku.");
		}
	}

	public String translateToEn(String polishContent) {
		polishContent = polishContent.toLowerCase();
		
		String result = "";
		if(this.isPolishWordExists(polishContent)) {
		for (int i = 0; i < this.dict.length; i++) {
			if (this.dict[i].getPolishWord().equals(polishContent)) {
				result = this.dict[i].getEnglishWord();
			}
		}
		}else {
			System.out.println("Słowo " + polishContent + "nie zostało znalezione");
		}
			
		return result;
	}

	public String translateToPl(String englishContent) {
		englishContent = englishContent.toLowerCase();
		
		String result = "";
		if(this.isEnglishWordExists(englishContent)) {
		for (int i = 0; i < this.dict.length; i++) {
			if (this.dict[i].getEnglishWord().equals(englishContent)) {
				result = this.dict[i].getPolishWord();
			}
		}
		}else {
			System.out.println("Word " + englishContent + " not found");
		}
			
		return result;
	}

	private int countLines(String filename) throws FileNotFoundException {
		File f = new File(this.path + filename);
		Scanner scan = new Scanner(f);
		int i = 0;
		while (scan.hasNextLine()) {
			i++;
			scan.nextLine();
		}
		scan.close();
		return i;
	}
}
