package zad10;

public class Word {

	private String polishWord;
	private String englishWord;
	
	public Word(String polishWord, String englishWord) {
		this.polishWord = polishWord;
		this.englishWord = englishWord;
	}
	
	
	public String getPolishWord() {
		return this.polishWord;
	}
	public void setPolishWord(String polishWord) {
		this.polishWord = polishWord;
	}
	public String getEnglishWord() {
		return this.englishWord;
	}
	public void setEnglishWord(String englishWord) {
		this.englishWord = englishWord;
	}
	
	
}
