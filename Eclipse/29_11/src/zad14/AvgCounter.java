package zad14;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class AvgCounter implements Closeable {

	private int firstNum = 0;
	private int lastNum = 0;
	String path;
	Scanner user = new Scanner(System.in);
	
	public AvgCounter() {
		path = "src//zad14//test_";
	}
	
	public AvgCounter(String path) {
		path = this.path;
	}
	
	private void  getFilesNumber() {
		System.out.println("Podaj liczbę początkową:");
		this.firstNum = this.user.nextInt();
		System.out.println("Podaj liczbę końcową:");
		this.lastNum = this.user.nextInt();
	}
	
	public double countAvg() {
		double avg = 0.0;
		getFilesNumber();
		
		for(int i = this.firstNum; i <= this.lastNum; i++) {
			avg += readFileAvg(i);
		}
		
		return avg / (this.lastNum+1 - this.firstNum);
	}
	
	public double readFileAvg(int fileNumber) {
		File f = new File(this.path+fileNumber+".txt");
		double avg = 0.0;
		int i = 0;

		try {
			Scanner scan = new Scanner(f);
			while(scan.hasNextLine()) {
				avg += Double.parseDouble(scan.nextLine().replace(",",  "."));
				i++;
			}
			
		} catch (FileNotFoundException e) {
		System.out.println(e.getMessage());
		}
		return avg/i;
	}

	@Override
	public void close() throws IOException {
		user.close();
	}
	
}
