package zad1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sentence {

	// Napisz klasę Sentence, która będzie zawierała metodę:
	// +readSentence(filename:String):String
	// odczytującą kolejne linie z pliku tekstowego i zwracającą je jako zdanie
	// (pierwsza litera duża, na końcu kropka). Wykorzystaj plik test.txt z
	// pakietu zad1. Obsłuż wyjątek FileNotFoundException w metodzie
	// readSentence()

	public String readSentence(String filename) {

		File f = new File("src\\zad1\\"+filename);
		String result = "";
		try {
			Scanner scan = new Scanner(f);
			
			while(scan.hasNextLine()) {
				result += scan.nextLine() + " ";
			}

			result = result.substring(0, 1).toUpperCase() + result.substring(1, result.length() - 1);
			
			if (!(result.endsWith("."))) {
				result += ".";
			}
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		
		
		return result;
		
	}

}
