package zad2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import zad1.Sentence;

public class Sentencer extends Sentence {

//	Napisz klasę Sentencer będącą rozszerzeniem klasy Sentence i dostarczającą metodę:
//		+writeSentence(filename:String, sentence:String):void
//		która zapisze do pliku tekstowego przekazany ciąg znaków. Plik zapisz w pakiecie zad2 pod nazwą mySentence.txt
		
	public void writeSentence(String filename, String sentence) {
		
		File f = new File("src\\zad2\\" + filename);
		try {
			FileOutputStream fos = new FileOutputStream(f, true);
			PrintWriter pw = new PrintWriter(fos);
			pw.println(sentence);
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
	
}
