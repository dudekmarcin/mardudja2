package zad5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {

	// Napisz klasę LengthChecker, która dostarczy metody:
	// -isProperLength(String arg, int len):boolean
	// -readFile(String filename):String[]
	// -writeFile(String[] fileContent):void
	// +make(String fileInput, int len):void
	// Działanie klasy powinno wyglądać następująco; w metodzie make()
	// wywołujemy pozostałe prywatne metody w taki sposób, aby w pliku wynikowym
	// znalazły się słowa dłuższe niż przekazany drugi argument. Do odczytu słów
	// użyj pliku words.txt, wynik pracy zapisz words_X.txt, gdzie X to właśnie
	// drugi przekazany parametr metody make().

	private boolean isProperLength(String arg, int len) {
		return (arg.length() > len);
	}

	private String[] readFile(String filename) {
		String[] result;
		String words = "";
		File f = new File("src\\zad5\\words.txt");
		try {
			Scanner scan = new Scanner(f);
			while (scan.hasNextLine()) {
				words += scan.nextLine() + "\t";
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		result = words.split("\t");
		return result;

	}

	private void writeFile(String[] fileContent, int len) {
		File f = new File("src\\zad5\\words_"+len+".txt");
		
		try {
			FileOutputStream fos = new FileOutputStream(f, true);
			PrintWriter pw = new PrintWriter(fos);
			for(int i = 0; i < fileContent.length; i++) {
				if(this.isProperLength(fileContent[i], len)) {
					pw.println(fileContent[i]);
				}
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public void make(String fileInput, int len) {
		String[] words = this.readFile(fileInput);
		this.writeFile(words, len);
	}

}
