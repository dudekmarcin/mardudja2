package zad15;

public class Data {

	private String label;
	private double value;
	private double percentValue = 0;
	
	public Data(String label, int value) {
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public double getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public double getPercentValue() {
		return this.percentValue;
	}
	
	public void setPercentValue(double percentValue) {
		this.percentValue = percentValue;
	}
	
}
