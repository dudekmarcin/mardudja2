package zad15;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Graph {

	private Data[] datas;
	private String path = "src//zad15//";
	private String filename;
	private double sum;
	
	public Graph() {
		this.filename = "data.txt";
		try {
			this.datas = new Data[this.countLines()];
			this.readFile();
			this.getPercentValue();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public Graph(String filename) {
		this.filename = filename;
		try {
			this.datas = new Data[this.countLines()];
			this.readFile();
			this.getPercentValue();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void updateGraph() {
		try {
			this.datas = new Data[this.countLines()];
			this.readFile();
			this.getPercentValue();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void getPercentValue() {
		for(int i = 0; i < datas.length; i++) {
			datas[i].setPercentValue(datas[i].getValue() / this.sum * 100); 
		}
	}
	
	public void drawGraph() {
		String column = "";
		for(int i = 0; i < datas.length; i++) {
			
			for(int j = 1; j <= datas[i].getPercentValue(); j++) {
				column += "# ";
			}
			System.out.println(i+1 + " | " + column + " (" + datas[i].getPercentValue() + "%)");
			column = "";
		}
		System.out.println();
		for(int i = 0; i < datas.length; i++) {
			System.out.println(i+1 + " - " + datas[i].getLabel());
		}
	}
	
	public void drawVerticalGraph() {
		String column = "";
		String labelsNum = "";
		String percentValues = "";
		double max = datas[0].getPercentValue();
		
		for(int i = 0; i < datas.length; i++) {
			max = (datas[i].getPercentValue() > max) ? datas[i].getPercentValue() : max;
			labelsNum += "  " + (i+1) + "    ";
			percentValues += datas[i].getPercentValue()+ "%  ";
		}
		
		for(double i = max; i > 0; i--) {
			
			for(int j = 0; j < datas.length; j++) {
				if(datas[j].getValue() >= i) {
					column += "   #   ";
				} else {
					column +="       ";
				} 
			}
			if(i > 1) {
			column += "\n";
			}
		}
		System.out.println(column);
		System.out.println(percentValues);
		System.out.println(labelsNum);
		System.out.println();
		for(int i = 0; i < datas.length; i++) {
			System.out.println(i+1 + " - " + datas[i].getLabel());
		}
	}
	
	private void readFile() throws FileNotFoundException {
		File f = new File(this.path + this.filename);
			Scanner scan = new Scanner(f);
			int i = 0;
			this.sum = 0;
			while(scan.hasNextLine()) {
				String[] line = scan.nextLine().split("\t");
				datas[i] = new Data(line[0], Integer.parseInt(line[1]));
				this.sum += datas[i].getValue();
				i++;
			}
			scan.close();
	}
	
	private int countLines() throws FileNotFoundException {
		File f = new File(this.path + this.filename);
		Scanner scan = new Scanner(f);
		int i = 0;
		while (scan.hasNextLine()) {
			i++;
			scan.nextLine();
		}
		scan.close();
		return i;
	}
	
	
}
