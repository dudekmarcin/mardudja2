package zad9;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class PresentChecker implements Closeable {

	// Napisz klasę PresentChecker, która dostarczy metody i pola:
	// -filename:String
	// +PresentChecker
	// +PresentChecker(filename:String)
	// -checkIfExists(sentence:String):boolean
	// +readWords():void
	// Metoda readWords() powinna wczytywać od użytkownika słowa,
	// sprawdzać czy jest ono zapisane w pliku i zwracać informację do konsoli
	// czy podane słowo występuje w słowniku czy nie. Słowa zapisane są w pliku
	// words.txt w pakiecie zad9.
	private Scanner user = new Scanner(System.in);
	private String filename;
	private String path = "src//zad9//";

	public PresentChecker() {
		this.filename = "words.txt";
	}

	public PresentChecker(String filename) {
		this.filename = filename;
	}

	public void readWords() {
		System.out.println("Podaj słowo do sprawdzenia:");
		String result = (this.checkIfExists(user.nextLine())) ? "Podane słowo znajduje się w słowniku"
				: "Podanego słowa nie ma w słowniku";
		System.out.println(result);
	}

	private boolean checkIfExists(String sentence) {
		File f = new File(this.path + this.filename);
		boolean isExist = false;
		try {
			Scanner scan = new Scanner(f);
			while (scan.hasNextLine()) {
				if (scan.nextLine().equals(sentence)) {
					isExist = true;
					break;
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return isExist;
	}

	@Override
	public void close() throws IOException {
			this.user.close();

	}

}
