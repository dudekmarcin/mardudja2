package zad4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {

	// Napisz klasę TempConverter, która będzie dostarczała metody:
	// +toKelvin(temp:double):double
	// +toFahrenheit(temp:double):temp
	// +readTemp(filename:String):double[]
	// +writeTemp(temp:double[]):void
	// W pliku tekstowym tempC.txt w pakiecie zad4 znajdują się poszczególne
	// temperatury dla danych miesięcy zapisane jako temperatura w Celciusach.
	// Do plików tempK.txt oraz tempF.txt zapisz (w pakiecie zad3) odpowiednio
	// wyniki przekonwertowanej temperatury w Kelvinach i Fahrenheitach.

	public double toKelvin(double temp) {
		return temp + 273.15;
	}
	
	public double toFahrenheit(double temp) {
		return 5 * (temp - 32) / 9;
	}
	
	public double[] readTemp(String filename) {
		File f = new File("src\\zad4\\"+filename);
		double[] result = new double[12];
		try {
			int i = 0;
			Scanner scan = new Scanner(f);
			while(scan.hasNextLine()) {
				result[i] = scan.nextDouble();
				++i;
			}
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		return result;
	}
	
	public void writeTempK(double[] temp) {
		File fK = new File("src\\zad4\\tempK.txt");
		try {
			FileOutputStream fos = new FileOutputStream(fK, true);
			PrintWriter pw = new PrintWriter(fos);
			for(int i = 0; i < temp.length; i++) {
				pw.println(toKelvin(temp[i]));
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
	public void writeTempF(double[] temp) {
		File fF = new File("src\\zad4\\tempF.txt");
		try {
			FileOutputStream fos = new FileOutputStream(fF, true);
			PrintWriter pw = new PrintWriter(fos);
			for(int i = 0; i < temp.length; i++) {
				pw.println(toFahrenheit(temp[i]));
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
}
