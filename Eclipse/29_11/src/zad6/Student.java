package zad6;

public class Student {
	// Napisz klasę Student, która będzie dostarczała pola:
	// -index:int
	// -name:String
	// -secondName:String
	// -course:String
	// -averageMark:double
	// oraz konstruktory (bez i sparametryzowany), gettery i settery.
	// Następnie utwórz klasę University, która będzie zawierała następujące
	// metody:
	// -isStudentExists(index:int):boolean
	// +getStudent(index:int):Student
	// +putStudent(student:Student):void
	// Klasa Univeristy powinna działać na pliku students.txt znajdującym się w
	// pakiecie zad6. Format pliku powinien być następujący (bez używania
	// serializacji obiektu):
	// 123 Paweł Testowy Algorytmy 4.12
	// Wartości powinny być rozdzielone znakiem tabulacji "\t". Metoda
	// getStudent()
	// powinna zwracać nową instancję klasy Student z uzupełnionymi danymi
	// studenta według indeksu, a metoda putStudent() dodawać studenta na końcu
	// listy, w przypadku, gdy taki student nie istnieje. Do sprawdzenia
	// występowania studenta o zadanym indeksie wykorzystaj prywatną metodę
	// isStudentExists().
	
	private int index;
	private String name;
	private String secondName;
	private String course;
	private double averageMark;
	
	public Student() {
		this.index = 0;
		this.name = "Bob";
		this.secondName = "Budowniczy";
		this.course = "Budownictwo";
		this.averageMark = 0.0;
	}
	
	public Student(int index, String name, String secondName, String course, double avg) {
		this.index = index;
		this.name = name;
		this.secondName = secondName;
		this.course = course;
		this.averageMark = avg;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public double getAverageMark() {
		return averageMark;
	}

	public void setAverageMark(double averageMark) {
		this.averageMark = averageMark;
	}
	
	
	
}
