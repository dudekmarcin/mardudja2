package zad6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class University {

	// Następnie utwórz klasę University, która będzie zawierała następujące
	// metody:
	// -isStudentExists(index:int):boolean
	// +getStudent(index:int):Student
	// +putStudent(student:Student):void
	// Klasa Univeristy powinna działać na pliku students.txt znajdującym się w
	// pakiecie zad6. Format pliku powinien być następujący (bez używania
	// serializacji obiektu):
	// 123 Paweł Testowy Algorytmy 4.12
	// Wartości powinny być rozdzielone znakiem tabulacji "\t". Metoda
	// getStudent()
	// powinna zwracać nową instancję klasy Student z uzupełnionymi danymi
	// studenta według indeksu, a metoda putStudent() dodawać studenta na końcu
	// listy, w przypadku, gdy taki student nie istnieje. Do sprawdzenia
	// występowania studenta o zadanym indeksie wykorzystaj prywatną metodę
	// isStudentExists().
	
	private boolean isStudentExists(int index) {
		boolean result = false;
		File f = new File("src\\zad6\\students.txt");
		try {
			Scanner scan = new Scanner(f);
			String indx = "" + index;
			while(scan.hasNextLine()) {
				String[] line = scan.nextLine().split("\t");
				if(line[0].equals(indx)) {
					result = true;
					break;
				}
			scan.close();	
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return result;
	}
	
	public Student getStudent(int index) {
		
			File f = new File("src\\zad6\\students.txt");
			Student student = null;
			try {
				Scanner scan = new Scanner(f);
				String indx = "" + index;
				while(scan.hasNextLine()) {
					String temp = scan.nextLine();
					if(temp.contains(indx)) {
						String[] studentData = temp.split("\t");
						student = new Student(Integer.parseInt(studentData[0]), studentData[1], studentData[2], studentData[3], Double.parseDouble(studentData[4].replace(",", ".")));
						return student;
					} 
				scan.close();	
				}
				if(student == null) {
					throw new IllegalArgumentException("Nie ma studenta o podanym numerze indeksu");
				}
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
			return null;
	}
	
	public void putStudent(Student student) {
		if(!(this.isStudentExists(student.getIndex()))) {
			File f = new File("src\\zad6\\students.txt");
			try {
				FileOutputStream fos = new FileOutputStream(f, true);
				PrintWriter pw = new PrintWriter(fos);
				pw.println(student.getIndex() + "\t" + student.getName() + "\t" + student.getSecondName() + "\t" + student.getCourse() + "\t" + student.getAverageMark());
				pw.close();
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
			
		} else {
			System.out.println("Student z podanym numerem indeksu jest już zapisany!");
		}
	}
	
	
	
}
