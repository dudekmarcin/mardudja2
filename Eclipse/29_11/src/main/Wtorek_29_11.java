package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import zad1.Sentence;
import zad10.Dict;
import zad11.FileIO;
import zad14.AvgCounter;
import zad15.Graph;
import zad2.Sentencer;
import zad3.MoneyConverter;
import zad4.TempConverter;
import zad5.LengthChecker;
import zad7.AvgChecker;
import zad9.PresentChecker;

public class Wtorek_29_11 {
	public static void main(String[] args) throws FileNotFoundException {
	
//		File f = new File("src//main//test.txt");
//		Scanner scan = new Scanner(f);
//
//		while (scan.hasNextLine()) {
//			System.out.println(scan.nextLine());
//		}
//
//		File f2 = new File("src\\main\\test2.txt");
//		FileOutputStream fos = new FileOutputStream(f2, true);
//		// true otwiera plik w trybie  append, inaczej plik
//		// zostanie nadpisany (poprzednia zawartosc
//		// zostanie utracona);
//		
//		PrintWriter pw = new PrintWriter(fos);
//		pw.println("xx Pryzkładowy tekst pochodzący z naszej aplikacji");
//		pw.close();
		
//		Sentence z1 = new Sentence();
//		System.out.println(z1.readSentence("test.txt"));
//		
//		Sentencer z2 = new Sentencer();
//		z2.writeSentence("mySentence.txt", "Głębokie przemyślenie filozoficzne");
//		z2.writeSentence("mySentence.txt", "Cytat z Paulo Coehlo");
		
		
//		MoneyConverter z3 = new MoneyConverter();
//		double tmp = z3.convert(100, "EUR", "USD");
//		System.out.println(tmp);
//		
//		System.out.println(z3.getCourse("ISK"));
//		System.out.println(z3.convert(100, "ISK", "USD"));
//		
//		TempConverter z4 = new TempConverter();
//		 z4.writeTempF(z4.readTemp("tempC.txt"));
//		 z4.writeTempK(z4.readTemp("tempC.txt"));

//		LengthChecker z5 = new LengthChecker();
//		z5.make("words.txt", 8);
		
//		AvgChecker z7 = new AvgChecker("marks.txt");
//		z7.process();

//		PresentChecker z9 = new PresentChecker();
//		z9.readWords();
//		z9.readWords();
		
//		Dict z10 = new Dict();
//		z10.addWord("kaczka", "duck");
//		System.out.println(z10.translateToPl("duck"));
//		z10.addWord("dom", "house");
//		System.out.println(z10.translateToPl("house"));
//		z10.removeWord("kaczka");
//		System.out.println(z10.translateToPl("duck"));
		
		FileIO z11 = new FileIO("loremipsum.txt");
		String[] file = z11.readFile();
//		System.out.println(file.length);
//		System.out.println(file[5]);
//		System.out.println(z11.readFileAsString());
//		System.out.println(z11.readLine(10));
		file = z11.readLines(14, 15);
//		for(int i = 0; i < file.length; i++) {
//				System.out.println(file[i]);
//		}
//		String example = "Example";
//		String[] content = new String[] {"Ex1", "Ex2", "Ex3"};
////		z11.insertBefore(10, example);
////		z11.insertAfter(20, example);
//		//z11.append(example);
//		//z11.copyFile("lorem2.txt");
//		//z11.updateLine(5, example);
//		z11.updateLines(13, content);
		
		
//		Graph z15 = new Graph();
//		z15.drawGraph();
//		z15.drawVerticalGraph();
		
		AvgCounter z14 = new AvgCounter();
		double avg = z14.countAvg();
		System.out.println(avg);
		avg = z14.countAvg();
		System.out.println(avg);

	}
}