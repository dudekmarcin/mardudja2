package zad8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Columner {

	// Napisz klasę Columner zawierającą metody i pola:
	// -filename:String
	// -currentColumn:double[]
	// +Columner
	// +Columner(filename:String)
	// +sumColumn(column:int):double
	// +avgColumn(column:int):double
	// +countColumn(column:int):int
	// +maxColumn(column:int):double
	// +minColumn(column:int):double
	// -readFile():void
	// +writeColumn(filename:String):void
	// +readColumn(column:int)
	// Metoda readColumn() powinna odczytywać wartości aktualnej kolumny do pola
	// currentColumn. Metody *Column powinny zwracać sumę, średnią elementów
	// oraz ich ilość,
	// maksymalny i minimalny element.
	// Metoda writeColumn() powinna zapisywać wartości wybranej kolumny do
	// pliku. W przypadku
	// braku danej kolumny (ilość kolumn w pliku mniejsza niż przekazany
	// argument) wyrzuć wyjątek
	// IllegalArgumentException.

	private String filename;
	private double[] currentColumn;
	private String path = "src//zad8//";

	public Columner() {
		this.filename = "data.txt";
		try {
			this.currentColumn = new double[this.countLines()];
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	public Columner(String filename) {
		this.filename = filename;
		try {
			this.currentColumn = new double[this.countLines()];
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public double sumColumn(int column) {
		this.readColumn(column);
		double sum = 0.0;
		for(int i = 0; i < this.currentColumn.length; i++) {
			sum += this.currentColumn[i];
		}
		return sum;
	}
	
	public double avgColumn(int column) {
		this.readColumn(column);
		double avg = 0.0;
		for(int i = 0; i < this.currentColumn.length; i++) {
			avg += this.currentColumn[i];
		}
		return avg/this.currentColumn.length;
	}

	public int countColumn(int column) {
		this.readColumn(column);
		return this.currentColumn.length;
	}

	public double maxColumn(int column) {
		this.readColumn(column);
		double max = this.currentColumn[0];
		for(int i = 1; i < this.currentColumn.length; i++) {
			max = (max < this.currentColumn[i]) ? this.currentColumn[i] : max;
		}
		
		return max;
		
	}

	public double minColumn(int column) {
		this.readColumn(column);
		double min = this.currentColumn[0];
		for(int i = 1; i < this.currentColumn.length; i++) {
			min = (min > this.currentColumn[i]) ? this.currentColumn[i] : min;
		}
		
		return min;
	}

	private void readFile() {
		
	}

	public void writeColumn(String filename) {
		File f = new File(this.path+filename);
		try {
			FileOutputStream fos = new FileOutputStream(f, true);
			PrintWriter pw = new PrintWriter(fos);
			for(int i = 0; i < this.currentColumn.length; i++) {
				pw.println(this.currentColumn[i]);
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public void readColumn(int column) throws IllegalArgumentException {
		File f = new File(this.path + this.filename);
		try {
			if (column > this.countColumns()) {
				throw new IllegalArgumentException("Nie ma takiej kolumny");
			}
		} catch (FileNotFoundException e1) {
			System.out.println(e1.getMessage());
		}
		
		try {
			Scanner scan = new Scanner(f);
			int i = 0;
			while (scan.hasNextLine()) {
				String[] row = scan.nextLine().split("\t");
				this.currentColumn[i] = Double.parseDouble(row[column].replace(",", "."));
				i++;
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	private int countLines() throws FileNotFoundException {
		File f = new File(this.path + this.filename);
		Scanner scan = new Scanner(f);
		int i = 0;
		while (scan.hasNextLine()) {
			i++;
			scan.nextLine();
		}
		return i;
	}

	private int countColumns() throws FileNotFoundException {
		File f = new File(this.path + this.filename);
		Scanner scan = new Scanner(f);
		int result = 0;
		while (scan.hasNextLine()) {
			String[] rows = scan.nextLine().split("\t");
			result = (result < rows.length) ? rows.length : result;
		}
		return result;
	}

}
