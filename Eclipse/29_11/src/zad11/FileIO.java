package zad11;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileIO {
	// Napisz klasę FileIO, która będzie zawierała następujące pola i metody:
	// -filename:String
	// +readFile():String[]
	// +readFileAsString():String
	// +readLine(line:int):String
	// +readLines(lineStart:int, lineEnd:int):String[]
	// +writeLine(line:int, content:String):void
	// +writeLines(lineStart:int, content:String[]):void
	// +updateLine(line:int, content:String):void
	// +updateLines(lineStart:int, content:String[]):void
	// +append(content:String):void
	// +insertBefore(line:int, content:String):void
	// +insertAfter(line:int, content:String):void
	// +copyFile(newFilename:String):void
	// które będą realizowały odczyt, zapis poszczególnych wartości (linii)
	// według przekazanych parametrów.
	// Następnie napiszmy aplikację, która:
	// Korzystając z metody readLines(); napisz program, który pozwoli na
	// wczytanie z klawiatury od
	// użytkownika linii początkowej, linii końcowej oraz ilości wyświetlanych
	// rekordów. Program powinien pozwolić
	// na przewijanie i doczytywanie kolejnych / poprzednich rekordów
	// wyświetlając domyślnie tylko tyle, ile mieści
	// się we wczytanym zakresie.

	private String filename;
	private final String path = "src//zad11//";

	public FileIO(String filename) {
		this.filename = filename;
	}

	public String[] readFile() {
		File f = new File(this.path + this.filename);
		String[] file = new String[1];
		try {
			file = new String[this.countLines(this.filename)];
			Scanner scan = new Scanner(f);
			int i = 0;
			while (scan.hasNextLine()) {
				file[i] = scan.nextLine();
				i++;

			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return file;
	}

	public String readFileAsString() {
		File f = new File(this.path + this.filename);
		String file = "";
		try {
			Scanner scan = new Scanner(f);
			while (scan.hasNextLine()) {
				file += scan.nextLine() + " ";
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return file;
	}

	public String readLine(int line) throws IllegalArgumentException {
		File f = new File(this.path + this.filename);
		String[] file = new String[1];
		try {
			file = new String[this.countLines(this.filename)];
			if (line > file.length) {
				throw new IllegalArgumentException("Plik nie ma aż tylu linii.");
			}

			Scanner scan = new Scanner(f);
			int i = 0;
			while (scan.hasNextLine()) {
				file[i] = scan.nextLine();
				i++;
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return file[line - 1];
	}

	public String[] readLines(int lineStart, int lineEnd) {
		File f = new File(this.path + this.filename);

		try {
			if (lineStart > this.countLines(this.filename) || lineEnd > this.countLines(this.filename)) {
				throw new IllegalArgumentException("Plik nie ma aż tylu linii.");
			}
		} catch (FileNotFoundException e1) {
			System.out.println(e1.getMessage());
		}

		String[] file = new String[lineEnd - lineStart + 1];

		try {

			Scanner scan = new Scanner(f);
			int i = 0;
			int j = 0;
			while (scan.hasNextLine()) {
				if(i >= lineStart && i <= lineEnd) {
					file[j] = scan.nextLine();
					j++;
				} else {
					scan.nextLine();
				}
				
				i++;
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return file;
	}

	public void writeLine(int line, String content) {
		this.insertBefore(line+1, content);
	}

	public void writeLines(int lineStart, String[] content) {
		for(int i = 0; i < content.length; i++, lineStart++) {
			this.insertBefore(lineStart+1, content[i]);
		}
	}

	public void updateLine(int line, String content) {
		File f = new File(this.path + this.filename);
		try {
			String[] file = new String[this.countLines(this.filename)];
			Scanner scan = new Scanner(f);
			int i = 0;
			while(scan.hasNextLine()) {
				if(i == line-1) {
					file[i] = content;
					scan.nextLine();
				} else {
					file[i] = scan.nextLine();
				}
				i++;
			}
			scan.close();
		FileOutputStream fos = new FileOutputStream(f);
		PrintWriter pw = new PrintWriter(fos);
		for(i = 0; i < file.length; i++) {
			pw.println(file[i]);
		}
		pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}	
	}

	public void updateLines(int lineStart, String[] content) {
		File f = new File(this.path + this.filename);
		try {
			String[] file = new String[this.countLines(this.filename)];
			Scanner scan = new Scanner(f);
			int i = 0;
			int j = 0;
			while(scan.hasNextLine()) {
				if(i >= lineStart-1 && j < content.length) {
					file[i] = content[j];
					scan.nextLine();
					j++;
				} else {
					file[i] = scan.nextLine();
				}
				i++;
			}
			scan.close();
		FileOutputStream fos = new FileOutputStream(f);
		PrintWriter pw = new PrintWriter(fos);
		for(i = 0; i < file.length; i++) {
			pw.println(file[i]);
		}
		pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}	
	}
	

	public void append(String content) {
		File f = new File(this.path + this.filename);
		try {
			FileOutputStream fos = new FileOutputStream(f, true);
			PrintWriter pw = new PrintWriter(fos);
			pw.println(content);
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public void insertBefore(int line, String content) {
		String[] file = this.readFile();
		File f = new File(this.path+this.filename);
		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			for(int i = 0; i < file.length; i++) {
				if(i == line-1) {
					pw.println(content);
				}
				pw.println(file[i]);
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public void insertAfter(int line, String content) {
		String[] file = this.readFile();
		File f = new File(this.path+this.filename);
		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			for(int i = 0; i < file.length; i++) {
				pw.println(file[i]);
				if(i == line-1) {
					pw.println(content);
				}
				
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public void copyFile(String newFilename) {
		File f1 = new File(this.path + this.filename);
		File f2 = new File(this.path + newFilename);
		try {
			Scanner scan = new Scanner(f1);
			FileOutputStream fos = new FileOutputStream(f2);
			PrintWriter pw = new PrintWriter(fos);
			
			while(scan.hasNextLine()) {
				pw.println(scan.nextLine());
			}
			
			pw.close();
			scan.close();
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
	}

	private int countLines(String filename) throws FileNotFoundException {
		File f = new File(this.path + filename);
		Scanner scan = new Scanner(f);
		int i = 0;
		while (scan.hasNextLine()) {
			i++;
			scan.nextLine();
		}
		scan.close();
		return i;
	}
}
