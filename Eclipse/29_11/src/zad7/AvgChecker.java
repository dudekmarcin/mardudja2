package zad7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class AvgChecker {

	// Utwórz klasę AvgChecker, która dostarczy pola i metody:
	// -filename:String
	// +AvgChecker(String filename)
	// +process():void
	// Konstruktor powinien ustawiać pole prywatne na wartość przekazaną w
	// parametrze. Metoda process() powinna odczytać wszystkie wartości z pliku
	// przekazanego jako parametr konstruktora, a następnie nadpisać plik w taki
	// sposób, aby znalazły się w nim wartości powyżej średniej. Średnią należy
	// liczyć poziomo. według kolumn, które są rozdzielone znakiem tabulacji.
	// Przykład:
	// Paweł Testowy 5 6 2
	// Daje średnią 4,33(3). Jeżeli średnia wszystkich średnich z pliku będzie
	// mniejsza niż 4,33(3) taki wpis powinien pozostać w pliku.

	private String filename;
	private String path = "src//zad7//";

	public AvgChecker(String filename) {
		this.filename = filename;
	}

	public void process() {
		File f = new File(this.path + filename);
		try {
			Scanner scan = new Scanner(f);
			String[] lines = new String[this.countLines()];
			Double[] gradesAvg = new Double[this.countLines()];
			int i = 0;
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String tempPeople[] = line.split("\t");
				String tempGrades[] = tempPeople[1].split(" ");
				lines[i] = line;
				gradesAvg[i] = 0.0;
				for (int j = 0; j < tempGrades.length; j++) {
					gradesAvg[i] += Double.parseDouble(tempGrades[j].replace(",", "."));
				}
				gradesAvg[i] /= tempGrades.length;

				i++;
			}
			scan.close();
			double totalAvg = 0.0;
			for (i = 0; i < gradesAvg.length; i++) {
				totalAvg += gradesAvg[i];
			}
			totalAvg /= gradesAvg.length;
			System.out.println("Średnia ogólna: "+ totalAvg);

			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			for (i = 0; i < lines.length; i++) {
				if (gradesAvg[i] >= totalAvg) {
					pw.println(lines[i]);
				}
			}
			pw.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private int countLines() throws FileNotFoundException {
		File f = new File(this.path + this.filename);
		Scanner scan = new Scanner(f);
		int i = 0;
		while (scan.hasNextLine()) {
			i++;
			scan.nextLine();
		}
		return i;
	}

}
