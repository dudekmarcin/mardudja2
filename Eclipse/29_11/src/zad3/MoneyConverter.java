package zad3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Pattern;

public class MoneyConverter {

	// Napis klasę MoneyConverter, która dostarczy metody:
	// -readCourse(currency:String):double
	// +convert(money:double, to:String):double
	// +convert(money:double, to:String, from:String):double
	// Prywatna metoda readCourse() powinna zwracać kurs dla przekazanej waluty
	// jako wartość double.
	// Jeżeli zostanie wywołana metoda convert() bez trzeciego parametru
	// przyjmujemy, że przeliczanie kosztów nastąpi z waluty PLN. Wykorzystaj
	// plik currency.txt do dokonania testowych obliczeń.

	private double readCourse(String currency) {
		double current_course = 0.0;
		File course = new File("src\\zad3\\currency.txt");
		try {
			Scanner scan = new Scanner(course);

			while (scan.hasNextLine()) {

				String[] tmp = scan.nextLine().split("\t");
				String[] tmp2 = tmp[0].split(" ");
				//String.contains zamiast tmp array
				if (tmp2[1].equals(currency.toUpperCase())) {					
					current_course = Double.parseDouble(tmp[1].replace(",", "."));
					current_course /= Integer.parseInt(tmp2[0]);
				}		
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return current_course;

	}

	public double getCourse(String currency) {
		return this.readCourse(currency);
	}
	
	public double convert(double money, String to) {
		return money / this.readCourse(to);
	}

	public double convert(double money, String to, String from) {
		return (money * this.readCourse(from)) / this.readCourse(to);
	}
}
