package pl.dweb.separatum;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("votum-manager");
		    EntityManager entityManager = entityManagerFactory.createEntityManager();
		    entityManager.close();
	}

}
