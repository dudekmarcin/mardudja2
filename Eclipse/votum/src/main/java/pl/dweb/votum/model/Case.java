package pl.dweb.votum.model;

import java.util.Date;

public class Case {

		private Long caseID;
	    private String signature;
	    private Date date;
	    private Integer instanceID;
	    private Integer judgesNumber;
	    private Integer separatumNumber;
	    private Long leaderID;
	    private Long controlSubjectID;
	    private Long protectedValueID;
	    private Integer pointsNumber;
	    private String sentenceURL;

	    public Case() {}

		public Long getCaseID() {
			return caseID;
		}

		public void setCaseID(Long i) {
			this.caseID = i;
		}

		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public Integer getInstanceID() {
			return instanceID;
		}

		public void setInstanceID(Integer instanceID) {
			this.instanceID = instanceID;
		}

		public Integer getJudgesNumber() {
			return judgesNumber;
		}

		public void setJudgesNumber(Integer judgesNumber) {
			this.judgesNumber = judgesNumber;
		}

		public Integer getSeparatumNumber() {
			return separatumNumber;
		}

		public void setSeparatumNumber(Integer separatumNumber) {
			this.separatumNumber = separatumNumber;
		}

		public Long getLeaderID() {
			return leaderID;
		}

		public void setLeaderID(Long leaderID) {
			this.leaderID = leaderID;
		}

		public Long getControlSubjectID() {
			return controlSubjectID;
		}

		public void setControlSubjectID(Long controlSubjectID) {
			this.controlSubjectID = controlSubjectID;
		}

		public Long getProtectedValueID() {
			return protectedValueID;
		}

		public void setProtectedValueID(Long protectedValueID) {
			this.protectedValueID = protectedValueID;
		}

		public Integer getPointsNumber() {
			return pointsNumber;
		}

		public void setPointsNumber(Integer pointsNumber) {
			this.pointsNumber = pointsNumber;
		}

		public String getSentenceURL() {
			return sentenceURL;
		}

		public void setSentenceURL(String sentenceURL) {
			this.sentenceURL = sentenceURL;
		}

		@Override
		public String toString() {
			return "Case [caseID=" + caseID + ", signature=" + signature + ", date=" + date + ", instanceID="
					+ instanceID + ", judgesNumber=" + judgesNumber + ", separatumNumber=" + separatumNumber
					+ ", leaderID=" + leaderID + ", controlSubjectID=" + controlSubjectID + ", protectedValueID="
					+ protectedValueID + ", pointsNumber=" + pointsNumber + ", sentenceURL=" + sentenceURL + "]";
		}
	   
	   

	
}
