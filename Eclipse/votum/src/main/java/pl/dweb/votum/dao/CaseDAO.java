package pl.dweb.votum.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.stereotype.Repository;
import org.xml.sax.SAXException;

import pl.dweb.votum.model.Case;

@Repository
public class CaseDAO {

	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
		}
	}

	private Connection getConnection() throws SQLException, ParserConfigurationException, SAXException, IOException {
//		DBAccess dba = new DBAccess();
//		Map<String, String> DBData = dba.getAccessData();
		return DriverManager.getConnection("jdbc:mysql://", "", "");
	}

	private void closeConnection(Connection connection) {
		if (connection == null) {
			return;
		}
		try {
			connection.close();
		} catch (SQLException e) {
		}
	}

		
	public List<Case> findAllQuestion() {
		String sql = "select * from cases";
		List<Case> result = new ArrayList<Case>();
		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement statment = connection.prepareStatement(sql);
			ResultSet resultSet = statment.executeQuery();
			while (resultSet.next()) {
				Case c = new Case();
				c.setCaseID(resultSet.getLong("case_id"));
				c.setSignature(resultSet.getString("signature"));
				c.setDate(resultSet.getDate("date"));
				c.setInstanceID(resultSet.getInt("instance"));
				c.setJudgesNumber(resultSet.getInt("number_of_judges"));
				c.setSeparatumNumber(resultSet.getInt("number_of_votsep"));
				c.setLeaderID(resultSet.getLong("leader_id"));
				c.setControlSubjectID(resultSet.getLong("control_subject"));
				c.setProtectedValueID(resultSet.getLong("protected_value"));
				c.setPointsNumber(resultSet.getInt("sentence_point_number"));
				c.setSentenceURL(resultSet.getString("sentence_url"));
				result.add(c);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeConnection(connection);
		}
		return result;
	}
	
//	public List<Case> generateQuestionsList(int[] id) {
//		List<Case> result = new ArrayList<Case>();
//		Connection connection = null;
//		try {
//			connection = getConnection();
//			for(int i = 0; i < id.length; i++) {
//			String sql = "select * from questions where id=" + id[i];
//			PreparedStatement statment = connection.prepareStatement(sql);
//			ResultSet resultSet = statment.executeQuery();
//			while (resultSet.next()) {
//				Question q = new Question();
//				q.setId(resultSet.getInt("id"));
//				q.setContent(resultSet.getString("content"));
//				q.addAnswer(AnswerType.A, resultSet.getString("answerA"));
//				q.addAnswer(AnswerType.B, resultSet.getString("answerB"));
//				q.addAnswer(AnswerType.C, resultSet.getString("answerC"));
//				q.addAnswer(AnswerType.D, resultSet.getString("answerD"));
//				switch (resultSet.getString("ok")) {
//				case "A":
//					q.setCorrect(AnswerType.A);
//					break;
//				case "B":
//					q.setCorrect(AnswerType.B);
//					break;
//				case "C":
//					q.setCorrect(AnswerType.C);
//					break;
//				case "D":
//					q.setCorrect(AnswerType.D);
//					break;
//				}
//				q.setImg(resultSet.getString("img"));
//
//				result.add(q);
//			}
//			}
//		} catch (SQLException | ParserConfigurationException | SAXException | IOException e) {
//			System.out.println(e.getMessage());
//		} finally {
//			closeConnection(connection);
//		}
//		return result;
//	}
//
//	public Question findQuestionById(int id) {
//		String sql = "select * from questions where id=" + id;
//		Connection connection = null;
//		Question q = new Question();
//		try {
//			connection = getConnection();
//			PreparedStatement statment = connection.prepareStatement(sql);
//			ResultSet resultSet = statment.executeQuery();
//			if (resultSet.next()) {
//				q.setId(resultSet.getInt("id"));
//				q.setContent(resultSet.getString("content"));
//				q.addAnswer(AnswerType.A, resultSet.getString("answerA"));
//				q.addAnswer(AnswerType.B, resultSet.getString("answerB"));
//				q.addAnswer(AnswerType.C, resultSet.getString("answerC"));
//				q.addAnswer(AnswerType.D, resultSet.getString("answerD"));
//				switch (resultSet.getString("ok")) {
//				case "A":
//					q.setCorrect(AnswerType.A);
//					break;
//				case "B":
//					q.setCorrect(AnswerType.B);
//					break;
//				case "C":
//					q.setCorrect(AnswerType.C);
//					break;
//				case "D":
//					q.setCorrect(AnswerType.D);
//					break;
//				}
//				q.setImg(resultSet.getString("img"));
//
//			}
//		} catch (SQLException | ParserConfigurationException | SAXException | IOException e) {
//			System.out.println(e.getMessage());
//		} finally {
//			closeConnection(connection);
//		}
//		return q;
//	}
//	
//	public int getMaxId() {
//		String sql = "select id from questions order by id desc limit 1";
//		Connection connection = null;
//		int id = 0;
//		try {
//			connection = getConnection();
//			PreparedStatement statment = connection.prepareStatement(sql);
//			ResultSet resultSet = statment.executeQuery();
//			while (resultSet.next()) {
//				id = resultSet.getInt("id");
//				
//			}
//		} catch (SQLException | ParserConfigurationException | SAXException | IOException e) {
//			System.out.println(e.getMessage());
//		} finally {
//			closeConnection(connection);
//		}
//		return id;
//	}
}
