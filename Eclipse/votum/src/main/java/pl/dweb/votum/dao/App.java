package pl.dweb.votum.dao;

import java.util.List;

import ch.qos.logback.core.net.SyslogOutputStream;
import pl.dweb.votum.model.Case;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CaseDAO dao = new CaseDAO();
		
		List<Case> cases = dao.findAllQuestion();
		
		cases.forEach(c -> System.out.println(c));
		
	}

}
