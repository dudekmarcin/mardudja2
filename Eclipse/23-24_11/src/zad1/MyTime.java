package zad1;


public class MyTime {

	private int hour = 0;
	private int minute = 0;
	private int second = 0;

	public MyTime() {
	}

	public MyTime(int hour, int minute, int second) {
		if (hour >= 0 && hour < 24) {
			this.hour = hour;
		}

		if (minute >= 0 && minute < 60) {
			this.minute = minute;
		}

		if (second >= 0 && second < 60) {
			this.second = second;
		}
	}

	public void setTime(int hour, int minute, int second) {
		if (hour >= 0 && hour < 24) {
			this.hour = hour;
		}

		if (minute >= 0 && minute < 60) {
			this.minute = minute;
		}

		if (second >= 0 && second < 60) {
			this.second = second;
		}
	}

	public int getHour() {
		return this.hour;
	}

	public int getMinute() {
		return this.minute;
	}

	public int getSecond() {
		return this.second;
	}

	public void setHour(int hour) {
		if (hour >= 0 && hour < 24) {
			this.hour = hour;
		}
	}

	public void setMinute(int minute) {
		if (minute >= 0 && minute < 60) {
			this.minute = minute;
		}
	}

	public void setSecond(int second) {
		if (second >= 0 && second < 60) {
			this.second = second;
		}
	}

	@Override
	public String toString() {
		String result = "";

		if (hour < 10) {
			result += "0";
		}
		result += this.hour + ":";

		if (minute < 10) {
			result += "0";
		}
		result += this.minute + ":";

		if (second < 10) {
			result += "0";
		}
		result += this.second;

		return result;
	}

	public MyTime nextSecond() {
		int sec = this.second + 1;
		int min = this.minute;
		int h = this.hour;
		if (sec > 59) {
			sec = 0;
			min = this.minute + 1;
			if (min > 59) {
				min = 0;
				h = this.hour + 1;
				if (h > 23) {
					h = 0;
				}
			}
		}
		return new MyTime(h, min, sec);
	}

	public MyTime nextMinute() {
		int min = this.minute + 1;
		int h = this.hour;
		if (min > 59) {
			min = 0;
			h = this.hour + 1;
			if (h > 23) {
				h = 0;
			}
		}
		return new MyTime(h, min, this.second);
	}

	public MyTime nextHour() {
		int h = (this.hour == 23) ? 0 : this.hour + 1;
		return new MyTime(h, this.minute, this.second);
	}

	public MyTime previousSecond() {
		int sec = this.second - 1;
		int min = this.minute;
		int h = this.hour;
		if (sec < 0) {
			sec = 59;
			min = this.minute - 1;
			if (min < 0) {
				min = 59;
				h = this.hour - 1;
				if (h < 0) {
					h = 23;
				}
			}
		}
		return new MyTime(h, min, sec);
	}

	public MyTime previousMinute() {
		int min = this.minute - 1;
		int h = this.hour;
		if (min < 0) {
			min = 59;
			h = this.hour - 1;
			if (h < 0) {
				h = 23;
			}
		}
		return new MyTime(h,min,this.second);
	}

	public MyTime previousHour() {
		int h = (this.hour == 0) ? 23 : this.hour - 1;
		return new MyTime(h, this.minute, this.second);
	}
	
	
	
// private boolean checkMyTime(int hour, int minute, int second)
// throws IllegalHourException, IllegalMinuteException,
// IllegalSecondException {
//
// if (hour >= 0 && hour < 24) {
// this.hour = hour;
// } else
// throw new IllegalHourException();
//
// if (minute >= 0 && minute < 60) {
// this.minute = minute;
// } else
// throw new IllegalMinuteException();
//
// if (second >= 0 && minute < 60) {
// this.second = second;
// } else
// throw new IllegalSecondException();
//
// return true;
// }
//
// private boolean checkHour(int hour) throws IllegalHourException {
//
// if (hour >= 0 && hour < 24) {
// this.hour = hour;
// } else
// throw new IllegalHourException();
//
// return true;
// }
//
// public boolean checkMinSec(int minsec) throws IllegalMinuteException {
//
// if (minute >= 0 && minute < 60) {
// this.minute = minute;
// } else
// throw new IllegalMinuteException();
//
// return true;
// }
//
// if (second >= 0 && minute < 60) {
// this.second = second;
// } else
// throw new IllegalSecondException();
//
// return true;
// }
//
}
