package zad9;

public class ServeMenager extends Thread {

	private Counter counter;
	private String ticket;
	public ServeMenager(Counter counter, String ticket) {
		this.counter = counter;
		this.ticket = ticket;
	}
	
	@Override
	public void run() {
		if (ticket != null && (!ticket.equals(""))) {
			try {
				System.out.println(
						"Okienko " + counter.getNumber() + " obsługuje aktualnie klienta z numerkiem " + ticket);
				this.sleep(5000);
				System.out.println("Okienko " + counter.getNumber() + " zakończyło obsługę klienta numerkiem " + ticket);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
