package zad9;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Counter {

	private Set<String> supportedDeparts = new LinkedHashSet<>();
	private int number;
	private boolean free;

	public Counter(int name, String... departs) {
		this.number = name;
		this.free = true;
		for (int i = 0; i < departs.length; i++) {
			this.supportedDeparts.add(departs[i].toUpperCase());
		}
	}

	public boolean isSupported(String depart) {
		boolean result = false;
		for (String dep : this.supportedDeparts) {
			if (dep.equals(depart)) {
				result = true;
			}
		}
		return result;
	}

	public boolean isFree() {
		return this.free;
	}
	
	public int getNumber() {
		return this.number;
	}

	public Set<String> getSupportedDeparts() {
		return this.supportedDeparts;
	}

	public String nextConsumer(Set<String> ticketList) {
		Iterator<String> ticketItr = ticketList.iterator();
		Iterator<String> departsItr = this.supportedDeparts.iterator();
		String ticket;
		String depart;

		while (ticketItr.hasNext()) {
			ticket = ticketItr.next();
			while (departsItr.hasNext()) {
				System.out.println("Okienko " + this.number +" Porównywany bilet: " + ticket);
				depart = departsItr.next();
				System.out.println("Porównywany oddział: " + depart);
				if (ticket.substring(0, 1).equals(depart)) {
					ticketItr.remove();
					ConsumersMenagment.decreaseTLL();
					System.out.println("Okienko " + this.getNumber() + " przyjęło do obsługi klienta z numerkiem " + ticket);
					return ticket;
				}
			}
		}

		return null;
	}

//	public void serveConsumer(String ticket) {
//		if (ticket != null && (!ticket.equals(""))) {
//					try {
//						System.out.println(
//								"Okienko " + this.number + " obsługuje aktualnie klienta z numerkiem " + ticket);
//						tsleep(5000);
//						System.out.println("Okienko " + this.number + " zakończyło obsługę klienta numerkiem " + ticket);
//					} catch (InterruptedException e) {
//						System.out.println(e.getMessage());
//					}
//				}
//	}

	@Override
	public String toString() {
		return "Okienko " + this.number + " obsługuje oddziały " + supportedDeparts;
	}
}
