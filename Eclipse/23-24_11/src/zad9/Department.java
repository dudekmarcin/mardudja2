package zad9;

public class Department {
	
	private String depart_sign;
	private int ticket_number = 0;
	
	public Department(String sign) {
		this.depart_sign = sign.toUpperCase();
	}
	
	public String getDepartSign() {
		return this.depart_sign;
	}
	
	public String getTicket() {
		return this.depart_sign + ++this.ticket_number;
	}

	@Override 
	public String toString() {
		return "Department "+this.depart_sign+" (last ticket: "+this.depart_sign+this.ticket_number+")";
	}
}
