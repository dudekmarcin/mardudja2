package zad9;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConsumersMenagment implements Runnable {

	private static Set<String> ticketList = new LinkedHashSet<>();
	private Counter counter;
	private static int tLL = 0;

	public ConsumersMenagment(Counter counter) {
		this.counter = counter;
	}
	

	@Override
	public void run() {
		boolean isFree = true;
		String ticket;
		while (!isFree) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
				;
			}
		}

		synchronized (ConsumersMenagment.ticketList) {
			isFree = false;
			ticket = counter.nextConsumer(ConsumersMenagment.getTicketList());
			isFree = true;
			notifyAll();

		}
		Thread thread = new ServeMenager(counter, ticket);
		thread.start();
	}

	public static Set<String> getTicketList() {
		return ConsumersMenagment.ticketList;
	}

	public static int getTicketListLength() {
		return ConsumersMenagment.tLL;
	}

	public static void decreaseTLL() {
		--ConsumersMenagment.tLL;
	}

	public static void setTicketList(Set<String> list) {
		ConsumersMenagment.ticketList = list;
		ConsumersMenagment.tLL = list.size();
	}

	public static void addToTicketList(String ticket) {
		ConsumersMenagment.ticketList.add(ticket);
		ConsumersMenagment.tLL = ConsumersMenagment.ticketList.size();
	}

}
