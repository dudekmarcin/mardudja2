package zad2;

public class MyDate {

	private int year;
	private int month;
	private int day;

	private static String[] strMonths = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
			"Dec" };
	private static String[] strDays = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
	private static int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	public static boolean isLeapYear(int year) {
		return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
	}

	public static boolean isValidDate(int year, int month, int day) {
//		if(!(year > 0 && year <= 9999)) {
//			System.out.println("Nieparwidłowy rok: "+ year);
//			return false;
//		}
//		if(!(month > 0 && month <= 12)) {
//			System.out.println("Nieparwidłowy meisiąc: "+ month);
//			return false;
//		}
//		if(!((day <= daysInMonths[month - 1] || (month == 2 && isLeapYear(year) && day <= 29)))) {
//			System.out.println("Nieparwidłowy dzień: "+ day);
//			return false;
//		}
//		
//		return true;
		
		return ((year > 0 && year <= 9999) && (month > 0 && month <= 12)
				&& ((day <= daysInMonths[month - 1] || (month == 2 && isLeapYear(year) && day <= 29))));
		
		
	}

	public static int getDayOfWeek(int year, int month, int day) {
		int[] monthsTable = { 0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5 };
		if(isLeapYear(year)) {
			monthsTable[0] = 6;
			monthsTable[1] = 2;
		}
		int temp = (year/4) + year;
		temp -= year/100;
		temp += year/400;
		temp += day;
		temp += monthsTable[month-1];
		temp--;
		temp = temp %7;
		
		return temp;
	}

	public MyDate(int year, int month, int day) {
		if (isValidDate(year, month, day)) {
			this.year = year;
			this.month = month;
			this.day = day;
		} else {
			System.out.println("Nieprawidłowa data, obiekt przyjął datę 1.01.1000");
			this.year = 1000;
			this.month = 1;
			this.day = 1;
		}
	}

	public void setDate(int year, int month, int day) {
		if (isValidDate(year, month, day)) {
			this.year = year;
			this.month = month;
			this.day = day;
		} else {
			System.out.println("Nieprawidłowa data, nie zmieniono daty");
		}
	}

	public int getYear() {
		return this.year;
	}

	public int getMonth() {
		return this.month;
	}

	public int getDay() {
		return this.day;
	}

	public void setYear(int year) {
		if(year > 0 && year <= 9999) {
		this.year = year;
		} else {
			System.out.println("Rok spoza zakresu, nie zmieniono wartości");

		}
	}

	public void setMonth(int month) {
		if (month > 0 && month <= 12) {
			this.month = month;
		} else {
			System.out.println("Nie ma takiego miesiąca, nie zmieniono wartości");
		}
	}

	public void setDay(int day) {
		if (day <= daysInMonths[this.month - 1]) {
			this.day = day;
		} else {
			System.out.println("Nie ma takiego dnia w " + strMonths[this.month - 1] + ", nie zmieniono wartości");
		}
	}

	@Override
	public String toString() {
		return strDays[getDayOfWeek(this.year, this.month, this.day)] + " " + this.day + " " + strMonths[this.month - 1] + " "
				+ year;
	}

	public MyDate nextDay() {
		int d = (this.day < daysInMonths[this.month - 1]) ? this.day + 1 : ((isLeapYear(this.year) && this.month == 2 && this.day == daysInMonths[this.month - 1]) ? 29 : 1);
		int m = (d == 1) ? ((this.month == 12) ? m = 1 : this.month + 1) : this.month;
		int y = (m == 1) ? this.year + 1 : this.year;

		return new MyDate(y, m, d);
	}

	public MyDate nextMonth() {
		int m = (this.month == 12) ? m = 1 : this.month + 1;
		int y = (m == 1) ? this.year + 1 : this.year;

		return new MyDate(y, m, this.day);
	}

	public MyDate nextYear() {
		return new MyDate(this.year + 1, this.month, this.day);
	}

	public MyDate previousDay() {
		int d = (this.day == 1) ? ((isLeapYear(this.year) && this.month == 3) ? 29 : daysInMonths[month-2]) : this.day - 1;
		int m = (d == daysInMonths[month-2]) ? ((this.month == 1) ? m = 12 : this.month - 1) : this.month;
		int y = (m == 12) ? this.year - 1 : this.year;

		return new MyDate(y, m, d);
	}

	public MyDate previousMonth() {
		int m = (this.month == 1) ? m = 12 : this.month - 1;
		int d = (isLeapYear(this.year) && m == 2) ?   29 : daysInMonths[m-1];
		int y = (m == 12) ? this.year - 1 : this.year;

		return new MyDate(y, m, d);
	}

	public MyDate previousYear() {
		return new MyDate(this.year - 1, this.month, this.day);
	}

}
