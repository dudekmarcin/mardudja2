package zad6;

public class MovablePoint extends Point {

	private float xSpeed = 0.0f;
	private float ySpeed = 0.0f;
	
	public MovablePoint() {
		super();
	}
	
	public MovablePoint(float xSpeed, float ySpeed) {
		super();
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;
	}
	
	public MovablePoint(float x, float y, float xSpeed, float ySpeed) {
		super(x,y);
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;
	}

	public float getXSpeed() {
		return this.xSpeed;
	}

	public void setXSpeed(float xSpeed) {
		this.xSpeed = xSpeed;
	}

	public float getYSpeed() {
		return this.ySpeed;
	}

	public void setYSpeed(float ySpeed) {
		this.ySpeed = ySpeed;
	}
	
	public void setSpeed(float xSpeed, float ySpeed) {
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;
	}
	
	public float[] getSpeed() {
		float[] array = new float[] {this.xSpeed, this.ySpeed};
		return array;
	}
	
	@Override
	public String toString() {
		return "("+this.getX()+","+this.getY()+"), speed=("+this.xSpeed+","+this.ySpeed+")";
	}
	
	public MovablePoint move() {
		this.setX(this.getX()+this.xSpeed);
		this.setY(this.getY()+this.ySpeed);
		
		return this;
	}
	
}
