package zad3;

public class Cylinder extends Circle {

	private double height = 1.0;
	
	public Cylinder() {
		super();
	}
	
	public Cylinder(double radius) {
		super(radius);
	}
	
	public Cylinder(double radius, double height) {
		super(radius);
		this.height = height;
	}
	
	public Cylinder(double radius, double height, String color) {
		super(radius, color);
		this.height = height;
	}
	
	public double getHeight() {
		return this.height;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	@Override
	public String toString() {
		return "Cylinder [radius="+this.getRadius()+", height="+this.height+", color="+this.getColor()+"]";
	}
	
	@Override
	public double getArea() {
		return 2*Math.PI*this.getRadius()*(this.getRadius()+this.height);
	}
	
	public double getVolume() {
		return super.getArea()*this.height;
	}
}
