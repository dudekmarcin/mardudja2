package exceptions;

public class IllegalSecondException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -776172561309309110L;

	public IllegalSecondException() {
		super("Niedozwolona wartosc sekundowa!");
	}
}
