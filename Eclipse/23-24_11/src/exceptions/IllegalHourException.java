package exceptions;

public class IllegalHourException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3420880931834130740L;

	public IllegalHourException() {
		super("Niedozwolona wartosc godzinowa!");
	}
}
