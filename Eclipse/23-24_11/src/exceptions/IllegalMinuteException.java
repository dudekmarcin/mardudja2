package exceptions;

public class IllegalMinuteException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 778695046025107847L;

	public IllegalMinuteException() {
		super("Niedozwolona wartosc minutowa!");
	}
}
