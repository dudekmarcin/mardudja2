package zad8;

public enum MembersType {
	PREMIUM(0.2, 0.1), 
	GOLD(0.15, 0.1), 
	SILVER(0.1, 0.1);

	private final double serviceDiscount;
	private final double productDiscount;

	private MembersType(double serviceDiscount, double productDiscount) {
		this.serviceDiscount = serviceDiscount;
		this.productDiscount = productDiscount;
	}
	
	public double getServiceDiscount() {
		return this.serviceDiscount;
	}
	
	public double getProductDiscount() {
		return this.productDiscount;
	}
}
