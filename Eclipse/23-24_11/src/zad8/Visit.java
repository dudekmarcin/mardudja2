package zad8;

import java.util.Date;

public class Visit {

	private Customer customer;
	private Date date;
	private double serviceExpense;
	private double productExpense;
		
	public Visit(Customer customer, Date date) {
		this.customer = customer;
		this.date = date;
	}
	
	public Visit(String name, Date date) {
		this.customer = new Customer(name);
		this.date = date;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getServiceExpense() {
		return this.serviceExpense;
	}

	public void setServiceExpense(double serviceExpense) {
		if(this.customer.getMemberType() != null) {
			serviceExpense -= serviceExpense*this.customer.getMemberType().getServiceDiscount();
		}
		this.serviceExpense = serviceExpense;
	}

	public double getProductExpense() {
		return this.productExpense;
	}

	public void setProductExpense(double productExpense) {
		if(this.customer.getMemberType() != null) {
			productExpense -= productExpense*this.customer.getMemberType().getProductDiscount();
		}
		this.productExpense = productExpense;

	}
	
	public double getTotalExpense() {
		if(this.productExpense > 0 && this.serviceExpense > 0) {
			this.productExpense -= this.productExpense*0.1;
		}
		return this.productExpense + this.serviceExpense;
	}
	
	public double getServiceDiscountRate(String type) {
		return 0;
	}
	
	public double getProductDiscountRate(String type) {
		return 0;
	}
	
	@Override
	public String toString() {
		return "Visit [customer="+this.customer.getName()
		+", date="+this.date+", services cost="+this.serviceExpense
		+", products cost="+this.productExpense+", total cost="+this.getTotalExpense()+"]";
	}
	
	

}
