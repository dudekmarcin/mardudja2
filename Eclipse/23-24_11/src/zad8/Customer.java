package zad8;

public class Customer {
	
	private String name;
	private boolean member = false;
	private MembersType memberType;
	
	public Customer(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public boolean isMember() {
		return this.member;
	}
	
	public void setMember(boolean member) {
		this.member = member;
	}
	
	public MembersType getMemberType() {
		return memberType;
	}
	
	public void setMemberType(MembersType type) {
		this.setMember(true);
		this.memberType = type;
	}
	
	@Override 
	public String toString() {
		return "Customer [name="+this.name+", member="+this.member+", member type="+this.memberType+"]";
	}
	
	
}
