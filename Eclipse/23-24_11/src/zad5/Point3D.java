package zad5;

public class Point3D extends Point2D {

	private float z = 0.0f;
	
	public Point3D() {
		super();
	}
	
	public Point3D(float x, float y, float z) {
		super(x,y);
		this.z = z;
	}
	
	public float getZ() {
		return this.z;
	}
	
	public void setZ(float z) {
		this.z = z;
	}
	
	public float[] getXYZ() {
		float[] result = new float[] {this.getX(), this.getY(), this.z};
		return result;
	}

	public void setXYZ(float x, float y, float z) {
		this.setX(x);
		this.setY(y);
		this.z = z;
	}
	
	@Override
	public String toString() {
		return "("+this.getX()+","+this.getY()+","+this.z+")";
	}
	
}
