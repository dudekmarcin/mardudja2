import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import zad1.MyTime;
import zad2.MyDate;
import zad3.Circle;
import zad3.Cylinder;
import zad4.Person;
import zad4.Staff;
import zad4.Student;
import zad6.MovablePoint;
import zad8.Customer;
import zad8.MembersType;
import zad8.Visit;
import zad9.ConsumersMenagment;
import zad9.Counter;
import zad9.Department;

public class Sroda_23_11 {

	public static void main(String[] args) throws InterruptedException {

		Department deptA = new Department("a");
		Department deptB = new Department("b");
		Department deptC = new Department("c");
		Department deptD = new Department("d");
		
		Counter c1 = new Counter(1, "A", "B");
		Counter c2 = new Counter(2, "B", "C", "D");
		Counter c3 = new Counter(3, "D");

		
		

		Random rand = new Random();
		
		while(ConsumersMenagment.getTicketList().size() <= 10) {
			switch (rand.nextInt(4)) {
			case 0:
				ConsumersMenagment.addToTicketList(deptA.getTicket());
				break;
			case 1:
				ConsumersMenagment.addToTicketList(deptB.getTicket());
				break;
			case 2:
				ConsumersMenagment.addToTicketList(deptC.getTicket());
				break;
			case 3:
				ConsumersMenagment.addToTicketList(deptD.getTicket());
				break;
			default:
				
				break;
			}
		}

		System.out.println(ConsumersMenagment.getTicketList());
		

		Thread threadC1 = new Thread(new ConsumersMenagment(c1));
	    Thread threadC2 = new Thread(new ConsumersMenagment(c2));
	    Thread threadC3 = new Thread(new ConsumersMenagment(c3));
	  
	    threadC1.start();
	    Thread.sleep(10);
	    threadC2.start();
	    Thread.sleep(10);
	    threadC3.start();
		
		while(ConsumersMenagment.getTicketListLength() > 0) {
			System.out.println();
			threadC1.run();
			threadC2.run();
			threadC3.run();
		System.out.println("\n"+ConsumersMenagment.getTicketList());
		}
		
		

		
		
		
		
		// MyTime time1 = new MyTime();
		// System.out.println(time1);
		//
		// time1.setTime(23, 59, 59);
		// System.out.println(time1);
		//
		// MyTime time2 = time1.nextSecond();
		// System.out.println(time2);
		//
		// time2 = time2.previousMinute();
		// System.out.println(time2);

		//
		// MyDate date1 = new MyDate(2012, 2, 27);
		// System.out.println(date1);
		// MyDate date2 = date1.nextDay();
		// System.out.println(date2);
		// date2 = date2.nextDay();
		// System.out.println(date2);
		// date2 = date2.nextDay();
		// System.out.println(date2);
		// date2 = date2.nextMonth();
		// System.out.println(date2);
		// date2 = date2.nextYear();
		// System.out.println(date2);
		// System.out.println("-----");
		// date2 = date2.previousDay();
		// System.out.println(date2);
		// date2 = date2.previousYear();
		// System.out.println(date2);
		// date2 = date2.previousMonth();
		// System.out.println(date2);

		// Circle c1 = new Circle(5,"yellow");
		// System.out.println(c1);
		// System.out.println(c1.getArea());
		//
		//
		// Cylinder w1 = new Cylinder(5,3,"brown");
		// System.out.println(w1);
		// System.out.println(w1.getArea());

		// Person mike = new Person("Mike", "Somewhere");
		// System.out.println(mike);
		//
		// Staff jake = new Staff("Jake", "Somewhere else", "Jedi Academy",
		// 1000);
		// System.out.println(jake);
		//
		// Student billy = new Student("Billy", "Far away", "Economy", 4,
		// 1500.50);
		// System.out.println(billy);

		// MovablePoint mp = new MovablePoint(4,3,6,7);
		// System.out.println(mp);
		// mp = mp.move();
		// System.out.println(mp);

		// Customer klient = new Customer("Grażyna");
		// System.out.println(klient);
		// klient.setMemberType(MembersType.PREMIUM);
		// System.out.println(klient);
		//
		// Visit v1 = new Visit(klient, new Date());
		// v1.setProductExpense(100);
		// v1.setServiceExpense(120);
		// System.out.println(v1);
		// klient.setMemberType(MembersType.SILVER);
		// v1 = new Visit(klient, new Date());
		// v1.setProductExpense(100);
		// v1.setServiceExpense(120);
		// System.out.println(v1);
		//
		// Visit v2 = new Visit(new Customer("Basia"), new Date());
		// v2.setProductExpense(100);
		// v2.setServiceExpense(120);
		// System.out.println(v2);
		//
		// Visit v3 = new Visit(new Customer("Gosia"), new Date());
		// v3.setProductExpense(100);
		// System.out.println(v3);
		//
		// Visit v4 = new Visit(new Customer("Kasia"), new Date());
		// v4.setServiceExpense(120);
		// System.out.println(v4);
	}

}
