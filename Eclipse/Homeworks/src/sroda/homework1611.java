package sroda;

public class homework1611 {
	
public static String spellNumber(int number) {
		
		String[] hundredsArr = {"sto","dwiescie", "trzysta", "czterysta", "piecset", "szescset", "siedemset", "osiemset", "dziewiecset"};
		String[] tensArr = {"nascie", "dwadziescia", "trzydziesci", "czterdziesci", "piecdziesiat", "szescdziesiat", "siedemdziesiat", "osiemdziesiat", "dziewiecdziesiat"};
		String[] unitsArr = {"jeden", "dwa", "trzy", "cztery", "piec", "szesc", "siedem", "osiem", "dziewiec"};
		
		String result ="";
		
		int hundreds = number/100;
		int tens = (number-hundreds*100)/10;
		int units = number-(hundreds*100)-(tens*10);
		
		if(hundreds > 0) {
			result += hundredsArr[hundreds-1];
		}
		if(tens > 1) {
			result += " "+tensArr[tens-1];
		}
		if(units > 0) {
			result += " "+unitsArr[units-1];
		} else if(units == 0) {
			result += "zero";
		}
		
		if(tens == 1 && units ==0) {
			result = "dziesiec";
		} else if(tens == 1) {
			result += tensArr[tens-1];
		}
		
		return result;
		}
		

	public static void main(String[] args) {

		// 8. Dane jest osiedle bloków. Każdy blok zawiera 2 klatki, po 2 piętra
		// (+ parter), na każdym piętrze znajdują się dwa mieszkania. Napisz
		// skrypt, który wyświetli adres każdego mieszkania tak, aby można było
		// zaadresować kopertę. Bloki znajdują się tylko pod numerami
		// nieparzystymi - pierwszy blok znajduje się pod numerem 1. Funkcja
		// powinna przyjmować nazwę ulicy oraz ostatni numer, który występuje na
		// danej ulicy (może być parzysty lub nieparzysty) oraz na wyjściu
		// drukować poszczególne adresy.

		// 9. Napisz funkcję, sprawdzającą czy przekazany argument (String) jest
		// palindromem (należy uwzględnić pomijanie spacji).

		// 10. Napisz zadanie które wyświetla wszystkie liczby pierwsze z
		// zakresu od 1 do 100.

		// 11. Napisz funkcję, która przetłumaczy wybraną liczbę całkowitą z
		// zakresu <0-999>, na jej odpowiednik słownie np. 123 -> sto
		// dwadzieścia trzy.
		
		System.out.println(spellNumber(0));
		

		// 12. Na farmie występuje n królików. Nieparzyste z nich, mają niestety
		// tylko jedno ucho, parzyste są w komplecie. Dla wczytanego n wypisz
		// ile uszu znajduje się na farmie
		
		int n = 14;
		
		//int uszy = (n%2 == 0) ? (n/2)*3 : (n/2)*3-1;
		for(int i=1;i<14;i++) {
			int uszy = (i%2 == 0) ? (i/2)*3 : ((i-1)/2)*3+1;
		//	System.out.println(i+": "+uszy);
		}
	}

}
