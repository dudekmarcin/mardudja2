package wtorek;

public class homework1511loops {

	public static void main(String[] args) {

		// 1. Napisz pętlę for, która automatycznie uzupełni wartości w tablicy
		// wartościami od 0 do 5, następnie wyświetl tablicę w konsoli;
		System.out.println("Zadanie 1:");
		int[] arr1 = new int[6];
		for (int i = 0; i < 6; i++) {
			arr1[i] = i;
			System.out.print(arr1[i] + " ");
		}
		System.out.println("\n");

		// 2. Napisz pętlę for, która automatycznie uzupełni wartości w tablicy
		// liczbami parzystymi z zakresu <0,10>, wynik wyświetl w konsoli -
		// weź pod uwagę złożoność obliczeniową pętli;

		System.out.println("Zadanie 2:");
		int[] arr2 = new int[5];
		for(int i=0, j=2;j<=10;i++, j+=2) {
			arr2[i] = j;
			System.out.print(arr2[i] + " ");
		}
		System.out.println("\n");

		// 3. Napisz pętlę for, która uzupełni wartości w tablicy wartościami
		// i+3 w zakresie <100, 130>, wyświetl tablicę w konsoli;
		System.out.println("Zadanie 3:");
		int[] arr3 = new int[11];

		for(int i=0, j=100; j<=130; i++, j+=3) {
			arr3[i] = j;
			System.out.print(arr3[i] + " ");
		}
		System.out.println("\n");

		
		// 4. Napisz pętlę while, która wczyta 10 elementów do tablicy (typ
		// elementów jest dowolny)
		
		System.out.println("Zadanie 4:");
		int[] arr4 = new int[10];
		int i=0;
		while(i<10) {
			arr4[i] = i+1;
			System.out.print(arr4[i] + " ");
			i++;
		}
		System.out.println("\n");


		// 5. Napisz pętlę do..while, która wczyta dokładnie 5 elementów do
		// tablicy;
		
		System.out.println("Zadanie 5:");
		int[] arr5 = new int[5];
		i = 0;
		do {
			arr5[i] = i+1;
			System.out.print(arr5[i]+" ");
			i++;
		} while(i<5);
		System.out.println("\n");


		// 6. Napisz pętlę do..while, która będzie wczytywała elementy do
		// tablicy dopóki użytkownik nie poda
		// argumentu - 1.
		// double[] arr = {-5, 2.2, 3.011,-10, 15,-13, 123.14,-1/2 };
		// double sum = 0;
		
		System.out.println("Zadanie 6:");
		double[] arr6 = {-5, 2.2, 3.011,-10, 15,-13, 123.14,-1.0/2, -1 };
		double sum = 0;
		
		i = 0;
		do {
			sum += arr6[i];
			i++;
		} while(arr6[i] != -1);
		System.out.println("Suma: "+sum+"\n");

		// 7. Napisz pętlę for, która w zmiennej sum zsumuje tylko liczby
		// ujemne, a następnie wyświetl sumę w konsoli;
		
		System.out.println("Zadanie 7:");
		sum = 0;
		for(int j=0; j<arr6.length;j++) {
			if(arr6[j] < 0) {
				sum +=arr6[j];
			}
		}
		System.out.println("Suma ujemnych: "+sum+"\n");

		// 8. Napisz pętlę for, która w zmiennej sum zsumuje tylko liczby
		// nieparzyste, a następnie wyświetli sumę w konsoli;
		
		System.out.println("Zadanie 8:");
		sum = 0;
		for(int j=0;j<arr6.length;j++) {
			if(Math.abs(arr6[j]) % 2 == 1) {
				sum += arr6[j];
			}
		}
		System.out.println("Suma nieparzystych: "+sum+"\n");
		
		// 9. Napisz pętlę while, która zsumuje w zmiennej sum tylko liczby
		// dodatnie, wynik wyświetl w konsoli;
		
		System.out.println("Zadanie 9:");
		sum = 0;
		i = 0;
		while(i < arr6.length) {
			if(arr6[i] > 0) {
				sum += arr6[i];
			}
			i++;
		}
		System.out.println("Suma dodatnich: "+sum+"\n");
		
		// 10. Napisz pętlę while, która w zmiennej sum zsumuje tylko i
		// wyłącznie elementy całkowite (bez
		// zmiennoprzecinkowych);
		
		System.out.println("Zadanie 10:");
		sum = 0;
		i = 0;
		
		while(i < arr6.length) {
			if(arr6[i] % 2 == 1 || arr6[i] % 2 == 0) {
				sum += arr6[i];
			}
			i++;
		}
		System.out.println("Suma dodatnich: "+sum);

	}

}
