package wtorek;

public class homework1511 {

	public static void main(String[] args) {

		int x = 5, y = 10, z = 15;

		// 1. Napisz warunek sprawdzający, czy x jest większe od y.
		
		System.out.println("Zadanie 1:");
		if (x > y) {
			System.out.println(x + " jest większe od " + y);
		} else {
			System.out.println(x + " nie jest większe od " + y);

		}
		System.out.println();
		// 2. Napisz warunek sprawdzający, czy podwojona wartość x jest równa y.
		
		System.out.println("Zadanie 2:");
		if(2*x == y) {
			System.out.println("Podwojona wartosc "+x+" jest równa "+y);
		} else {
			System.out.println("Podwojona wartosc "+x+" nie jest równa "+y);

		}
		System.out.println();
		// 3. Napisz warunek sprawdzający, czy suma x oraz y równa się z.
		
		System.out.println("Zadanie 3:");
		if(x+y==z) {
			System.out.println(x+" + "+y+" jest rowne "+y);
		} else {
			System.out.println(x+" + "+y+" nie jest rowne "+y);
		}
		System.out.println();
		// 4. Napisz warunek sprawdzający, czy różnica z oraz y równa jest x.
		
		System.out.println("Zadanie 4:");
		if(z-y == x) {
			System.out.println("Roznica "+z+" i "+y+" jest rowna "+x);
		} else {
			System.out.println("Roznica "+z+" i "+y+" nie jest rowna "+x);
		}
		System.out.println();
		// 5. Sprawdź, czy negacja warunku: potrojonej wartości x równej z + 1
		// zwraca prawdę czy fałsz?
		
		System.out.println("Zadanie 5:");
		boolean isTrue = (3*x == z+1);
		System.out.println(isTrue);
		System.out.println();
		// 6. Jaki wynik (prawdę lub fałsz) zwróci różnica liczb z, y, x? Fałsz 
		// reprezentowany jest przez jaką liczbę?
		
		System.out.println("Zadanie 6:");
		System.out.println("W javie to chyba nie przejdzie :)");
			System.out.println();
		// *7. Sprawdź, czy liczba x jest stało- czy zmiennoprzecinkowa?
		
		System.out.println("Zadanie 7:");
		
		
		System.out.println();
		// 8. Napisz warunek sprawdzający czy iloczyn x jest większy od ilorazu z
		// oraz x powiększonego o 1.
		
		System.out.println("Zadanie 8:");
		if(x*x > z/x+1) {
			System.out.println("Kwadrat "+x+" jest wiekszy od ilorazu "+z+" i "+x+1);
		} else {
			System.out.println("Kwadrat "+x+" nie jest wiekszy od ilorazu "+z+" i "+x+1);
		}
		System.out.println();

		// 9 Sprawdź, czy kwadrat liczby y równy jest równy sumie iloczynu x
		// oraz y i kwadratu liczby x
		
		System.out.println("Zadanie 9:");
		if(y*y == (x*y)+(x*x)) {
			System.out.println("Kwadrat "+x+" jest rowny sumie iloczynu "+x+" i "+y+" i kwadratu "+x);
		} else {
			System.out.println("Kwadrat "+x+" nie jest rowny sumie iloczynu "+x+" i "+y+" i kwadratu "+x);
		}
		System.out.println();
	}

}
