package com.sda.mocks;

import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Czas na troche powazniejsze zadanie. Tym razem nalezy napisac testy ORAZ
 * funkcjonalnosci.
 * 
 * Pytajcie, pytajcie, pytajcie.
 * 
 * Klasy nad ktorymi bedziemy pracowac to {@code BankAccountTest} oraz
 * {@code BankAccount}. Opisy poszczegolnych zadanian znajduja sia nad
 * odpowiadajacymi testami + jedno tutaj:
 * 
 * Pierwszym krokiem jest stworzenie zmiennej typu {@code BankAccountApi}, ktora 
 * bedziemy mogli zamockowac.
 */
@RunWith(MockitoJUnitRunner.class)
public class BankOperationsTest {

	@Mock	
    private BankAccountApi account;

	private BankOperations operations;
	
	@Before
	public void setUp() {
		operations = new BankOperations(account);
	}

	
    /**
     * Test ten powinien sprawdzac funkcjonalnosc metody
     * {@code BankAccount#payWholeDebt()}.
     * 
     * Metoda ta powinna dzialac w nastepujacy sposob:
     *  - Jesli srodki na koncie sa wieksze niz dlug powinna zwracac "Success".
     *  - Jesli srodki na koncie sa rowne kwocie dlugi powinna zwracac
     *     "Success. Now you are broke".
     *  - Jesli kwota dlugu jest wieksza niz srodki na koncie metoda powinna zwracac
     *     "Not enough money".
     */
	@Test
    public void payWholeDebtTest() {
    	payWholeDebtParametrized(100.0d, 50.0d, "Success");
    	payWholeDebtParametrized(100.0d, 100.0d, "Success. Now you are broke");
    	payWholeDebtParametrized(100.0d, 150.0d, "Not enough money");

    }

	private void payWholeDebtParametrized(double balance, double debt, String msg) {
		when(account.getBalance()).thenReturn(balance);
		when(account.getDebt()).thenReturn(debt);
		String result = operations.payWholeDebt();
    	assertThat(result, is(msg));
	}
    /**
     * Test ten powinien sprawdzac funkcjonalnosc metody
     * {@code BankAccount#isDebtPayable()}.
     * 
     * Metoda ta powinna dzialac w nastepujacy sposob:
     *  - Jesli srodki na koncie sa wieksze lub rowne kwocie dlugu powinna zwracac true.
     *  - W innym wypadku powinna zwracac false.
     */
    public void isDebtPayableTest() {
    }

    /**
     * Test ten powinien sprawdzac funkcjonalnosc metody
     * {@code BankAccount#howManyThousands()}.
     *
     * Metoda ta powinna zwracac liczbe pelnych tysiecy dostepnych na koncie, tj:
     * dla kwoty 1234 -> 1
     * dla kwoty 10 -> 0
     * dla kwoty 7999 -> 7
     */
    public void howManyThousandsTest() {
    }

    /**
     * Test ten powinien sprawdzac funkcjonalnosc metody
     * {@code BankAccount#addTenBucks()}.
     * 
     * Metoda ta powinna dodawac 10.0d do stanu konta.
     */
    public void addTenBucksTest() {
    }

    /**
     * Test ten powinien sprawdzac funkcjonalnosc metody
     * {@code BankAccount#addForeingCurrency(double, double)}.
     * 
     * Metoda ta powinna dodawac kwote przekazana jako pierwszy argument
     * pomnozona przez drugi argument.
     */
    public void addForeingCurrencyTest() {
    }

    /**
     * Test ten powinien sprawdzac funkcjonalnosc metody
     * {@code BankAccount#payDebtAndCloseAccount()}
     * 
     * Metoda ta powinna dzialac w nastepujacy sposob:
     *  - Jesli ilosc srodkow na koncie jest nie wystarcza, by splacic
     *      dlug powinien byc rzucany wyjatek IllegalStateException
     *  - Jesli ilosc srodkow na koncie wystarcza, by splacic dlug, od 
     *    srodkow dostepnych powinna byc odejmowana kwota dlugu oraz powinna
     *    byc zawolana metoda {@code BankAccountApi#closeAccount()}.
     */
    public void payDebtAndCloseAccountTest() {
    }
}
