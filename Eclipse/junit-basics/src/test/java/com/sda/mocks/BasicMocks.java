package com.sda.mocks;

import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BasicMocks {

    @Mock
    private BankAccountApi account;

    @Before
    public void prepare() {
        when(account.getBalance()).thenReturn(20.0d);
        when(account.getOwner()).thenReturn("Piotrek");
        when(account.getDebt()).thenThrow(new IllegalStateException("TOO MUCH DEBT. SELL YOUR KIDNEY."));
    }

    @Test
    public void basics() {
        assertThat(account.getBalance(), is(20.0d));
        assertThat(account.getOwner(), is("Piotrek"));
        try {
            account.getDebt();
            fail();
        } catch (IllegalStateException ex) {
            assertThat(ex.getMessage(), is("TOO MUCH DEBT. SELL YOUR KIDNEY."));
        }
    }
    
    @Test
    public void counting() {
        verifyZeroInteractions(account);
        account.getBalance();
        verify(account, times(1)).getBalance();
        account.getBalance();
        account.getBalance();
        verify(account, times(3)).getBalance();
        account.addMoney(100.0d);
        verify(account, times(1)).addMoney(100.0d);
        verify(account, times(0)).addMoney(150.0d);
    }

}
