package com.sda.exercises;

import static org.junit.Assert.*;

import org.junit.Test;
import static com.sda.exercises.SuperGreeter.superGreet;

/**
 * Spojrz na klase {@code SuperGreeter}. Twoim zadaniem jest napisac testy
 * sprawdzajace dzialanie tej klasy. Testy te powinny sprawdzac nastepujace
 * przypadki:
 *  * Test(a moze testy?) sprawdzajace czy faktycznie metoda wita osobe
 *    przekazana jako argument.
 *  * Test sprawdzajacy jako zachowuje sie metoda po przekazaniu funkcji null
 *  * Test sprawdzajacy jak zachowuje sie metoda po przekazaniu pustego Stringa.
 *  * Test sprawdzajacy jak zachowuje sie metoda po przekazaniu arugmentu "World".
 */
public class BasicExercise {

	@Test
	public void testSuperGreeterConstruct() {
		String testHello = superGreet("Marcin");
		assertTrue(testHello.equals("Hello, Marcin! You definetly look great today!"));
		assertEquals("Marcin", testHello.substring(7, 13));
	}
	
	@Test
	public void testSuperGreeterConstructWithVar() {
		String name = "Karolina";
		String testHello = superGreet(name);
		assertEquals(name, testHello.substring(7, 15));
	}
	
	@Test
	public void testSuperGreeterNull() {
		shouldGreetDefaultValue(null);
	}
	
	@Test
	public void testSuperGreeterEmpty() {
		shouldGreetDefaultValue("");
	}
	
	@Test
	public void testSuperGreeterWorld() {
		String name = "World";
		String testHello = superGreet(name);
		assertEquals("World", testHello.substring(7, 12));
	}
	
	private void shouldGreetDefaultValue(String name) {
		String testHello = superGreet(name);
		assertEquals("World", testHello.substring(7, 12));
	}
	
}
