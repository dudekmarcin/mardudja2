package com.sda.exercises;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import static java.util.Arrays.asList;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;



/**
 * Uff, czyz nie swierzba Cie rece by samemu stworzyc testy? Spojrz na klase
 * {@code StudentCouncil} - reprezentuje ona rade uczniow. Zapozniaj sie z nia.
 * 
 * PYTAJCIE, PYTAJCIE, PYTAJCIE - prowadzacego, siebie nawzajem, wasatego wujka
 * Google i ciocie Stack Overflow.
 * 
 * Stworz testy sprawdzajace nastepujace funkcjonalnosci:
 *  - Dodawanie studenta
 *  - Dodawanie 10 studentow
 *  - Usuwanie studenta
 *  - Usuniecie >1 studentow.
 *  - Pobieranie lidera, kiedy nikogo nie ma
 *  - Udane pobieranie lidera
 *  - Wybor nowego lidera, kiedy nikogo nie ma
 *  - Udane wybieranie nowego lidera.
 * 
 * Po tym jak uda Ci sie napisac testy, wez kilka glebokich wdechow i zastanow
 * sie co mozna poprawic. Jesli wszystko jest wspaniale, sprobuj rozwiazac
 * ponizsze zadania:
 *  - Czy mozna napisac test badajacy w jakiej kolejnosci znajduja sie studenci? Jesli tak,
 *    stworz taki test.
 *  - Stworz metode w klasie {@code StudentCouncil}, ktora zwroci nowa 
 *    liste ze studentami posortowanymi ALFABETYCZNIE. Oczywisice napisz do niej test.
 *  - Stworz metode, ktora bedzie wybierala na lidera studenta o danym imieniu.
 *    Jesli nie ma studenta o danym imieniu, metoda powinna rzucac
 *    {@code IllegalArgumentException}. Oczywisice testy rowniez.
 */
public class StudentCouncilExercise {
	
	@Test
	public void testIsStudentsAreSorted() {
		List<String> studentList = new ArrayList<>(asList("Adam Kowalski", "Roman Kwiatkowski", "Wojciech Karolak", "Sam Anderson", "Luke Skywaker", "Tony Stark", "Gregory House", "James Wilson", "Lisa Cuddy", "Alison Cameron"));
		StudentCouncil council = new StudentCouncil(studentList); 
		assertEquals(council.getStudents(), council.getSortedStudents());
	}
	
	@Test
	public void testAddStudent() {
		List<String> studentList = new ArrayList<>();
		StudentCouncil council = new StudentCouncil(studentList); 
		assertTrue(council.addStudent("Roman Kwiatkowski"));
		List<String> retrievedStudents = council.getStudents();
		assertThat(retrievedStudents.size(), is(1) );
		assertThat(retrievedStudents.get(0), is("Roman Kwiatkowski") );

	}
	
	@Test
	public void testAddTenStudents() {
		List<String> studentList = new ArrayList<>();
		StudentCouncil council = new StudentCouncil(studentList); 
		String[] array = new String[] {"Adam Kowalski", "Roman Kwiatkowski", "Wojciech Karolak", "Sam Anderson", "Luke Skywaker", "Tony Stark", "Gregory House", "James Wilson", "Lisa Cuddy", "Alison Cameron"};
		for(int i=0;i<10;i++) {
			assertTrue(council.addStudent(array[i]));
		}
		assertTrue(council.getStudents().size() == 10);
		
		List<String> anotherStudentList = new ArrayList<>(asList("Adam Kowalski", "Roman Kwiatkowski", "Wojciech Karolak", "Sam Anderson", "Luke Skywaker", "Tony Stark", "Gregory House", "James Wilson", "Lisa Cuddy", "Alison Cameron"));
		StudentCouncil anotherCouncil = new StudentCouncil(anotherStudentList); 
		assertTrue(anotherCouncil.getSortedStudents().size() == 10);
	}
	
	@Test
	public void testRemoveStudent() {
		List<String> studentList = new ArrayList<>(asList("Adam Kowalski", "Roman Kwiatkowski", "Wojciech Karolak", "Sam Anderson", "Luke Skywaker", "Tony Stark", "Gregory House", "James Wilson", "Lisa Cuddy", "Alison Cameron"));
		StudentCouncil council = new StudentCouncil(studentList); 
		council.removeStudent("Sam Anderson");
		assertFalse(council.getStudents().contains("Sam Anderson"));
	}
	
	@Test
	public void testRemoveMoreStudents() {
		List<String> studentList = new ArrayList<>(asList("Adam Kowalski", "Roman Kwiatkowski", "Wojciech Karolak", "Sam Anderson", "Luke Skywaker", "Tony Stark", "Gregory House", "James Wilson", "Lisa Cuddy", "Alison Cameron"));
		StudentCouncil council = new StudentCouncil(studentList); 
		
		String[] toRemove = new String[] {"Sam Anderson", "Roman Kwiatkowski", "Adam Kowalski"};
		
		for(int i=0;i<3;i++){
			assertTrue(council.getStudents().contains(toRemove[i]));
			council.removeStudent(toRemove[i]);
			assertFalse(council.getStudents().contains(toRemove[i]));
		}

	}
	
	
	@Test (expected = IllegalStateException.class)
	public void testGetLeaderIfEmpty()  {
		List<String> studentList = new ArrayList<>();
		StudentCouncil council = new StudentCouncil(studentList);
		council.getLeader();
		council.electNewLeader();
	}
	
	@Test
	public void testGetLeader() {
		List<String> studentList = new ArrayList<>(asList("Adam Kowalski", "Roman Kwiatkowski", "Wojciech Karolak", "Sam Anderson", "Luke Skywaker", "Tony Stark", "Gregory House", "James Wilson", "Lisa Cuddy", "Alison Cameron"));
		StudentCouncil council = new StudentCouncil(studentList); 
		assertEquals(council.getLeader(), null);
		for(int i=0;i<100;i++) {
		council.electNewLeader();
		assertFalse(council.getLeader().equals(null));
		}
	}
	
	@Test
	public void testGetSortedStudent() {
		List<String> studentList = new ArrayList<>(asList("Roman Kwiatkowski", "Adam Kowalski", "Wojciech Karolak", "Sam Anderson", "Luke Skywaker", "Tony Stark", "Gregory House", "James Wilson", "Lisa Cuddy", "Alison Cameron"));
		StudentCouncil council = new StudentCouncil(studentList); 
		List<String> myList = new ArrayList<>();
		myList = council.getSortedStudents();
		assertEquals("Adam Kowalski", myList.get(0));
	}

	
	@Test(expected = IllegalArgumentException.class)
	public void testSetLeader()  {
		List<String> studentList = new ArrayList<>(asList("Roman Kwiatkowski", "Adam Kowalski", "Wojciech Karolak", "Sam Anderson", "Luke Skywaker", "Tony Stark", "Gregory House", "James Wilson", "Lisa Cuddy", "Alison Cameron"));
		StudentCouncil council = new StudentCouncil(studentList);
		council.electNewLeader();
		council.setLeader("Johny Deep");		
	}
}
