package com.sda.mocks;

public interface BankAccountApi {

    String getOwner();

    double getBalance();

    double getDebt();

    void addMoney(double amount);

    boolean payDebt(double amount);

    void closeAccount();

}
