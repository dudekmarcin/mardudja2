package com.sda.mocks;

public class BankOperations {

    private final BankAccountApi accountApi;

    public BankOperations(BankAccountApi account) {
        this.accountApi = account;
    }

    public String payWholeDebt() {
    	double debt = accountApi.getDebt();
    	double balance = accountApi.getBalance();
    	String result = "";
    	if(balance > debt) {
    		result = "Success";
    	} else if(balance == debt) {
    		result = "Success. Now you are broke";
    	} else {
    		result = "Not enough money";
    	}
    	return result;
    }
 
 
    public boolean isDebtPayable() {
        return false;
    }

    public int howManyThousands() {
        return 0;
    }

    public void addTenBucks() {
    }

    public void addForeingCurrency(double amount, double rate) {
    }

    public void payDebtAndCloseAccount() {
        
    }

}
