package com.sda.exercises;

public class SuperGreeter {

    public static String superGreet(String name) {
        if (name == null || name.isEmpty()) {
            name = "World";
        }
        return String.format("Hello, %s! You definetly look great today!", name);
    }

}
