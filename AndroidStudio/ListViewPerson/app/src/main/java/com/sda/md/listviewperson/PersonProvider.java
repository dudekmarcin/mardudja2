package com.sda.md.listviewperson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-01-17.
 */

public class PersonProvider {

    List<Person> persons;

    public List<Person> getPersons() {
        persons = new ArrayList<>();
        persons.add(new Person());
        persons.add(new Person("Kamil", "Stoch"));
        persons.add(new Person("Genowefa", "Pigwa"));
        persons.add(new Person("Edward", "Gierek"));
        persons.add(new Person("Tadeusz", "Norek"));

        return persons;
    }
}
