package com.sda.md.listviewperson;

/**
 * Created by RENT on 2017-01-17.
 */

public enum Sort {
    NONE,
    INCREASING,
    DECREASING;
}
