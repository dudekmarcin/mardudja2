package com.sda.md.listviewperson;

/**
 * Created by RENT on 2017-01-17.
 */

public class Person {
    private String name;
    private String lastname;

    public Person() {
        name = "Jan";
        lastname = "Kowalski";
    }

    public Person(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getName() {
        return name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return lastname + " " + name;
    }

}
