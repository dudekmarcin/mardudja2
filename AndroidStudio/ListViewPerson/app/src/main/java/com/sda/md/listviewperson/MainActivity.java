package com.sda.md.listviewperson;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends Activity {

    private ListView listView;
    private Button addPerson;
    private EditText name;
    private EditText lastname;
    private List<Person> persons;
    private PersonListAdapter adapter;
    private Switch sort_switch;

    private Comparator<Person> comparator = new Comparator<Person>() {
        @Override
        public int compare(Person person, Person t1) {
            int result = person.getLastname().compareToIgnoreCase(t1.getLastname());
            if (result == 0) {
                return person.getName().compareToIgnoreCase(t1.getName());
            } else {
                return result;
            }
        }
    };

    private Comparator<Person> descComparator = new Comparator<Person>() {
        @Override
        public int compare(Person person, Person t1) {
            return -1 * comparator.compare(person, t1);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.person_list_view);
        persons = new PersonProvider().getPersons();
        adapter = new PersonListAdapter(persons);
        listView.setAdapter(adapter);
        name = (EditText) findViewById(R.id.name);
        lastname = (EditText) findViewById(R.id.lastname);
        addPerson = (Button) findViewById(R.id.add_person);
        sort_switch = (Switch) findViewById(R.id.sort_switch);

        sort_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                sorting(checked);
                adapter.notifyDataSetChanged();

            }
        });

        addPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                persons.add(new Person(name.getText().toString(), lastname.getText().toString()));
                name.setText("");
                lastname.setText("");
                adapter.notifyDataSetChanged();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                persons.remove(i);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void sorting(boolean order) {
        if (order) {
            Collections.sort(persons, comparator);
        } else {
            Collections.sort(persons, descComparator);
        }
    }

    class PersonListAdapter extends BaseAdapter {

        private List<Person> persons;

        public PersonListAdapter(List<Person> persons) {
            this.persons = persons;
        }

        @Override
        public int getCount() {
            return persons.size();
        }

        @Override
        public Object getItem(int i) {
            return persons.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView;
            if (view != null) {
                textView = (TextView) view;
            } else {
                textView = new TextView(MainActivity.this);
            }
            textView.setText(persons.get(i).toString());
            return textView;
        }


    }
}



