package com.sda.md.persondatabinding;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sda.md.persondatabinding.databinding.ActivityMainBinding;

public class MainActivity extends Activity {

    Person person;
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        person = new Person("Adam", "Nowak", 40);
        binding.setUser(person);

        binding.eraseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                person.setName("");
                person.setSurname("");
                binding.ageField.setText("");
            }
        });



        binding.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                person.setName(binding.nameField.getText().toString());
            }
        });

    }


}
