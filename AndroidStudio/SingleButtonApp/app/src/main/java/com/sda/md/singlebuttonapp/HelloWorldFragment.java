package com.sda.md.singlebuttonapp;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


public class HelloWorldFragment extends Fragment {



       public HelloWorldFragment() {}

    public static HelloWorldFragment newInstance(String param1, String param2) {
        HelloWorldFragment fragment = new HelloWorldFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final MainActivity activity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_hello_world, container, false);
        Button actionButton  = (Button) view.findViewById(R.id.action_button);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showToast();
            }
        });
        return view;
    }


}
