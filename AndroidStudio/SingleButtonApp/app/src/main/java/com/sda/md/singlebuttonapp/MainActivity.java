package com.sda.md.singlebuttonapp;

import android.app.FragmentManager;import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private android.app.FragmentManager fragmentManager;
    private Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void showToast() {
        Toast.makeText(getApplicationContext(), "Hello world", Toast.LENGTH_SHORT).show();

    }

}
