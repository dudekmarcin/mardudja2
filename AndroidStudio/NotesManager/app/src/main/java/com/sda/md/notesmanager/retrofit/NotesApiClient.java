package com.sda.md.notesmanager.retrofit;

import com.sda.md.notesmanager.DTO.NoteRequest;
import com.sda.md.notesmanager.DTO.NoteResponse;
import com.sda.md.notesmanager.DTO.NotesListResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by RENT on 2017-02-07.
 */

public interface NotesApiClient {

    @GET("/plugin/notes.list")
    Call<NotesListResponse> getNotesList();

    @GET(" plugin/note.details")
    Call<NoteResponse> getNote(@Query("id") String id);

    @POST("/plugin/notes.create")
    Call<ResponseBody> addNote(@Body NoteRequest note);

    @PUT("/plugin/notes.update")
    Call<ResponseBody> updateNote(@Body NoteRequest note);
}
