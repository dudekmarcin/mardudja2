package com.sda.md.notesmanager.DTO;

/**
 * Created by RENT on 2017-02-07.
 */

public class NoteRequest {

    private String id;
    private String name;
    private String content;

    public NoteRequest(String name, String content) {
        this.name = name;
        this.content = content;
    }

    public NoteRequest(String id, String name, String content) {
        this.id = id;
        this.name = name;
        this.content = content;
    }
}
