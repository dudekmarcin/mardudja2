package com.sda.md.notesmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.sda.md.notesmanager.AsyncTasks.NotesListAsync;
import com.sda.md.notesmanager.DTO.NotesListItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sda.md.notesmanager.NoteDetailsActivity.REQUEST;

public class MainActivity extends AppCompatActivity implements NotesListAdapter.ListItemClickListener, NotesListAsync.NotesListListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private NotesListAdapter adapter;
    private NotesListAsync notesListAsync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new NotesListAdapter(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        notesListAsync = new NotesListAsync(this);
        notesListAsync.execute();

    }

    @Override
    public void onListDownloaded(List<NotesListItem> notes) {
        if(adapter == null) {
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
            return;
        }
        adapter.setNotes(notes);
    }

    @Override
    public void onItemClick(NotesListItem item) {
        Intent intent = new Intent(this, NoteDetailsActivity.class);
        intent.putExtra("noteID", item.getId());
        startActivity(intent);
    }

    @OnClick(R.id.add_button)
    public void addNote() {
        startActivityForResult(new Intent(this, EditNoteActivity.class), REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST) {
            if(resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Dodano notatkę", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
