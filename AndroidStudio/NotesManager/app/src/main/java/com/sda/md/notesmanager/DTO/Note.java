package com.sda.md.notesmanager.DTO;

/**
 * Created by RENT on 2017-02-07.
 */

public class Note {

    private String id;
    private String name;
    private String content;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
