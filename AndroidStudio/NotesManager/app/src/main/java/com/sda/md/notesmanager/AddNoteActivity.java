package com.sda.md.notesmanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.sda.md.notesmanager.AsyncTasks.NoteAddAsync;
import com.sda.md.notesmanager.DTO.NoteRequest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNoteActivity extends AppCompatActivity {

    @BindView(R.id.note_add_name)
    TextView name;
    @BindView(R.id.note_add_content)
    TextView content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.add_button)
    public void addNote() {
        NoteRequest note = new NoteRequest(name.getText().toString(), content.getText().toString());
        new NoteAddAsync().execute(note);
    }
}
