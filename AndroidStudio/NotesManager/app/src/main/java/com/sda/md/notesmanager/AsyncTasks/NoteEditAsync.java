package com.sda.md.notesmanager.AsyncTasks;

import android.os.AsyncTask;

import com.sda.md.notesmanager.DTO.NoteRequest;
import com.sda.md.notesmanager.retrofit.NotesApiClient;
import com.sda.md.notesmanager.retrofit.NotesApiClientFactory;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by RENT on 2017-02-07.
 */

public class NoteEditAsync extends AsyncTask<NoteRequest, Void, Boolean> {

    private final NotesApiClient apiClient;

    public NoteEditAsync() {
        this.apiClient = new NotesApiClientFactory().create();
    }

    @Override
    protected Boolean doInBackground(NoteRequest... noteRequests) {
        try {
            edit(noteRequests[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Boolean edit(NoteRequest note) throws IOException {
        Response<ResponseBody>  response = apiClient.updateNote(note).execute();
        return response.isSuccessful();
    }
}
