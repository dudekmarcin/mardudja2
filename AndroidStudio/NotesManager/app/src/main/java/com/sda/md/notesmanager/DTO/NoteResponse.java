package com.sda.md.notesmanager.DTO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RENT on 2017-02-07.
 */

public class NoteResponse {

    @SerializedName("data")
    private Note note;

    public Note getNote() {
        return note;
    }

}
