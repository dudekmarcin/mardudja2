package com.sda.md.notesmanager.DTO;

/**
 * Created by RENT on 2017-02-07.
 */

public class NotesListItem {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
