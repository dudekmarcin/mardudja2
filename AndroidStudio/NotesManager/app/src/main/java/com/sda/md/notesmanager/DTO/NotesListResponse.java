package com.sda.md.notesmanager.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RENT on 2017-02-07.
 */

public class NotesListResponse {

    @SerializedName("data")
    private List<NotesListItem> notes;

    public List<NotesListItem> getNotes() {
        return notes;
    }
}
