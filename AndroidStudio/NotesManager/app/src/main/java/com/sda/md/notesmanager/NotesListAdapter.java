package com.sda.md.notesmanager;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sda.md.notesmanager.DTO.NotesListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-02-07.
 */

public class NotesListAdapter extends RecyclerView.Adapter<NotesListAdapter.NotesListHolder> {

    private List<NotesListItem> notes;
    private final ListItemClickListener listener;

    public NotesListAdapter(ListItemClickListener listener) {
        this.listener = listener;
        notes = new ArrayList<>();

    }

    @Override
    public NotesListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout, parent, false);
        return new NotesListHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(NotesListHolder holder, int position) {
        holder.name.setText(notes.get(position).getName());
        holder.item = notes.get(position);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public void setNotes(List<NotesListItem> notes) {
        this.notes = notes;
        notifyDataSetChanged();
    }

    public class NotesListHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private final ListItemClickListener listener;
        private NotesListItem item;

        public NotesListHolder(final View itemView, ListItemClickListener clickListener) {
            super(itemView);
            listener = clickListener;
            name = (TextView) itemView.findViewById(R.id.note_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public interface ListItemClickListener {
        public void onItemClick(NotesListItem item);
    }
}
