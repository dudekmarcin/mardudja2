package com.sda.md.notesmanager;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.sda.md.notesmanager.AsyncTasks.NoteAddAsync;
import com.sda.md.notesmanager.AsyncTasks.NoteEditAsync;
import com.sda.md.notesmanager.DTO.NoteRequest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditNoteActivity extends AppCompatActivity {

    @BindView(R.id.note_edit_name)
    EditText name;
    @BindView(R.id.note_edit_content)
    EditText content;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        ButterKnife.bind(this);

        if(getIntent().hasExtra("noteName")) {
            name.setText(getIntent().getStringExtra("noteName"));
        }
        if(getIntent().hasExtra("noteContent")) {
            content.setText(getIntent().getStringExtra("noteContent"));
        }
    }

    @OnClick(R.id.edit_button)
    public void edit() {
        if(getIntent().hasExtra("noteID")) {
            NoteRequest note = new NoteRequest(getIntent().getStringExtra("noteID"),
                    name.getText().toString(), content.getText().toString());

            new NoteEditAsync().execute(note);
        } else {
            NoteRequest note = new NoteRequest(name.getText().toString(), content.getText().toString());
            new NoteAddAsync().execute(note);
        }

        setResult(Activity.RESULT_OK);
        finish();
    }
}
