package com.sda.md.notesmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.sda.md.notesmanager.AsyncTasks.NoteDetailsAsync;
import com.sda.md.notesmanager.DTO.Note;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoteDetailsActivity extends AppCompatActivity implements NoteDetailsAsync.NoteDownloadListener {

    @BindView(R.id.note_det_name)
    TextView name;

    @BindView(R.id.note_det_content)
    TextView content;

    private String id;
    static final int REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);
        ButterKnife.bind(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        new NoteDetailsAsync(this).execute(getIntent().getStringExtra("noteID"));
    }

    @Override
    public void onNoteDownload(Note note) {
        name.setText(note.getName());
        content.setText(note.getContent());
        id = note.getId();
    }

    @OnClick(R.id.edit_button)
    public void editNote() {
        Intent intent = new Intent(this, EditNoteActivity.class);
        intent.putExtra("noteID", id);
        intent.putExtra("noteName", name.getText().toString());
        intent.putExtra("noteContent", content.getText().toString());
        startActivityForResult(intent, REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST) {
            if(resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Zmieniono notatkę", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
