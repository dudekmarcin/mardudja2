package com.sda.md.notesmanager.AsyncTasks;

import android.os.AsyncTask;

import com.sda.md.notesmanager.DTO.NotesListItem;
import com.sda.md.notesmanager.DTO.NotesListResponse;
import com.sda.md.notesmanager.retrofit.NotesApiClient;
import com.sda.md.notesmanager.retrofit.NotesApiClientFactory;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

/**
 * Created by RENT on 2017-02-07.
 */

public class NotesListAsync extends AsyncTask<Void, Void, List<NotesListItem>> {

    private final NotesApiClient apiClient;
    private final NotesListListener listener;

    public NotesListAsync(NotesListListener listener) {
        this.listener = listener;
        this.apiClient = new NotesApiClientFactory().create();
    }

    @Override
    protected List<NotesListItem> doInBackground(Void... voids) {
        try {
            return getList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<NotesListItem> notesList) {
        listener.onListDownloaded(notesList);
    }

    private List<NotesListItem> getList() throws IOException {
        Response<NotesListResponse> response = apiClient.getNotesList().execute();
        if(response.isSuccessful()) {
            return response.body().getNotes();
        }
        return null;

    }

    public interface NotesListListener {
        public void onListDownloaded(List<NotesListItem> notes);
    }
}
