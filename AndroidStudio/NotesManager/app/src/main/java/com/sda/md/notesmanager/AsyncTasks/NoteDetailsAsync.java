package com.sda.md.notesmanager.AsyncTasks;

import android.os.AsyncTask;

import com.sda.md.notesmanager.DTO.Note;
import com.sda.md.notesmanager.DTO.NoteResponse;
import com.sda.md.notesmanager.retrofit.NotesApiClient;
import com.sda.md.notesmanager.retrofit.NotesApiClientFactory;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by RENT on 2017-02-07.
 */

public class NoteDetailsAsync extends AsyncTask<String, Void, Note> {

    private final NotesApiClient apiClient;
    private final NoteDownloadListener listener;

    public NoteDetailsAsync(NoteDownloadListener listener) {
        this.listener = listener;
        apiClient = new NotesApiClientFactory().create();
    }


    @Override
    protected Note doInBackground(String... strings) {
        try {
            return getNote(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Note note) {
        listener.onNoteDownload(note);


    }

    private Note getNote(String id) throws IOException {
        Response<NoteResponse> response = apiClient.getNote(id).execute();
        if(response.isSuccessful()) {
            return response.body().getNote();
        }
        return null;
    }

    public interface NoteDownloadListener {
        public void onNoteDownload(Note note);
    }
}
