package com.sda.md.notesmanager.AsyncTasks;

import android.os.AsyncTask;

import com.sda.md.notesmanager.DTO.NoteRequest;
import com.sda.md.notesmanager.retrofit.NotesApiClient;
import com.sda.md.notesmanager.retrofit.NotesApiClientFactory;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by RENT on 2017-02-07.
 */

public class NoteAddAsync extends AsyncTask<NoteRequest, Void, Boolean> {

    private final NotesApiClient apiClient;

    public NoteAddAsync() {
        this.apiClient = new NotesApiClientFactory().create();
    }

    @Override
    protected Boolean doInBackground(NoteRequest... noteRequests) {
        try {
            addNote(noteRequests[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Boolean addNote(NoteRequest note) throws IOException {
        Response<ResponseBody> response = apiClient.addNote(note).execute();
        return response.isSuccessful();
    }

}
