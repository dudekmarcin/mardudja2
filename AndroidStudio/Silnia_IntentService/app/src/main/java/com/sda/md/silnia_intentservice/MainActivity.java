package com.sda.md.silnia_intentservice;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText number;
    private static TextView wynikField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        number = (EditText) findViewById(R.id.number);
        wynikField = (TextView) findViewById(R.id.wynik);
    }

    public void silnia(View view) {
        int liczba = Integer.parseInt(number.getText().toString());
        Intent intent = new Intent(getApplicationContext(), SilniaService.class);
        intent.putExtra("liczba", liczba);
        startService(intent);
    }


    public static class SilniaService extends IntentService {


        public SilniaService() {
            super("SilniaService");
        }

        @Override
        protected void onHandleIntent(Intent intent) {
          if(intent != null) {
              if(intent.hasExtra("liczba")) {
                  int liczba = intent.getIntExtra("liczba", 0);

                  int wynik = 1;
                  for(int i = 1; i <= liczba; i++) {
                      wynik += i;
                  }
                  final int finalresult = wynik;
                  new Handler(Looper.getMainLooper()).post(new Runnable() {
                      @Override
                      public void run() {
                          wynikField.setText(String.valueOf(finalresult));
                      }
                  });

              }
          }
        }
    }
}
