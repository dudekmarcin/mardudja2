package com.sda.md.catnews;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RENT on 2017-02-02.
 */

public class RetroApiClientFactory {


    public RetroApiClient create() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://91.134.143.223:9000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(RetroApiClient.class);
    }
}
