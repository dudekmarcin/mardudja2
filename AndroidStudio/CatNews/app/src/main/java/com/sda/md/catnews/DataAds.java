package com.sda.md.catnews;

/**
 * Created by RENT on 2017-02-02.
 */
public class DataAds implements Item {

   private String adContent;
   private Integer index;

    public String getAdContent() {
        return adContent;
    }

    @Override
    public Integer getIndex() {
        return index;
    }

    @Override
    public int getItemType() {
        return Item.ADSTYPE;
    }
}
