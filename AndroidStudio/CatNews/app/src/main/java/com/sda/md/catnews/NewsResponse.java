package com.sda.md.catnews;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RENT on 2017-02-02.
 */
public class NewsResponse {

    @SerializedName("data")
    private List<DataNews> news;

    public NewsResponse(List<DataNews> news) {
        this.news = news;
    }

    public List<DataNews> getData() {
        return news;
    }
}
