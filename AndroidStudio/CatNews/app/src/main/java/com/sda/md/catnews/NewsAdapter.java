package com.sda.md.catnews;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-02-02.
 */

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DataNews> dataNews = new ArrayList<>();
    private List<Item> items;
    private Context context;

    public NewsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getItemType();
    }

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_layout, parent, false);
        return new NewsHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch(getItemViewType(position)) {
            case Item.NEWSTYPE:
                NewsHolder newsHolder = (NewsHolder) holder;
                DataNews news = (DataNews) items.get(position);
                Picasso.with(context)
                        .load(news.getImageUrl())
                        .fit()
                        .centerCrop()
                        .into(newsHolder.image);
                newsHolder.title.setText(news.getTitle());
                newsHolder.content.setText(news.getContent());
                break;
            case Item.ADSTYPE:
                AdsHolder adsHolder = (AdsHolder) holder;
                DataAds ads = (DataAds) items.get(position);
                adsHolder.content.setText(ads.getAdContent());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return dataNews.size();
    }


    public void setData(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class NewsHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView content;
        private ImageView image;

        public NewsHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.news_title);
            content = (TextView) itemView.findViewById(R.id.news_content);
            image = (ImageView) itemView.findViewById(R.id.news_image);
        }
    }

    public class AdsHolder extends RecyclerView.ViewHolder {
        private TextView content;

        public AdsHolder(View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.ad_content);
        }
    }
}
