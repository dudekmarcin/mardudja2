package com.sda.md.catnews;

import java.util.Comparator;
import java.util.List;

/**
 * Created by RENT on 2017-02-02.
 */

public class ContentManager {
    private List<DataAds> ads;
    private List<DataNews> news;

    public ContentManager(AdsResponse ads, NewsResponse news) {
        this.ads = ads.getData();
        this.news = news.getData();
    }

    public List<DataAds> getAds() {
        return ads;
    }

    public List<DataNews> getNews() {
        return news;
    }

    public NewsResponse getData() {
        ads.sort(new Comparator<DataAds>() {
            @Override
            public int compare(DataAds dataAds, DataAds t1) {
                return dataAds.getIndex().compareTo(t1.getIndex());
            }
        });

        news.sort(new Comparator<DataNews>() {
            @Override
            public int compare(DataNews dataNews, DataNews t1) {
                return dataNews.getIndex().compareTo(t1.getIndex());
            }
        });
        return null;
    }
}
