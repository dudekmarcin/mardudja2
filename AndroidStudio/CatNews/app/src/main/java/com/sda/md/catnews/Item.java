package com.sda.md.catnews;

/**
 * Created by RENT on 2017-02-02.
 */
public interface Item {

    public final static int NEWSTYPE = 1;
    public final static int ADSTYPE = 2;

    public abstract int getItemType();

    public abstract Integer getIndex();


}
