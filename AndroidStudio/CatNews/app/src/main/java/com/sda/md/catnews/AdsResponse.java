package com.sda.md.catnews;

import java.util.List;

/**
 * Created by RENT on 2017-02-02.
 */
public class AdsResponse {

    private String result;
    private List<DataAds> data;

    public String getResult() {
        return result;
    }

    public List<DataAds> getData() {
        return data;
    }
}