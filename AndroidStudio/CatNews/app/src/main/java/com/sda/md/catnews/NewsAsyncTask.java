package com.sda.md.catnews;

import android.os.AsyncTask;
import android.speech.tts.Voice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by RENT on 2017-02-02.
 */

public class NewsAsyncTask extends AsyncTask<Void, Void, List<Item>> {

    private RetroApiClient retroApiClient;
    private NewsDownloadedListener newsDownloadedListener;

    public NewsAsyncTask(NewsDownloadedListener newsDownloadedListener) {
        this.newsDownloadedListener = newsDownloadedListener;
        RetroApiClientFactory retrofitApiClientFactory = new RetroApiClientFactory();
        this.retroApiClient = retrofitApiClientFactory.create();
    }


    @Override
    protected List<Item> doInBackground(Void... voids) {
        try {
            NewsResponse news = getNews();
            AdsResponse ads = getAds();
            List<DataAds> dataAdsList = ads.getData();
            List<DataNews> dataNewsList = news.getData();
            List<Item> response = new ArrayList<>();
            for(DataNews singleNews : dataNewsList) {
                response.add(singleNews);
            }
            for(DataAds singleAd : dataAdsList) {
                response.add(singleAd);
            }

            response.sort(new Comparator<Item>() {
                @Override
                public int compare(Item o1, Item o2) {
                    return o1.getIndex().compareTo(o2.getIndex());
                }
            });


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Item> items) {
        newsDownloadedListener.onNewsDownloaded(items);
    }

    private NewsResponse getNews() throws IOException {

        Call<NewsResponse> call = retroApiClient.getNews();
        Response<NewsResponse> response = call.execute();
        if (response.isSuccessful()) { //http 200+
            return response.body();
        }
        return null;
    }

    private AdsResponse getAds() throws IOException {

        Call<AdsResponse> call = retroApiClient.getAds();
        Response<AdsResponse> response = call.execute();
        if (response.isSuccessful()) { //http 200+
            return response.body();
        }
        return null;
    }

    public interface NewsDownloadedListener {
        void onNewsDownloaded(List<Item> items);
    }

}


