package com.sda.md.catnews;

/**
 * Created by RENT on 2017-02-02.
 */
public class DataNews implements Item {

    private String title;
    private String content;
    private String imageUrl;
    private Integer index;

    public DataNews(String title, String content, String imageUrl, int index) {
        this.title = title;
        this.content = content;
        this.imageUrl = imageUrl;
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public Integer getIndex() {
        return index;
    }

    @Override
    public int getItemType() {
        return Item.NEWSTYPE;
    }
}
