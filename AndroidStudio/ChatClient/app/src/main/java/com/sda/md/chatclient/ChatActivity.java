package com.sda.md.chatclient;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatActivity extends AppCompatActivity implements Runnable, Inotifiable {

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.chat_field)
    TextView content;

    @BindView(R.id.msg_field)
    EditText msg;

    private Client client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        new Thread(this).start();
    }

    @Override
    public void run() {
        String ip = getIntent().getStringExtra("IP");
        client = new Client(ip, this);
    }

    @Override
    public void addMessager(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                content.setText(content.getText() + "\n" + msg);
            }
        });
    }

    @OnClick(R.id.send_button)
    public void send(){
        new Thread(new Runnable() {

            @Override
            public void run() {
                client.write(msg.getText().toString());
            }
        }).start();
        addMessager("You: "+ msg.getText().toString());
        msg.setText("");
    }
}
