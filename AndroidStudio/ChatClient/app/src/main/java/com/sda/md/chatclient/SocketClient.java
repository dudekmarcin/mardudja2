package com.sda.md.chatclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by RENT on 2017-02-25.
 */

public class SocketClient {

    private PrintWriter writer;
    private BufferedReader reader;
    private Inotifiable inotifiable;

    public SocketClient() {}

    public void startConnection(String hostname, Integer port) {
        try {
            Socket socket = new Socket(hostname, port);
            socket.setSoTimeout(5000);
            writer = new PrintWriter(socket.getOutputStream());
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            Thread readerThread = new Thread(new ReaderClass(reader, inotifiable));
            readerThread.start();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Socket didn't open");
        }
    }

    public synchronized void write(String content) {
        writer.println(content);
        writer.flush();
    }

    public Inotifiable getInotifiable() {
        return inotifiable;
    }

    public void setInotifiable(Inotifiable inotifiable) {
        this.inotifiable = inotifiable;
    }
}
