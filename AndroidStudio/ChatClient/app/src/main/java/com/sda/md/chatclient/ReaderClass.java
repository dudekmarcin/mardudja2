package com.sda.md.chatclient;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by RENT on 2017-02-25.
 */

public class ReaderClass implements Runnable {

    private BufferedReader reader;
    Inotifiable inotifiable;

    public ReaderClass(BufferedReader reader, Inotifiable inotifiable) {
        this.reader = reader;
        this.inotifiable = inotifiable;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if(reader.ready()) {
                   inotifiable.addMessager(reader.readLine());
                } else {
                    Thread.sleep(100);
                }
            } catch (IOException e) {
                System.out.println("Nothing in return");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
