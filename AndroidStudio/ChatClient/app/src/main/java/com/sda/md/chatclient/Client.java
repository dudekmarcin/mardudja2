package com.sda.md.chatclient;

/**
 * Created by RENT on 2017-02-25.
 */

public class Client {
    public final static String CONST_HOSTNAME = "192.168.1.195";
    //    public final static String CONST_HOSTNAME = "10.10.43.2";
    public final static Integer CONST_PORT = 10000;


    private SocketClient client;

    public Client() {
        client = new SocketClient();
        client.startConnection(CONST_HOSTNAME, CONST_PORT);
    }

    public Client(String ip, Inotifiable inotifiable) {
        client = new SocketClient();
        client.setInotifiable(inotifiable);
        client.startConnection(ip, CONST_PORT);
    }

    public void write(String content) {
        client.write(content);
    }


}
