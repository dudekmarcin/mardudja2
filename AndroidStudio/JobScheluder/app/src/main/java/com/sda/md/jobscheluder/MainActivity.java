package com.sda.md.jobscheluder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.result_field)
    TextView resultField;

    private MyReceiver myReceiver;
    private  FirebaseJobDispatcher dispatcher;
    private Job myJob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        myReceiver = new MyReceiver();
        dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag(MyJobService.TAG)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver, new IntentFilter(MyJobService.MSG));
        dispatcher.mustSchedule(myJob);
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        dispatcher.cancel("MyJobServiceMsg");
        super.onStop();
    }

    class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            resultField.setText(intent.getStringExtra(MyJobService.MSG));
        }
    }
}
