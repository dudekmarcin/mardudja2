package com.sda.md.jobscheluder;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RENT on 2017-03-06.
 */


public class MyJobService extends JobService {

    public static final String TAG = "com.sda.md.jobscheluder.job";
    public static final String MSG = "com.sda.md.jobscheluder.msg";
    private AsyncTask<String, String, String> asyncTask;

    public MyJobService() {
        asyncTask = new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... strings) {
                Log.d("!!! JOBASYNCTASK ", "start async taks");
                return SimpleDateFormat.getTimeInstance().format(new Date());
            }

            @Override
            protected void onPostExecute(String s) {
                sendMsg(s);
            }
        };
    }

    @Override
    public boolean onStartJob(JobParameters job) {
        Log.d("!!! MYJOBSERVICE", "onStartJob !!!");
        asyncTask.execute();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        Log.d("!!! MYJOBSERVICE", "onStopJob !!!");
        if (asyncTask != null) {
            asyncTask.cancel(true);
            asyncTask = null;
        }
        return false;
    }

    private void sendMsg(String msg) {
        Intent intent = new Intent(MSG);
        intent.putExtra("MyJobServiceMsg", msg);
        LocalBroadcastManager.getInstance(MyJobService.this).sendBroadcast(intent);
    }
}
