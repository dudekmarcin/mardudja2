package com.sda.md.intentserviceapp;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.widget.Toast;

public class WaitingService extends IntentService {

    public WaitingService() {
        super("WaitingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            SystemClock.sleep(10000);
            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Komunikat", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
