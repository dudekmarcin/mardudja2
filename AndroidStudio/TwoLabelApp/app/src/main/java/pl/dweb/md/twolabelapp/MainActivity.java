package pl.dweb.md.twolabelapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private TextView name;
    private TextView lastname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = (TextView) findViewById(R.id.name);
        lastname = (TextView) findViewById(R.id.lastname);
    }

    public void labelFiller(View view) {
        if(name.getText().equals("")) {
            name.setText("Marcin");
        } else if(lastname.getText().equals("")) {
            lastname.setText("Dudek");
        }
    }


}
