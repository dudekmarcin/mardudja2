package com.sda.md.tictactoe;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by RENT on 2017-02-04.
 */

public interface ApiClient {

    @POST("/user")
    @Headers({"X-BAASBOX-APPCODE: 1234567890", "Content-Type: application/json"})
    Call<LoginResponse> register(@Body Register register);

    @POST("/login")
    @Headers({"X-BAASBOX-APPCODE: 1234567890", "Content-Type: application/json"})
    Call<LoginResponse> login(@Body Login login);

    @POST("/logout")
    Call<ResponseBody> logout(@Header("X-BB-SESSION") String token);

}
