package com.sda.md.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements TokenListener {

    @BindView(R.id.log_login)
    TextView loginField;

    @BindView(R.id.log_pass)
    TextView passField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login_button)
    public void login() {
        String login = loginField.getText().toString();
        String pass = passField.getText().toString();

        if(!login.isEmpty() && !pass.isEmpty()) {
            new LoginAsyncTask(this).execute(new Login(login, pass));
        } else {
            Toast.makeText(this, "Hasła nie są identyczne", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onLoginSuccess() {
        Intent intent = new Intent(this, AfterLoginActivity.class);
        intent.putExtra("user_login", loginField.getText().toString());
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
    @Override
    public void onLoginFailed() {
        Toast.makeText(this, "logowanie nieudane", Toast.LENGTH_SHORT).show();
    }
}
