package com.sda.md.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity implements TokenListener {


    @BindView(R.id.reg_login)
    TextView loginField;

    @BindView(R.id.reg_pass)
    TextView passField;

    @BindView(R.id.reg_confirm_pass)
    TextView confirmPassField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.reg_button)
    public void register() {
        String login = loginField.getText().toString();
        String pass1 = passField.getText().toString();
        String pass2 = confirmPassField.getText().toString();

        if(!login.isEmpty() && !pass1.isEmpty() && pass1.equals(pass2)) {
            new RegisterAsyncTask(this).execute(new Register(login, pass1));
        } else {
            Toast.makeText(this, "Hasła nie są identyczne", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoginSuccess() {
        Toast.makeText(this, "logowanie udane", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginFailed() {
        Toast.makeText(this, "logowanie nieudane", Toast.LENGTH_SHORT).show();
    }
}
