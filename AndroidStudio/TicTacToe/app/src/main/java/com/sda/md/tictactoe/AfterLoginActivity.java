package com.sda.md.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AfterLoginActivity extends AppCompatActivity {

    @BindView(R.id.username_field)
    TextView loginField;
    private User user = User.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_login);
        ButterKnife.bind(this);
        loginField.setText(getIntent().getStringExtra("user_login"));
    }

    @OnClick(R.id.logout)
    public void logout() {
        user.logout();
        finishActivity(1);

    }
}
