package com.sda.md.tictactoe;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RENT on 2017-02-04.
 */
public class UserTokenResponse {

    @SerializedName("X-BB-SESSION")
    private String token;

    public String getToken() {
        return token;
    }
}
