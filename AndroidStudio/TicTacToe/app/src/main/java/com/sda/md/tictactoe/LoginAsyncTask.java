package com.sda.md.tictactoe;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by md on 2/6/17.
 */

public class LoginAsyncTask extends AsyncTask<Login, Void, Boolean> {

    private final TokenListener listener;
    private final User user;

    public LoginAsyncTask(TokenListener listener) {
        user = User.getInstance();
        this.listener = listener;
    }

    @Override
    protected Boolean doInBackground(Login... params) {
        try {
            user.login(params[0]);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    protected void onPostExecute(Boolean result) {
        if(result) {
            listener.onLoginSuccess();
        } else {
            listener.onLoginFailed();
        }

    }

}
