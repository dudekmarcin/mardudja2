package com.sda.md.tictactoe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.go_register)
    public void register() {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @OnClick(R.id.go_login)
    public void login() {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
