package com.sda.md.tictactoe;

import android.content.Intent;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RENT on 2017-02-04.
 */

public class LoginResponse {
    private Integer http_code;
    @SerializedName("data")
    private UserTokenResponse token;

    public UserTokenResponse getToken() {
        return token;
    }

    public Integer getHttp_code() {
        return http_code;
    }
}
