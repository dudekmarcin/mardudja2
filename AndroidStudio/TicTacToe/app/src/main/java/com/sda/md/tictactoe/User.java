package com.sda.md.tictactoe;

import android.util.Log;

import java.io.IOException;
import java.text.ParseException;

import retrofit2.Response;

/**
 * Created by RENT on 2017-02-04.
 */

public class User {

    private static User instance = new User();
    private String token;
    private final ApiClient apiClient;

    private User() {
        apiClient = new ApiClientFactory().create();
    }

    public static User getInstance() {
        if(instance == null) {
            instance = new User();
        }
        return instance;
    }

    public void register(Register register) throws IOException {
        Response<LoginResponse> response = apiClient.register(register).execute();
        if(response.isSuccessful()) {
            token = response.body().getToken().getToken();
        }
    }

    public void login(Login login) throws IOException {
        Response<LoginResponse> response = apiClient.login(login).execute();
        if(response.isSuccessful()) {
            token = response.body().getToken().getToken();
        }
    }

    public void logout() {
        try {
            apiClient.logout(token).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        token = null;
    }


    public String getToken() {
        return token;
    }
}
