package com.sda.md.tictactoe;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by RENT on 2017-02-04.
 */

public class RegisterAsyncTask extends AsyncTask<Register, Void, Boolean> {

    private final User user;
    private final TokenListener listener;

    public RegisterAsyncTask(TokenListener listener) {
        this.listener = listener;
        user = User.getInstance();
    }

    @Override
    protected Boolean doInBackground(Register... params) {
        try {
            user.register(params[0]);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }    }

    @Override
    protected void onPostExecute(Boolean result) {
        if(result) {
            listener.onLoginSuccess();
        } else {
            listener.onLoginFailed();
        }    }

}
