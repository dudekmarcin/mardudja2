package com.sda.md.tictactoe;

/**
 * Created by md on 2/6/17.
 */
public interface TokenListener {
    public void onLoginSuccess();
    public void onLoginFailed();
}
