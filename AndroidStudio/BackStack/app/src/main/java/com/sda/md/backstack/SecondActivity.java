package com.sda.md.backstack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecondActivity extends AppCompatActivity {

    @BindView(R.id.sec_label)
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);
        text.setText(SimpleDateFormat.getDateTimeInstance().format(new Date()));

    }

    @OnClick(R.id.sec_button)
    public void startFirstActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }
}
