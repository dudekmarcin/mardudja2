package com.sda.md.openbinfilewithfragment_07;

import android.os.Environment;
import android.util.Log;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-02-21.
 */


public class FileManager {

        public static final FileManager instance = new FileManager();
        private static final String FOLDERNAME = "axis";
        private DataInputStream binWriter;
        private File containingFolder;

        private FileManager() {
        }


        public void openInputStreams() {
            containingFolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + FOLDERNAME);
            if (!containingFolder.exists()) {
            }
        }

        public List<String> listDir() {
            List<String> filenames = new ArrayList<>();
            File[] files = containingFolder.listFiles();
            for (File f : files) {
                filenames.add(f.getName());
            }

            return filenames;
        }

        public List<String> readFile(String filename) {
            List<String> axis = new ArrayList<>();
            try {
                FileInputStream fis = new FileInputStream(Environment.getExternalStorageDirectory().getAbsolutePath()
                        + "/" + FOLDERNAME + "/" + filename);
                binWriter = new DataInputStream(fis);
                Float line = binWriter.readFloat();
                while(line != null) {
                    float[] temp = new float[3];
                    temp[0] = line;
                    line = binWriter.readFloat();
                    temp[1] = line;
                    line = binWriter.readFloat();
                    temp[2] = line;
                    axis.add("x: " + temp[0] + ", y: " + temp[1] + ", z: " + temp[2]);

                    line = binWriter.readFloat();
                    Log.e("!!! line form file", line.toString());
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeInputStreams();
            }

            return axis;
        }



        public void closeInputStreams() {
            try {
                binWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

}
