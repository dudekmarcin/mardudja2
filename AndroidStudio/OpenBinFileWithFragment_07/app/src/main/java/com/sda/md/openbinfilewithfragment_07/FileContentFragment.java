package com.sda.md.openbinfilewithfragment_07;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FileContentFragment extends Fragment {

    private ListView listView;
    private TextView label;

    public FileContentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_file_content, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView = (ListView) getView().findViewById(R.id.file_list_view);
        label = (TextView) getView().findViewById(R.id.file_label);
        if(getArguments() != null) {
            String filename = getArguments().getString("filename");
            List<String> axis = FileManager.instance.readFile(filename);
            label.setText(filename);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, axis);
            listView.setAdapter(adapter);
        }


    }
}
