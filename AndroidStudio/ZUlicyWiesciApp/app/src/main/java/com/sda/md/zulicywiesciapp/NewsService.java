package com.sda.md.zulicywiesciapp;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by RENT on 2017-03-04.
 */

public interface NewsService {

    @GET("/articles")
    Call<NewsList> getBookList(@QueryMap Map<String, String> options);


}
