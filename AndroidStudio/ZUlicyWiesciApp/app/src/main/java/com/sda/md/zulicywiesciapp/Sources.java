package com.sda.md.zulicywiesciapp;

/**
 * Created by RENT on 2017-03-04.
 */

public enum Sources {

    CNN("cnn");

    private String name;
    private Sources (String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
