package com.sda.md.zulicywiesciapp;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-03-04.
 */

public class ApiOptions {

    private static final String apiKey = "5a0545c03e2946b99c7250931882f0d0";

    private Map<String, String> options;

    public ApiOptions() {
        options = new HashMap<>();
        options.put("apiKey", apiKey);
        options.put("source", Sources.CNN.getName());
    }

    public void setSource(Sources source) {
        options.put("source", source.getName());
    }

    public void AddOption(String key, String value) {
        options.put(key, value);
    }
}
