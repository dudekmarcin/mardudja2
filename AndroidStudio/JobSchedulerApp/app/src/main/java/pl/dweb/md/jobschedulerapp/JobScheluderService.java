package pl.dweb.md.jobschedulerapp;

import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.widget.Toast;

public class JobScheluderService extends JobService {

    private Handler mJobHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage( Message msg ) {
            Toast.makeText( getApplicationContext(),
                    "JobService task running", Toast.LENGTH_SHORT )
                    .show();
            jobFinished( (JobParameters) msg.obj, false );
            return true;
        }

    } );

    public JobScheluderService() {
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        SystemClock.sleep(10000);

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }


}
