package com.sda.md.masterdetailapp;


import android.os.Bundle;
import android.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class DeatilsFragment extends Fragment {

    private TextView showInfo;

    public DeatilsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_deatils, container, false);
        showInfo = (TextView) view.findViewById(R.id.show_text);
        return view;
    }

    public void showBaseInfo() {
        showInfo.setText("Something base info");
    }

    public void showDetails() {
        showInfo.setText("Something details");
    }
}
