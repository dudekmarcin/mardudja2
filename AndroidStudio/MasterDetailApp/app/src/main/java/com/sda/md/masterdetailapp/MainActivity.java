package com.sda.md.masterdetailapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MasterFragment.MDActivity {

    private DeatilsFragment detailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        detailsFragment = (DeatilsFragment) getFragmentManager().findFragmentById(R.id.details_fragment);
    }

    public void showBaseInfo() {
        detailsFragment.showBaseInfo();
    }

    public void showDetails() {
        detailsFragment.showDetails();
    }
}
