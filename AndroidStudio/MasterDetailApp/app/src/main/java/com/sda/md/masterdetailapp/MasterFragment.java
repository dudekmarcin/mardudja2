package com.sda.md.masterdetailapp;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class MasterFragment extends Fragment {


    public MasterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_master, container, false);

        final MDActivity mainActivity = (MDActivity) getActivity();

        Button baseInfo = (Button) view.findViewById(R.id.base_info);
        Button details = (Button) view.findViewById(R.id.details);

        baseInfo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mainActivity.showBaseInfo();
            }
        });

        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.showDetails();
            }
        });

        return view;
    }

    public interface MDActivity  {

        public void showDetails();
        public void showBaseInfo();

    }

}
