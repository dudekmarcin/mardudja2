package com.sda.md.countrysim;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by RENT on 2017-01-21.
 */
@RunWith(MockitoJUnitRunner.class)
public class ArmyTest {

    @InjectMocks
    private Army army;

    @Mock
    private Population population;

    @Test
    public void countCostTest() {
        Mockito.when(population.getCount()).thenReturn(100);
        assert army.countCost() == 10 * 100;
    }

}
