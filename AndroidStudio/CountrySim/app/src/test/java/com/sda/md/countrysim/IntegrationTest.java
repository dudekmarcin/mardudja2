package com.sda.md.countrysim;

import org.junit.Test;

/**
 * Created by RENT on 2017-01-19.
 */

public class IntegrationTest {

    @Test
    public void test() {
        Population population = new Population(1000);
        Army army = new Army(population);
        Building building = new Building();
        Economy economy = new Economy(building, army, population);
        State state = new State(economy);
        System.out.println(state.getMoney());
        state.countBalance();
        System.out.println(state.getMoney());

    }
}
