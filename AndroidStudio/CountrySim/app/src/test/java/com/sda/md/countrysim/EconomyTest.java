package com.sda.md.countrysim;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EconomyTest {

    @InjectMocks
    Economy economy;

    @Mock
    Building building;
    @Mock
    Army army;
    @Mock
    Population population;

    @Test
    public void countCost() {
        Mockito.when(building.countCost()).thenReturn(100);
        Mockito.when(army.countCost()).thenReturn(50);
        assert economy.countCost() == 150;
    }

    @Test
    public void countGainTest() {
        Mockito.when(population.countTaxes()).thenReturn(1000);
        Mockito.when(building.countProduction()).thenReturn(1000);
        assert economy.countGain() == 2000;
    }
}
