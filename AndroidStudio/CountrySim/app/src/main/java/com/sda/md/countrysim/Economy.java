package com.sda.md.countrysim;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 Udostępnia metydy countCost() i countGain() liczące koszty i przychody na

 podstawie wartości zwracanych przez klasy Army, Buildings i Population.
 */
@Singleton
public class Economy {

    private Building building;
    private Army army;
    private Population population;

    @Inject
    public Economy(Building building, Army army, Population population) {
        this.building = building;
        this.army = army;
        this.population = population;
    }

    public int countCost() {
        return building.countCost() + army.countCost();
    }

    public int countGain() {
        return population.countTaxes() + building.countProduction();
    }
}
