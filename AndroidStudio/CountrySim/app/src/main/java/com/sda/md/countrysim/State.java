package com.sda.md.countrysim;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 Przechowuje stan skarbca

 o Udostępnia metodę countBalance(), która odejmuje od skarbca koszty, a

 dodaje przychody zwracane przed klasę economy
 */
@Singleton
public class State {

    private int money = 1000;
    private Economy economy;

    @Inject
    public State(Economy economy) {
        this.economy = economy;
    }

    public void countBalance() {
        money -= economy.countCost();
        money += economy.countGain();
    }

    public int getMoney() {
        return money;
    }

}
