package com.sda.md.countrysim;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 Generuje przychów (podatki) – countTaxes(), zwraca ilość ludności

 getCount()

 */
@Singleton
public class Population {

    private int count;

    @Inject
    public Population() {}

    public Population(int count) {
        this.count = count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int countTaxes() {
        return count * 100;
    }

    public int getCount() {
        return count;
    }
}
