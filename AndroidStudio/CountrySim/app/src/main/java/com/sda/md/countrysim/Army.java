package com.sda.md.countrysim;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 Udostępnia metodę countCost() obliczającą koszt utrzymania armi

 (costPerSoldier*percentOfActiveSorldiers*population.getCount()) */
@Singleton
public class Army {

    private Population population;
    private int costPerSoldier = 100;
    private double percentOfActiveSorldiers = 0.1;

    @Inject
    public Army(Population population) {
        this.population = population;
    }

    public int countCost() {
        return  (int) (costPerSoldier * percentOfActiveSorldiers * population.getCount());
    }
}
