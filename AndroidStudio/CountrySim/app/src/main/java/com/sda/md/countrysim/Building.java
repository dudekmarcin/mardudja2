package com.sda.md.countrysim;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 Generuje koszt i przychód – countCost(), countProduction()
 */
@Singleton
public class Building {

    @Inject
    public Building() {
    }

    public int countCost() {
        return 100;
    }

    public int countProduction() {
        return 1000;
    }

}
