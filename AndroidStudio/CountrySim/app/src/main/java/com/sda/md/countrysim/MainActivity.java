package com.sda.md.countrysim;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private TextView money;
    private Button countBtn;
    private State state;
    private  EconomyComponent economyComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        money = (TextView) findViewById(R.id.money);
        countBtn = (Button) findViewById(R.id.count_btn);

        economyComponent = DaggerEconomyComponent.create();
        Population population = economyComponent.population();
        population.setCount(1000);
        state = economyComponent.state();

        money.setText(String.valueOf(state.getMoney()));

        countBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                state.countBalance();
                money.setText(String.valueOf(state.getMoney()));
            }
        });

    }
}
