package com.sda.md.masterdetailform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetailActivity extends AppCompatActivity {

    public static final String PAGE = "page";
    public static final int DEPOSIT = 0;
    public static final int CALCULATOR = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        int page = getIntent().getIntExtra(PAGE, DEPOSIT);
        switch (page) {
            case DEPOSIT: formularzLokaty(); break;
            case CALCULATOR: formularzWalut(); break;
        }
    }

    public void formularzLokaty() {
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, new InvestmentFragment());
        transaction.commit();
    }

    public void formularzWalut() {
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, new CurrencyCalcFragment());
        transaction.commit();
    }
}
