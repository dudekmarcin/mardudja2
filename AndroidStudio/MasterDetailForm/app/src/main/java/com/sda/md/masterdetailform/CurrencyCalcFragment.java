package com.sda.md.masterdetailform;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import static android.widget.AdapterView.*;


/**
 * A simple {@link Fragment} subclass.
 */
public class CurrencyCalcFragment extends Fragment {

    private CurrencyCalcLogic currencyCalcLogic = new CurrencyCalcLogic();

    public CurrencyCalcFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_currency_calc, container, false);
        EditText amount = (EditText) view.findViewById(R.id.amount);
        Spinner baseCurrency = (Spinner) view.findViewById(R.id.base_currency);
        Spinner targetCurrency = (Spinner) view.findViewById(R.id.target_currency);

//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, currencyCalcLogic.getCurrencies());
//        baseCurrency.setAdapter(adapter);
//        baseCurrency.setOnItemClickListener(new OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        targetCurrency.setAdapter(adapter);

        return view;
    }

}
