package com.sda.md.masterdetailform;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void formularzLokaty() {
        if(findViewById(R.id.frame_layout) != null) {
            android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, new InvestmentFragment());
            transaction.commit();
        } else {
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra(DetailActivity.PAGE, DetailActivity.DEPOSIT);
            startActivity(intent);
        }

    }

    public void formularzWalut() {
        if(findViewById(R.id.frame_layout) != null) {
            android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, new CurrencyCalcFragment());
            transaction.commit();
        } else {
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra(DetailActivity.PAGE, DetailActivity.CALCULATOR);
            startActivity(intent);
        }
    }
}
