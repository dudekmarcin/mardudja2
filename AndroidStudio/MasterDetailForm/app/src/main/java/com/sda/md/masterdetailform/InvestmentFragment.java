package com.sda.md.masterdetailform;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class InvestmentFragment extends Fragment {

    public InvestmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_investment, container, false);
        final EditText valueField = (EditText) view.findViewById(R.id.valueField);
        final EditText lengthField = (EditText) view.findViewById(R.id.investment_length);
        final EditText percentField = (EditText) view.findViewById(R.id.investment_percent);
        final TextView result = (TextView) view.findViewById(R.id.pokaz_wynik);
        Button actionButton = (Button) view.findViewById(R.id.calc_button);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    double value = Double.parseDouble(String.valueOf(valueField.getText()));
                    double length = Double.parseDouble(String.valueOf(lengthField.getText()));
                    double percent = Double.parseDouble(String.valueOf(percentField.getText()));
                    DepositLogic dl = new DepositLogic(value, length, percent);
                    result.setText(String.valueOf(dl.calcDeposit()));
                } catch(NumberFormatException e) {
                    result.setText("Błędne dane");
                }
            }
        });

        return view;
    }

}
