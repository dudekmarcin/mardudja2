package com.sda.md.masterdetailform;

/**
 * Created by RENT on 2017-01-12.
 */

public enum Currency {
    PLN(1.0),
    USD(3.25),
    EUR(4.31);

    private final double rate;
    private Currency(double rate) {
        this.rate = rate;
    }

    public double getRate() {
        return this.rate;
    }

}
