package com.sda.md.masterdetailform;

/**
 * Created by RENT on 2017-01-12.
 */


public class DepositLogic {

    private double kwota;
    private double okres;
    private double procent;

    public DepositLogic(double kwota, double okres, double procent) {
        this.kwota = kwota;
        this.okres = okres;
        this.procent = procent;
    }

    public double calcDeposit() {
        return (kwota * Math.pow((1 + procent/100), okres)) - kwota;
    }


}
