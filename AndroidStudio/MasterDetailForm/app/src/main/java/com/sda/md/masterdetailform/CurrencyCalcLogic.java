package com.sda.md.masterdetailform;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-01-12.
 */

public class CurrencyCalcLogic {

    List<Currency> currencies;

    public CurrencyCalcLogic() {
        currencies = new ArrayList<>();
        currencies.add(Currency.PLN);
        currencies.add(Currency.EUR);
        currencies.add(Currency.USD);
    }

    public List<Currency> getCurrencies() {
        return this.currencies;
    }

    public double[] calc(int base, int target, double amount) {

        double baseRate = this.currencies.get(base).getRate();
        double targetRate = this.currencies.get(target).getRate();
        double finalRate = (baseRate > targetRate) ? baseRate - targetRate : targetRate - baseRate;
        double[] result = new double[2];
        result[0] = finalRate * amount;
        result[1] = finalRate;
        return result;
    }

}
