package com.sda.md.masterdetailform;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class MasterFragment extends Fragment {


    public MasterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_master, container, false);

        final MainActivity activity = (MainActivity) getActivity();

        Button lokataButton = (Button) view.findViewById(R.id.lokata);
        Button walutyButton = (Button) view.findViewById(R.id.waluty);
        lokataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.formularzLokaty();
            }
        });

        walutyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.formularzWalut();
            }
        });

        return view;
    }

}
