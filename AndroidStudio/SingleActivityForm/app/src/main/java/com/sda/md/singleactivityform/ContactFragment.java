package com.sda.md.singleactivityform;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment {

    private EditText mail;
    private EditText phone;

    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        final MainActivity activity = (MainActivity) getActivity();
        mail = (EditText) view.findViewById(R.id.contact_mail);
        phone = (EditText) view.findViewById(R.id.contact_phone);
        Button next = (Button) view.findViewById(R.id.contact_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(contactValidation()) {
                    android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(activity.frame.getId(), new PasswordFragment());
                    transaction.commit();
                }

            }
        });
        return view;
    }

    private boolean contactValidation() {
        boolean isOK = true;

        if(mail.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś maila", Toast.LENGTH_SHORT).show();
        } else {
            String[] validateMail = mail.getText().toString().split("@");
            if(validateMail.length != 2 || validateMail[0].isEmpty() || validateMail[1].isEmpty()) {
               isOK = false;
                Toast.makeText(getContext(), "Błędny adres email", Toast.LENGTH_SHORT).show();

            }
        }

        if(phone.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś telefonu", Toast.LENGTH_SHORT).show();
        } else {
            try {
                if(phone.getText().toString().length() != 9) {
                    throw  new NumberFormatException();
                }
                int validatePhone = Integer.parseInt(phone.getText().toString());
            } catch (NumberFormatException e) {
                isOK = false;
                Toast.makeText(getContext(), "Niaprawidłowy format numeru", Toast.LENGTH_SHORT).show();
            }
        }



        return isOK;
    }

}
