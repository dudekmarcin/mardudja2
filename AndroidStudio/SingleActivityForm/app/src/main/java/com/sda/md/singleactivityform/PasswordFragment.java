package com.sda.md.singleactivityform;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordFragment extends Fragment {


    private EditText pass;
    private EditText confirmPass;

    public PasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_password, container, false);
        final MainActivity activity = (MainActivity) getActivity();
        pass = (EditText) view.findViewById(R.id.password_pass);
        confirmPass = (EditText) view.findViewById(R.id.password_confirm);
        Button next = (Button) view.findViewById(R.id.password_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(passValidate()) {
                    android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(activity.frame.getId(), new OptionFragment());
                    transaction.commit();
                }
            }
        });
        return view;
    }

    private boolean passValidate() {
        boolean isOK = true;

        if(pass.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś hasła", Toast.LENGTH_SHORT).show();
        } else if(confirmPass.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Potwierdź hasło", Toast.LENGTH_SHORT).show();
        } else if(!confirmPass.getText().toString().equals(pass.getText().toString())) {
            isOK = false;
            Toast.makeText(getContext(), "Podane hasła są różne", Toast.LENGTH_SHORT).show();
        }

        return isOK;
    }

}
