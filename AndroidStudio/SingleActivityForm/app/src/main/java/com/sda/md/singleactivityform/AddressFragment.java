package com.sda.md.singleactivityform;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddressFragment extends Fragment {

    private EditText street;
    private EditText housenumber;
    private EditText zipcode;
    private EditText city;
    private EditText country;

    public AddressFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_address, container, false);
        final MainActivity activity = (MainActivity) getActivity();
        street = (EditText) view.findViewById(R.id.address_street);
        housenumber = (EditText) view.findViewById(R.id.address_housenumber);
        zipcode = (EditText) view.findViewById(R.id.address_zipcode);
        city = (EditText) view.findViewById(R.id.address_city);
        country = (EditText) view.findViewById(R.id.address_country);
        Button next = (Button) view.findViewById(R.id.address_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(addressValidation()) {
                    android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(activity.frame.getId(), new ContactFragment());
                    transaction.commit();
                }
            }
        });
        return view;
    }

    private boolean addressValidation() {
        boolean isOK = true;

        if(street.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś ulicy", Toast.LENGTH_SHORT).show();
        }

        if(housenumber.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś numeru domu", Toast.LENGTH_SHORT).show();
        }

        if(zipcode.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś kodu pocztowego", Toast.LENGTH_SHORT).show();
        } else {


        }

        if(city.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś miasta", Toast.LENGTH_SHORT).show();
        }

        if(country.getText().toString().isEmpty()) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś kraju", Toast.LENGTH_SHORT).show();
        }

        return isOK;
    }

}
