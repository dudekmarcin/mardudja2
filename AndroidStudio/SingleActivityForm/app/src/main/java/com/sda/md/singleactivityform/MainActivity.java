package com.sda.md.singleactivityform;

import android.app.Activity;
import android.os.Bundle;
import android.widget.FrameLayout;

public class MainActivity extends Activity {

//    Utwórz ekran zawierający formularz rejestracji nowego użytkownika.
//    Formularz powinien być podzielony na sekcje:
//    Dane użytkownika: imię, nazwisko, data urodzenia, płeć
//    Adres: ulica i numer domu, kod pocztowy, miasto, kraj
//    Dane kontaktowe: email, numer telefonu
//    Hasło: hasło, potwierdzenie hasła
//    Opcje: Pole umożliwiające zapisanie się do newslettera
//    Wszystkie pola są obowiązkowe do wypełnienia. Po kliknięciu przycisku OK, dane powinny zostać sprawdzone. Po
//    poprawnej rejestracji, należy wyświetlić ekran logowania oraz wyświetlić informację, że konto zostało utworzone.
//    Zadanie dodatkowe:
//    Zmodyfikuj layout ekranu logowania tak, aby korzystał z RelativeLayout

    public FrameLayout frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frame = (FrameLayout) findViewById(R.id.frame_layout);
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(frame.getId(), new UserDataFragment());
        transaction.commit();
    }
}
