package com.sda.md.singleactivityform;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserDataFragment extends Fragment {

    EditText userName;
    EditText userLastname;
    MyDatePicker userBirth;
    RadioGroup sex;

    public UserDataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_data, container, false);
        final MainActivity activity = (MainActivity) getActivity();
        userName = (EditText) view.findViewById(R.id.user_name);
        userLastname = (EditText) view.findViewById(R.id.user_lastname);
        userBirth = (MyDatePicker) view.findViewById(R.id.date_picker);
        sex = (RadioGroup) view.findViewById(R.id.user_sex);
        Button next = (Button) view.findViewById(R.id.user_data_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(userDataValidation()) {
                    android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(activity.frame.getId(), new AddressFragment());
                    transaction.commit();
                }
            }
        });
        return view;
    }

    private boolean userDataValidation() {
        boolean isOK = true;

        if(userName.getText().toString().equals("")) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś imienia", Toast.LENGTH_SHORT).show();
        }

        if(userLastname.getText().toString().equals("")) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś nazwiska", Toast.LENGTH_SHORT).show();
        }

        if(userBirth.getCalendar().getTime().equals(Calendar.getInstance())) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wpisałeś daty urodzenia", Toast.LENGTH_SHORT).show();
        } else if(userBirth.getCalendar().getTime().after(Calendar.getInstance().getTime())) {
            isOK = false;
            Toast.makeText(getContext(), "Błędna data", Toast.LENGTH_SHORT).show();
        }

        if(sex.getCheckedRadioButtonId() == -1) {
            isOK = false;
            Toast.makeText(getContext(), "Nie wybrałeś płci", Toast.LENGTH_SHORT).show();
        }

        return isOK;
    }

}
