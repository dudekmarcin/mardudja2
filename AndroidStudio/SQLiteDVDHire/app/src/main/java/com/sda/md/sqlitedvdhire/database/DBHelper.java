package com.sda.md.sqlitedvdhire.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sda.md.sqlitedvdhire.model.CategoryEntity;
import com.sda.md.sqlitedvdhire.model.DVDEntity;

import java.sql.SQLException;

/**
 * Created by RENT on 2017-02-13.
 */

public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DB_NAME = "hire_db.db";
    private static final int DB_VERSION = 9;
    private Dao<DVDEntity, Long> dvdDao;
    private Dao<CategoryEntity, Long> categoryDao;
    private static final String TAG = DBHelper.class.getSimpleName();


    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, DVDEntity.class);
            TableUtils.createTable(connectionSource, CategoryEntity.class);
            Log.e(TAG, "creating table");

            createCategory();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createCategory() throws SQLException {
        categoryDao.create(new CategoryEntity("Akcja"));
        categoryDao.create(new CategoryEntity("S-F"));
        categoryDao.create(new CategoryEntity("Dramat"));
        categoryDao.create(new CategoryEntity("Komedia"));
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, DVDEntity.class, true);
            TableUtils.dropTable(connectionSource, CategoryEntity.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        onCreate(database, connectionSource);
    }

    public Dao<DVDEntity, Long> getDVDDao () throws SQLException {
        if(dvdDao == null) {
            dvdDao = getDao(DVDEntity.class);
        }
        return dvdDao;
    }

    public Dao<CategoryEntity, Long> getCategoryDao () throws SQLException {
        if(categoryDao == null) {
            categoryDao = getDao(CategoryEntity.class);
        }
        return categoryDao;
    }
}
