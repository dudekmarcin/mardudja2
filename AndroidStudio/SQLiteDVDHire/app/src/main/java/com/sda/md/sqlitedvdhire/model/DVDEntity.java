package com.sda.md.sqlitedvdhire.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by RENT on 2017-02-13.
 */

@DatabaseTable(tableName="dvds")
public class DVDEntity {

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField
    private String title;

    @DatabaseField
    private Integer amount;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "category_id")
    private CategoryEntity category;

    public DVDEntity(String title, Integer amount) {
        this.title = title;
        this.amount = amount;
    }

    public DVDEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return id + ". " + title + "\n" + "ilość: " + amount;
    }
}
