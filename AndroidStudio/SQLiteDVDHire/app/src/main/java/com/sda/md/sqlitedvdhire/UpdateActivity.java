package com.sda.md.sqlitedvdhire;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sda.md.sqlitedvdhire.database.DBHelper;
import com.sda.md.sqlitedvdhire.model.DVDEntity;

import java.sql.SQLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateActivity extends AppCompatActivity {

    @BindView(R.id.update_title)
    EditText dvdTitle;

    @BindView(R.id.update_amount)
    EditText dvdAmount;

    @BindView(R.id.updatE_id)
    TextView dvdId;

    Long id;

    Dao<DVDEntity, Long> dao;
    DVDEntity dvd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        ButterKnife.bind(this);
        id = getIntent().getLongExtra("dvd_update_id", 0);
        try {
            DBHelper dbHelper = OpenHelperManager.getHelper(this, DBHelper.class);
            dao = dbHelper.getDVDDao();
            Log.e("DAO:", dao.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Log.e("INTENT ID: ", id.toString());

        try {
            dvd = dao.queryForId(id);
            dvdId.setText("ID: " + dvd.getId().toString());
            dvdTitle.setText(dvd.getTitle());
            dvdAmount.setText(dvd.getAmount().toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.save_button)
    public void saveChanges() {
        dvd.setTitle(dvdTitle.getText().toString());
        dvd.setAmount(Integer.parseInt(dvdAmount.getText().toString()));
        try {
            dao.update(dvd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        setResult(Activity.RESULT_OK);
        finish();
    }
}
