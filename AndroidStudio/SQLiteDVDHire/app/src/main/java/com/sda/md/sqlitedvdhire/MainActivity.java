package com.sda.md.sqlitedvdhire;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sda.md.sqlitedvdhire.database.DBHelper;
import com.sda.md.sqlitedvdhire.model.CategoryEntity;
import com.sda.md.sqlitedvdhire.model.DVDEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.add_title)
    TextView title;
    @BindView(R.id.add_amount)
    TextView amount;
    @BindView(R.id.list_view)
    ListView listView;
    @BindView(R.id.category_spinner)
    Spinner spinner;
    Dao<DVDEntity, Long> dao;
    Dao<CategoryEntity, Long> categoryDao;
    HireAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        try {
            DBHelper dbHelper = OpenHelperManager.getHelper(this, DBHelper.class);
            dao = dbHelper.getDVDDao();
            categoryDao = dbHelper.getCategoryDao();
            Log.e("catDAO: ", categoryDao.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        adapter = new HireAdapter();
        listView.setAdapter(adapter);
        final Intent intent = new Intent(this, DetailsActivity.class);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                intent.putExtra("dvd_id", l);
                startActivityForResult(intent,2);
            }
        });

        List<CategoryEntity> categories = getCategories();
        SpinnerAdapter catAdapter = new ArrayAdapter<CategoryEntity>(this, android.R.layout.simple_spinner_item, categories);
        spinner.setAdapter(catAdapter);

    }

    private List<CategoryEntity> getCategories() {
        try {
            return categoryDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @OnClick(R.id.add_button)
    public void addDVD() {
        CategoryEntity selectedCat = (CategoryEntity) spinner.getSelectedItem();
        DVDEntity dvd = new DVDEntity(title.getText().toString(), Integer.parseInt(amount.getText().toString()));
        dvd.setCategory(selectedCat);
        try {
            dao.create(dvd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        adapter.setData(getData());
        title.setText("");
        amount.setText("");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 2) {
            if(resultCode == Activity.RESULT_OK) {
                adapter.setData(getData());

            }
        }
    }

    private List<DVDEntity> getData() {
        try {
            return dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    class HireAdapter extends BaseAdapter {

        private List<DVDEntity> dvds = new ArrayList<>();

        public HireAdapter() {
            dvds = getData();
        }

        @Override
        public int getCount() {
            return dvds.size();
        }

        @Override
        public Object getItem(int i) {
            return dvds.get(i);
        }

        @Override
        public long getItemId(int i) {
            return dvds.get(i).getId();
        }


        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView;
            if (view != null) {
                textView = (TextView) view;
            } else {
                textView = new TextView(MainActivity.this);
            }
            textView.setText(dvds.get(i).getId().toString() + ". " + dvds.get(i).getTitle());
            return textView;
        }

        public void setData(List<DVDEntity> dvds) {
            this.dvds = dvds;
            notifyDataSetChanged();
        }

    }

}
