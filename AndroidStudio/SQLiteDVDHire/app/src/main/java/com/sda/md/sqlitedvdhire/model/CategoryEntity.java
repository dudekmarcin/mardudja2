package com.sda.md.sqlitedvdhire.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by RENT on 2017-02-14.
 */

@DatabaseTable(tableName = "categories")
public class CategoryEntity {

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField
    private String name;

    @ForeignCollectionField
    private Collection<DVDEntity> dvds;

    public CategoryEntity() {}

    public CategoryEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<DVDEntity> getDvds() {
        return dvds;
    }

    public void setDvds(Collection<DVDEntity> dvds) {
        this.dvds = dvds;
    }

    @Override
    public String toString() {
        return name;
    }
}
