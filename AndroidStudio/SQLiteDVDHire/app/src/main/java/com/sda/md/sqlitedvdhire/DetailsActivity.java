package com.sda.md.sqlitedvdhire;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sda.md.sqlitedvdhire.database.DBHelper;
import com.sda.md.sqlitedvdhire.model.DVDEntity;

import java.sql.SQLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.dvd_id)
    TextView dvdId;

    @BindView(R.id.dvd_title)
    TextView dvdTitle;

    @BindView(R.id.dvd_content)
    TextView dvdContent;

    @BindView(R.id.dvd_category)
    TextView dvdCategory;

    Dao<DVDEntity, Long> dao;
    private Long id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        try {
            DBHelper dbHelper = OpenHelperManager.getHelper(this, DBHelper.class);
            dao = dbHelper.getDVDDao();
            Log.e("DAO:", dao.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        id = getIntent().getLongExtra("dvd_id", 0);
        Log.e("INTENT ID: ", id.toString());
        getData();


    }

    private void getData() {
        DVDEntity dvd;
        try {
            dvd = dao.queryForId(id);
            dvdId.setText("ID: " + dvd.getId().toString());
            dvdTitle.setText("Tytuł: " + dvd.getTitle());
            dvdContent.setText("Ilość: " + dvd.getAmount().toString());
            dvdCategory.setText("Kategoria: " + dvd.getCategory().getName());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.del_button)
    public void delEntity() {
        try {
            dao.deleteById(id);
            startActivity(new Intent(this, MainActivity.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.return_btn)
    public void backToList() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    @OnClick(R.id.upd_btn)
    public void updateEntity() {
        Intent intent = new Intent(this, UpdateActivity.class);
        intent.putExtra("dvd_update_id", id);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1) {
            if(resultCode == Activity.RESULT_OK) {
                getData();
            }
        }
    }
}
