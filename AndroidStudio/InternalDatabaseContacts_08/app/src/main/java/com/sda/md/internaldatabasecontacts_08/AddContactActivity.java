package com.sda.md.internaldatabasecontacts_08;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sda.md.internaldatabasecontacts_08.database.DBHelper;
import com.sda.md.internaldatabasecontacts_08.model.Contact;

import java.sql.SQLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddContactActivity extends AppCompatActivity {

    @BindView(R.id.name)
    EditText nameField;

    @BindView(R.id.lastname)
    EditText lastnameField;

    @BindView(R.id.phone_number)
    EditText numberField;

    private Dao<Contact, Long> dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        ButterKnife.bind(this);

        try {
            DBHelper dbHelper = OpenHelperManager.getHelper(this, DBHelper.class);
            dao = dbHelper.getContactDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(getIntent().hasExtra("CONTACT_ID")) {
            try {
                Contact contact  = dao.queryForId(getIntent().getLongExtra("CONTACT_ID", 0));
                nameField.setText(contact.getName());
                lastnameField.setText(contact.getLastname());
                numberField.setText(contact.getNumber());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.save_button)
    public void saveToDB() {
        String name = nameField.getText().toString();
        String lastname = lastnameField.getText().toString();
        String number = numberField.getText().toString();

        Contact contact = new Contact(name, lastname, number);

        if(getIntent().hasExtra("CONTACT_ID")) {
            contact.setId(getIntent().getLongExtra("CONTACT_ID", 0));
        }

        try {
            if(getIntent().hasExtra("CONTACT_ID")) {
                dao.update(contact);
            } else {
                dao.create(contact);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finish();
    }
}
