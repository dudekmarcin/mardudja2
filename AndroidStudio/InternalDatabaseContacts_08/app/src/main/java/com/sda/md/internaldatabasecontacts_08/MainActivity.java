package com.sda.md.internaldatabasecontacts_08;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sda.md.internaldatabasecontacts_08.database.DBHelper;
import com.sda.md.internaldatabasecontacts_08.model.Contact;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.contact_listview)
    ListView listView;

    Dao<Contact, Long> dao;
    private List<Contact> contacts;
    private ArrayAdapter<Contact> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        contacts = new ArrayList<>();

        try {
            DBHelper dbHelper = OpenHelperManager.getHelper(this, DBHelper.class);
            dao = dbHelper.getContactDao();
            contacts = dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, contacts);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, AddContactActivity.class);
                intent.putExtra("CONTACT_ID", adapter.getItem(i).getId());
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                PopupWindow window = createPopup(i);
                window.showAsDropDown(view, 0, 0);
                return true;            }
        });
    }

    private PopupWindow createPopup(final int position) {
        final PopupWindow window = new PopupWindow(this);

        Button newButton = new Button(this);
        newButton.setText("Delete");
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                try {
                    dao.deleteById(contacts.get(position).getId());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                contacts.remove(contacts.get(position));
                adapter.notifyDataSetChanged();
                window.dismiss();
            }
        });
        window.setFocusable(true);
        window.setContentView(newButton);
        window.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        return window;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_menu,menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add_contact) {
            Intent i = new Intent(this, AddContactActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            contacts = dao.queryForAll();
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, contacts);
            listView.setAdapter(adapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }



    }
}
