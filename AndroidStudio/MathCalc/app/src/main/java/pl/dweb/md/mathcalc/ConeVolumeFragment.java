package pl.dweb.md.mathcalc;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConeVolumeFragment extends Fragment {


    public ConeVolumeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_conevolume, container, false);
        final EditText radiusField = (EditText) view.findViewById(R.id.radius);
        final EditText heightField = (EditText) view.findViewById(R.id.height);
        Button calcButton = (Button) view.findViewById(R.id.calcButton);
        final TextView coneVolume = (TextView) view.findViewById(R.id.cone_volume);

        calcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double r = Double.parseDouble(String.valueOf(radiusField.getText()));
                    double h = Double.parseDouble(String.valueOf(heightField.getText()));
                    coneVolume.setText(Double.toString(calcVolume(r,h)));
                } catch (NumberFormatException e) {
                    coneVolume.setText("Podano błędne wartości");
                }
            }
        });
        return view;
    }

    private double calcVolume(double r, double h) {
        return (Math.PI*r*r*h)/3;
    }
}
