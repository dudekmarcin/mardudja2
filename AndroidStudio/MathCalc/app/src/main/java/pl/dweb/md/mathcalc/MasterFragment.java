package pl.dweb.md.mathcalc;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class MasterFragment extends Fragment {


    public MasterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_master, container, false);

        final MainActivity activity = (MainActivity) getActivity();

        Button triangleArea = (Button) view.findViewById(R.id.triangle_area_button);
        Button quadraticEquation = (Button) view.findViewById(R.id.quadratic_equation_button);
        Button coneVolume = (Button) view.findViewById(R.id.cane_volume_button);

        triangleArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.triangleArea();
            }
        });

        quadraticEquation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.quadraticEquation();
            }
        });

        coneVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.coneVolume();
            }
        });

        return view;
    }

}
