package pl.dweb.md.mathcalc;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class TriangleAreaFragment extends Fragment {


    public TriangleAreaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_triangle_area, container, false);
        final EditText aField = (EditText) view.findViewById(R.id.a);
        final EditText hField = (EditText) view.findViewById(R.id.height);
        final TextView triangle_area = (TextView) view.findViewById(R.id.triangle_area);
        Button calcButton = (Button) view.findViewById(R.id.calcButton);

        calcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double a = Double.parseDouble(String.valueOf(aField.getText()));
                    double h = Double.parseDouble(String.valueOf(hField.getText()));
                    triangle_area.setText(Double.toString(calcArea(a, h)));
                } catch (NumberFormatException e) {
                    triangle_area.setText("Podano błędne wartości");
                }
            }
        });

                return view;
    }

    private double calcArea(double a, double h) {
        return (a*h)/2;
    }

}
