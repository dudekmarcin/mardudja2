package pl.dweb.md.mathcalc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent  = new Intent(getApplicationContext(), DetailActivity.class);
    }

    public void triangleArea() {

        if(findViewById(R.id.frame_layout) != null) {
            android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, new TriangleAreaFragment());
            transaction.commit();
        } else {
            intent.putExtra("method", "triangleArea");
            startActivity(intent);
        }
    }

    public void quadraticEquation() {
        if(findViewById(R.id.frame_layout) != null) {
            android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, new QuadraticEquationFragment());
            transaction.commit();
        } else {
            intent.putExtra("method", "quadraticEquation");
            startActivity(intent);
        }
    }

    public void coneVolume() {
        if(findViewById(R.id.frame_layout) != null) {
           android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
           transaction.replace(R.id.frame_layout, new ConeVolumeFragment());
           transaction.commit();
    } else {
        intent.putExtra("method", "coneVolume");
        startActivity(intent);
    }
    }
}
