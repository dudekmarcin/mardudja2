package pl.dweb.md.mathcalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        if(getIntent().hasExtra("method")) {
            android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();

            switch(getIntent().getStringExtra("method")) {
                case "triangleArea": transaction.replace(R.id.frame_layout, new TriangleAreaFragment());
                    break;
                case "quadraticEquation": transaction.replace(R.id.frame_layout, new QuadraticEquationFragment());
                    break;
                case "coneVolume": transaction.replace(R.id.frame_layout, new ConeVolumeFragment());
                    break;
            }

            transaction.commit();

        }
    }
}
