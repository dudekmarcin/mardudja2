package pl.dweb.md.mathcalc;


import android.os.Bundle;
import android.app.Fragment;
import android.util.StringBuilderPrinter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuadraticEquationFragment extends Fragment {


    public QuadraticEquationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quadratic_equation, container, false);
        final EditText aField = (EditText) view.findViewById(R.id.a);
        final EditText bField = (EditText) view.findViewById(R.id.b);
        final EditText cField = (EditText) view.findViewById(R.id.c);
        final TextView x1 = (TextView) view.findViewById(R.id.x1);
        final TextView x2 = (TextView) view.findViewById(R.id.x2);
        Button calcButton = (Button) view.findViewById(R.id.calcButton);

        calcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double a = Double.parseDouble(String.valueOf(aField.getText()));
                    double b = Double.parseDouble(String.valueOf(bField.getText()));
                    double c = Double.parseDouble(String.valueOf(cField.getText()));
                    if(calcQuadraticEquation(a, b, c) != null) {
                        double[] result = calcQuadraticEquation(a, b, c);
                        x1.setText(Double.toString(result[0]));
                        if(result.length == 2) {
                            x2.setText(Double.toString(result[1]));
                        } else {
                            x2.setText("");
                        }
                    } else {
                        x1.setText("Delta ujemna");
                        x2.setText("Nie ma rozwiązania :(");

                    }

                } catch (NumberFormatException e) {
                    x1.setText("Podano błędne dane");
                }
            }
        });

        return view;
    }


    private double[] calcQuadraticEquation(double a, double b, double c) {
        double delta = b*b - 4*a*c;
        double[] result = null;

        if(delta == 0) {
            result = new double[1];
            result[0] = -b/2*a;
        } else if(delta > 0) {
            result = new double[2];
            result[0] = (-b - Math.sqrt(delta))/2*a;
            result[1] = (-b + Math.sqrt(delta))/2*a;
        }

        return result;
    }

}
