package com.sda.md.savetodotasksstorage;

import android.content.Context;
import android.widget.Toast;

import com.sda.md.savetodotasksstorage.model.Task;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-02-18.
 */

public class FileManager {

    private static final String FILENAME = "tasks.txt";
    private static final FileManager instance = new FileManager();
    private final List<Task> taskList;

    private FileManager() {
        taskList = new LinkedList<>();
    }

    public static FileManager getInstance() {
        return instance;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void save(Context context) {
        saveToFile(context, taskList);
    }

    public void load(Context context) {
        taskList.addAll(readFromFile(context));
    }

    private void saveToFile(Context context, List<Task> list) {
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            OutputStreamWriter writer = new OutputStreamWriter(fos);
            PrintWriter printWriter = new PrintWriter(writer);

            for(Task task : list) {
                printWriter.println(task.toSerializedString());
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Nie znaleziono pliku", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(context, "Błąd zapisu", Toast.LENGTH_SHORT).show();
        }
    }

    private List<Task> readFromFile(Context context) {
        List<Task> list = new LinkedList<>();
        try {
            FileInputStream fis = context.openFileInput(FILENAME);
            InputStreamReader streamReader = new InputStreamReader(fis);
            BufferedReader reader = new BufferedReader(streamReader);
            String line = reader.readLine();
            while(line != null && !line.isEmpty()) {
                list.add(new Task(line));
                line = reader.readLine();
            }
            reader.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Nie znaleziono pliku", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(context, "Błąd odczytu", Toast.LENGTH_SHORT).show();
        }
        return list;
    }
}
