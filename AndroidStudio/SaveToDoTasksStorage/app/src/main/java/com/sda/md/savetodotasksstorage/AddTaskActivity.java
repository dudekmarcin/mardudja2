package com.sda.md.savetodotasksstorage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sda.md.savetodotasksstorage.model.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sda.md.savetodotasksstorage.R.id.date;

public class AddTaskActivity extends AppCompatActivity {

    @BindView(R.id.title)
    EditText titleField;

    @BindView(R.id.content)
    EditText contentField;

    @BindView(date)
    TextView dateField;

    @BindView(R.id.save_button)
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        ButterKnife.bind(this);
        if(getIntent().hasExtra("Task")) {
            fillFields();
        } else {
            dateField.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
            checkIfEmptyFields();

            TextWatcher watcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    checkIfEmptyFields();
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            };

            titleField.addTextChangedListener(watcher);
            contentField.addTextChangedListener(watcher);

        }
    }

    private void fillFields() {
        Task task = new Task(getIntent().getStringExtra("Task"));
        titleField.setText(task.getTitle());
        dateField.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(task.getDate()));
        contentField.setText(task.getContent());
        titleField.setFocusable(false);
        contentField.setFocusable(false);
        saveButton.setEnabled(false);
    }

    private void checkIfEmptyFields() {
        if (titleField.getText().toString().isEmpty() && contentField.getText().toString().isEmpty()) {
            saveButton.setEnabled(false);
        } else {
            saveButton.setEnabled(true);
        }
    }

    @OnClick(R.id.save_button)
    public void saveTask() {
        Task task = createTask();
        FileManager.getInstance().getTaskList().add(task);
        FileManager.getInstance().save(getApplicationContext());
        setResult(RESULT_OK);
        finish();

    }

    private Task createTask() {
        Date date = new Date();
        String title = titleField.getText().toString();
        String content = contentField.getText().toString();
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateField.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Task(title, content, date);

    }
}
