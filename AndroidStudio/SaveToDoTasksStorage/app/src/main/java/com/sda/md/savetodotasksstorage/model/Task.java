package com.sda.md.savetodotasksstorage.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RENT on 2017-02-18.
 */

public class Task {

    private String title;
    private String content;
    private Date date;
    private static final String SEPARATOR = ";";

    public Task(String title, String content, Date date) {
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public Task (String serialized) {
        String[] data = serialized.split(SEPARATOR);
        title = data[Fields.TITLE.getIndex()];
        date = new Date();
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(data[Fields.DATE.getIndex()]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        content = data[Fields.CONTENT.getIndex()];
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", date=" + date +
                '}';
    }

    public String toSerializedString() {
        return title +  SEPARATOR + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date) + SEPARATOR + content;
    }

    private enum Fields {
        TITLE(0),
        DATE(1),
        CONTENT(2);

        private int index;

        private Fields(int index) {
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

