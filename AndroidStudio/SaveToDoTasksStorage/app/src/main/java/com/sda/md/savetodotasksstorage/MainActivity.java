package com.sda.md.savetodotasksstorage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.sda.md.savetodotasksstorage.model.Task;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final int ADD_TASK_REQ = 1;

    @BindView(R.id.linear)
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FileManager.getInstance().load(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();

        linearLayout.removeAllViews();
        List<Task> tasks = FileManager.getInstance().getTaskList();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(linearLayout.getLayoutParams());
        params.topMargin = 15;
        for(final Task task : tasks) {
            Button button = new Button(this);
            button.setText(task.getTitle());
            button.setBackgroundColor(Color.LTGRAY);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, AddTaskActivity.class);
                    intent.putExtra("Task", task.toSerializedString());
                    startActivity(intent);
                }
            });
            linearLayout.addView(button, params);
        }

    }

    @OnClick(R.id.add_task_button)
    public void addTask() {
        startActivityForResult(new Intent(this, AddTaskActivity.class), ADD_TASK_REQ);
    }

}
