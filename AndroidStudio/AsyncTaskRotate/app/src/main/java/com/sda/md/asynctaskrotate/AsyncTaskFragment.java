package com.sda.md.asynctaskrotate;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;


/**
 * A simple {@link Fragment} subclass.
 */
public class AsyncTaskFragment extends Fragment implements CountingTask.CountingListener {

    private SeekBar seekBar;
    private boolean asynctaskStarted = false;
    private CountingTask countingTask;

    public AsyncTaskFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_async_task, container, false);
        seekBar = (SeekBar) view.findViewById(R.id.seekBar);

        if(!asynctaskStarted) {
            countingTask = new CountingTask(this);
            countingTask.execute();
            asynctaskStarted = true;
        }

        return view;
    }

    @Override
    public void onProgress(int value) {
        seekBar.setProgress(value);
    }
}
