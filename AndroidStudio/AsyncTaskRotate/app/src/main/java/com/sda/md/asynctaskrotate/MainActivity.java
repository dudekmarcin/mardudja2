package com.sda.md.asynctaskrotate;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private static final String ASYNC_FRAGMENT_TAG = "async_fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(ASYNC_FRAGMENT_TAG);

        if(fragment == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.activity_main, new AsyncTaskFragment(), ASYNC_FRAGMENT_TAG)
                    .commit();
        }

    }
}
