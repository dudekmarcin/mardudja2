package com.sda.md.asynctaskrotate;

import android.os.AsyncTask;

/**
 * Created by RENT on 2017-02-06.
 */

public class CountingTask extends AsyncTask<Void, Integer, Void> {

    private final CountingListener listener;

    public CountingTask(CountingListener listener) {
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            for (int i = 1; i <= 100; i++) {
                publishProgress(i);
                Thread.sleep(150);
            }
        } catch (InterruptedException e) {
                e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        listener.onProgress(values[0]);
    }

    public interface CountingListener {
        public void onProgress(int value);
    }

}
