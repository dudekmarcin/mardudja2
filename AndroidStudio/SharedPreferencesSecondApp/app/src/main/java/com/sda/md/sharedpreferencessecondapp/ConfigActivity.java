package com.sda.md.sharedpreferencessecondapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sda.md.sharedpreferencessecondapp.MainActivity.SHARED_PREFERENCES_NAME;

public class ConfigActivity extends AppCompatActivity {

    public static final String COLOR_PREF = "button_color";
    public static final String SIZE_PREF = "button_size";
    public static final String PADDING_PREF = "button_padding";
    public static final String MARGIN_PREF = "button_margin";


    @BindView(R.id.color_spinner)
    Spinner colorSpinner;
    @BindView(R.id.edit_size)
    EditText size;
    @BindView(R.id.edit_padding)
    EditText padding;
    @BindView(R.id.edit_margin)
    EditText margin;
    private SharedPreferences preferences;
    private ArrayAdapter colorsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        ButterKnife.bind(this);
        preferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        colorsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Colors.values());
        colorSpinner.setAdapter(colorsAdapter);
        loadPreferences();
    }

    @OnClick(R.id.save_button)
    public void save() {
        savePreferences();
        setResult(RESULT_OK);
        finish();
    }

    private void savePreferences() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(COLOR_PREF, String.valueOf(colorSpinner.getSelectedItem()));
        editor.putInt(SIZE_PREF, Integer.parseInt(size.getText().toString()));
        editor.putInt(PADDING_PREF, Integer.parseInt(padding.getText().toString()));
        editor.putInt(MARGIN_PREF, Integer.parseInt(margin.getText().toString()));
        editor.commit();
    }

    private void loadPreferences() {
        SharedPreferences pref = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        size.setText(String.valueOf(pref.getInt(SIZE_PREF, 12)));
        padding.setText(String.valueOf(pref.getInt(PADDING_PREF, 0)));
        margin.setText(String.valueOf(pref.getInt(MARGIN_PREF, 0)));
        String color = pref.getString(COLOR_PREF, "DEFAULT");
        int colorPosition = colorsAdapter.getPosition(Colors.valueOf(color));
        colorSpinner.setSelection(colorPosition);
    }
}
