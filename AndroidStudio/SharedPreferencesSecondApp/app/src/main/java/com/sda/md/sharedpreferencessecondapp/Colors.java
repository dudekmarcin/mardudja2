package com.sda.md.sharedpreferencessecondapp;

/**
 * Created by RENT on 2017-02-15.
 */

public enum Colors {
    DEFAULT (R.color.colorPrimary),
    GREEN (R.color.green),
    BLUE (R.color.blue),
    RED (R.color.red),
    YELLOW (R.color.yellow),
    PINK (R.color.pink),
    CYAN (R.color.cyan);

    private int color;

    private Colors(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }
}
