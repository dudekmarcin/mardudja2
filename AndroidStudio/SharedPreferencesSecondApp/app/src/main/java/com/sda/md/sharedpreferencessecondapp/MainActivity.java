package com.sda.md.sharedpreferencessecondapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.sda.md.sharedpreferencessecondapp.ConfigActivity.COLOR_PREF;
import static com.sda.md.sharedpreferencessecondapp.ConfigActivity.MARGIN_PREF;
import static com.sda.md.sharedpreferencessecondapp.ConfigActivity.PADDING_PREF;
import static com.sda.md.sharedpreferencessecondapp.ConfigActivity.SIZE_PREF;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;

    public static final int CONFIG_REQ =1;
    public static final String SHARED_PREFERENCES_NAME = "buttons.preferences";
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        preferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
    }

    public void editSettings() {
        Intent intent = new Intent(this, ConfigActivity.class);
        startActivityForResult(intent, CONFIG_REQ);
    }

    private void clearSettings() {
        preferences.edit().clear().commit();
        finish();
        startActivity(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadPreferences();
    }

    private void loadPreferences() {
        if(preferences.contains(COLOR_PREF)) {
            Colors color = Colors.valueOf(preferences.getString(COLOR_PREF, "DEFAULT"));
            int fontSize = preferences.getInt(SIZE_PREF, 10);
            int width = preferences.getInt(PADDING_PREF, 0);
            int margin = preferences.getInt(MARGIN_PREF, 0);
            setButtonStyle(button1, color, fontSize, width, margin);
            setButtonStyle(button2, color, fontSize, width, margin);
            setButtonStyle(button3, color, fontSize, width, margin);
        }
    }

    private void setButtonStyle(Button btn, Colors color, int fontSize, int width, int margin) {
        btn.setBackgroundColor(getResources().getColor(color.getColor()));
        btn.setTextSize(fontSize);
        btn.setWidth(width);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, margin, 0, margin);
       btn.setLayoutParams(params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_edit) {
            editSettings();
            return true;
        } else if(id == R.id.action_clear) {
            clearSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
