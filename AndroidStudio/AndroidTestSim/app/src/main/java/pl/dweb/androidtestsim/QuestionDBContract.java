package pl.dweb.androidtestsim;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by md on 1/19/17.
 */

public class QuestionDBContract  extends SQLiteOpenHelper {
    private static final String DEBUG_TAG = "SqLiteQuestionMgr";

        public static final String DB_NAME = "testsimsqlite";
        public static final String TABLE_NAME = "questions";
        public static String DB_PATH = "";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_CONTENT = "content";
        public static final String COLUMN_ANSA = "answerA";
        public static final String COLUMN_ANSB = "answerB";
        public static final String COLUMN_ANSC = "answerC";
        public static final String COLUMN_ANSD = "answerD";
        public static final String COLUMN_CORRECT = "ok";
        public static final String COLUMN_IMG = "img";
        public static final String COLUMN_YEAR = "rok";
        public static final String COLUMN_MONTH = "miesiac";
        public static final int DB_VERSION = 1;
       private final Context mContext;




    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
                TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CONTENT + " TEXT, " +
                COLUMN_ANSA + " TEXT, " +
                COLUMN_ANSB + " TEXT, " +
                COLUMN_ANSC + " TEXT, " +
                COLUMN_ANSD + " TEXT, " +
                COLUMN_CORRECT + " TEXT, " +
                COLUMN_IMG + " TEXT, " +
                COLUMN_YEAR + "TEXT" +
                COLUMN_MONTH + "TEXT" + ")";

    public QuestionDBContract(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        this.mContext = context;

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(db);
    }

    public void addQuestion(Question question){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_CONTENT, question.getContent());
        contentValues.put(COLUMN_ANSA, question.getAnswerA());
        contentValues.put(COLUMN_ANSB, question.getAnswerB());
        contentValues.put(COLUMN_ANSC, question.getAnswerC());
        contentValues.put(COLUMN_ANSD, question.getAnswerD());
        contentValues.put(COLUMN_CORRECT, question.getOk());
        contentValues.put(COLUMN_IMG, question.getImg());
        contentValues.put(COLUMN_YEAR, question.getYear());
        contentValues.put(COLUMN_MONTH, question.getMonth());

        db.insert(TABLE_NAME, null, contentValues);

        db.close();
    }

    public List getAllQuestions(){

        List list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()){
            do {

                Question question = new Question();

                question.setId(Integer.parseInt(cursor.getString(0)));
                question.setContent(cursor.getString(1));
                question.setAnswerA(cursor.getString(2));
                question.setAnswerB(cursor.getString(3));
                question.setAnswerC(cursor.getString(4));
                question.setAnswerD(cursor.getString(5));
                question.setOk(cursor.getString(6));
                question.setImg(cursor.getString(7));
                question.setYear(Integer.parseInt(cursor.getString(8)));
                question.setMonth(Integer.parseInt(cursor.getString(9)));

                list.add(question);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return list;
    }

    public int getQuestionCount(){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(query , null);

        int countOfQuestions = cursor.getCount();

        cursor.close();

        return countOfQuestions;
    }


    private void copyDataBase() throws IOException
    {
        InputStream mInput = mContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0)
        {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

}



