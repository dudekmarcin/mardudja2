package pl.dweb.androidtestsim;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button singleBtn;
    private Button testBtn;
    private Intent singleIntent;
    private Intent testIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        singleBtn = (Button) findViewById(R.id.single_btn);
        testBtn = (Button) findViewById(R.id.test_btn);
        singleIntent = new Intent(this, Single.class);
        testIntent = new Intent(this, Test.class);

        singleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(singleIntent);
            }
        });

        testBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(testIntent);
            }
        });


    }
}
