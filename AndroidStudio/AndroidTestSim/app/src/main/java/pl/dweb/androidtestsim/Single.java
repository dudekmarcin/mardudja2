package pl.dweb.androidtestsim;

import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Single extends AppCompatActivity {

    private TextView content;
    private RadioGroup answers;
    private Button button;
    private RadioButton answerA;
    private RadioButton answerB;
    private RadioButton answerC;
    private RadioButton answerD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);
        content = (TextView) findViewById(R.id.question_content);
        answers = (RadioGroup) findViewById(R.id.answers);
        answerA = (RadioButton) findViewById(R.id.answer_a);
        answerB = (RadioButton) findViewById(R.id.answer_b);
        answerC = (RadioButton) findViewById(R.id.answer_c);
        answerD = (RadioButton) findViewById(R.id.answer_d);
        button = (Button) findViewById(R.id.check_btn);

        final Question question = new Question();
        question.setContent("We florystyce używane są magnesy do mocowania:");
        question.setAnswerA("metalowych elementów ozdobnych");
        question.setAnswerB("korsarzy do sukni");
        question.setAnswerC("metalowych konstrukcji na ścianie");
        question.setAnswerD("metalowych naczyń");
        question.setOk("B");
        content.setText(question.getContent());
        answerA.setText(question.getAnswerA());
        answerB.setText(question.getAnswerB());
        answerC.setText(question.getAnswerC());
        answerD.setText(question.getAnswerD());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(answers.getCheckedRadioButtonId() != -1) {
                    markAnswers();
                    switch (answers.getCheckedRadioButtonId()) {
                        case R.id.answer_a:
                            setColors(answerA);
                            break;
                        case R.id.answer_b:
                            setColors(answerB);
                            break;
                        case R.id.answer_c:
                            setColors(answerC);

                            break;
                        case R.id.answer_d:
                            setColors(answerD);

                            break;
                    }
                }
            }
        });

    }

    private void setColors(RadioButton answer) {
        answer.setBackgroundColor(getResources().getColor(R.color.userAnswer));
    }

    private void markAnswers() {
        answerA.setBackgroundColor(getResources().getColor(R.color.badAnswer));
        answerC.setBackgroundColor(getResources().getColor(R.color.badAnswer));
        answerD.setBackgroundColor(getResources().getColor(R.color.badAnswer));
        answerB.setBackgroundColor(getResources().getColor(R.color.goodAnswer));

    }
}
