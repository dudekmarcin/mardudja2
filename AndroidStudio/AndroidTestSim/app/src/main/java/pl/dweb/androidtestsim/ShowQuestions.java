package pl.dweb.androidtestsim;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;



public class ShowQuestions extends AppCompatActivity {

    private ListView listView;
    private QuestionDBContract dbh;
    private List<Question> questionsList;
    private DisplayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_questions);

        listView = (ListView) findViewById(R.id.listView);

        dbh = new QuestionDBContract(this);

        questionsList = dbh.getAllQuestions();

        adapter = new DisplayAdapter(questionsList);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });


    }


    class DisplayAdapter extends BaseAdapter {

        List<Question> list;

        DisplayAdapter(List<Question> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView;
            if (view != null) {
                textView = (TextView) view;
            } else {
                textView = new TextView(ShowQuestions.this);
            }
            textView.setText(list.get(i).toString());
            return textView;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        questionsList = dbh.getAllQuestions();
        listView.setAdapter(new DisplayAdapter(questionsList));
        adapter.notifyDataSetChanged();
    }
 }

