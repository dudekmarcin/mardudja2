package com.sda.md.sharedpreferencesfirstapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditActivity extends AppCompatActivity {

    public static final String SHARED_PREFERENCES_NAME = "user.preferences";
    public static final String PREFERENCE_NAME = "KEY_NAME";
    public static final String PREFERENCE_LASTNAME = "KEY_LASTNAME";
    public static final String PREFERENCE_COUNTRY = "KEY_COUNTRY";
    public static final String PREFERENCE_LANG = "KEY_LANG";

    @BindView(R.id.edit_name)
    EditText editName;
    @BindView(R.id.edit_lastname)
    EditText editLastname;
    @BindView(R.id.country_spinner)
    Spinner countrySpinner;
    @BindView(R.id.lang_spinner)
    Spinner langSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);
        editName.setText(getIntent().getStringExtra(PREFERENCE_NAME));
        editLastname.setText(getIntent().getStringExtra(PREFERENCE_LASTNAME));

        ArrayAdapter countryAdapdter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Countries.values());
        countrySpinner.setAdapter(countryAdapdter);
        String country = getIntent().getStringExtra(PREFERENCE_COUNTRY);
        int position =countryAdapdter.getPosition(Countries.valueOf(country));
        countrySpinner.setSelection(position);

        ArrayAdapter langAdapter = new ArrayAdapter<Langs>(this, android.R.layout.simple_spinner_item, Langs.values());
        langSpinner.setAdapter(langAdapter);
        String lang = getIntent().getStringExtra(PREFERENCE_LANG);
        int langPosition = langAdapter.getPosition(Langs.valueOf(lang));
        langSpinner.setSelection(langPosition);

    }

    @OnClick(R.id.save_button)
    public void savePrefs() {
        if(editName.getText() == null ||
                editName.getText().toString().equals("") ||
                editLastname.getText() == null ||
                editLastname.getText().toString().equals("")) {
            Toast.makeText(this, "Musiz podać imię i nazwisko", Toast.LENGTH_SHORT).show();
        } else {
            saveSharedPreferenced();
            setResult(RESULT_OK);
            finish();
        }
    }

    @OnClick(R.id.cancel_button)
    public void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void saveSharedPreferenced() {
        SharedPreferences preferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putString(PREFERENCE_NAME, editName.getText().toString());
        preferencesEditor.putString(PREFERENCE_LASTNAME, editLastname.getText().toString());
        preferencesEditor.putString(PREFERENCE_COUNTRY, String.valueOf(countrySpinner.getSelectedItem()));
        preferencesEditor.putString(PREFERENCE_LANG, String.valueOf(langSpinner.getSelectedItem()));
        boolean isOK = preferencesEditor.commit();
        if(isOK) {
            Toast.makeText(this, "Dane zapisane poprawnie", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Dane nie zostały zapisane poprawnie", Toast.LENGTH_SHORT).show();
        }
    }
}
