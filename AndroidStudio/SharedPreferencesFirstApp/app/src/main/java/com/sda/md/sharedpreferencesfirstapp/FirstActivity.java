package com.sda.md.sharedpreferencesfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sda.md.sharedpreferencesfirstapp.EditActivity.PREFERENCE_COUNTRY;
import static com.sda.md.sharedpreferencesfirstapp.EditActivity.PREFERENCE_LANG;
import static com.sda.md.sharedpreferencesfirstapp.EditActivity.PREFERENCE_LASTNAME;
import static com.sda.md.sharedpreferencesfirstapp.EditActivity.PREFERENCE_NAME;
import static com.sda.md.sharedpreferencesfirstapp.EditActivity.SHARED_PREFERENCES_NAME;

public class FirstActivity extends AppCompatActivity {

    public static final int EDIT_REQ = 1;

    @BindView(R.id.firstname)
    TextView firstname;
    @BindView(R.id.lastname)
    TextView lastname;
    @BindView(R.id.country)
    TextView country;
    @BindView(R.id.lang)
    TextView lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.edit_button)
    public void editPrefs() {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(PREFERENCE_NAME, firstname.getText().toString());
        intent.putExtra(PREFERENCE_LASTNAME, lastname.getText().toString());
        intent.putExtra(PREFERENCE_COUNTRY, country.getText().toString());
        intent.putExtra(PREFERENCE_LANG, lang.getText().toString());
        startActivityForResult(intent, EDIT_REQ);

    }

    @Override
    protected void onResume() {
        loadSharedPreferences();
        super.onResume();
    }

    private void loadSharedPreferences() {
        SharedPreferences preferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        if(!preferences.contains(PREFERENCE_NAME) || !preferences.contains(PREFERENCE_LASTNAME)) {
            editPrefs();
        }

        firstname.setText(preferences.getString(EditActivity.PREFERENCE_NAME, "Preference not found"));
        lastname.setText(preferences.getString(EditActivity.PREFERENCE_LASTNAME, "Preference not found"));
        country.setText(preferences.getString(EditActivity.PREFERENCE_COUNTRY, "Preference not found"));
        lang.setText(preferences.getString(EditActivity.PREFERENCE_LANG, "Preference not found"));
    }

}

