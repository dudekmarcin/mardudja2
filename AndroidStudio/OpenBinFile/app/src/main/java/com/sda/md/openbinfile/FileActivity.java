package com.sda.md.openbinfile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FileActivity extends AppCompatActivity {

    @BindView(R.id.file_list_view)
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);
        ButterKnife.bind(this);
        if(getIntent().hasExtra("Filename")) {
            List<String> axis = FileManager.instance.readFile(getIntent().getStringExtra("Filename"));
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, axis);
            listView.setAdapter(adapter);
        }
    }
}
