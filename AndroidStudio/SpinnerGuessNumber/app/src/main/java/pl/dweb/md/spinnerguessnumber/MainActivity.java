package pl.dweb.md.spinnerguessnumber;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private TextView label;
    private Spinner spinner;
    private int number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        label = (TextView) findViewById(R.id.label);
        spinner = (Spinner) findViewById(R.id.spinner);

        Random random = new Random();
        number = random.nextInt(11);

        List<String> list = new ArrayList<>();
        for(int i = 0; i < 11; i++) {
            list.add(String.valueOf(i));
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int spinnerElement, long l) {
                int sparsowanaLiczba = Integer.parseInt(adapter.getItem(spinnerElement));
                if (sparsowanaLiczba == number) {
                    label.setTextColor(Color.GREEN);
                    label.setText("Wygrałeś!");
                } else {
                    label.setTextColor(Color.RED);
                    label.setText("Zła liczba");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        }




}
