package com.sda.md.localbroadcastreceiverintentservice;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Date;

/**
 * Created by RENT on 2017-03-02.
 */

public class MyService extends IntentService {

    public static final String MY_ACTION ="com.sda.md.localbroadcastreceiverintentsercice.myaction";

    public MyService() {
        super("MyService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Intent msgIntent = new Intent(MY_ACTION);
        String msg = java.text.SimpleDateFormat.getDateTimeInstance().format(new Date());
        msgIntent.putExtra("message", msg);
        LocalBroadcastManager.getInstance(this).sendBroadcast(msgIntent);
    }
}
