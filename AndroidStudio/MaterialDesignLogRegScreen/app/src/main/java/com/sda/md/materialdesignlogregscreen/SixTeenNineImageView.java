package com.sda.md.materialdesignlogregscreen;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by RENT on 2017-01-26.
 */

public class SixTeenNineImageView extends ImageView {
    public SixTeenNineImageView(Context context) {
        super(context);
    }

    public SixTeenNineImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SixTeenNineImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SixTeenNineImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMS, int heightMS) {
        int height = MeasureSpec.getSize(widthMS) * 9 / 16;
        int heightSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMS, heightSpec);
    }
}
