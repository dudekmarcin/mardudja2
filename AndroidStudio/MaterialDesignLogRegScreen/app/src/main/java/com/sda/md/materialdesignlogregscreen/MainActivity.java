package com.sda.md.materialdesignlogregscreen;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindBitmap;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.activity_main)
   CoordinatorLayout layout;

    @BindView(R.id.login)
    TextInputEditText login;

    @BindView(R.id.pass)
    TextInputEditText pass;

    public final static int REGISTER = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login_btn)
    void logToApp() {
        if(!TextUtils.isEmpty(login.getText().toString()) && !TextUtils.isEmpty(pass.getText().toString())) {
            Intent intent = new Intent(getApplicationContext(), AfterLoginActivity.class);
            intent.putExtra("email_address", login.getText().toString());
            startActivity(intent);
        }
    }

    @OnClick(R.id.fab)
    void register() {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivityForResult(intent, REGISTER);
    }

    @OnClick(R.id.about_btn)
    void about(){
        Intent intent = new Intent(getApplicationContext(), AboutAppActivity.class);
        startActivityForResult(intent, REGISTER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REGISTER) {
            if(resultCode == RESULT_OK) {
                Snackbar.make(layout, R.string.register_ok_msg, Snackbar.LENGTH_LONG).show();

            }
        }
    }
}
