package com.sda.md.materialdesignlogregscreen;


import android.animation.Animator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    @BindView(R.id.invisible_vc)
    CardView cardView;
    @BindView(R.id.click_btn)
    Button clickBtn;

    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        ButterKnife.bind(this, view);
//        cardView = (CardView) view.findViewById(R.id.invisible_vc);
//        clickBtn = (Button) view.findViewById(R.id.click_btn);
        clickBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cardView.getVisibility() == View.INVISIBLE) {
                    cardView.setVisibility(cardView.VISIBLE);
                    float endRadius = (float) Math.hypot(cardView.getWidth(), cardView.getHeight());
                    Animator animator = ViewAnimationUtils.createCircularReveal(cardView, cardView.getWidth() / 2, cardView.getHeight() / 2, 0, endRadius);
                    animator.setDuration(1000);
                    animator.start();
                } else {
                    float endRadius = (float) Math.hypot(cardView.getWidth(), cardView.getHeight());
                    Animator animator = ViewAnimationUtils.createCircularReveal(cardView, cardView.getWidth() / 2, cardView.getHeight() / 2, endRadius, 0);
                    animator.setDuration(1000);
                    animator.start();
                    cardView.setVisibility(cardView.INVISIBLE);

                }
            }
        });
        return view;
    }


}
