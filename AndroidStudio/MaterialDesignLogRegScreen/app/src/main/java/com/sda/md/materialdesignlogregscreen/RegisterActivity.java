package com.sda.md.materialdesignlogregscreen;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.cancel_btn)
    void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.register_btn)
    void register() {
        setResult(RESULT_OK);
        finish();
    }
}
