package com.sda.md.retrobook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sda.md.retrobook.dto.BookDetails;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDetailsActivity extends AppCompatActivity implements BookDetailsAsyncTask.BookDetailsDownloadListener {

    private ImageView image;
    private TextView author;
    private TextView title;
    private BookCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        image = (ImageView) findViewById(R.id.book_img);
        author = (TextView) findViewById(R.id.book_author);
        title = (TextView) findViewById(R.id.book_name);
//        new BookDetailsAsyncTask(this).execute(getIntent().getStringExtra("bookID"));
        callback = new BookCallback();
        callback.getBookDetails(getIntent().getStringExtra("bookID"), this, this);


    }

    @Override
    public void onDownload(BookDetails book) {
        if(book == null) {
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
            return;
        }
        author.setText(book.getAuthor());
        title.setText(book.getName());
        Picasso.with(this)
                .load(book.getImageUrl())
                .fit()
                .centerCrop()
                .into(image);

    }
}
