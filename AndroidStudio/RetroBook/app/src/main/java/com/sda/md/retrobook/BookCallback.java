package com.sda.md.retrobook;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.sda.md.retrobook.dto.BookList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RENT on 2017-02-06.
 */

public class BookCallback {

    private BookApiClient bookApiClient = new BookApiClientFactory().create();

    public void getBookList(final RecyclerView rv, final Context context) {

        bookApiClient.getBookList().enqueue(new Callback<BookList>() {
            @Override
            public void onResponse (Call< BookList > call, Response< BookList > response){
            if (response.isSuccessful()) {
                BookListAdapter adapter = (BookListAdapter) rv.getAdapter();
                if(adapter == null) {
                    Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                    return;
                }


               adapter.setBooks(response.body().getBooks());


            } else {
                Toast.makeText(context, "NIe udało się pobrać listy książek", Toast.LENGTH_SHORT).show();
            }
            }

            @Override
            public void onFailure (Call < BookList > call, Throwable t){
                t.printStackTrace();
            }
        });
    }


    public void getBookDetails(String id, final BookDetailsAsyncTask.BookDetailsDownloadListener listener, final Context contetx) {
        BookApiClient bookApiClient = new BookApiClientFactory().create();
        bookApiClient.getBookDetails(id).enqueue(new Callback<BookDetailsRoot>() {
            @Override
            public void onResponse(Call<BookDetailsRoot> call, Response<BookDetailsRoot> response) {
                if(response.isSuccessful()) {
                    listener.onDownload(response.body().getData());
                } else {
                    Toast.makeText(contetx, "Nie udało się załadować szczegółów książki", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BookDetailsRoot> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
