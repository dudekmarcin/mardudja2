package com.sda.md.retrobook.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RENT on 2017-02-04.
 */

public class BookList {

    @SerializedName("data")
    List<Book> books;

    public List<Book> getBooks() {
        return books;
    }
}
