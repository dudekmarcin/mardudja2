package com.sda.md.retrobook;

import com.sda.md.retrobook.dto.BookDetails;
import com.sda.md.retrobook.dto.BookList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by RENT on 2017-02-04.
 */

public interface BookApiClient {

    @GET("/plugin/book.list")
    Call<BookList> getBookList();

    @GET("/plugin/book.details")
    Call<BookDetailsRoot> getBookDetails(@Query("id") String id);

}
