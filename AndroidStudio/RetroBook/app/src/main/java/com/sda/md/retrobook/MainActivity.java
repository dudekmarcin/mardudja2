package com.sda.md.retrobook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.sda.md.retrobook.dto.Book;

public class MainActivity extends AppCompatActivity implements BookListAdapter.OnBookClickedListener {

    private RecyclerView recyclerView;
    private BookListAdapter bookListAdapter;
    private BookCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        bookListAdapter = new BookListAdapter(this, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(bookListAdapter);
        callback = new BookCallback();
//        new BookListAsyncTask(this).execute();

        callback.getBookList(recyclerView, this);


    }

    @Override
    public void bookClicked(Book book) {
        Intent intent = new Intent(this, BookDetailsActivity.class);
        intent.putExtra("bookID", book.getId());
        startActivity(intent);
    }
}
