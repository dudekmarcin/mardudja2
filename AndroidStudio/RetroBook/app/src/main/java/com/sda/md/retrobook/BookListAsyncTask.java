package com.sda.md.retrobook;

import android.os.AsyncTask;

import com.sda.md.retrobook.dto.Book;
import com.sda.md.retrobook.dto.BookList;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by RENT on 2017-02-04.
 */

public class BookListAsyncTask extends AsyncTask<Void, Void, List<Book>> {

    private BookApiClient bookApiClient;
    private BookListDownloadListener listener;

    public BookListAsyncTask(BookListDownloadListener listener) {
        this.listener = listener;
        BookApiClientFactory bacFactory = new BookApiClientFactory();
        this.bookApiClient = bacFactory.create();
    }


    @Override
    protected List<Book> doInBackground(Void... voids) {
        try {
            return getBookList();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<Book> books) {

        listener.onDownload(books);
    }

    private List<Book> getBookList() throws IOException {

        Call<BookList> call = bookApiClient.getBookList();
        Response<BookList> response = call.execute();
        if (response.isSuccessful()) { //http 200+
            return response.body().getBooks();
        }
        return null;
    }

    public interface BookListDownloadListener {
        public void onDownload(List<Book> booklist) ;
    }
}
