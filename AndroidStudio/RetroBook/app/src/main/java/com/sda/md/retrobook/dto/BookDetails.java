package com.sda.md.retrobook.dto;

/**
 * Created by RENT on 2017-02-04.
 */

public class BookDetails {

    private String name;
    private String author;
    private String imageUrl;
    private String id;

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getId() {
        return id;
    }
}
