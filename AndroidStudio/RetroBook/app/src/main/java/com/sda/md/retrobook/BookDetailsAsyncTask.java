package com.sda.md.retrobook;

import android.os.AsyncTask;

import com.sda.md.retrobook.dto.Book;
import com.sda.md.retrobook.dto.BookDetails;
import com.sda.md.retrobook.dto.BookList;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by RENT on 2017-02-04.
 */

public class BookDetailsAsyncTask extends AsyncTask<String, Void, BookDetails> {

    private BookApiClient bookApiClient;
    private BookDetailsDownloadListener listener;

    public BookDetailsAsyncTask(BookDetailsDownloadListener listener) {
        this.listener = listener;
        BookApiClientFactory bacFactory = new BookApiClientFactory();
        this.bookApiClient = bacFactory.create();
    }


    @Override
    protected BookDetails doInBackground(String... strings) {
        if(strings != null && strings.length > 0) {
            try {
                return getBookDetails(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(BookDetails books) {
        listener.onDownload(books);
    }

    private BookDetails getBookDetails(String id) throws IOException {

        Call<BookDetailsRoot> call = bookApiClient.getBookDetails(id);
        Response<BookDetailsRoot> response = call.execute();
        if (response.isSuccessful()) { //http 200+
            return response.body().getData();
        }
        return null;
    }

    public interface BookDetailsDownloadListener {
        public void onDownload(BookDetails book) ;
    }
}
