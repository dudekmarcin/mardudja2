package com.sda.md.retrobook;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sda.md.retrobook.dto.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-02-04.
 */

public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.BookListHolder> {

    private List<Book> books = new ArrayList<>();
    private Context context;
    private OnBookClickedListener onBookClickedListener;

    public BookListAdapter(Context context, OnBookClickedListener onBookClick) {
        this.context = context;
        onBookClickedListener = onBookClick;
    }

    @Override
    public BookListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.booklist_layout, parent, false);
        return new BookListHolder(view, onBookClickedListener);
    }

    @Override
    public void onBindViewHolder(BookListHolder holder, int position) {
        holder.bookTitle.setText(books.get(position).getName());
        holder.item = books.get(position);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public void setBooks(List<Book> books) {
        this.books = books;
        notifyDataSetChanged();
    }


    public class BookListHolder extends RecyclerView.ViewHolder {

        private OnBookClickedListener onBookClickedListener;
        private TextView bookTitle;
        private Book item;

        public BookListHolder(View itemView, final OnBookClickedListener onBookClickedListener) {
            super(itemView);
            bookTitle = (TextView) itemView.findViewById(R.id.book_title);
            this.onBookClickedListener = onBookClickedListener;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBookClickedListener.bookClicked(item);
                }
            });
        }


    }


    public interface OnBookClickedListener {
        public void bookClicked(Book book);
    }
}
