package com.sda.md.cameraapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ImageAdapter.ImageListener {

    private static final int PHOTO_REQ = 1;
    private ImageAdapter adapter;
    String imgPath;
    Uri imgUri;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ImageAdapter(this, this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.photo) {
            takePhoto();
            return true;
        }
        return false;
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(intent.resolveActivity(getPackageManager()) != null) {

            File file = null;
            try {
                file = createFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(file != null) {
                imgUri = FileProvider.getUriForFile(this, "com.sda.md.cameraapp.fileprovider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
            }

            startActivityForResult(intent, PHOTO_REQ);
        }
    }

    private File createFile() throws IOException {
        String stamp = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(new Date());
        String filename = "IMG-" + stamp;
        File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File img = File.createTempFile(filename, ".jpg", dir);
        imgPath = img.getAbsolutePath();
        return img;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == PHOTO_REQ && resultCode == RESULT_OK) {
            String desc = SimpleDateFormat.getDateTimeInstance().format(new Date());
            Image img = new Image(imgUri, desc);
            adapter.addImg(img);
        }
    }

    @Override
    public void onImageClicked(Image image) {
        Intent intent = new Intent(this, ImageActivity.class);
        intent.setData(image.getUri());
        startActivity(intent);
    }
}
