package com.sda.md.cameraapp;

import android.net.Uri;

/**
 * Created by RENT on 2017-03-01.
 */

public class Image {

    private Uri uri;
    private String description;

    public Image(Uri uri, String description) {
        this.uri = uri;
        this.description = description;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Image{" +
                "uri=" + uri +
                ", description='" + description + '\'' +
                '}';
    }
}

