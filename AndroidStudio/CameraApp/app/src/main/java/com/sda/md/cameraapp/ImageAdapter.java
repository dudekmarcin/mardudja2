package com.sda.md.cameraapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by RENT on 2017-03-01.
 */

public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Image> images;
    private Context context;
    private ImageListener listener;


    public ImageAdapter(Context context, ImageListener imageListener) {
        images = new ArrayList<>();
        this.context = context;
        listener = imageListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ImageHolder imgHolder = (ImageHolder) holder;
        final Image image = images.get(position);
        imgHolder.add(image);
        imgHolder.onClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onImageClicked(image);
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void addImg(Image img) {
        images.add(0, img);
        notifyItemInserted(0);
    }

    class ImageHolder extends RecyclerView.ViewHolder {

        ImageView photoThumb;
        TextView desc;

        public ImageHolder(View itemView) {
            super(itemView);
            photoThumb = (ImageView) itemView.findViewById(R.id.photo_thumb);
            desc = (TextView) itemView.findViewById(R.id.photo_date);
        }

        public void add(Image img) {
            Picasso.with(photoThumb.getContext()).load(img.getUri()).fit().centerInside().into(photoThumb);
            desc.setText(img.getDescription());
        }

        public void onClick(View.OnClickListener listener) {
            photoThumb.setOnClickListener(listener);
        }
    }

    interface ImageListener {
        void onImageClicked(Image image);
    }
}
