package com.kkrygier.dbexample2.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.kkrygier.dbexample2.db.model.Task;

import java.sql.SQLException;

/**
 * Created by Kamil_Krygier on 2017-01-07.
 */
public class TaskDbHelper extends OrmLiteSqliteOpenHelper {
    private static final String DB_NAME = "tasks_db.db";
    private static final int DB_VERSION = 13;
    private static final String TAG = TaskDbHelper.class.getSimpleName();
    private Dao<Task, Long> taskDao;

    public TaskDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, Task.class);
            Log.d(TAG, "table tasks created");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            TableUtils.dropTable(connectionSource, Task.class, false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        onCreate(database, connectionSource);
    }

    public Dao<Task, Long> getTaskDao() throws SQLException {
        if (null == taskDao) {
            taskDao = getDao(Task.class);
        }
        return taskDao;
    }

}
