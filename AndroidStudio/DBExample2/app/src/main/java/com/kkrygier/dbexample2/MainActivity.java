package com.kkrygier.dbexample2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.kkrygier.dbexample2.db.TaskDbHelper;
import com.kkrygier.dbexample2.db.model.Task;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private Button saveTaskButton;
    private EditText taskContentEditText;
    private ListView tasksListView;
    private Dao<Task, Long> taskDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TaskDbHelper taskDbHelper = OpenHelperManager.getHelper(this, TaskDbHelper.class);
        try {
            taskDao = taskDbHelper.getTaskDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        taskContentEditText = (EditText) findViewById(R.id.task_content_edit_text);

        saveTaskButton = (Button) findViewById(R.id.create_task_button);
        saveTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String taskContent = taskContentEditText.getText().toString();
                Task task = new Task();
                task.setContent(taskContent);

                try {
                    taskDao.create(task);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                refreshTasks();
                taskContentEditText.setText("");
            }
        });

        tasksListView = (ListView) findViewById(R.id.tasks_list_view);

        refreshTasks();

    }

    private void refreshTasks() {
        List<Task> tasks = null;
        try {
            tasks = taskDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<String>tasksText = getTextFromTaskList(tasks);
        setNewTasks(tasksText);
    }

    private List<String> getTextFromTaskList(List<Task> tasks) {

        List<String> result = new ArrayList<>();
        for (Task task: tasks) {
            String content = task.getContent();
            result.add(content);
        }
        return result;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OpenHelperManager.releaseHelper();
    }

    private void setNewTasks(List<String> tasks) {
        ListAdapter tasksAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, tasks);
        tasksListView.setAdapter(tasksAdapter);
    }

}
