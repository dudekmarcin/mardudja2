package com.kkrygier.dbexample2.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Kamil_Krygier on 2017-01-07.
 */

@DatabaseTable(tableName = "tasks")
public class Task {

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField
    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
