package pl.dweb.sqlitetut;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by md on 1/20/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static String databaseName = "StudentDatabase";
    private static int databaseVersion = 1;
    private static final String TABLE_NAME = "Students";
    private static final String COLUMN_INDEX = "Col_Index";
    private static final String COLUMN_STUDENT_NAME = "StudentName";
    private static final String COLUMN_STUDENT_ROLLNUMBER = "StudentRollNumber";


    public DatabaseHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(db);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_INDEX + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + COLUMN_STUDENT_NAME + " TEXT,"
                + COLUMN_STUDENT_ROLLNUMBER + " TEXT" + ")";

        db.execSQL(CREATE_TABLE);
    }

    public void addStudent(Student student){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_STUDENT_NAME, student.getStudentName());
        contentValues.put(COLUMN_STUDENT_ROLLNUMBER, student.getStudentRollNumber());

        db.insert(TABLE_NAME, null, contentValues);

        db.close();
    }

    public List getAllStudents(){

        List list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()){
            do {

                Student student = new Student();

                student.setStudentIndex(Integer.parseInt(cursor.getString(0)));
                student.setStudentName(cursor.getString(1));
                student.setStudentRollNumber(cursor.getString(2));

                list.add(student);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return list;
    }

    public int updateStudent(Student student){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_INDEX  , student.getStudentIndex());
        contentValues.put(COLUMN_STUDENT_NAME , student.getStudentName());
        contentValues.put(COLUMN_STUDENT_ROLLNUMBER, student.getStudentRollNumber());

        int row = db.update(TABLE_NAME, contentValues, COLUMN_INDEX + "=?", new String[]{String.valueOf(student.getStudentIndex())});

        db.close();

        return row;

    }
    public void deleteStudent(Student student){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_NAME , COLUMN_INDEX + "=?" , new String[]{String.valueOf(student.getStudentIndex())});

        db.close();
    }

    public int getStudentCount(){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(query , null);

        int countOfStudents = cursor.getCount();

        cursor.close();

        return countOfStudents;
    }
    public long getColumnIndexValue(int pos){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(query , null);

        //this is auto incremented value of columnIndex column
        cursor.moveToPosition(pos);

        return cursor.getLong(0);
    }



}
