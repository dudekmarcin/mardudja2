package pl.dweb.sqlitetut;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText name;
    private EditText roll;
    private DatabaseHelper dbh;
    private Button insertBtn;
    private Button viewBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.studentName);
        roll = (EditText) findViewById(R.id.studentRoll);
        insertBtn = (Button) findViewById(R.id.insertBtn);
        viewBtn = (Button) findViewById(R.id.viewBtn);


        dbh = new DatabaseHelper(getApplicationContext());



        insertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Student student = new Student();

                student.setStudentName(name.getText().toString());
                student.setStudentRollNumber(roll.getText().toString());

                dbh.addStudent(student);

                Toast.makeText(getApplicationContext() , "Data Inserted " , Toast.LENGTH_SHORT).show();

                viewBtn.setVisibility(View.VISIBLE);
            }
        });

        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext() , DisplayStudentData.class));
            }
        });


    }
}
