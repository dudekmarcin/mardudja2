package pl.dweb.sqlitetut;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/* Android SQLite Databse */
public class DisplayStudentData extends AppCompatActivity {

    private ListView listView;
    private DatabaseHelper dbh;
    private List<Student> studentList;
    private DisplayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_student_data);

        listView = (ListView) findViewById(R.id.listView);

        dbh = new DatabaseHelper(this);

        studentList = dbh.getAllStudents();

        adapter = new DisplayAdapter(studentList);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                moreOptions(position);
            }
        });


    }

    private void moreOptions(final int pos) {

        new AlertDialog.Builder(this)
                .setTitle("Choose Options : ")
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with update
                        performUpdate(pos);
                    }
                })
                .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        performDelete(pos);
                    }
                })
                .show();

    }

    private void performUpdate(int pos) {
        Intent intent = new Intent(this , UpdateData.class);
        intent.putExtra("pos" , pos);
        intent.putExtra("name" , studentList.get(pos).getStudentName());
        intent.putExtra("roll" , studentList.get(pos).getStudentRollNumber());
        startActivity(intent);
    }

    private void performDelete(final int pos) {
        new AlertDialog.Builder(this)
                .setTitle("Are You Sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Student student = studentList.get(pos);

                        dbh.deleteStudent(student);

                        studentList.remove(pos);
                        adapter.notifyDataSetChanged();

                        Toast.makeText(getApplicationContext() , "Deleted Successfully" , Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("No" , null)
                .show();
    }

    class DisplayAdapter extends BaseAdapter{

        List<Student> list;

        DisplayAdapter(List<Student> list){
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

            View rootView = inflater.inflate(R.layout.list_item , null);

            ViewHolder holder=new ViewHolder(rootView);

            holder.index.setText(Integer.toString(list.get(position).getStudentIndex()));
            holder.name.setText(list.get(position).getStudentName());
            holder.roll.setText(list.get(position).getStudentRollNumber());


            return rootView;
        }


        public class ViewHolder {

            TextView index;
            TextView name;
            TextView roll;

            ViewHolder(View rootView){
                index = (TextView) rootView.findViewById(R.id.index);
                name = (TextView) rootView.findViewById(R.id.name);
                roll = (TextView) rootView.findViewById(R.id.roll);
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        studentList = dbh.getAllStudents();
        listView.setAdapter(new DisplayAdapter(studentList));
        adapter.notifyDataSetChanged();
    }
}