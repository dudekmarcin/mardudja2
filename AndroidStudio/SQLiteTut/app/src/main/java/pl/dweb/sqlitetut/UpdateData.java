package pl.dweb.sqlitetut;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/* Android SQLite Database Tutorial */
public class UpdateData extends AppCompatActivity {

    private EditText name;
    private EditText roll;
    private DatabaseHelper dbh;
    private Button updateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_data);

        name = (EditText) findViewById(R.id.studentName);
        roll = (EditText) findViewById(R.id.studentRoll);
        updateBtn = (Button) findViewById(R.id.updateBtn);



        final int rowToUpdate = getIntent().getExtras().getInt("pos");

        String sName = getIntent().getExtras().getString("name");
        String sRoll = getIntent().getExtras().getString("roll");

        name.setText(sName);
        roll.setText(sRoll);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbh = new DatabaseHelper(getApplicationContext());

                Student student = new Student();

                student.setStudentIndex((int) dbh.getColumnIndexValue(rowToUpdate));
                student.setStudentName(name.getText().toString());
                student.setStudentRollNumber(roll.getText().toString());

                dbh.updateStudent(student);

                Toast.makeText(getApplicationContext() , "Database Updated" , Toast.LENGTH_SHORT).show();

                finish();
            }
        });


    }
}