package pl.dweb.md.thirdapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private EditText input1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input1 = (EditText) findViewById(R.id.input1);
    }

    public void switchLabel(View view) {
        if(findViewById(1990) ==null) {
            TextView newTV = new TextView(this);
            newTV.setId(1990);
            newTV.setText(input1.getText());
            newTV.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            newTV.setTextSize(32);
            LinearLayout linear = (LinearLayout) findViewById(R.id.activity_main);
            linear.addView(newTV);
        } else {
            TextView existTV = (TextView) findViewById(1990);
            existTV.setText(input1.getText());
        }

    }

}
