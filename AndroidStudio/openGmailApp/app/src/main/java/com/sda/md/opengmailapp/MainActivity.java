package com.sda.md.opengmailapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.mailTo)
    EditText mailTo;

    @BindView(R.id.mailSubject)
    EditText mailSubject;

    @BindView(R.id.mailContent)
    EditText mailConetnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    public void composeEmail(String[] addresses, String subject, String content) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("*/*");
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        if(intent.resolveActivity(getPackageManager()) != null) {
          startActivity(intent);
        }
    }

    @OnClick(R.id.sendBtn)
    public void sendMail() {
        composeEmail(new String[]{mailTo.getText().toString()}, mailSubject.getText().toString(), mailConetnt.getText().toString());
    }

}
