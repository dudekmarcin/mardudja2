package com.sda.md.sqlitenotesapp;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sda.md.sqlitenotesapp.database.DAO.TaskDAO;
import com.sda.md.sqlitenotesapp.database.DBHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.note_title)
    TextView noteTitle;

    @BindView(R.id.note_content)
    TextView noteContent;

    @BindView(R.id.list_view)
    ListView listView;

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private NotesAdapter adapter;
    private TaskDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        dao = new TaskDAO(this);
        adapter = new NotesAdapter();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               dao.removeNote(i);
               adapter.setData(getData());
            }
        });

    }

    @Override
    protected void onDestroy() {
        dao.close();
        super.onDestroy();
    }

    @OnClick(R.id.add_btn)
    public void addNote() {
        dao.addNote(new Note( noteTitle.getText().toString(), noteContent.getText().toString()));
        adapter.setData(getData());
        noteTitle.setText("");
        noteContent.setText("");
    }

    private List<Note> getData() {
        return dao.getAll();
    }


    class NotesAdapter extends BaseAdapter {

        private List<Note> notes = new ArrayList<>();

        public NotesAdapter() {
            notes = getData();
        }

        @Override
        public int getCount() {
            return notes.size();
        }

        @Override
        public Object getItem(int i) {
            return notes.get(i);
        }

        @Override
        public long getItemId(int i) {
            return notes.get(i).getId();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView;
            if (view != null) {
                textView = (TextView) view;
            } else {
                textView = new TextView(MainActivity.this);
            }
            textView.setText(notes.get(i).toString());
            return textView;
        }

        public void setData(List<Note> notes) {
            this.notes = notes;
            notifyDataSetChanged();
        }

    }

}
