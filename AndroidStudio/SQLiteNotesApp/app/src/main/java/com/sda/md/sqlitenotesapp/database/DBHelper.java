package com.sda.md.sqlitenotesapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by RENT on 2017-02-11.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "NotesDB.db";
    private static final int DB_VERSION = 1;
    private static final String CREATE_DB = "CREATE TABLE " + DBContract.NoteEntry.TABLE_NAME
            + " (" + DBContract.NoteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DBContract.NoteEntry.TABLE_COLUMN_TITLE + " text, "
            + DBContract.NoteEntry.TABLE_COLUMN_CONTENT + " text, "
//            + DBContract.NoteEntry.TABLE_COLUMN_STATUS
             + " text)";

    private static final String DROP_DB = "DROP TABLE IF EXIST " + DBContract.NoteEntry.TABLE_NAME;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_DB);
        onCreate(sqLiteDatabase);
    }
}
