package com.sda.md.sqlitenotesapp.database.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sda.md.sqlitenotesapp.Note;
import com.sda.md.sqlitenotesapp.database.DBContract;
import com.sda.md.sqlitenotesapp.database.DBHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-02-11.
 */

public class TaskDAO {

    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public TaskDAO(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public List<Note> getAll() {
        String columns[] = new String[]{DBContract.NoteEntry._ID, DBContract.NoteEntry.TABLE_COLUMN_TITLE, DBContract.NoteEntry.TABLE_COLUMN_CONTENT};
        Cursor result = db.query(DBContract.NoteEntry.TABLE_NAME, columns, null, null, null, null, null);
        List<Note> notes = new ArrayList<>();
        while(result.moveToNext()) {
            notes.add(new Note(result.getInt(0), result.getString(1), result.getString(2)));
        }
        result.close();
        return notes;
    }

    public Note getNote(int id) {
        String columns[] = new String[]{DBContract.NoteEntry._ID, DBContract.NoteEntry.TABLE_COLUMN_TITLE, DBContract.NoteEntry.TABLE_COLUMN_CONTENT};
        Cursor result = db.query(DBContract.NoteEntry.TABLE_NAME, columns, "where id = ?", new String[]{""+id}, null, null, null);
        Note note = new Note();
        while(result.moveToNext()) {
            note.setId(result.getInt(0));
            note.setTitle(result.getString(1));
            note.setContent(result.getString(2));
        }
        result.close();
        return note;
    }

    public void addNote(Note note) {
        ContentValues values = new ContentValues();
        values.put(DBContract.NoteEntry.TABLE_COLUMN_TITLE, note.getTitle());
        values.put(DBContract.NoteEntry.TABLE_COLUMN_CONTENT, note.getContent());
        db.insert(DBContract.NoteEntry.TABLE_NAME, null, values);
    }

    public void removeNote(int id) {
        db.delete(DBContract.NoteEntry.TABLE_NAME, DBContract.NoteEntry._ID + " = " + id, null);
    }

    public void updateNote(Note note, int id) {
        ContentValues values = new ContentValues();
        values.put(DBContract.NoteEntry.TABLE_COLUMN_TITLE, note.getTitle());
        values.put(DBContract.NoteEntry.TABLE_COLUMN_CONTENT, note.getContent());
        db.update(DBContract.NoteEntry.TABLE_NAME, values, DBContract.NoteEntry._ID + " = " + id, null);
    }

    public void close() {
        dbHelper.close();
        db.close();
    }
}
