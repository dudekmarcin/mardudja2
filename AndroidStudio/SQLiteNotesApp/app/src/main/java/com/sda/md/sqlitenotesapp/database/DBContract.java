package com.sda.md.sqlitenotesapp.database;

import android.provider.BaseColumns;

/**
 * Created by RENT on 2017-02-11.
 */

public class DBContract {
    public static class NoteEntry implements BaseColumns {
        public static final String TABLE_NAME = "notes";
        public static final String TABLE_COLUMN_TITLE= "title";
        public static final String TABLE_COLUMN_CONTENT = "content";
//        public static final String TABLE_COLUMN_STATUS = "status";
    }
}
