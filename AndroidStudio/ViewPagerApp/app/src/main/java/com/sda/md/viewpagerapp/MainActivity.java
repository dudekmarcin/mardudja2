package com.sda.md.viewpagerapp;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.mainVP);
        MyAdapter adapter = new MyAdapter(getFragmentManager());
        viewPager.setAdapter(adapter);

    }

    class MyAdapter extends FragmentPagerAdapter {

       private FragmentManager fragmentManager;

        public MyAdapter(FragmentManager fm) {
            super(fm);
            fragmentManager = fm;
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0: return new FirstFragment();
                case 1: return new SecondFragment();
                case 2: return new ThirdFragment();
            }
            throw new RuntimeException("coś poszło nie tak");
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
