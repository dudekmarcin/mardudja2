package com.sda.md.userdataapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private EditText name;
    private  EditText lastname;
    private SeekBar age;
    private String ageVal;
    private TextView showAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        name = (EditText) findViewById(R.id.editName);
        lastname = (EditText) findViewById(R.id.editLastname);
        age = (SeekBar) findViewById(R.id.editAge);
        showAge = (TextView) findViewById(R.id.showAge);

        age.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ageVal = String.valueOf(progress);
                showAge.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void getBack(View view) {
        Intent intent = new Intent();
        intent.putExtra("name", name.getText().toString());
        intent.putExtra("lastname", lastname.getText().toString());
        intent.putExtra("age", ageVal);
        setResult(RESULT_OK, intent);
        finish();
    }
}
