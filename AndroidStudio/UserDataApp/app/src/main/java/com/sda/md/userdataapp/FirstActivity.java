package com.sda.md.userdataapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FirstActivity extends AppCompatActivity {

    private TextView name;
    private TextView lastname;
    private TextView age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        name = (TextView) findViewById(R.id.name);
        lastname = (TextView) findViewById(R.id.lastname);
        age = (TextView) findViewById(R.id.age);
    }

    public void edit(View view) {
        Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1) {
            if(resultCode == RESULT_OK) {
                if(data.hasExtra("name")) {
                    name.setText(data.getStringExtra("name"));
                }

                if(data.hasExtra("lastname")) {
                    lastname.setText(data.getStringExtra("lastname"));
                }

                if(data.hasExtra("age")) {
                    age.setText(data.getStringExtra("age"));
                }
            }
        }
    }

}
