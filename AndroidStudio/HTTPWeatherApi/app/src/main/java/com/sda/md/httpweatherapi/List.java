package com.sda.md.httpweatherapi;

import android.os.Looper;

/**
 * Created by RENT on 2017-01-31.
 */

public class List {

    private Integer clouds;

    private Long dt;

    private Integer humidity;

    private Float pressure;

    private Float speed;

    private Float deg;

    private Weather[] weather;

    private Temp temp;

    public Integer getClouds ()
    {
        return clouds;
    }

    public void setClouds (Integer clouds)
    {
        this.clouds = clouds;
    }

    public Long getDt ()
    {
        return dt;
    }

    public void setDt (Long dt)
    {
        this.dt = dt;
    }

    public Integer getHumidity ()
    {
        return humidity;
    }

    public void setHumidity (Integer humidity)
    {
        this.humidity = humidity;
    }

    public Float getPressure ()
    {
        return pressure;
    }

    public void setPressure (Float pressure)
    {
        this.pressure = pressure;
    }

    public Float getSpeed ()
    {
        return speed;
    }

    public void setSpeed (Float speed)
    {
        this.speed = speed;
    }

    public Float getDeg ()
    {
        return deg;
    }

    public void setDeg (Float deg)
    {
        this.deg = deg;
    }

    public Weather[] getWeather ()
    {
        return weather;
    }

    public void setWeather (Weather[] weather)
    {
        this.weather = weather;
    }

    public Temp getTemp ()
    {
        return temp;
    }

    public void setTemp (Temp temp)
    {
        this.temp = temp;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [clouds = "+clouds+", dt = "+dt+", humidity = "+humidity+", pressure = "+pressure+", speed = "+speed+", deg = "+deg+", weather = "+weather+", temp = "+temp+"]";
    }
}
