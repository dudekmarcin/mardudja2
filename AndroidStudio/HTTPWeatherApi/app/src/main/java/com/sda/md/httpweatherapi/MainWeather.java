package com.sda.md.httpweatherapi;

/**
 * Created by RENT on 2017-01-31.
 */

public class MainWeather {

        private Float message;

        private Integer cnt;

        private String cod;

        private List[] list;

        private City city;

        public Float getMessage ()
        {
            return message;
        }

        public void setMessage (Float message)
        {
            this.message = message;
        }

        public Integer getCnt ()
        {
            return cnt;
        }

        public void setCnt (Integer cnt)
        {
            this.cnt = cnt;
        }

        public String getCod ()
        {
            return cod;
        }

        public void setCod (String cod)
        {
            this.cod = cod;
        }

        public List[] getList ()
        {
            return list;
        }

        public void setList (List[] list)
        {
            this.list = list;
        }

        public City getCity ()
        {
            return city;
        }

        public void setCity (City city)
        {
            this.city = city;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [message = "+message+", cnt = "+cnt+", cod = "+cod+", list = "+list+", city = "+city+"]";
        }
}
