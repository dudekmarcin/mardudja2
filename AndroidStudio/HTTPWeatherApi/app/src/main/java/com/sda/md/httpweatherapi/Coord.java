package com.sda.md.httpweatherapi;

/**
 * Created by RENT on 2017-01-31.
 */

public class Coord {

    private Float lon;

    private Float lat;

    public Float getLon ()
    {
        return lon;
    }

    public void setLon (Float lon)
    {
        this.lon = lon;
    }

    public Float getLat ()
    {
        return lat;
    }

    public void setLat (Float lat)
    {
        this.lat = lat;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lon = "+lon+", lat = "+lat+"]";
    }
}
