package com.sda.md.httpweatherapi;

/**
 * Created by RENT on 2017-01-31.
 */

public class City {

    private Coord coord;

    private Integer id;

    private String name;

    private Integer population;

    private String country;

    public Coord getCoord ()
    {
        return coord;
    }

    public void setCoord (Coord coord)
    {
        this.coord = coord;
    }

    public Integer getId ()
    {
        return id;
    }

    public void setId (Integer id)
    {
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Integer getPopulation ()
    {
        return population;
    }

    public void setPopulation (Integer population)
    {
        this.population = population;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [coord = "+coord+", id = "+id+", name = "+name+", population = "+population+", country = "+country+"]";
    }
}
