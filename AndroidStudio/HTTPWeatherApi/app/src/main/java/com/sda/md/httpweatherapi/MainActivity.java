package com.sda.md.httpweatherapi;

import android.os.AsyncTask;
import android.support.v4.internal.view.SupportSubMenu;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

//    @BindView(R.id.longitude)
//    TextView longitude;
//
//    @BindView(R.id.latitude)
//    TextView latitude;



//    @BindView(R.id.country)
//    TextView country;

//    @BindView(R.id.sunrise)
//    TextView sunrise;
//
//    @BindView(R.id.sunset)
//    TextView sunset;
//
//    @BindView(R.id.day_long)
//    TextView dayLong;

    @BindView(R.id.activity_main)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.date)
    TextView date;

    private GetData getData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getData = new GetData();
        getData.execute("Gdynia");

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData = new GetData();
                getData.execute("Gdynia");
            }
        });
    }

    public void setWeahter(String data) {

        Gson gson = new Gson();

        MainWeather forecast = gson.fromJson(data, MainWeather.class);

        city.setText(forecast.getCity().getName());
        List[] list = forecast.getList();
        Weather[] weathers = list[0].getWeather();

        DateTime dateDT = new DateTime(list[0].getDt() * 1000);
        date.setText(dateDT.toString("DD/MM/YYYY"));
        mainCondition.setText(weathers[0].getMain());
        temp.setText(String.valueOf(list[0].getTemp().getDay()) + "\u00B0C");
        pressure.setText(String.valueOf(list[0].getPressure()) + " hPa");
        clouds.setText(String.valueOf(list[0].getClouds()) + "%");
        humidity.setText(String.valueOf(list[0].getHumidity()) + "%");


//        try {
//            JSONObject weather = new JSONObject(data);
//            String Slongitude = weather.getJSONObject("coord").getString("lon");
//            String Slatitude = weather.getJSONObject("coord").getString("lat");
//            String Scondition = weather.getJSONArray("weather").getJSONObject(0).getString("main");
//            String Scity = weather.getString("name");
//            String Stemp = weather.getJSONObject("main").getString("temp");
//            String Spressure = weather.getJSONObject("main").getString("pressure");
//            String Shumidity = weather.getJSONObject("main").getString("humidity");
//            String Sclouds = weather.getJSONObject("clouds").getString("all");
//            String Scountry = weather.getJSONObject("sys").getString("country");
//            Long Lsunrise = Long.parseLong(weather.getJSONObject("sys").getString("sunrise")) * 1000;
//            Long Lsunset = Long.parseLong(weather.getJSONObject("sys").getString("sunset")) * 1000;
//
//            DateTime sunriseDate = new DateTime(Lsunrise);
//            DateTime sunsetDate = new DateTime(Lsunset);
//            Period PdayLong = new Period(Lsunrise, Lsunset);
//            PeriodFormatter formatter = new PeriodFormatterBuilder().appendHours().appendSeparator(":").appendMinutes().toFormatter();
//
//            longitude.setText(Slongitude + "\u00B0");
//            latitude.setText(Slatitude + "\u00B0");
//            mainCondition.setText(Scondition);
//            city.setText(Scity);
//            temp.setText(Stemp + "\u00B0C");
//            pressure.setText(Spressure + " hPa");
//            humidity.setText(Shumidity + "%");
//            clouds.setText(Sclouds + "%");
//            country.setText(Scountry);
//            sunrise.setText(sunriseDate.toString("HH:mm"));
//            sunset.setText(sunsetDate.toString("HH:mm"));
//            dayLong.setText(PdayLong.toString(formatter) + " h");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }


    private class GetData extends AsyncTask<String, Void, String> {

        private String data;

        @Override
        protected String doInBackground(String... strings) {

            HttpURLConnection connection = null;
            try {
                String address = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";
                String city = strings[0];
                String country;
                try {
                    country = strings[1];
                } catch (IndexOutOfBoundsException e) {
                    country = "pl";
                }
                String params = "&units=metric&cnt=5";
                String apiCode = "&appid=1055c1734fba9bc867388696e84e8e67";

                URL url = new URL(address + city + "," + country + params + apiCode);
                connection = (HttpURLConnection) url.openConnection();

                InputStream inputStream = new BufferedInputStream(connection.getInputStream());

                String data = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
                return data;

            } catch (java.io.IOException e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String data) {
//            this.data = data;
            MainActivity.this.setWeahter(data);
            swipeRefreshLayout.setRefreshing(false);
        }

        public String getData() {
            return data;
        }
    }



}
