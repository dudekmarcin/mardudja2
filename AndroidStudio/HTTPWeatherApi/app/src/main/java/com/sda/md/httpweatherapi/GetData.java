package com.sda.md.httpweatherapi;

import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by RENT on 2017-01-30.
 */

public class GetData extends AsyncTask<String, Void, String> {

    private String data;

    @Override
    protected String doInBackground(String... strings) {

        HttpURLConnection connection = null;
        try {
            String address = "http://api.openweathermap.org/data/2.5/weather?q=";
            String city = strings[0];
            String country;
            try {
                country = strings[1];
            } catch (IndexOutOfBoundsException e) {
                country = "pl";
            }
            String params = "&units=metric";
            String apiCode = "&appid=1055c1734fba9bc867388696e84e8e67";

            URL url = new URL(address + city + "," + country + params + apiCode);
            connection = (HttpURLConnection) url.openConnection();

            InputStream inputStream = new BufferedInputStream(connection.getInputStream());

            String data = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
            return data;

        } catch (java.io.IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
