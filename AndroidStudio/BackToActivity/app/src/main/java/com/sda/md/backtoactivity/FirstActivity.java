package com.sda.md.backtoactivity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;


//2. Uruchomienie Activity w celu otrzymania rezultatu.
// Pierwsze activity po kliknięciu guzika uruchamia drugie
// Activity w celu otrzymania rezultatu (startActivityForResult).
// Drugie Activity po uruchomieniu wyświetla pole tekstowe i guzik.
// Po wpisaniu tekstu a następnie kliknięciu guzika chcemy wykonać
// powrót do pierwszego activity i wyświetlenie przekazanego
// (// z activity-2 do activity-1) tekstu.
public class FirstActivity extends AppCompatActivity {

    static final int REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

    }

    public void goToSecondActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
        startActivityForResult(intent, REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST) {
            if(resultCode == Activity.RESULT_OK) {
                if (findViewById(1990) == null) {
                    TextView newTV = new TextView(this);
                    newTV.setId(1990);
                    newTV.setText(data.getStringExtra("msg"));
                    newTV.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    newTV.setTextSize(32);
                    LinearLayout linear = (LinearLayout) findViewById(R.id.activity_first);
                    linear.addView(newTV);
                } else {
                    TextView existTV = (TextView) findViewById(1990);
                    existTV.setText(data.getStringExtra("msg"));
                }
            }
        }
    }

}
