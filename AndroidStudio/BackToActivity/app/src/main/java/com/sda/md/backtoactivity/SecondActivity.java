package com.sda.md.backtoactivity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        input = (EditText) findViewById(R.id.editText);
    }

    public void goBack(View view) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("msg", input.getText().toString());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
