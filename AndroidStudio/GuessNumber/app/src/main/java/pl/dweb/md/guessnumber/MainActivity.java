package pl.dweb.md.guessnumber;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Integer number;
    private EditText numField;
    private TextView answer;
    private TextView tip;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Random random = new Random();
        number = random.nextInt(11);
        numField = (EditText) findViewById(R.id.number);
        answer = (TextView) findViewById(R.id.answer);
        tip = (TextView) findViewById(R.id.tip);
    }

    public void checkAnswer(View view) {
       Integer guessing = Integer.parseInt(numField.getText().toString());
        if(guessing == number) {
            answer.setText("Odgadłeś!");
            answer.setTextColor(Color.GREEN);
        } else {
            answer.setText("zła liczba");
            answer.setTextColor(Color.RED);
        }
        numField.setText("");

    }
}
